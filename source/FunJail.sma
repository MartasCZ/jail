#include <amxmodx>
#include <amxmisc>	
#include <fakemeta_util>
#include <engine>
#include <hamsandwich>
#include <stripweapons>
#include <fvault>
#include <sqlx>
#include <unixtime>
#include <mtlabs>

#pragma dynamic							32768 
#define PLUGIN 							"FunJail"
#define VERSION							"BETA"
#define AUTHOR 							"Marta$.?!^^"

/*
0) Revize kodu
   - Dokoncit veskere TODO a logovani pouze k testovani
   - Nektere veci nejsou pod DEBUG modem!!
   - Reorganizace kodu
	
1) Blokovane soubory!
  - shield
  - c4
  - flashlight
  - p228
  - ump45
  - backpack 
  - npc sounds
  - nightvision 
 
2) Funkce delane specialne pro tyto mapy! 
  >> jb_oasis 
  	- plugin_precache()
  		- Neblokuje se cachovani modelu sheildu
   - register_touch()
  		- trigger_hurt 			- Healovaci stanice
  		- trigget_gravity		- Nastaveni gravitace 
  
  >> jb_cp_assis
  	- register_touch()
  		- trigger_hurt			- Healovaci stanice
 */

#define DEBUG
#define LOCALHOST

// Config
#define MAX_HEALTH_CT					200.0			// Maximalni pocet HP pro bachare!
#define MAX_HEALTH_T					200.0  			// Maximalni pocet HP pro vezne!
#define MAX_HRACU						32				// Maximalni pocet hracu na serveru!
#define MAX_ODHAZOVACICH_NOZU   		5				// Maximalni mnozstvi odhazovacich nozu, ktere muze hrac vlastnit!
#define DELAY_SPECIAL					600.0			// Pocet sekund mezi specialy! 
#define SPECIAL_START_LIMIT				30.0			// Pocet sekund po zacatku kole, kdy jde spustit special
#define HONENA_GRAVITACE				0.8				// Zakladni gravitace pro honenou
#define HONENA_RYCHLOST					400.0			// Zakladni rychlost pro honenou
#define ZAKLADNI_MAPA					"jb_skynet"		// Zakladni mapa
#define DELKA_JEDNE_MAPY				30				// Zakladni hraci doba pro mapu (Minuty)

// Macra
#define playerId(%1) 					(1 <= %1 <= 32)
#define zivyHrac(%1) 					((1 <= %1 <= 32) && get_bit(g_ZivyHrac, %1))
#define hracPripojen(%1) 				((1 <= %1 <= 32) && get_bit(g_HracPripojen, %1))
#define vec_copy(%1,%2)					( %2[0] = %1[0], %2[1] = %1[1],%2[2] = %1[2])
#define maXP(%1,%2)						(g_MojeInfo[%1][Ucet_Konto] >= %2)
#define write_coord_fl(%1)				engfunc(EngFunc_WriteCoord, %1)
#define pocetHracu()					(g_OnlineHracu[CS_TEAM_CT] + g_OnlineHracu[CS_TEAM_T])
#define jeAdmin(%1)						(g_MojeInfo[%1][Ucet_Flags] == HRAC_ADMIN)
#define jeVIP(%1)						(g_MojeInfo[%1][Ucet_Flags] == HRAC_VIP || jeAdmin(%1))
#define jeMod(%1)						(g_ServerInfo[Special_Typ] == %1)
#define task_r(%1,%2,%3)				set_task(%1, %2, %3, _, _, "b")

// Multimodel
#define MULTIMODEL_KNIFE_IDLE			13
#define MULTIMODEL_KNIFE_ROTATE			14
#define MULTIMODEL_SCHOVKA_START		15
#define MULTIMODEL_SCHOVKA_KONEC		30

// SQL Config
#if defined LOCALHOST
#define SQL_HOST  						"127.0.0.1"
#define SQL_USER  						"root"
#define SQL_PASS  						""
#define SQL_DB    						"jail"
#else
#define SQL_HOST  						"46.234.101.32"
#define SQL_USER  						"root"
#define SQL_PASS  						"mysqlroot"
#define SQL_DB    						"jail"
#endif

// SQL Tabulky
#define TABULKA_CTBANY					"jail_ct_bany"
#define TABULKA_VEZEN_STATS				"jail_stats_vezen"
#define TABULKA_UCTY					"jail_ucty"
#define TABULKA_VIP_KODY				"jail_vip_kody"
#define TABULKA_PRAVA					"jail_prava"
#define TABULKA_BANY					"jail_bany"

// Tasky
#define TASK_ODPOCET					3200
#define TASK_COLT						3300
#define TASK_HONENABOX					3400
#define TASK_HONENAODPOCET				3500
#define TASK_CHCANI						3600
#define TASK_REDBULL					3700
#define TASK_PLANT						3800
#define TASK_MYSQL						3900
#define TASK_NOVYSIMON					4000
#define TASK_PINGCHECKER				4100
#define TASK_ICECUBE					4200
#define TASK_RESPAWN					4300
#define TASK_ZONA						4400
#define TASK_MAPVOTE					4500
#define TASK_DUEL						4600

#if !defined DEBUG
#define TASK_RESTART         			6000 
#else
#define TASK_DRAW_SIZE					6000
#endif

// Offsets
#define EXTRAOFFSET_WEAPONS				4
#define EXTRAOFFSET						5
#define OFFSET_PLAYER					41
#define OFFSET_WEAPONID					43
#define OFFSET_NEXT_PRIMARY_ATTACK		46
#define OFFSET_NEXT_SECONDARY_ATTACK	47
#define OFFSET_WEAPONIDLE				48
#define OFFSET_CLIPAMMO					51
#define OFFSET_INRELOAD					54
#define OFFSET_NEXTATTACK				83
#define OFFSET_TEAM						114
#define OFFSET_MONEY					115
#define OFFSET_PRIMWEAPON				116
#define OFFSET_ARGSTIME					198
#define OFFSET_HUDInitialized			349
#define OFFSET_ACTIVEITEM				373
#define OFFSET_PLAYERITEM				376
#define OFFSET_AWM_AMMO   				377 
#define OFFSET_SCOUT_AMMO  				378
#define OFFSET_PARA_AMMO  				379
#define OFFSET_FAMAS_AMMO  				380
#define OFFSET_M3_AMMO  				381
#define OFFSET_USP_AMMO  				382
#define OFFSET_FIVESEVEN_AMMO  			383
#define OFFSET_DEAGLE_AMMO  			384
#define OFFSET_P228_AMMO  				385
#define OFFSET_GLOCK_AMMO  				386
#define OFFSET_FLASH_AMMO  				387
#define OFFSET_HE_AMMO  				388
#define OFFSET_SMOKE_AMMO 				389
#define OFFSET_C4_AMMO 					390
#define OFFSET_CSDEATHS					444
#define OFFSET_VGUI						510

// Files
#define FILE_STORAGE_SKINY    			"jail_data_skiny"
#define FILE_STORAGE_ANNOUNCER			"jail_data_chat"
#define FILE_STORAGE_MAPDATA			"jail_data_map"
	
// Ostatni
#define MOTD_PRAVIDLA_URL				"http://dl.crazygaming.eu/pravidla.html"
#define KEY_GUN_MOLOTOV					777766

// Cenik
#define OBCHOD_VEZEN_SROUBOVAK			15
#define OBCHOD_VEZEN_ZABIKUCH			30
#define OBCHOD_VEZEN_PACIDLO			40
#define OBCHOD_VEZEN_MACETA				50
#define OBCHOD_VEZEN_MOTOROVKA			60
#define OBCHOD_VEZEN_VOLNYDEN			100
#define OBCHOD_VEZEN_PADAK				20
#define OBCHOD_VEZEN_GRANATY			20
#define OBCHOD_VEZEN_DEAGLE				30
#define OBCHOD_VEZEN_REDBULL			20  
#define OBCHOD_VEZEN_ANTILASER			20

#define OBCHOD_BACHAR_KATANA			50
#define OBCHOD_BACHAR_PADAK				20
#define OBCHOD_BACHAR_MINA				20

#define DEALER_MOLOTOV					20
#define DEALER_PREVLEK					10
#define DEALER_ODHAZOVACI_NOZIK			5
#define DEALER_PACIDLO					OBCHOD_VEZEN_PACIDLO / 2
#define DEALER_MOTOROVKA				OBCHOD_VEZEN_MOTOROVKA / 2

// Bits
new g_ZivyHrac
new g_HracPripojen
new g_VolnyDen
new g_AutoVolnyDen
new g_Mikrofon
new g_Trail
new g_HledanyVezen
new g_Padak
new g_RedBull
new g_CTBan
new g_Molotov
new g_VodniPistol
new g_AntiLaser
new g_Zmrazen
new g_ZmenaTeamu
new g_Schovan
new g_Prodlouzeni

// MSG
new g_MsgSayText
new g_MsgTeamInfo
new g_MsgScreenFade
new g_MsgScreenShake
new g_MsgScoreInfo
new g_MsgDeathMsg
new g_MsgShowMenu
new g_MsgIconStatus

// Sprites
new g_sprRing
new g_sprBloodDrop
new g_sprBloodSpray
new g_sprTrail
new g_sprExploze
new g_sprShockwave
new g_sprFire
new g_sprSmoke
new g_sprVodniGun
new g_sprItemBox
new g_sprGlass
new g_sprIceCubeExploze
new g_sprIceCubeFrostgib
#if defined DEBUG
new g_sprDot
#endif

// Eventy
new g_fwPlayerThink
new g_fwPlayerThink_Post
new g_fwUpdateClient

// Arrays
new Array: g_aHonenaSpawn
new Array: g_aEntityArray
new Array: g_aChatAuto
new Array: g_aDealerData
new Array: g_aSeznamMap

// Tries
new Trie: g_tDealerSklad
new Trie: g_tSpecialniSkiny
new Trie: g_tVIPKodBlockList
new Trie: g_tMapVoteMapy
new Trie: g_tMapVoteHraci

enum _hud 
{ 
	_hudPos, 
	Float:_x, 
	Float:_y
}

new const g_HudSync[][_hud] =
{
	{0, 0.93, 0.50},		// [00] Body Status 0
	{0, 0.93, 0.53},		// [01] Body Status 1
	{0, 0.93, 0.56},		// [02] Body Status 2
	{0, 0.1, 0.3},			// [03] Hledani vezni
	{0, 0.1, 0.55}, 		// [04] Volne dny
	{0, -1.0, 0.78},		// [05] Target info
	{0, -1.0, 0.25}, 		// [06] Odpocet
	{0, -1.0, 0.72},		// [07] Timer
	{0, -1.0, 0.0},			// [08] Status
	{0, -1.0, 0.43},		// [09] Info
	{0, -1.0, 0.60}			// [10] Server info
}

enum eDHud {
	Float:x, 
	Float:y
}

new const g_DHudPozice[4][eDHud] =
{
	{-1.0, 0.65},		// [0] Uprostred || Pod zamerovacem
	{-1.0, 0.35}, 		// [1] Uprostred || Nad zamerovacem
	{-1.0, 0.75},		// [2] Informacni DHud (Posledni Vezen, Vitez Schovky)
	{-1.0, 0.72}		// [3] Stred dole
}

enum _:EntityData
{
	Ent_Data_Id, 
	Entity: Ent_Data_Typ
}

enum Entity
{
	ENTITA_SROUBOVAK = 0,
	ENTITA_ZABIKUCH,
	ENTITA_PACIDLO,
	ENTITA_MACETA,
	ENTITA_OBUSEK,
	ENTITA_KATANA,
	ENTITA_MINA,
	ENTITA_ITEM_BOX,
	ENTITA_ICECUBE,
	ENTITA_MOLOTOV,
	ENTITA_BALON,
	ENTITA_PADAK,
	ENTITA_BODY,
	ENTITA_ODHOZENY_NUZ,
	ENTITA_MOTOROVKA,
	ENTITA_MOLOTOV_DROP
}

enum LogTypy 
{
	LOG_UCTY,
	LOG_CTBANNER,
	LOG_STATS,
	LOG_VIP,
	LOG_BANY
}

enum AdminLogTyp
{
	LOG_ADMIN_KICK,
	LOG_ADMIN_BAN
}

enum VezenskyTeam
{
	VEZEN_TEAM_ZADNY = 0,
	VEZEN_TEAM_ZELENY,
	VEZEN_TEAM_RUZOVY
}

enum SpecialniDnyTyp
{
	SPECIAL_ZADNY = 0,
	SPECIAL_FREEDAY,
	SPECIAL_PRESTRELKA,
	SPECIAL_ONLYHS,
	SPECIAL_ARENA,
	SPECIAL_ONESHOT,
	SPECIAL_SCHOVKA, 
	SPECIAL_HONENA
}

enum HonenaOdmena 
{
	ODMENA_ZADNA = 0,
	ODMENA_GRAVITACE,
	ODMENA_NEVIDITELNOST,
	ODMENA_VODNIGUN,
	ODMENA_PAST
}

new const CasyOdpoctu[4] = {3, 5, 10, 15}
new const NazvyDuelModu[3][] = {"Normal", "Only HS", "Extra HP"}
new const NazvyTeamu[VezenskyTeam][] = {"Zadny", "Zeleny", "Ruzovy"}
new const NazvySpecialu[SpecialniDnyTyp][] = {"Zadny", "Volny Den", "Prestrelka", "Only HS", "Arena", "OneShot", "Schovka", "Honena"}
new const LogSlozky[LogTypy][] = {"ucty", "ctbanner", "stats", "vip", "bany"}
new const AdminLogSlozky[AdminLogTyp][] = {"kick", "ban"}

new const CTGunMenu[7] = {0, 1, 2, 3, 4, 7, 8}
new const ArenaGuns[8] = {0, 1, 2, 3, 6, 7, 8, 9}
new const DuelZbrane[4] = {9, 10, 6, 2}
new const AimDuely[7] = {0, 1, 3, 4, 5, 7, 8}

new const ZbraneNazvy[13][] = {"M4a1", "Ak47", "Awp", "Mp5", "Famas", "Galil", "Scout", "M249", "M3", "Deagle", "USP", "Glock", "Elite"}
new const ZbraneGun[13][] = {"weapon_m4a1", "weapon_ak47", "weapon_awp", "weapon_mp5navy", "weapon_famas", "weapon_galil", "weapon_scout", "weapon_m249", "weapon_m3", "weapon_deagle", "weapon_usp", "weapon_glock18", "weapon_elite"}
new const ZbraneCSW[13] = { CSW_M4A1, CSW_AK47, CSW_AWP, CSW_MP5NAVY, CSW_FAMAS, CSW_GALIL, CSW_SCOUT, CSW_M249, CSW_M3, CSW_DEAGLE, CSW_USP, CSW_GLOCK18, CSW_ELITE}
new const ZbraneAmmo[13] = { 90, 90, 30, 120, 90, 90, 90, 200, 32, 35, 100, 120, 120}

new const WeaponsMaxBpAmmo[] = { 0, 30, 90, 200, 90, 32, 100, 100, 35, 52, 120, 2, 1, 1, 1}

new const SmazatEntity[8][] = {"func_hostage_rescue", "info_hostage_rescue", "func_bomb_target", "info_bomb_target", "hostage_entity", "info_vip_start", "func_vip_safetyzone", "func_escapezone"}
new const BalonEntity[12][] = {"worldspawn", "func_wall", "func_door",  "func_door_rotating", "func_wall_toggle", "func_breakable", "func_pushable", "func_train", "func_illusionary", "func_button", "func_rot_button", "func_rotating"}

new const HonenaBoxyOdmena[HonenaOdmena][] = {"Zadna", "Rychlost & Gravitace", "Neviditelnost", "Vodni Pistol", "Past"}
new const EntityName[Entity][] = {"Sroubovak", "Nozik", "Pacidlo", "Maceta", "Obusek", "Katana", "LaserMina", "ItemBox", "IceCube", "Molotov", "Balon", "Padak", "Body", "PohozenyNuz", "Motorovka", "MolotovDrop"} 

new const ChatZpravaBarva[3][] = {"!cy", "!ctt", "!cg"}
new const ChatTagPrefix[3][] = {"", "VIP", "Admin"}

new const CTBannerDuvody[5][] = {"Vlastni duvod", "Free Kill", "Spatny Mikrofon", "Poruseni pravidel", "Demence"}
new const CTBannerDelkaBanuNazev[8][] = {"Permanetni", "3 Hodiny", "12 Hodin", "24 Hodin", "3 Dny", "Tyden", "2 Tydny", "Mesic"}
new const CTBannerDelkaBanu[8] = {0, 10800, 43200, 86400, 259200, 604800, 1209600, 2419200}

new const Bany_Duvody[6][] = {"Vlastni duvod", "Wallhack", "Aimbot", "Multihack", "Nadavky", "Demence"}
new const Bany_DelkaNazev[9][] = {"Vlastni", "3 Hodiny", "12 Hodin", "24 Hodin", "3 Dny", "Tyden", "2 Tydny", "Mesic", "Permanetni"}
new const Bany_Delka[9] = {0, 180, 720, 1440, 4320, 10080, 20160, 43200, 0}

new const Dealer_Itemy_Nazev[5][] = {"Molotov", "Prevlek", "Nozik", "Pacidlo", "Motorovka"}
new const Dealer_Itemy_Max[5] = {5, 6, 20, 3, 2}

new const Vezen_Submodels[] = {1, 2, 11, 12}

new const ViewMod_Nazev[4][] = {"Normalni", "Postava A", "Postava B", "Seshora"}
new const Schovka_Model_Nazey[][] = {"Pocitac", "Antena", "Parez", "Paleta", "Lehatko", "Hasicak", "Hydrant", "Zidle", "Zebrik", "Kaktus", "Krabice", "Kvetina", "Bariera", "Lavicka", "Strom", "Umyvadlo"}
new const Klobouk_Nazvy[][] = {"Moucha", "Assassin", "Zobak", "Klec", "Cola", "Dabel", "Sluchatka", "Oko", "Plynova maska", "Sekacek", "Vrtulnik", "Afro", "Batman", "Ksiltovka", "Motyl", "Koruna", "Klicek", "Indian", "Maska"}
new const Klobouk_Submodel[] = {2, 3, 4, 6, 7, 8, 9, 11, 13, 14, 15, 16, 23, 38, 35, 33, 32, 31, 30}

enum Barvy
{
	BARVA_CERVENA = 0,
	BARVA_ZLUTA,
	BARVA_ZELENA,
	BARVA_MODRA,
	BARVA_RUZOVA,
	BARVA_MODRA_SVETLA,
}

static const BarvyRGB[Barvy][3] = 
{
	{255, 0, 0},        	// Cervena
	{255, 140, 10},			// Zluta
	{0, 255, 0},        	// Zelena
	{0, 0, 255},        	// Modra
	{255, 127, 127},		// Ruzova
	{42, 85, 201}			// Modra-Svetla
}

enum Noze
{
	NOZE_RUCE = 0,
	NOZE_ZABIKUCH,
	NOZE_SROUBOVAK,
	NOZE_PACIDLO,
	NOZE_MACETA,
	NOZE_MOTOROVKA,
	NOZE_OBUSEK,
	NOZE_RUKAVICE,
	NOZE_KATANA
}

enum _:AdminInfo
{
	Target_ID,
	Target_Delka,
	Target_Duvod[66]
}

enum DuelTypy
{
	DUEL_BOX = 0,
	DUEL_DEAGLE,
	DUEL_USP,
	DUEL_SCOUT,
	DUEL_AWP,
	DUEL_GRANATY,
	DUEL_AIM
}

enum _:DealerData
{
	Float: flDealer_Origin[3],
	Float: flDealer_Angle
}

enum HracFlags
{
	HRAC_NORMAL = 0, 
	HRAC_VIP, 
	HRAC_ADMIN
}

enum _:PlayerData
{
	CsTeams: Hrac_Team,
	Float: Hrac_Rychlost,
	PlayerModelTyp: Model_Typ,
	Model_Name[46],
	GunMenu_LastP,
	GunMenu_LastS,
	bool: GunMenu_Off,
	bool: GunMenu_Used,
	Trail_Barva[3],
	Trail_Origin[3],
	Trail_Ticku,
	Noze: MujNuz,
	Noze: MujNuz_Inventar,
	bool: UkrytyNuzStav,
	Noze: UkrytyNuz,
	Float: PosledniUkrytiNoze,
	bool: PrimarniUtokKnife,
	VezenskyTeam: VezenTym,
	AuthID[36],
	Special_Killu,
	Padak_Ent,
	VIP_Hat_Ent,
	VIP_Hat_Id,
	bool: VIP_Chcal,
	VIP_ChcaniOdpocet,
	OdhazovacichNozu,
	Float: PosledniOdhozeni,
	RedBullOdpocet,
	Float: BodyStatusCas,
	BodyStatusPozice,
	PocetMin,
	Float: Dealer_Cooldown,
	Ping_Sum,
	Ping_Samples,
	bool: Honena_Chycen,
	Float: CustomGun_LastAttack,
	Schovka_Entity,
	Schovka_Model,
	Float: Schovka_Angle,
	Schovka_Kamera,
	Honena_Odpocet,
	HonenaOdmena: Honena_ItemBox_Odmena,
	Honena_VodniGun_Effekt,
	Honena_Vezen_Zivotu,
	Float: Honena_AimLock_Angles[3],
	Float: Odhozeni_Xp_Last,
	Float: Dotknuti_Zony,
	Ucet_ID,
	Ucet_Konto,
	HracFlags:Ucet_Flags,
	Float: Ucet_Cas,
	MCTBan_Vytvoren,
	MCTBan_Duvod[40],
	MCTBan_Delka,
	MCTBan_Vyprsi,
	MCTBan_Admin[33],
	bool: Stats_Existuji,
	Stats_Kill_Simon,
	Stats_Kill_Bachar,
	Stats_Kill_Box,
	Stats_Smrti,
	Stats_Vzpour,
	Stats_VyhranychDuelu,
	Aktivace_Kod[6],
	Aktivace_Heslo[11],
	Aktivace_Delka,
	Aktivace_Vyprsi,
	Aktivace_Pokusu
}

enum _:ServerDataInfo
{
	Duel_idT,
	Duel_idCT,
	Duel_Zbran,
	Duel_Mod,
	DuelTypy: Duel_Typ,
	bool: Duel_NoZoom,
	bool: Duel_Bezi,
	Balon_Ent,
	Balon_Drzi,
	Float: Balon_SpawnOrigin[3],
	Float: Hry_Stopky_Start,
	bool: Hry_Stopky_Bezi,
	bool: Hry_Spray_Metr,
	bool: Hry_SimonKroky,
	bool: Hry_Globalni_Mic,
	Float: PosledniSpecial,
	bool: VzpouraVyvolana,
	Posledni_Vezen,
	Box,
	Simon_Id, 
	ArenaGun,
	HonickaChycenych,
	HonickaThinkEntita,
	bool: BlockDeathMsg,
	Mina_EntTyp,
	Handle: SQL_Tuple,
	SQL_Error,
	Cely_Entita,
	Map_Button_Simon[10],
	Map_Button_Blacklist[10],
	Odehranych_Kol,
	Dealer_Ent,
	bool: Dealer_Animation_Idle,
	Float: Dealer_LastAnimation,
	bool: Special_Spusten,
	Float: Zacatek_Kola,
	Rozdeleni_Hrac_Id,
	SpecialniDnyTyp: Special_Typ,
	bool: Special_Limit[SpecialniDnyTyp],
	Odpocet_Cas,
	bool: Mapa_Prodlouzena
}

#if defined DEBUG
enum _:EditorData 
{
	Editor_Ent_Balon[33],
	Editor_Ent_Cely[33],
	Array:Editor_Button_Simon,
	Array:Editor_Button_Blacklist,
	Camera_Mod
}

new g_EditorData[EditorData]
#endif

enum PlayerModelTyp
{
	MODEL_NORMAL = 0,
	MODEL_VIP_MALE,
	MODEL_VIP_FEMALE,
	MODEL_CUSTOM
}

new g_AdminInfo[33][AdminInfo]
new g_MojeInfo[33][PlayerData]
new g_ServerInfo[ServerDataInfo]	

enum
{
	POWERUP_THINK,
	BEAMBREAK_THINK,
	EXPLOSE_THINK
};

enum 
{
    SCOREATTRIB_ARG_PLAYERID = 1,
    SCOREATTRIB_ARG_FLAGS
};

enum ( <<= 1 ) 
{
    SCOREATTRIB_FLAG_NONE = 0,
    SCOREATTRIB_FLAG_DEAD = 1,
    SCOREATTRIB_FLAG_BOMB,
    SCOREATTRIB_FLAG_VIP
};

new g_ZivychHracu[CsTeams]
new g_OnlineHracu[CsTeams]

static const OdpocetZvuky[12][] =
{
	"mjail/odpocet/one.wav",
	"mjail/odpocet/two.wav",
	"mjail/odpocet/three.wav",
	"mjail/odpocet/four.wav",
	"mjail/odpocet/five.wav",
	"mjail/odpocet/six.wav",
	"mjail/odpocet/seven.wav",
	"mjail/odpocet/eight.wav",
	"mjail/odpocet/nine.wav",
	"mjail/odpocet/ten.wav",
	"mjail/odpocet/20_sec.wav",
	"mjail/odpocet/30_sec.wav"
}

static const OneShotColtModel[2][] = 
{
	"models/mjail/oneshot/v_colt.mdl",
	"models/mjail/oneshot/p_colt.mdl"
}

static const OneShotColtSound[3][] = 
{
	"mjail/oneshot/colt_clipin.wav",
	"mjail/oneshot/colt_clipout.wav",
	"mjail/oneshot/colt_shoot1.wav"
}

static const ModelyEntit[4][] = 
{
	"models/mjail/motorovka.mdl",
	"models/mjail/entity07.mdl",
	"models/mjail/dealer.mdl",
	"models/mjail/hats.mdl"
}

static const vModelyNozu[Noze][] = 
{
	"models/mjail/noze/v_ruce.mdl",
	"models/mjail/noze/v_nozik.mdl",
	"models/mjail/noze/v_sroubovak.mdl",
	"models/mjail/noze/v_pacidlo.mdl",
	"models/mjail/noze/v_maceta.mdl",
	"models/mjail/noze/v_motorovka.mdl",
	"models/mjail/noze/v_obusek.mdl",
	"models/mjail/noze/v_rukavice.mdl",
	"models/mjail/noze/v_katana.mdl"
}

static const pModelyNozu[Noze][] = 
{
	"models/mjail/noze/p_ruce.mdl",
	"models/mjail/noze/p_nozik.mdl",
	"models/mjail/noze/p_sroubovak.mdl",
	"models/mjail/noze/p_pacidlo.mdl",
	"models/mjail/noze/p_maceta.mdl",
	"models/mjail/noze/p_motorovka.mdl",
	"models/mjail/noze/p_obusek.mdl",
	"models/mjail/noze/p_rukavice.mdl",
	"models/mjail/noze/p_katana.mdl"
}

static const MolotovModely[2][] =
{
	"models/mjail/molotov/v_molotov.mdl",
	"models/mjail/molotov/p_molotov.mdl"
}

static const VodniPistolModely[2][] =
{
	"models/mjail/vodni_gun/v_vodnigun.mdl",
	"models/mjail/vodni_gun/p_vodnigun.mdl"
}

static const RuceSound[4][] = 
{
	"mjail/noze/ruce/knife_hit.wav",
	"mjail/noze/ruce/knife_slash.wav",
	"mjail/noze/ruce/knife_hitwall.wav",
	"mjail/noze/ruce/knife_stab.wav"
}

static const SroubovakSound[3][] =
{
	"mjail/noze/sroubovak/sroubovak_hit.wav",
	"mjail/noze/sroubovak/sroubovak_hitwall.wav",
	"mjail/noze/sroubovak/sroubovak_slash.wav"
}

static const NozikSound[3][] =
{
	"mjail/noze/nozik/knife_hit.wav",
	"mjail/noze/nozik/knife_hitwall.wav",
	"mjail/noze/nozik/knife_slash.wav"
}

static const PacidloSound[6][] =
{
	"mjail/noze/pacidlo/knife_deploy1.wav",
	"mjail/noze/pacidlo/knife_hit1.wav",
	"mjail/noze/pacidlo/knife_hit2.wav",
	"mjail/noze/pacidlo/knife_hitwall1.wav",
	"mjail/noze/pacidlo/knife_slash1.wav",
	"mjail/noze/pacidlo/knife_stab.wav"
}

static const MacetaSound[5][] = 
{
	"mjail/noze/maceta/mt_draw.wav",
	"mjail/noze/maceta/mt_hit1.wav",
	"mjail/noze/maceta/mt_hit2.wav",
	"mjail/noze/maceta/mt_hitwall.wav",
	"mjail/noze/maceta/mt_miss.wav"
}

static const MotorovkaSound[5][] = 
{
	"mjail/noze/motorovka/knife_deploy1.wav",
	"mjail/noze/motorovka/knife_hit.wav",
	"mjail/noze/motorovka/knife_hitwall.wav",
	"mjail/noze/motorovka/knife_slash.wav",
	"mjail/noze/motorovka/knife_stab.wav"
}

static const ObusekSound[6][] = 
{
	"mjail/noze/obusek/knife_deploy1.wav",
	"mjail/noze/obusek/knife_hit1.wav",
	"mjail/noze/obusek/knife_hit2.wav",
	"mjail/noze/obusek/knife_hitwall1.wav",
	"mjail/noze/obusek/knife_slash1.wav",
	"mjail/noze/obusek/knife_stab.wav"
}

static const KatanaSound[6][] = 
{
	"mjail/noze/katana/katana_deploy1.wav",
	"mjail/noze/katana/katana_hwall.wav",
	"mjail/noze/katana/katana_slash1.wav",
	"mjail/noze/katana/katana_stab.wav",
	"mjail/noze/katana/katana1.wav",
	"mjail/noze/katana/katana1.wav"
}

static const MinySound[4][] =
{
	"mjail/mina/mine_deploy.wav",
	"mjail/mina/mine_charge.wav",
	"mjail/mina/mine_activate.wav",
	"mjail/mina/mina_attack.wav"
}

static const BoxZvuky[3][] = 
{
	"mjail/box/rukavice_hit.wav",
	"mjail/box/rukavice_hit_wall.wav",
	"mjail/box/gong.wav"
}

static const RuzneZvuky[13][] =
{
	"mjail/lulani.wav",
	"mjail/alarm.wav",
	"mjail/molotov_fire.wav",
	"mjail/molotov_explosion.wav",
	"mjail/padak.wav",
	"mjail/freeday_start.wav",
	"mjail/freeday_end.wav",
	"mjail/item_box.wav",
	"mjail/icecube.wav",
	"mjail/duel.wav",
	"mjail/water-1.wav",
	"mjail/money_pickup.wav",
	"mjail/bounce.wav"
}


static const CTSkiny[4][] =
{
	"mjailctv1",  		/* Normal */
	"mjailctv2",  		/* VIP Muzsky */
	"mjailctv3",		/* VIP Zensky */
	"mjailctv4"			/* Simon */
}

static const TSkiny[3][] =
{
	"mjailtv3",			/* Normal */
	"mjailtv2",			/* VIP Muzsky */
	"mjailtv3"			/* VIP Zensky */
}

public plugin_init() 
{
	register_plugin(PLUGIN, VERSION, AUTHOR)
	register_cvar(PLUGIN, VERSION, FCVAR_SERVER|FCVAR_SPONLY)
	
	g_MsgSayText = get_user_msgid("SayText")
	g_MsgTeamInfo = get_user_msgid("TeamInfo")
	g_MsgScreenFade = get_user_msgid("ScreenFade")
	g_MsgScreenShake = get_user_msgid("ScreenShake")
	g_MsgScoreInfo = get_user_msgid("ScoreInfo")
	g_MsgDeathMsg = get_user_msgid("DeathMsg")
	g_MsgShowMenu = get_user_msgid("ShowMenu")
	g_MsgIconStatus = get_user_msgid("StatusIcon");
	
	register_message(g_MsgSayText, "MessageReturnHandled")
	register_message(g_MsgDeathMsg, "MessageDeathMsg")
	register_message(g_MsgShowMenu, "HlavniMenuClassic")
	register_message(get_user_msgid("VGUIMenu"), "HlavniMenuVGUI")
	register_message(get_user_msgid("StatusText"), "MessageReturnHandled")
	register_message(get_user_msgid("StatusIcon"), "MessageStatusIcon")
	register_message(get_user_msgid("TextMsg"), "MessageTextMsg")
	register_message(get_user_msgid("AmmoX"), "Message_AmmoX")
	register_message(get_user_msgid("ScoreAttrib"), "MessageScoreAttrib");
	register_message(get_user_msgid("Money"), "MessageMoney");
	register_message(get_user_msgid("HideWeapon"), "MessageHideWeapon");
	
	set_msg_block(get_user_msgid("MOTD"), BLOCK_SET);
	
	RegisterHam(Ham_Think, "grenade", "SpecialniGranaty")
	RegisterHam(Ham_Spawn, "player", "SpawnutiHrace_Post", 1)
	RegisterHam(Ham_Killed, "player", "ZabitiHrace_Pre")
	RegisterHam(Ham_TakeDamage, "player", "DamageHrace_Pre")
	RegisterHam(Ham_TraceAttack, "player", "AttackHrace_Pre")
	RegisterHam(Ham_TraceAttack, "player", "AttackHrace_Post", 1)
	RegisterHam(Ham_AddPlayerItem, "player", "BloknoutZbrane") 
	RegisterHam(Ham_Touch, "info_target", "EntityTouch")
	RegisterHam(Ham_Use, "func_healthcharger", "BloknoutHeal_Ham")
	RegisterHam(Ham_Use, "func_recharge", "BloknoutHeal_Ham")
	RegisterHam(Ham_Use, "func_button", "ZmacknutiButtonu")
	RegisterHam(Ham_Weapon_SecondaryAttack, "weapon_scout", "BloknoutZoom")
	RegisterHam(Ham_Weapon_SecondaryAttack, "weapon_awp", "BloknoutZoom")
	RegisterHam(Ham_ObjectCaps, "player", "FwdHamObjectCaps", 1);
	RegisterHam(Ham_Item_PreFrame, "player", "MaximalniRychlost", 1)
	RegisterHam(Ham_Weapon_PrimaryAttack, "weapon_knife", "Knife_PrimaryAttack_Post", 1)
	RegisterHam(Ham_Weapon_SecondaryAttack, "weapon_knife", "Knife_SecondaryAttack_Post", 1)
	
	register_forward(FM_SetModel, "fw_SetModel")
	register_forward(FM_Think, "fw_EntityThink")
	register_forward(FM_SetClientKeyValue, "fw_SetClientKeyValue")
	register_forward(FM_Voice_SetClientListening, "fw_NastaveniMikrofonu")
	register_forward(FM_GetGameDescription, "fw_GameDesc") 
	register_forward(FM_EmitSound, "fw_EmitSound")
	register_forward(FM_CmdStart, "fw_cmdStart_Post", 1)
	
	register_logevent("KonecKola", 2, "1=Round_End")
	register_logevent("KonecKola", 2, "0=World triggered", "1&Restart_Round_")
	register_logevent("KonecKola", 2, "0=World triggered", "1=Game_Commencing")
	register_logevent("NoveKolo", 2, "0=World triggered", "1=Round_Start")
	
	register_event("StatusValue", "PlayerStatus", "be", "1=2", "2!0")
	register_event("StatusValue", "PlayerStatus", "be", "1=1", "2=0")
	register_event("CurWeapon", "fw_EvCurWeapon", "be", "1=1")
	register_event("23", "SprayMetr", "a", "1=112")
	
	register_think("Balon", "BalonThink")
	register_think("Dealer", "DealerThink")
	
	new i
	for(i = 0; i < sizeof BalonEntity; i++)
		register_touch("Balon", BalonEntity[i], "DotknutiMiceOkoli")
	
	register_touch("Balon", "player", "DotknutiMiceHrac")
	register_touch("player", "player", "HonenaTouchFunkce")
	register_touch("trigger_hurt", "player", "BloknoutHeal_Engine")
	register_touch("trigger_gravity", "player", "BloknoutHeal_Engine")
	register_touch("zona_ent", "player", "ZonaTouch")
	register_touch("weaponbox", "player", "SebraniZbrane")
	register_touch("armoury_entity", "player", "SebraniZbrane")
	register_touch("weapon_shield", "player", "SebraniZbrane")
	register_touch(EntityName[ENTITA_BODY], "player", "Entity_Touch_Body")
	register_touch(EntityName[ENTITA_ITEM_BOX], "player", "Entity_Touch_ItemBox")
	
	register_impulse(100, "FlashlightEvent")
		
	// Say Handler
	register_clcmd("DuvodKicku", "KickDuvod_Handler")
	register_clcmd("DuvodBanu", "BanDuvod_Handler")
	register_clcmd("DelkaBanu", "BanDelka_Handler")
	register_clcmd("Duvod", "CTBanner_DuvodSayHandler")
	register_clcmd("VIPKod", "VIPSystem_KodHandler")
	#if defined DEBUG
	register_clcmd("NazevZony", "NazevZony_SayHandler")
	#endif
	
	// Events
	register_clcmd("jointeam", "HandlerReturn")
	register_clcmd("joinclass", "HandlerReturn")
	register_clcmd("say", "NormalniChat")
	register_clcmd("say_team", "TeamChat")
	register_clcmd("drop", "DropFunkce")
	
	// Testing
	#if defined DEBUG
	register_clcmd("say /ent", "cmdEntitaTest")
	register_clcmd("say /aa", "cmdStatsTest")
	register_clcmd("say /as", "cmdTest")
	register_clcmd("say /button", "cmdEntityButton")
	register_clcmd("say /size", "cmdEntitySize")
	register_clcmd("say /vip", "VIPMenu")
	#endif
	
	// Hrac
	register_clcmd("say /lr", "cmdPosledniPrani")
	register_clcmd("say /pp", "cmdPosledniPrani")
	register_clcmd("say /shop", "cmdShop")
	register_clcmd("say /gun", "cmdGunMenu")
	register_clcmd("say /simon", "cmdSimon")
	register_clcmd("say /menu","HlavniMenu_Rozcestnik")
	register_clcmd("say /hats", "HatMenu")
	register_clcmd("say /prodlouzit", "ProdlouzeniHlasovani")
	
	// Bindy
	register_clcmd("+fa_laser","ZacitPlantovatMinu");
   	register_clcmd("-fa_laser","ZrusitPlantovaniMiny");
	register_clcmd("fa_chcat", "cmdChcat")
	register_clcmd("fa_noze", "OdhoditKnife")
	register_clcmd("fa_body", "OdhoditBody")
	
	// Admin
	register_concmd("amxmodmenu", "AmxmodxMenu")
	register_concmd("simon_menu", "VedouciVeznice")	
	register_concmd("ct_banner", "CTBanner_MenuHraci")
	#if defined DEBUG
	register_concmd("martas_admin", "MartasAdministrace")
	#endif
	
	for(i = 0; i < sizeof(g_HudSync); i++)
		g_HudSync[i][_hudPos] = CreateHudSyncObj()
		
	new entita
	entita = fm_create_entity("info_target")
	set_pev(entita, pev_classname, "HudStatus")
	set_pev(entita, pev_nextthink, get_gametime() + 4.0)
	register_think("HudStatus", "HudStatusThink")
	
	entita = fm_create_entity("info_target")
	set_pev(entita, pev_classname, "SetinovaEntita")
	set_pev(entita, pev_nextthink, get_gametime() + 0.1)
	register_think("SetinovaEntita", "SetinovaEntitaThink")
	
	#if !defined DEBUG
	g_ServerInfo[Odpocet_Cas] = 15
	set_task(1.0, "AutomatickyRestart", TASK_RESTART, _, _, "b")
	#endif
	
	new iEntity, iMaxEntities, szModel[2]
	iEntity = MAX_HRACU, iMaxEntities = get_global_int( GL_maxEntities );
	while( ++iEntity <= iMaxEntities ) {
		if( pev_valid( iEntity ) && entity_get_int( iEntity, EV_INT_rendermode ) == kRenderGlow ) {
			entity_get_string( iEntity, EV_SZ_model, szModel, 1 );
			
			if( szModel[ 0 ] == '*' )
				entity_set_int( iEntity, EV_INT_rendermode, kRenderNormal );
		}
	}
	
	Db_KontrolaPripojeni()
	g_aEntityArray = ArrayCreate(EntityData)
	NacistMapy()
	NacistMapData()
	NacistSpecialniSkiny()
	set_task(float((DELKA_JEDNE_MAPY - 5) * 60), "SpustitHlasovaniMapy")
}

public plugin_precache()
{
	register_forward(FM_PrecacheModel, "BlockModelPrecache")
	register_forward(FM_PrecacheSound, "BlockSoundPrecache")

	new i
	for(i = 0; i < sizeof(vModelyNozu); i++)
	{
		engfunc(EngFunc_PrecacheModel, vModelyNozu[Noze:i])
		engfunc(EngFunc_PrecacheModel, pModelyNozu[Noze:i])
	}
	
	new buffer[64]
	for (i = 0; i < sizeof(CTSkiny); i++)
	{
		format(buffer, 63, "models/player/%s/%s.mdl", CTSkiny[i], CTSkiny[i])
		engfunc(EngFunc_PrecacheModel, buffer)
	}
	
	for (i = 0; i < sizeof(TSkiny); i++)
	{
		format(buffer, 63, "models/player/%s/%s.mdl", TSkiny[i], TSkiny[i])
		engfunc(EngFunc_PrecacheModel, buffer)
	}
	
	for(i = 0; i < sizeof(ModelyEntit); i++)
		engfunc(EngFunc_PrecacheModel, ModelyEntit[i])
	
	for(i = 0; i < sizeof(MolotovModely); i++)
		engfunc(EngFunc_PrecacheModel, MolotovModely[i])
		
	for(i = 0; i < sizeof(VodniPistolModely); i++)
		engfunc(EngFunc_PrecacheModel, VodniPistolModely[i])
		
	for(i = 0; i < sizeof(OneShotColtModel); i++)
		engfunc(EngFunc_PrecacheModel, OneShotColtModel[i])
		
	for(i = 0; i < sizeof(OneShotColtSound); i++)
		engfunc(EngFunc_PrecacheSound, OneShotColtSound[i])

	for(i = 0; i < sizeof(BoxZvuky); i++)
		engfunc(EngFunc_PrecacheSound, BoxZvuky[i])
	
	for(i = 0; i < sizeof(OdpocetZvuky); i++)
		engfunc(EngFunc_PrecacheSound, OdpocetZvuky[i])
				
	for(i = 0; i < sizeof(RuceSound); i++)
		engfunc(EngFunc_PrecacheSound, RuceSound[i])

	for(i = 0; i < sizeof(KatanaSound); i++)
		engfunc(EngFunc_PrecacheSound, KatanaSound[i])
		
	for(i = 0; i < sizeof(NozikSound); i++)
		engfunc(EngFunc_PrecacheSound, NozikSound[i])
			
	for(i = 0; i < sizeof(SroubovakSound); i++)
		engfunc(EngFunc_PrecacheSound, SroubovakSound[i])
	
	for(i = 0; i < sizeof(PacidloSound); i++)
		engfunc(EngFunc_PrecacheSound, PacidloSound[i])
	
	for(i = 0; i < sizeof(MacetaSound); i++)
		engfunc(EngFunc_PrecacheSound, MacetaSound[i])
	
	for(i = 0; i < sizeof(MotorovkaSound); i++)
		engfunc(EngFunc_PrecacheSound, MotorovkaSound[i])
		
	for(i = 0; i < sizeof(ObusekSound); i++)
		engfunc(EngFunc_PrecacheSound, ObusekSound[i])
	
	for(i = 0; i < sizeof(RuzneZvuky); i++)
		engfunc(EngFunc_PrecacheSound, RuzneZvuky[i])
	
	for(i = 0; i < sizeof(MinySound); i++)
		engfunc(EngFunc_PrecacheSound, MinySound[i])
		
	#if defined DEBUG
	g_sprDot = engfunc(EngFunc_PrecacheModel, "sprites/dot.spr")
	#endif
	
	// Valve
	g_sprGlass = engfunc(EngFunc_PrecacheModel, "models/glassgibs.mdl")
	g_sprRing = engfunc(EngFunc_PrecacheModel, "sprites/white.spr")
	g_sprShockwave = engfunc(EngFunc_PrecacheModel, "sprites/shockwave.spr")
	g_sprBloodDrop = engfunc(EngFunc_PrecacheModel, "sprites/blood.spr")
	g_sprBloodSpray = engfunc(EngFunc_PrecacheModel, "sprites/bloodspray.spr")
	g_sprSmoke = engfunc(EngFunc_PrecacheModel, "sprites/black_smoke3.spr")
	
	// Vlastni
	g_sprFire = engfunc(EngFunc_PrecacheModel, "sprites/mjail/flames.spr")
	g_sprTrail = engfunc(EngFunc_PrecacheModel, "sprites/mjail/zbeam3.spr")
	g_sprExploze = engfunc(EngFunc_PrecacheModel, "sprites/mjail/bflare.spr")
	g_sprVodniGun = engfunc(EngFunc_PrecacheModel, "sprites/mjail/vodnigun.spr")
	g_sprItemBox = engfunc(EngFunc_PrecacheModel, "sprites/mjail/itembox.spr")
	g_sprIceCubeExploze = engfunc(EngFunc_PrecacheModel, "sprites/mjail/icecube_exp.spr")
	g_sprIceCubeFrostgib = engfunc(EngFunc_PrecacheModel, "sprites/mjail/icecube_frostgib.spr")
	
	// Nastaveni kamery pro schovku potrebuje tuhle picovinu!
	engfunc(EngFunc_PrecacheModel, "models/rpgrocket.mdl")
	
	register_forward(FM_Spawn, "precache_spawn", 1)
}

public plugin_cfg()
{
	set_cvar_num("mp_flashlight", 0);
	
	set_cvar_num("mp_forcechasecam", 1)
	set_cvar_num("mp_autoteambalance", 0)
	set_cvar_float("mp_buytime", 0.0)
	set_cvar_num("mp_freezetime", 1)
	set_cvar_num("mp_timelimit", DELKA_JEDNE_MAPY)
	set_cvar_num("mp_roundtime", 9)
	set_cvar_num("mp_limitteams", 0)
	set_cvar_num("mp_forcecamera", 1)
	
	set_cvar_float("mp_kickpercent", 0.0)
	set_cvar_num("mp_friendlyfire", 1) 
	set_cvar_num("mp_tkpunish", 0)
	set_cvar_num("mp_autokick", 0)
	
	set_cvar_num("sv_airaccelerate", 101)
	set_cvar_num("sv_proxies", 0)
	set_cvar_num("sv_stats", 0)
	set_cvar_num("sv_alltalk", 1)
	set_cvar_num("sv_voiceenable", 1)
	set_cvar_num("sv_voicequality", 5)
	set_cvar_string("sv_voicecodec", "voice_speex")
	
	set_task(2.0, "NacistChatZpravy")
	g_ServerInfo[Mina_EntTyp] = engfunc(EngFunc_AllocString, "func_breakable");
}

public plugin_end()
{
	if (g_aHonenaSpawn)
		ArrayDestroy(g_aHonenaSpawn)
		
	if (g_aEntityArray)
		ArrayDestroy(g_aEntityArray)
		
	if (g_aChatAuto) 
		ArrayDestroy(g_aChatAuto)
		
	#if defined DEBUG
	if (g_EditorData[Editor_Button_Simon])
		ArrayDestroy(g_EditorData[Editor_Button_Simon])
	
	if (g_EditorData[Editor_Button_Blacklist])
		ArrayDestroy(g_EditorData[Editor_Button_Blacklist])
	#endif
}

public client_authorized(id)
{	
	get_user_authid(id, g_MojeInfo[id][AuthID], 35)	
	set_bit(g_HracPripojen, id)
	AktualizovatPromenne(false)

	#if defined DEBUG
	if (is_user_bot(id))
		return
	#endif
	
	KontrolaBanu(id)
}

public client_disconnected(id, bool:drop, message[], maxlen)
{
	if(task_exists(id + TASK_CHCANI))
		remove_task(id + TASK_CHCANI)
		
	if (task_exists(id + TASK_PINGCHECKER))
		remove_task(id + TASK_PINGCHECKER)
		
	if (pev_valid(g_MojeInfo[id][VIP_Hat_Ent]))
		engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][VIP_Hat_Ent])
	
	if (pev_valid(g_MojeInfo[id][Padak_Ent]))
		engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][Padak_Ent])
	
	if (g_tMapVoteHraci && TrieKeyExists(g_tMapVoteHraci, g_MojeInfo[id][AuthID]))
	{
		new szMapa[34], mapHlasu
		TrieGetString(g_tMapVoteHraci, g_MojeInfo[id][AuthID], szMapa, 33)
		TrieGetCell(g_tMapVoteMapy, szMapa, mapHlasu)
		TrieSetCell(g_tMapVoteMapy, szMapa, mapHlasu - (jeVIP(id) ? 2 : 1))
	}
	
	switch (g_ServerInfo[Special_Typ])
	{
		case SPECIAL_ZADNY:
		{
			if(g_ServerInfo[Simon_Id] == id)
			{
				g_ServerInfo[Simon_Id] = 0
				JailDHud(id, 1, {255, 0, 0}, 0, 2.0, 0.0, 0.5, 0.5, "Simon se odpojil!^nVsechny prikazy se automaticky rusi!")
				UkoncitSimonFunkce()
				
				if(g_ZivychHracu[CS_TEAM_CT] > 1)
				{
					set_task(5.0, "VybratNahodnehoSimona", TASK_NOVYSIMON)
				} else if (g_ZivychHracu[CS_TEAM_CT] == 1)
				{
					VybratNahodnehoSimona()
				}
			}
			
			if(id == g_ServerInfo[Duel_idT] || id == g_ServerInfo[Duel_idCT])
			{
				if(g_ZivychHracu[CS_TEAM_CT] && get_bit(g_ZivyHrac, g_ServerInfo[Duel_idT]))
				{
					fm_set_user_rendering(g_ServerInfo[Duel_idT])
					PosledniPrani_HlavniMenu(g_ServerInfo[Duel_idT])
					
					if (g_ServerInfo[Duel_Typ] == DUEL_BOX)
					{
						g_MojeInfo[g_ServerInfo[Duel_idT]][MujNuz] = _:g_MojeInfo[g_ServerInfo[Duel_idT]][MujNuz_Inventar]
						g_MojeInfo[g_ServerInfo[Duel_idT]][MujNuz_Inventar] = _:NOZE_RUCE
					}
					
					OdebratZbrane(g_ServerInfo[Duel_idT], 0)
				}
				
				g_ServerInfo[Duel_idT] = 0
				g_ServerInfo[Duel_idCT] = 0
				g_ServerInfo[Duel_Mod] = 0
				g_ServerInfo[Duel_Bezi] = false
			}
			
			if(get_bit(g_VolnyDen, id))
			{
				clear_bit(g_VolnyDen, id)
				VolneDnyHudInfo()
			}
		}
		case SPECIAL_HONENA:
		{
			if (g_MojeInfo[id][Honena_Chycen])
				g_ServerInfo[HonickaChycenych]--
			
			KontrolaUkonceniHonicky()
		}
	}
	
	clear_bit(g_ZivyHrac, id)
	clear_bit(g_HracPripojen, id)
	clear_bit(g_VolnyDen, id)
	clear_bit(g_AutoVolnyDen, id)
	clear_bit(g_Mikrofon, id)
	clear_bit(g_Trail, id)
	clear_bit(g_HledanyVezen, id)
	clear_bit(g_Padak, id)
	clear_bit(g_RedBull, id)
	clear_bit(g_CTBan, id)
	clear_bit(g_Molotov, id)
	clear_bit(g_VodniPistol, id)
	clear_bit(g_AntiLaser, id)
	clear_bit(g_Zmrazen, id)
	clear_bit(g_ZmenaTeamu, id)
	
	clear_bit(g_Prodlouzeni, id)
	if (g_Prodlouzeni && PocetHlasuProdlouzeni() >= pocetHracu() / 2)
		ProdlouzeniMapy()
		
	g_MojeInfo[id][Hrac_Rychlost] = _:0.0
	g_MojeInfo[id][Model_Typ] = _:MODEL_NORMAL
	arrayset(g_MojeInfo[id][Model_Name], 0, 45)
	g_MojeInfo[id][Ucet_Konto] = 0
	g_MojeInfo[id][GunMenu_LastP] = 0
	g_MojeInfo[id][GunMenu_LastS] = 0
	g_MojeInfo[id][GunMenu_Off] = false
	g_MojeInfo[id][GunMenu_Used] = false
	g_MojeInfo[id][Trail_Barva] = _:{0, 0, 0}
	g_MojeInfo[id][Trail_Origin] = _:{0, 0, 0}
	g_MojeInfo[id][Trail_Ticku] = 0	
	g_MojeInfo[id][MujNuz] = _:NOZE_RUCE
	g_MojeInfo[id][MujNuz_Inventar] = _:NOZE_RUCE
	g_MojeInfo[id][UkrytyNuzStav] = false
	g_MojeInfo[id][UkrytyNuz] = _:NOZE_RUCE
	g_MojeInfo[id][PosledniUkrytiNoze] = _:0.0
	g_MojeInfo[id][VezenTym] = _:VEZEN_TEAM_ZADNY
	arrayset(g_MojeInfo[id][AuthID], 0, 36) 
	g_MojeInfo[id][Special_Killu] = 0
	g_MojeInfo[id][Padak_Ent] = 0
	g_MojeInfo[id][VIP_Hat_Ent] = 0
	g_MojeInfo[id][VIP_Hat_Id] = 0
	g_MojeInfo[id][VIP_Chcal] = false
	g_MojeInfo[id][VIP_ChcaniOdpocet] = 0
	g_MojeInfo[id][OdhazovacichNozu] = 0
	g_MojeInfo[id][PosledniOdhozeni] = _:0.0
	g_MojeInfo[id][BodyStatusCas] = _:0.0
	g_MojeInfo[id][BodyStatusPozice] = 0
	g_MojeInfo[id][PocetMin] = 0
	g_MojeInfo[id][Dealer_Cooldown] = _:0.0
	g_MojeInfo[id][Ping_Sum] = 0
	g_MojeInfo[id][Ping_Samples] = 0
	g_MojeInfo[id][Honena_Chycen] = false
	g_MojeInfo[id][Honena_Odpocet] = 0
	g_MojeInfo[id][Honena_ItemBox_Odmena] = ODMENA_ZADNA
	g_MojeInfo[id][Honena_VodniGun_Effekt] = 0
	g_MojeInfo[id][Honena_Vezen_Zivotu] = 0
	g_MojeInfo[id][Honena_AimLock_Angles] = _:{0.0, 0.0, 0.0}
	
	g_MojeInfo[id][Ucet_Konto] = 0
	if (g_MojeInfo[id][Ucet_ID] > 0)
	{
		AktualizovatUcetCas(id)
		UpdateVezenskeStats(id)
		g_MojeInfo[id][Ucet_ID] = 0
		g_MojeInfo[id][Ucet_Cas] = _:0.0
	}
	
	if (g_MojeInfo[id][MCTBan_Vytvoren] > 0)
	{
		g_MojeInfo[id][MCTBan_Vytvoren] = 0
		g_MojeInfo[id][MCTBan_Vyprsi] = 0
		g_MojeInfo[id][MCTBan_Delka] = 0
		arrayset(g_MojeInfo[id][MCTBan_Duvod], 0, 39)
		arrayset(g_MojeInfo[id][MCTBan_Admin], 0, 32)
	}
	
	g_MojeInfo[id][Stats_Existuji] =  false
	g_MojeInfo[id][Stats_Kill_Simon] = 0
	g_MojeInfo[id][Stats_Kill_Bachar] = 0
	g_MojeInfo[id][Stats_Kill_Box] = 0
	g_MojeInfo[id][Stats_Smrti] = 0
	g_MojeInfo[id][Stats_Vzpour] = 0
	g_MojeInfo[id][Stats_VyhranychDuelu] = 0 
	
	arrayset(g_MojeInfo[id][Aktivace_Kod], 0, 5)
	arrayset(g_MojeInfo[id][Aktivace_Heslo], 0, 10)
	g_MojeInfo[id][Aktivace_Delka] = 0
	g_MojeInfo[id][Aktivace_Vyprsi] = 0
	g_MojeInfo[id][Aktivace_Pokusu] = 0
	
	g_MojeInfo[id][Hrac_Team] = _:CS_TEAM_UNASSIGNED
	AktualizovatPromenne(false)
}

public client_PostThink(id)
{
	if(!g_ServerInfo[Hry_SimonKroky] || g_ServerInfo[Simon_Id] != id || !(entity_get_int(id, EV_INT_flags) & FL_ONGROUND) || entity_get_int(id, EV_ENT_groundentity))
		return PLUGIN_CONTINUE
	
	static Float: flOrigin[3], Float: flOriginLast[3]
	entity_get_vector(id, EV_VEC_origin, flOrigin)
	if(get_distance_f(flOrigin, flOriginLast) < 32.0)
	{
		return PLUGIN_CONTINUE
	}
	
	vec_copy(flOrigin, flOriginLast)
	flOrigin[2] -= entity_get_int(id, EV_INT_bInDuck) ? 18.0 : 36.0
	
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY, {0,0,0}, 0)
	write_byte(TE_WORLDDECAL)
	write_coord_fl(flOrigin[0])
	write_coord_fl(flOrigin[1])
	write_coord_fl(flOrigin[2])
	write_byte(105)
	message_end()

	return PLUGIN_CONTINUE
}

public NacistMapy()
{
	g_aSeznamMap = ArrayCreate(34)
	new szBuffer[34], radek, delka
	
	while (read_file("mapcycle.txt", radek++, szBuffer, charsmax(szBuffer), delka))
		ArrayPushString(g_aSeznamMap, szBuffer)
	
}

public AdminMapMenu(id)
{
	new szBuffer[34], szNum[3]
	new menu = menu_create("\rSeznam map", "AdminMapMenu_Handler")
	for (new i = 0; i < ArraySize(g_aSeznamMap); i++)
	{
		num_to_str(i, szNum, 2)
		ArrayGetString(g_aSeznamMap, i, szBuffer, charsmax(szBuffer))
		menu_additem(menu, szBuffer, szNum)
	}
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpatky")
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_display(id, menu, 0)
}

public AdminMapMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], szMapa[34], access, callback, index;
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName,charsmax(szName), callback);
	index = str_to_num(data)
	
	ArrayGetString(g_aSeznamMap, index, szMapa, charsmax(szMapa))
	ChatColor(0, "Zmena mapy na !g%s", szMapa)
	server_cmd("changelevel %s", szMapa)
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public ProdlouzeniHlasovani(id)
{	
	if (get_bit(g_Prodlouzeni, id))
	{
		client_print(id, print_center, "Muzes hlasovat pouze jednou!")
		return
	}
	
	if (g_ServerInfo[Mapa_Prodlouzena])
	{
		client_print(id, print_center, "Mapa jiz byla prodlouzena!")
		return
	}
	
	set_bit(g_Prodlouzeni, id)
	new polovinaHracu = pocetHracu() / 2
	new pocetHlasu = PocetHlasuProdlouzeni()
	if (pocetHlasu >= polovinaHracu)
	{
		ProdlouzeniMapy()
	} else
	{
		client_print(id, print_center, "Vas hlas byl prijat! (%d/%d)", pocetHlasu, polovinaHracu)
		JailHud(0, 10, BarvyRGB[BARVA_MODRA_SVETLA], 0, 2.0, 0.0, 1.0, 1.0, "[%d/%d hlasu pro prodlouzeni mapy]", pocetHlasu, polovinaHracu)
	}
}

public ProdlouzeniMapy()
{
	g_Prodlouzeni = 0
	g_ServerInfo[Mapa_Prodlouzena] = true
	set_cvar_num("mp_timelimit", DELKA_JEDNE_MAPY + 10)
	JailHud(0, 10, BarvyRGB[BARVA_MODRA_SVETLA], 0, 2.0, 0.0, 1.0, 1.0, "[Mapa prodlouzena o 10 minut]")
}

public PocetHlasuProdlouzeni()
{
	new pocetBodu
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (get_bit(g_Prodlouzeni, i))
			pocetBodu += jeVIP(i) ? 2 : 1
	}
	
	return pocetBodu
}

public HlasovaniMapyMenu(id)
{
	new szNum[3], szMapa[34]
	new menu = menu_create("\rVyber mapu", "HlasovaniMapyMenu_Handler")
	for (new i = 0; i < ArraySize(g_aSeznamMap); i++)
	{
		num_to_str(i, szNum, 2)
		ArrayGetString(g_aSeznamMap, i, szMapa, charsmax(szMapa))
		menu_additem(menu, szMapa, szNum)
	}
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpatky")
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_display(id, menu, 0)
}

public HlasovaniMapyMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], szMapa[34], access, callback, index;
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName,charsmax(szName), callback);
	
	index = str_to_num(data)
	ArrayGetString(g_aSeznamMap, index, szMapa, 33)
	client_print(id, print_center, "Hlas pro mapu %s byl prijat :)", szMapa)
	
	new pridatBodu = jeVIP(id) ? 2 : 1
	TrieSetString(g_tMapVoteHraci, g_MojeInfo[id][AuthID], szMapa)
	
	if (TrieKeyExists(g_tMapVoteMapy, szMapa))
	{
		new bodu
		TrieGetCell(g_tMapVoteMapy, szMapa, bodu)
		TrieSetCell(g_tMapVoteMapy, szMapa, bodu + pridatBodu)
	} else
	{
		TrieSetCell(g_tMapVoteMapy, szMapa, pridatBodu)
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public SpustitHlasovaniMapy()
{
	g_tMapVoteHraci = TrieCreate()
	g_tMapVoteMapy = TrieCreate()
	set_task(10.0, "UkoncitHlasovaniMapy", TASK_MAPVOTE)
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (get_bit(g_HracPripojen, i))
			HlasovaniMapyMenu(i)
	}
}

public UkoncitHlasovaniMapy()
{
	new szMapa[34]
	new druhuMap = TrieGetSize(g_tMapVoteMapy)
	if (!druhuMap)
	{
		new pocetMap = ArraySize(g_aSeznamMap)
		ArrayGetString(g_aSeznamMap, random_num(0, pocetMap - 1), szMapa, 33)
		set_cvar_string("amx_nextmap", szMapa)
	} else
	{
		new winHlasu, winIndex, dataHlasu
		for (new i = 0; i < ArraySize(g_aSeznamMap); i++)
		{
			ArrayGetString(g_aSeznamMap, i, szMapa, 33)
			if (!TrieKeyExists(g_tMapVoteMapy, szMapa))
				continue
				
			TrieGetCell(g_tMapVoteMapy, szMapa, dataHlasu)
			if (dataHlasu > winHlasu)
			{
				winHlasu = dataHlasu
				winIndex = i
			}
		}
		
		ArrayGetString(g_aSeznamMap, winIndex, szMapa, 33)
		set_cvar_string("amx_nextmap", szMapa)
	}
	
	TrieDestroy(g_tMapVoteHraci)
	TrieDestroy(g_tMapVoteMapy)
	JailHud(0, 10, BarvyRGB[BARVA_MODRA_SVETLA], 0, 2.0, 0.0, 1.0, 1.0, "[%s je vitezem hlasovani]", szMapa)
}

#if defined DEBUG
public cmdEntityButton(id)
{
	static body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Nebyla nalezena zadna entita!")
		return PLUGIN_HANDLED
	}
	
	static className[33]
	pev(ent, pev_classname, className, 32)
	client_print(id, print_chat, "%s id: %d", className, ent)
	return PLUGIN_HANDLED
}

public cmdTest(id)
{
	static ent, body
	get_user_aiming(id, ent, body)
	if (pev_valid(ent))
	{
		/*
		targetData = pev(ent, pev_target)
		client_print(0, print_chat, "target: %d", targetData)
		
		new iEnt = -1, findTarget, pocet
		while ((iEnt = engfunc(EngFunc_FindEntityByString, iEnt, "classname", "func_button")) > 0)
		{
			findTarget = pev(iEnt, pev_target)
			if (targetData == findTarget)
				pocet++
		}
		
		client_print(0, print_chat, "Nalezeno: %d", pocet)
		*/
		new szTarget[32], entId = -1, pocet;
		entity_get_string(ent, EV_SZ_target, szTarget, charsmax(szTarget));
		while ((entId = fm_find_ent_by_target(entId, szTarget)) > 0)
		{
			pocet++
		}
		
		client_print(id, print_chat, "pocet: %d | target: %s", pocet, szTarget)
	}
}

public GetEntityIdByTarget(const name[], const target[])
{	
	return engfunc(EngFunc_FindEntityByString, -1, "target", target)
}

public GetEntityIdByModel(const class[], const model[])
{
	return fm_find_ent_by_model(-1, class, model)
}
#endif

public NacistSpecialniSkiny()
{
	new baseDir[120], soubor[223]
	get_localinfo("amxx_basedir", baseDir, 119)
	format(soubor, 222, "%s/configs/jail/", baseDir)
	if(!dir_exists(soubor))
		mkdir(soubor)
		
	add(soubor, 222, FILE_STORAGE_SKINY)
	add(soubor, 222, ".mlabs")
	
	new fileFlags = file_size(soubor, 1)
	// Neexistuje
	if(fileFlags == -1)
	{
		write_file(soubor, "// ^"SteamID^" - ^"Soubor^" - ^"TModel (0 / 1)^"", 0)
		return PLUGIN_HANDLED
	}
	
	// Nic tam neni
	if (fileFlags == 0)
	{
		#if defined DEBUG
		server_print("[Jail] Nejsou definovane zadne specialni skiny!")
		#endif
		return PLUGIN_HANDLED
	}
	
	g_tSpecialniSkiny = TrieCreate()
	new radek, delka, buffer[256], modelData[3][64], modelFiles[2][40], tmodel
	radek = 0, delka = 0, tmodel = 0
		
	while (read_file(soubor, radek++, buffer, sizeof buffer - 1, delka))
	{
		if(buffer[0] == ';' || buffer[0] == '/')
			continue
		
		replace_all(buffer, 255, "^"", "")
		parse(buffer, modelData, 35, modelData, 45, modelData, 2)
		
		format(modelFiles[0], 39, "models/player/%s/%s.mdl", modelData[1], modelData[1])
		if(!file_exists(modelFiles[0]))
		{
			server_print("[Jail] Nebyl nalezen %s model!", modelData[1])
			continue
		}
		
		tmodel = str_to_num(modelData[2])
		if(tmodel)
		{
			format(modelFiles[1], 39, "models/player/%s/%sT.mdl", modelData[1], modelData[1])
			if(!file_exists(modelFiles[1]))
			{
				server_print("[Jail] Neexistuje T model pro %s !", modelData[1])
				continue
			}
		}
		
		engfunc(EngFunc_PrecacheModel, modelFiles[0])
		if (tmodel)
			engfunc(EngFunc_PrecacheModel, modelFiles[1])
			
		TrieSetString(g_tSpecialniSkiny, modelData[0], modelData[1])
	}
	
	return PLUGIN_HANDLED
}

public Knife_PrimaryAttack_Post(knife) 
{     
	static id; id = get_pdata_cbase(knife, OFFSET_PLAYER, EXTRAOFFSET_WEAPONS) 
	if (!zivyHrac(id))
		return HAM_IGNORED
	
	g_MojeInfo[id][PrimarniUtokKnife] = true
	if(g_MojeInfo[id][MujNuz] == Noze:NOZE_MOTOROVKA) 
	{ 
		set_pdata_float(knife, OFFSET_NEXT_PRIMARY_ATTACK, 0.6, EXTRAOFFSET_WEAPONS) 
		set_pdata_float(knife, OFFSET_NEXT_SECONDARY_ATTACK, 0.6, EXTRAOFFSET_WEAPONS) 
		set_pdata_float(knife, OFFSET_WEAPONIDLE, 0.6, EXTRAOFFSET_WEAPONS) 
         
		static Float:flPunchAngle[3] 
		flPunchAngle[0] = -5.6;
		entity_set_vector(id, EV_VEC_punchangle, flPunchAngle)     
    } 
     
	return HAM_IGNORED 
}

public Knife_SecondaryAttack_Post(knife) 
{     
	static id; id = get_pdata_cbase(knife, OFFSET_PLAYER, EXTRAOFFSET_WEAPONS) 
	if (!zivyHrac(id))
		return HAM_IGNORED	
	
	g_MojeInfo[id][PrimarniUtokKnife] = false
	if(g_MojeInfo[id][MujNuz] == Noze:NOZE_MOTOROVKA) 
	{ 
		set_pdata_float(knife, OFFSET_NEXT_PRIMARY_ATTACK, 1.6, EXTRAOFFSET_WEAPONS) 
		set_pdata_float(knife, OFFSET_NEXT_SECONDARY_ATTACK, 1.6, EXTRAOFFSET_WEAPONS) 
		set_pdata_float(knife, OFFSET_WEAPONIDLE, 1.6, EXTRAOFFSET_WEAPONS) 
         
		static Float:flPunchAngle[3] 
		flPunchAngle[0] = -8.0
		entity_set_vector(id, EV_VEC_punchangle, flPunchAngle)     
	} 
	
	return HAM_IGNORED 
}  
/*******************************************
			< VodniPistol >
********************************************/
public VodniPistol_AttackEfekt(id, Float:fVec1[3], Float:fVec2[3])
{
	static iVec1[3], Float:origin[3], Float:vSrc[3], Float:angles[3], Float:v_forward[3], Float:v_right[3], Float:v_up[3], Float:gun_position[3], Float:player_origin[3], Float:player_view_offset[3]
	FVecIVec(fVec1, iVec1)
	pev(id, pev_v_angle, angles)
	engfunc(EngFunc_MakeVectors, angles)
	global_get(glb_v_forward, v_forward)
	global_get(glb_v_right, v_right)
	global_get(glb_v_up, v_up)

	pev(id, pev_origin, player_origin)
	pev(id, pev_view_ofs, player_view_offset)
	xs_vec_add(player_origin, player_view_offset, gun_position)

	xs_vec_mul_scalar(v_forward, 24.0, v_forward)
	xs_vec_mul_scalar(v_right, 3.0, v_right)

	if ((pev(id, pev_flags) & FL_DUCKING) == FL_DUCKING)	
		xs_vec_mul_scalar(v_up, 6.0, v_up)
	else
		xs_vec_mul_scalar(v_up, -2.0, v_up)

	xs_vec_add(gun_position, v_forward, origin)
	xs_vec_add(origin, v_right, origin)
	xs_vec_add(origin, v_up, origin)

	vSrc[0] = origin[0]
	vSrc[1] = origin[1]
	vSrc[2] = origin[2]

	static Float:dist, CountDrops
	dist = get_distance_f(vSrc, fVec2)
	CountDrops = floatround(dist / 50.0)
	
	if (CountDrops > 20)
		CountDrops = 20
	
	if (CountDrops < 2)
		CountDrops = 2

	message_begin(MSG_PAS, SVC_TEMPENTITY, iVec1)
	write_byte(TE_SPRITETRAIL)
	write_coord_fl(vSrc[0])
	write_coord_fl(vSrc[1])
	write_coord_fl(vSrc[2])
	write_coord_fl(fVec2[0])
	write_coord_fl(fVec2[1])
	write_coord_fl(fVec2[2])
	write_short(g_sprVodniGun)
	write_byte(CountDrops)
	write_byte(0)
	write_byte(1)
	write_byte(60)
	write_byte(10)
	message_end()

	message_begin(MSG_PAS, SVC_TEMPENTITY, iVec1)
	write_byte(TE_BEAMPOINTS)
	write_coord_fl(fVec2[0])
	write_coord_fl(fVec2[1])
	write_coord_fl(fVec2[2])
	write_coord_fl(vSrc[0])
	write_coord_fl(vSrc[1])
	write_coord_fl(vSrc[2])
	write_short(g_sprVodniGun)
	write_byte(6)
	write_byte(200) 
	write_byte(1)
	write_byte(100)
	write_byte(0)
	write_byte(64); write_byte(64); write_byte(192);
	write_byte(192)
	write_byte(250) 
	message_end()
}
/*******************************************
				< Eventy >
********************************************/
public FlashlightEvent(id)
{
	if (zivyHrac(id) && g_MojeInfo[id][Hrac_Team] == CS_TEAM_T && g_MojeInfo[id][MujNuz] != Noze:NOZE_RUKAVICE && (g_MojeInfo[id][PosledniUkrytiNoze] + 1.5) < get_gametime()) 
	{
		g_MojeInfo[id][PosledniUkrytiNoze] = _:get_gametime()
		if (g_MojeInfo[id][UkrytyNuzStav])
		{
			g_MojeInfo[id][MujNuz] = _:g_MojeInfo[id][UkrytyNuz]
			g_MojeInfo[id][UkrytyNuz] = _:NOZE_RUCE;
			g_MojeInfo[id][UkrytyNuzStav] = false
			AktualizovatKnifeModel(id)
			PlayKnifeDeploySound(id, g_MojeInfo[id][MujNuz])
		}
		else
		{
			if (g_MojeInfo[id][MujNuz] != Noze:NOZE_RUCE)
			{
				g_MojeInfo[id][UkrytyNuz] = _:g_MojeInfo[id][MujNuz]
				g_MojeInfo[id][MujNuz] = _:NOZE_RUCE
				g_MojeInfo[id][UkrytyNuzStav] = true
				AktualizovatKnifeModel(id)
			}
		}
	}
	
	return PLUGIN_HANDLED
}

public PlayKnifeDeploySound(id, Noze: nuz)
{
	switch (nuz)
	{
		case NOZE_MACETA: emit_sound(id, CHAN_WEAPON, MacetaSound[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
		case NOZE_MOTOROVKA: emit_sound(id, CHAN_WEAPON, MotorovkaSound[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
		case NOZE_PACIDLO: emit_sound(id, CHAN_WEAPON, PacidloSound[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
		case NOZE_OBUSEK: emit_sound(id, CHAN_WEAPON, ObusekSound[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
		case NOZE_KATANA: emit_sound(id, CHAN_WEAPON, KatanaSound[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
	}
}

public precache_spawn(ent)
{
	if(pev_valid(ent))
	{
		new szClass[33]
		entity_get_string(ent, EV_SZ_classname, szClass, sizeof(szClass))
		for(new i = 0; i < sizeof(SmazatEntity); i++)
			if(equal(szClass, SmazatEntity[i]))
				remove_entity(ent)
	}
}

public fw_NastaveniMikrofonu(receiver, sender, bool:listen)
{
	if(receiver == sender)
	{
		return FMRES_IGNORED
	}
	
	if (jeAdmin(sender)) 
	{
		engfunc(EngFunc_SetClientListening, receiver, sender, true)
		return FMRES_SUPERCEDE
	}
	
	if(g_MojeInfo[sender][Hrac_Team] == CS_TEAM_CT && zivyHrac(sender))
	{
		engfunc(EngFunc_SetClientListening, receiver, sender, true)
		return FMRES_SUPERCEDE
	}
	
	if(g_MojeInfo[sender][Hrac_Team] == CS_TEAM_T && get_bit(g_Mikrofon, sender))
	{
		engfunc(EngFunc_SetClientListening, receiver, sender, true)
		return FMRES_SUPERCEDE
	}
	
	engfunc(EngFunc_SetClientListening, receiver, sender, false)
	return FMRES_SUPERCEDE
}

public BlockModelPrecache(const szModel[])
{
	static mapa[36]
	get_mapname(mapa, 35)
	if (containi(mapa, "jb_oasis") == -1 && containi(szModel, "shield") != -1)
	{
		forward_return(FMV_CELL, 0)
		return FMRES_SUPERCEDE
	}
	
	if (containi(szModel, "thingpack") != -1
	|| containi(szModel, "c4") != -1
	|| containi(szModel, "p228") != -1
	|| containi(szModel, "backpack") != -1
	|| containi(szModel, "ump45") != -1)
	{
		forward_return(FMV_CELL, 0)
		return FMRES_SUPERCEDE
	}
	
	return FMRES_IGNORED
}

public BlockSoundPrecache(const szSound[])
{
	if(containi(szSound, "c4") != -1
	|| containi(szSound, "flashlight") != -1
	|| containi(szSound, "npc") != -1
	|| containi(szSound, "nvg_") != -1
	|| containi(szSound, "equip_nvg") != -1
	|| containi(szSound, "p228") != -1
	|| containi(szSound, "ump45") != -1)
	{
		forward_return(FMV_CELL, 0)
		return FMRES_SUPERCEDE
	}
		
	return FMRES_IGNORED
}

public fw_SetModel(entity, model[])
{
	if(!g_Molotov || !pev_valid(entity))
		return FMRES_IGNORED
		
	if(equal(model[7], "w_fl", 4)) 
	{
		static id; id = pev(entity, pev_owner)
		if(get_bit(g_Molotov, id))
		{
			static Float:dmgtime
			pev(entity, pev_dmgtime, dmgtime)
			if(!dmgtime) 
				return FMRES_IGNORED
				
			set_pev(entity, pev_flTimeStepSound, KEY_GUN_MOLOTOV)
			SetSubModel(entity, ENTITA_MOLOTOV)
			return FMRES_SUPERCEDE
		}
	}
	
	return FMRES_IGNORED
}

public fw_cmdStart_Post(id, uc, irandom)
{
	if(!zivyHrac(id))
		return FMRES_IGNORED
	
	if(g_ServerInfo[Duel_Bezi] && g_ServerInfo[Duel_Typ] != DUEL_AIM && !(g_ServerInfo[Duel_Zbran] == 5 || g_ServerInfo[Duel_Zbran] == 0))
	{
		if(id == g_ServerInfo[Duel_idT] || id == g_ServerInfo[Duel_idCT])
		{
			static naboju
			naboju = fm_cs_get_user_bpammo(id, ZbraneCSW[DuelZbrane[g_ServerInfo[Duel_Zbran] - 1]])
			if(!naboju) 
				fm_cs_set_user_bpammo(id, ZbraneCSW[DuelZbrane[g_ServerInfo[Duel_Zbran] - 1]], 1)
		}
		
		return FMRES_IGNORED
	}
	
	static buttons;
	buttons = get_uc(uc, UC_Buttons)
	if(g_ServerInfo[Special_Typ] == SPECIAL_ONESHOT)
	{
		if(buttons & IN_ATTACK)
		{
			set_uc(uc, UC_Buttons, buttons & ~IN_ATTACK)
			
			static Float:flCurTime
			flCurTime = halflife_time()
			if(flCurTime - g_MojeInfo[id][CustomGun_LastAttack] < 3.5)
				return FMRES_IGNORED
		
			static coltEnt
			coltEnt = get_pdata_cbase(id, OFFSET_ACTIVEITEM, 5)
			fm_cs_set_weapon_ammo(coltEnt, 0)
			ColtFire(id, coltEnt)
			g_MojeInfo[id][CustomGun_LastAttack] = _:flCurTime
			
			return FMRES_IGNORED
		}
		
		if(buttons & IN_RELOAD)
			set_uc(uc, UC_Buttons, buttons & ~IN_RELOAD)
	}
	
	if(PadakPovolen(id))
	{
		if(buttons & IN_USE)
		{
			if(!(get_entity_flags(id) & FL_ONGROUND))
			{
				static Float:flVelocity[3]
				pev(id, pev_velocity, flVelocity)
				if(flVelocity[2] < 0)
				{
					if(g_MojeInfo[id][Padak_Ent] == 0)
					{
						g_MojeInfo[id][Padak_Ent] = create_entity("info_target")
						engfunc(EngFunc_SetModel, g_MojeInfo[id][Padak_Ent], ModelyEntit[1])
						set_pev(g_MojeInfo[id][Padak_Ent], pev_body, _:ENTITA_PADAK + 1)
						set_pev(g_MojeInfo[id][Padak_Ent], pev_movetype, MOVETYPE_FOLLOW)
						set_pev(g_MojeInfo[id][Padak_Ent], pev_aiment, id)
						Util_PlayAnimation(g_MojeInfo[id][Padak_Ent], _:ENTITA_PADAK)
						emit_sound(0, CHAN_AUTO, RuzneZvuky[4], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
					} else
					{
						flVelocity[2] = (flVelocity[2] + 40.0 < -100) ? flVelocity[2] + 40.0 : -100.0
						set_pev(id, pev_velocity, flVelocity)
					}
				} else
				{
					if(g_MojeInfo[id][Padak_Ent] > 0)
					{
						engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][Padak_Ent])
						g_MojeInfo[id][Padak_Ent] = 0
					}
				}
			} else
			{
				if(g_MojeInfo[id][Padak_Ent] > 0)
				{
					engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][Padak_Ent])
					g_MojeInfo[id][Padak_Ent] = 0
				}
			}
		} else if (pev(id, pev_oldbuttons) & IN_USE)
		{
			if(g_MojeInfo[id][Padak_Ent] > 0)
			{
				engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][Padak_Ent])
				g_MojeInfo[id][Padak_Ent] = 0
			}
		}
	}
	
	if (get_bit(g_VodniPistol, id) && get_user_weapon(id) == CSW_MP5NAVY)
	{
		if (buttons & IN_RELOAD)
			set_uc(uc, UC_Buttons, buttons & ~IN_RELOAD)
			
		if (buttons & IN_ATTACK)
		{
			set_uc(uc, UC_Buttons, buttons & ~IN_ATTACK)
			
			static Float:flCurTime
			flCurTime = halflife_time()
			if(flCurTime - g_MojeInfo[id][CustomGun_LastAttack] < 0.1)
				return FMRES_IGNORED
			
			static gun; 
			gun = fm_cs_get_weapon(id)
			if (!pev_valid(gun))
				return FMRES_IGNORED
				
			static clip; 
			clip = fm_cs_get_weapon_ammo(gun)
			if (clip < 1)
			{
				OdebratZbrane(id, 0)
				return FMRES_IGNORED
			}
				
			fm_cs_set_weapon_ammo(gun, clip - 1)
			emit_sound(id, CHAN_WEAPON, RuzneZvuky[10], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
			WeaponAnim(id, random_num(3, 4))
				
			static Float:plrViewAngles[3], Float:VecEnd[3], Float:VecDir[3], Float:PlrOrigin[3], Float:VecSrc[3], Float:VecDst[3]
			pev(id, pev_v_angle, plrViewAngles)
			pev(id, pev_origin, PlrOrigin)
			pev(id, pev_view_ofs, VecSrc)
			xs_vec_add(VecSrc, PlrOrigin, VecSrc)
				
			angle_vector(plrViewAngles, ANGLEVECTOR_FORWARD, VecDir);
			xs_vec_mul_scalar(VecDir, 8192.0, VecDst);
			xs_vec_add(VecDst, VecSrc, VecDst	);
				
			new hTrace = create_tr2()
			engfunc(EngFunc_TraceLine, VecSrc, VecDst, 0, id, hTrace)
				
			new hit = get_tr2(hTrace, TR_pHit)
			if (hit > 0 && zivyHrac(hit) && !(g_MojeInfo[hit][Honena_Chycen] || get_bit(g_Zmrazen, hit)))
			{
				if (g_MojeInfo[id][Hrac_Team] == g_MojeInfo[hit][Hrac_Team])
				{
					if (g_MojeInfo[hit][Honena_VodniGun_Effekt] < 20)
					{
						g_MojeInfo[hit][Honena_VodniGun_Effekt]++
					}
				} else
				{
					if (g_MojeInfo[hit][Honena_VodniGun_Effekt] > -20)
					{
						g_MojeInfo[hit][Honena_VodniGun_Effekt]--
					}
				}
					
				if (g_MojeInfo[hit][Honena_VodniGun_Effekt] > 0)
				{
					NastavitRychlost(hit, HONENA_RYCHLOST + float(g_MojeInfo[hit][Honena_VodniGun_Effekt] * 8))
					PlayerGlowAlpha(hit, {255, 42, 212}, g_MojeInfo[hit][Honena_VodniGun_Effekt] * 2)
				} else if (g_MojeInfo[hit][Honena_VodniGun_Effekt] < 0)
				{
					if (g_MojeInfo[hit][Honena_VodniGun_Effekt] <= -20)
					{
						g_MojeInfo[hit][Honena_VodniGun_Effekt] = 0
						ZmrazitHrace(hit)
					} else
					{
						static effectAbs
						effectAbs = abs(g_MojeInfo[hit][Honena_VodniGun_Effekt])
						NastavitRychlost(hit, HONENA_RYCHLOST - float(effectAbs * 20))
						PlayerGlowAlpha(hit, {42, 170, 255}, effectAbs * 2)
					}
				}
			}
				
			get_tr2(hTrace, TR_vecEndPos, VecEnd);
			VodniPistol_AttackEfekt(id, VecSrc, VecEnd)
			g_MojeInfo[id][CustomGun_LastAttack] = _:flCurTime
		}
	}
	
	return FMRES_IGNORED
}

public fw_SetClientKeyValue( id, const infobuffer[], const key[] )
{
	if(equal( key, "model" ))
		return FMRES_SUPERCEDE;
    
	return FMRES_IGNORED;
}

public fw_UpdateClientData_Post(id, sendweapons, handle)
{
	if(!get_bit(g_ZivyHrac, id))
		return FMRES_IGNORED
		
	if(g_ServerInfo[Special_Typ] == SPECIAL_ONESHOT) 
	{
		set_cd(handle, CD_flNextAttack, halflife_time() + 0.001)
	} else if (get_bit(g_VodniPistol, id) && get_user_weapon(id) == CSW_MP5NAVY)
	{
		set_cd(handle, CD_flNextAttack, halflife_time() + 0.001)
	}
	
	return FMRES_IGNORED
}

public fw_EmitSound(id, channel, const sample[], Float:volume, Float:attn, flags, pitch)
{
	if(!get_bit(g_HracPripojen, id))
		return FMRES_IGNORED

	// knife
	if(sample[8] == 'k' && sample[9] == 'n' && sample[10] == 'i')
	{
		// slash
		if(sample[14] == 's' && sample[15] == 'l' && sample[16] == 'a')
		{
			switch(g_MojeInfo[id][MujNuz])
			{
				case NOZE_RUCE:
				{
					engfunc(EngFunc_EmitSound, id, channel, RuceSound[1], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_SROUBOVAK:
				{
					engfunc(EngFunc_EmitSound, id, channel, SroubovakSound[2], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_ZABIKUCH:
				{
					engfunc(EngFunc_EmitSound, id, channel, NozikSound[2], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_PACIDLO:
				{
					engfunc(EngFunc_EmitSound, id, channel, PacidloSound[4], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_MACETA:
				{
					engfunc(EngFunc_EmitSound, id, channel, MacetaSound[4], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_MOTOROVKA:
				{
					engfunc(EngFunc_EmitSound, id, channel, MotorovkaSound[3], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_OBUSEK:
				{
					engfunc(EngFunc_EmitSound, id, channel, ObusekSound[4], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_KATANA:
				{
					engfunc(EngFunc_EmitSound, id, channel, KatanaSound[2], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
			}
		}
		// deploy
		if(sample[12] == 'e' && sample[14] == 'd' && sample[15] == 'e') 
		{
			switch(g_MojeInfo[id][MujNuz])
			{
				case NOZE_MACETA:
				{
					engfunc(EngFunc_EmitSound, id, channel, MacetaSound[0], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_MOTOROVKA:
				{
					engfunc(EngFunc_EmitSound, id, channel, MotorovkaSound[0], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_PACIDLO:
				{
					engfunc(EngFunc_EmitSound, id, channel, ObusekSound[0], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_OBUSEK:
				{
					engfunc(EngFunc_EmitSound, id, channel, PacidloSound[0], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_KATANA:
				{
					engfunc(EngFunc_EmitSound, id, channel, KatanaSound[0], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
			}
		}
		// hit
		if(sample[14] == 'h' && sample[15] == 'i' && sample[16] == 't')
		{
			// wall
			if(sample[17] == 'w') 
			{
				switch(g_MojeInfo[id][MujNuz])
				{
					case NOZE_RUCE:
					{
						engfunc(EngFunc_EmitSound, id, channel, RuceSound[2], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_SROUBOVAK:
					{
						engfunc(EngFunc_EmitSound, id, channel, SroubovakSound[1], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE
					}
					case NOZE_ZABIKUCH:
					{
						engfunc(EngFunc_EmitSound, id, channel, NozikSound[1], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE
					}
					case NOZE_MACETA:
					{
						engfunc(EngFunc_EmitSound, id, channel, MacetaSound[3], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_PACIDLO:
					{
						engfunc(EngFunc_EmitSound, id, channel, PacidloSound[3], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_MOTOROVKA:
					{
						engfunc(EngFunc_EmitSound, id, channel, MotorovkaSound[2], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_OBUSEK:
					{
						engfunc(EngFunc_EmitSound, id, channel, ObusekSound[3], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_RUKAVICE:
					{
						engfunc(EngFunc_EmitSound, id, channel, BoxZvuky[1], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_KATANA:
					{
						engfunc(EngFunc_EmitSound, id, channel, KatanaSound[1], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
				}
			}
			else
			{
				// player hit
				switch(g_MojeInfo[id][MujNuz])
				{
					case NOZE_RUCE:
					{
						engfunc(EngFunc_EmitSound, id, channel, RuceSound[0], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_SROUBOVAK:
					{
						engfunc(EngFunc_EmitSound, id, channel, SroubovakSound[0], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_ZABIKUCH:
					{
						engfunc(EngFunc_EmitSound, id, channel, NozikSound[0], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_PACIDLO:
					{
						engfunc(EngFunc_EmitSound, id, channel, PacidloSound[random_num(1, 2)], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_MACETA:
					{
						engfunc(EngFunc_EmitSound, id, channel, MacetaSound[random_num(1, 2)], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_MOTOROVKA:
					{
						engfunc(EngFunc_EmitSound, id, channel, MotorovkaSound[1], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_OBUSEK:
					{
						engfunc(EngFunc_EmitSound, id, channel, ObusekSound[random_num(1, 2)], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_RUKAVICE: 
					{
						engfunc(EngFunc_EmitSound, id, channel, BoxZvuky[0], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
					case NOZE_KATANA:
					{
						engfunc(EngFunc_EmitSound, id, channel, KatanaSound[random_num(4, 5)], volume, attn, flags, pitch)
						return FMRES_SUPERCEDE;
					}
				}
			}
		}
		// stab
		if(sample[14] == 's' && sample[15] == 't' && sample[16] == 'a')
		{
			switch(g_MojeInfo[id][MujNuz])
			{
				case NOZE_RUCE:
				{
					engfunc(EngFunc_EmitSound, id, channel, RuceSound[3], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_PACIDLO:
				{
					engfunc(EngFunc_EmitSound, id, channel, PacidloSound[5], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_MACETA:
				{
					engfunc(EngFunc_EmitSound, id, channel, MacetaSound[4], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_MOTOROVKA:
				{
					engfunc(EngFunc_EmitSound, id, channel, MotorovkaSound[4], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_OBUSEK:
				{
					engfunc(EngFunc_EmitSound, id, channel, ObusekSound[5], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_RUKAVICE:
				{
					engfunc(EngFunc_EmitSound, id, channel, BoxZvuky[0], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
				case NOZE_KATANA:
				{
					engfunc(EngFunc_EmitSound, id, channel, KatanaSound[3], volume, attn, flags, pitch)
					return FMRES_SUPERCEDE;
				}
			}
		}
	}
	
	return FMRES_IGNORED
}

public ColtFire(id, Gun)
{
	entity_set_vector(id, EV_VEC_punchangle, Float:{ -2.0, 0.0, 0.0 })
	emit_sound(id, CHAN_WEAPON, OneShotColtSound[2], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
	WeaponAnim(id, 3)
	
	static Cil, Telo, Data[1], startOrigin[3], endOrigin[3], Float: myOrigin[3]
	get_user_aiming(id, Cil, Telo)
	get_user_origin(id, endOrigin, 3)
	get_user_origin(id, startOrigin, 1)
	pev(id, pev_origin, myOrigin)
	
	if(Cil)
	{
		if(zivyHrac(Cil) && g_MojeInfo[id][Hrac_Team] != g_MojeInfo[Cil][Hrac_Team])
		{
			static Float: flOrigin[3]
			pev(Cil, pev_origin, flOrigin)
			CakanecKrve(flOrigin, 3)
			if(Telo == HIT_HEAD)
			{
				make_deathmsg(id, Cil, 1, "Colt")
				g_ServerInfo[BlockDeathMsg] = true
			} else
			{
				g_ServerInfo[BlockDeathMsg] = false
			}
			
			
			ExecuteHamB(Ham_TakeDamage, Cil, Gun, id, 500.0, DMG_BULLET); 
		}
	} else
	{
		message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
		write_byte(TE_GUNSHOTDECAL)
		write_coord(endOrigin[0])
		write_coord(endOrigin[1])
		write_coord(endOrigin[2])
		write_short(id)
		write_byte(random_num(41,45))
		message_end()  
	}

	message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
	write_byte(TE_TRACER)
	write_coord(startOrigin[0]);
	write_coord(startOrigin[1]);
	write_coord(startOrigin[2]);
	write_coord(endOrigin[0]);
	write_coord(endOrigin[1]);
	write_coord(endOrigin[2]);
	message_end();

	Data[0] = Gun
	set_task(1.0, "PrebitColt", id + TASK_COLT, Data, 1)
}

public PrebitColt(Data[], TaskID)
{
	static id, coltEnt, iData[1]
	coltEnt = Data[0]
	id = TaskID - TASK_COLT
	WeaponAnim(id, 4)
	
	if(pev_valid(coltEnt)) 
		fm_cs_set_weapon_ammo(Data[0], 1)
	
	iData[0] = 1
	set_task(0.3, "PrebitColtSound", id + TASK_COLT, iData, 1)
	
	iData[0] = 0
	set_task(1.05, "PrebitColtSound", id + TASK_COLT, iData, 1)
}

public PrebitColtSound(Data[], TaskID)
{
	static id; id = TaskID - TASK_COLT
	emit_sound(id, CHAN_WEAPON, OneShotColtSound[Data[0]], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
}

public fw_EvCurWeapon(id)
{
	if(!get_bit(g_ZivyHrac, id))
		return PLUGIN_CONTINUE
	  
	static AktualniZbran
	AktualniZbran = read_data(2);
	switch(AktualniZbran)
	{
		case CSW_MP5NAVY:
		{
			if(!get_bit(g_VodniPistol, id))
				return PLUGIN_CONTINUE
			
			set_pev(id, pev_viewmodel2, VodniPistolModely[0])
			set_pev(id, pev_weaponmodel2, VodniPistolModely[1])
			new Ent = find_ent_by_owner(-1,"weapon_mp5navy",id)
			if(Ent)
			{
				static Float:Delay
				Delay = get_pdata_float( Ent, 46, 4) * 1.01
				if (Delay > 0.0) set_pdata_float(Ent, 46, Delay, 4)
			}
		}
		case CSW_KNIFE:
		{
			set_pev(id, pev_viewmodel2, vModelyNozu[g_MojeInfo[id][MujNuz]])
			set_pev(id, pev_weaponmodel2, pModelyNozu[g_MojeInfo[id][MujNuz]])		
		}
		case CSW_DEAGLE:
		{
			if(g_ServerInfo[Special_Typ] == SPECIAL_ONESHOT)
			{
				set_pev(id, pev_viewmodel2, OneShotColtModel[0])
				set_pev(id, pev_weaponmodel2, OneShotColtModel[1])
			}
		}
		case CSW_FLASHBANG:
		{
			if(get_bit(g_Molotov, id))
			{
				set_pev(id, pev_viewmodel2, MolotovModely[0])
				set_pev(id, pev_weaponmodel2, MolotovModely[1])
			}
		}
	}
	
	return PLUGIN_CONTINUE
}

public PlayerStatus(id)
{		
	static typ; typ = read_data(1)	
	switch (typ)
	{
		case 1: ClearSyncHud(id, g_HudSync[5][_hudPos])
		case 2:
		{
			static hrac, szInfo[156], szData[33]
			hrac = read_data(2)
			if (g_ServerInfo[Special_Typ] == SPECIAL_SCHOVKA && get_bit(g_Schovan, hrac))
				return PLUGIN_HANDLED
				
			get_user_name(hrac, szData, 32)
			format(szInfo, 155, "[%s]^n[", szData)
			
			if(g_MojeInfo[id][Hrac_Team] == g_MojeInfo[hrac][Hrac_Team])
			{
				static iHP; iHP = pev(hrac, pev_health)
				format(szData, 32, "%d HP | ", iHP)
				add(szInfo, 155, szData)
			}
			
			if (g_MojeInfo[hrac][Hrac_Team] == CS_TEAM_T)
			{
				add(szInfo, 155, "Vezen")
			} else if (g_ServerInfo[Simon_Id] == hrac)
			{
				add(szInfo, 155, "Simon")
			} else
			{
				add(szInfo, 155, "Bachar")
			}
			
			format(szData, 32, " | %d $]", g_MojeInfo[hrac][Hrac_Team])
			add(szInfo, 155, szData)
			
			JailHud(id, 5, {5, 5, 5}, 0, 5.0, 0.0, 0.0, 0.0, szInfo)
		}
	}
	
	return PLUGIN_HANDLED
}

public DropFunkce(id)
{
	if(g_ServerInfo[Special_Typ] == SPECIAL_ONESHOT || g_ServerInfo[Special_Typ] == SPECIAL_ARENA || g_ServerInfo[Special_Typ] == SPECIAL_HONENA)
		return PLUGIN_HANDLED
	
	if (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)
		return PLUGIN_HANDLED
	
	switch (get_user_weapon(id))
	{
		case CSW_KNIFE:
		{
			if (DropableKnife(id))
			{
				DropnoutEntitu(id, g_MojeInfo[id][MujNuz], true)
				g_MojeInfo[id][MujNuz] = _:NOZE_RUCE
				AktualizovatKnifeModel(id)		
			}
		}
		case CSW_FLASHBANG:
		{
			if (get_bit(g_Molotov, id))
			{
				DropnoutMolotov(id)
				return PLUGIN_HANDLED
			}
		}
	}
	
	return PLUGIN_CONTINUE
}

public bool: DropableKnife(id)
{
	return (g_MojeInfo[id][MujNuz] == Noze:NOZE_RUCE || g_MojeInfo[id][MujNuz] == Noze:NOZE_RUKAVICE) ? false : true
}

// TODO Premistit nekde dolu mezi utility
public Entity: KnifeToEnt(Noze: nuz)
{
	switch(nuz)
	{
		case NOZE_SROUBOVAK: return ENTITA_SROUBOVAK
		case NOZE_ZABIKUCH: return ENTITA_ZABIKUCH	
		case NOZE_PACIDLO: return ENTITA_PACIDLO
		case NOZE_MACETA: return ENTITA_MACETA
		case NOZE_MOTOROVKA: return ENTITA_MOTOROVKA
		case NOZE_OBUSEK: return ENTITA_OBUSEK
		case NOZE_KATANA: return ENTITA_KATANA
	}
	
	return ENTITA_SROUBOVAK
}

public fw_GameDesc()
{
	static buffer[120]
	if (!buffer[0])
		format(buffer, 119, "%s [%s]", PLUGIN, VERSION)
	forward_return(FMV_STRING, buffer) 
	return FMRES_SUPERCEDE; 
} 

public BalonThink(balonEnt) 
{
	static majitel
	majitel = pev(balonEnt, pev_iuser1)
	if(majitel > 0) 
	{
		if(!get_bit(g_ZivyHrac, majitel)) 
		{
			RespawnoutBalon()
			return PLUGIN_CONTINUE;
		}

		static Float:flOrigin[3], Float:flAngles[3], Float:flOriginNew[3]
		pev(majitel, pev_v_angle, flAngles)
		pev(majitel, pev_origin, flOrigin)
        
		flOriginNew[0] = (floatcos(flAngles[1], degrees) * 55.0) + flOrigin[0]
		flOriginNew[1] = (floatsin(flAngles[1], degrees) * 55.0) + flOrigin[1]
		flOriginNew[2] = flOrigin[2];
		flOriginNew[2] -= (pev(majitel, pev_flags) & FL_DUCKING) ? 10 : 30
        
		static const Float:vVelocity[3] = {1.0, 1.0, 0.0}
		set_pev(balonEnt, pev_velocity, vVelocity)
		set_pev(balonEnt, pev_origin, flOriginNew)
		set_pev(balonEnt, pev_nextthink, halflife_time() + 0.05)
	} else 
	{
		static Float:flVelocity[3]
		pev(balonEnt, pev_velocity, flVelocity)
		if (vector_length(flVelocity) <= 3.0)
		{
			set_pev(balonEnt, pev_animtime, 0.0)
			set_pev(balonEnt, pev_framerate, 0.0)
		} else
		{
			set_pev(balonEnt, pev_nextthink, halflife_time() + 0.2)
		}
	}
    
	return PLUGIN_CONTINUE;
}

public DotknutiMiceHrac(balonEnt, id) 
{
	static majitel
	majitel = pev(balonEnt, pev_iuser1)
	if(majitel != 0)
		return PLUGIN_CONTINUE

	set_pev(balonEnt, pev_animtime, 0.0)
	set_pev(balonEnt, pev_framerate, 0.0)
	set_pev(balonEnt, pev_iuser1, id)
	set_pev(balonEnt, pev_solid, SOLID_NOT)
	set_pev(balonEnt, pev_nextthink, halflife_time() + 0.05)
		
	g_ServerInfo[Balon_Drzi] = id
	return PLUGIN_CONTINUE;
}

public DotknutiMiceOkoli(balon, worldEnt) 
{
	static Float:flVelocity[3]
	pev(balon, pev_velocity, flVelocity)
	if(floatround(vector_length(flVelocity)) < 20) 
		return PLUGIN_HANDLED
		
	flVelocity[0] *= 0.85
	flVelocity[1] *= 0.85
	flVelocity[2] *= 0.85
	set_pev(balon, pev_velocity, flVelocity)
		
	static Float: flAngles[3]
	vector_to_angle(flVelocity, flAngles)
	set_pev(balon, pev_angles, flAngles)
		
	emit_sound(balon, CHAN_ITEM, RuzneZvuky[12], 1.0, ATTN_NORM, 0, PITCH_NORM)
	return PLUGIN_CONTINUE;
}

public FwdHamObjectCaps(id) 
{
	if (!zivyHrac(id))
		return HAM_IGNORED
	
	if(g_ServerInfo[Balon_Ent] != 0 && g_ServerInfo[Balon_Drzi] == id)
	{
		OdkopnoutBalon(id)
		return HAM_IGNORED
	}
	
	#if !defined DEBUG
	static Float: gametime ; gametime = get_gametime();
	if(g_MojeInfo[id][Hrac_Team] == CS_TEAM_T && gametime - 1.0 > g_MojeInfo[id][Dealer_Cooldown])
	{
		static iTarget, iBody, szAimingEnt[32];
		get_user_aiming(id, iTarget, iBody, 75);
		pev(iTarget, pev_classname, szAimingEnt, charsmax(szAimingEnt));
		
		if(equal(szAimingEnt, "Dealer"))
			DealerObchod(id)
		
		g_MojeInfo[id][Dealer_Cooldown] = _:gametime;
    }
	#else
	static iTarget, iBody, szAimingEnt[32];
	get_user_aiming(id, iTarget, iBody, 75);
	pev(iTarget, pev_classname, szAimingEnt, charsmax(szAimingEnt));
	
	if(equal(szAimingEnt, "Dealer"))
		DealerObchod(id)
	#endif
	
	return HAM_IGNORED
}
	
public BloknoutZoom(Zbran)
{
	if(!g_ServerInfo[Duel_Bezi] || !g_ServerInfo[Duel_NoZoom])
		return HAM_IGNORED
	
	static id
	id = pev(Zbran, pev_owner)
	if(g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)
		return HAM_SUPERCEDE
	
	return HAM_IGNORED
}

public SpecialniGranaty(granat)
{
	if(!g_Molotov || !pev_valid(granat))
		return HAM_IGNORED
		
	static Float:dmgtime
	pev(granat, pev_dmgtime, dmgtime)
	if (dmgtime > get_gametime() || pev(granat, pev_flTimeStepSound) != KEY_GUN_MOLOTOV)
		return HAM_IGNORED	
			
	ExplozeMolotovu(granat)
	return HAM_SUPERCEDE
}

public ExplozeMolotovu(ent)
{
	static Float: flOrigin[3], majitel
	majitel = pev(ent, pev_owner)
	pev(ent, pev_origin, flOrigin)
	
	emit_sound(ent, CHAN_AUTO, RuzneZvuky[3], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	engfunc(EngFunc_RemoveEntity, ent)
	clear_bit(g_Molotov, majitel)
	
	static ent
	ent = create_entity("info_target")
	set_pev(ent, pev_classname, EntityName[ENTITA_MOLOTOV])
	set_pev(ent, pev_origin, flOrigin)
	set_pev(ent, pev_iuser1, majitel)
	set_pev(ent, pev_iuser2, g_MojeInfo[majitel][Hrac_Team])
	set_pev(ent, pev_iuser3, 25)
	set_pev(ent, pev_nextthink, get_gametime() + 0.2)
	PridatEntituDoArray(ent, ENTITA_MOLOTOV)
}

public SpawnutiHrace_Post(id)
{
	if(!is_user_alive(id))
		return HAM_IGNORED
	
	set_bit(g_ZivyHrac, id)
	g_MojeInfo[id][Hrac_Team] = _:fm_cs_get_user_team(id)
	fm_set_user_rendering(id)
	OdebratZbrane(id, 0)
	UkoncitMenu(id)

	#if !defined DEBUG
	if (!jeAdmin(id) && !task_exists(id + TASK_PINGCHECKER))
	{
		new param[1]
		param[0] = id
		set_task(5.0, "KontrolaPingu", id + TASK_PINGCHECKER, param, 1, "b")
	}
	#endif
	
	if(task_exists(id + TASK_CHCANI))
	{
		remove_task(id + TASK_CHCANI)
		emit_sound(id, CHAN_VOICE, "mjail/pissing.wav", 0.0, ATTN_NORM, 0, PITCH_NORM) 
	}
	
	if (g_ServerInfo[Special_Typ] != SPECIAL_FREEDAY)
	{
		g_MojeInfo[id][VIP_Chcal] = false
		if(get_bit(g_RedBull, id))
		{
			remove_task(id + TASK_REDBULL)
			clear_bit(g_RedBull, id)
		}
		
		if (g_MojeInfo[id][VezenTym] != VEZEN_TEAM_ZADNY)
			g_MojeInfo[id][VezenTym] = _:VEZEN_TEAM_ZADNY
		
		switch(g_MojeInfo[id][Hrac_Team])
		{
			case CS_TEAM_T:
			{
				if (get_bit(g_AutoVolnyDen, id))
				{
					PridelitVolnyDen(id)
					clear_bit(g_AutoVolnyDen, id)
				}
				
				if (get_bit(g_Molotov, id))
					fm_give_item(id, "weapon_flashbang")
					
				if (get_bit(g_AntiLaser, id))
					ZobrazitIconu(id, "suit_full", 1)
					
				set_pev(id, pev_health, MAX_HEALTH_T)
				g_MojeInfo[id][OdhazovacichNozu] = jeVIP(id) ? MAX_ODHAZOVACICH_NOZU : 0
				
				if (g_MojeInfo[id][UkrytyNuzStav])
				{
					g_MojeInfo[id][MujNuz] = _:g_MojeInfo[id][UkrytyNuz]
					g_MojeInfo[id][UkrytyNuzStav] = false
					g_MojeInfo[id][UkrytyNuz] = _:Noze:NOZE_RUCE
				} 
				else if (g_MojeInfo[id][MujNuz_Inventar] != Noze:NOZE_RUCE)
				{
					g_MojeInfo[id][MujNuz] = _:g_MojeInfo[id][MujNuz_Inventar]
					g_MojeInfo[id][MujNuz_Inventar] = _:NOZE_RUCE
				}

				if (g_MojeInfo[id][MujNuz] == Noze:NOZE_OBUSEK || g_MojeInfo[id][MujNuz] == Noze:NOZE_KATANA || g_MojeInfo[id][MujNuz] == Noze:NOZE_RUKAVICE)
					g_MojeInfo[id][MujNuz] = _:NOZE_RUCE

				#if defined DEBUG
				if (!is_user_bot(id))
				{
					fm_give_item(id, "weapon_deagle")
					fm_cs_set_user_bpammo(id, CSW_DEAGLE, 35)
				
					fm_give_item(id, "weapon_m4a1")
					fm_cs_set_user_bpammo(id, CSW_M4A1, 90)
				}
				#endif
			}
			case CS_TEAM_CT:
			{
				set_pev(id, pev_health, MAX_HEALTH_CT)
				g_MojeInfo[id][GunMenu_Used] = false
				g_MojeInfo[id][PocetMin] = jeVIP(id) ? 1 : 0
				g_MojeInfo[id][UkrytyNuz] = _:NOZE_RUCE
				g_MojeInfo[id][UkrytyNuzStav] = false
				
				if (g_MojeInfo[id][MujNuz_Inventar] != Noze:NOZE_RUCE)
				{
					g_MojeInfo[id][MujNuz] = _:g_MojeInfo[id][MujNuz_Inventar]
					g_MojeInfo[id][MujNuz_Inventar] = _:NOZE_RUCE
				}
				
				if (g_MojeInfo[id][MujNuz] != Noze:NOZE_OBUSEK && g_MojeInfo[id][MujNuz] != Noze:NOZE_KATANA)
				{
					g_MojeInfo[id][MujNuz] = _:NOZE_OBUSEK
				}
				
				if(g_MojeInfo[id][GunMenu_Off])
				{
					PridelitPosledniZbran(id);
				} else
				{
					GunMenu(id)
				}
			}
		}
	}
	
	AktualizovatPromenne(true)
	set_pdata_float(id, OFFSET_ARGSTIME, get_gametime() + 999999.0)
	
	if(get_bit(g_Trail, id))
		ZrusitTrail(id)
	
	AktualizovatModel(id)	
	if (g_MojeInfo[id][Ucet_Cas] == 0.0)
		g_MojeInfo[id][Ucet_Cas] = _:get_gametime()

	return HAM_IGNORED
}

public DamageHrace_Pre(victim, ent, attacker, Float:damage, damagebits)
{
	if(!hracPripojen(victim) || !hracPripojen(attacker))
		return HAM_IGNORED
	
	if(g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && !g_ServerInfo[Duel_Bezi] && g_MojeInfo[attacker][Hrac_Team] == CS_TEAM_T && g_MojeInfo[victim][Hrac_Team] == CS_TEAM_CT)
	{
		if(!get_bit(g_HledanyVezen, attacker))
			set_bit(g_HledanyVezen, attacker)
		
		if(!g_ServerInfo[VzpouraVyvolana])
		{
			emit_sound(0, CHAN_AUTO, RuzneZvuky[1], 1.0, ATTN_NORM, 0, PITCH_NORM)
			g_ServerInfo[VzpouraVyvolana] = true
			if (g_OnlineHracu[CS_TEAM_T] >= 5)
			{
				g_MojeInfo[attacker][Stats_Vzpour]++
				PridatBody(attacker, 2)
			}
		}
	}
	
	static Float: iDamage; 
	iDamage = damage
	if(get_user_weapon(attacker) == CSW_KNIFE)
	{
		static Float: fTemp[3];
		switch(g_MojeInfo[attacker][MujNuz])
		{
			case NOZE_RUCE:
			{
				iDamage = iDamage / 5.0 
				SetHamParamFloat(4, iDamage)
				return HAM_OVERRIDE
			}
			case NOZE_SROUBOVAK:
			{
				iDamage = iDamage / 3.0
				SetHamParamFloat(4, iDamage)
				return HAM_OVERRIDE
			}
			case NOZE_ZABIKUCH, NOZE_RUKAVICE:
			{
				iDamage = iDamage / 2.0
				SetHamParamFloat(4, iDamage)
				return HAM_OVERRIDE
			}
			case NOZE_PACIDLO, NOZE_OBUSEK:
			{
				iDamage = iDamage * 2
				if (iDamage > 60.0)
					iDamage = 60.0
					
				SetHamParamFloat(4, iDamage)
				return HAM_OVERRIDE
			}
			case NOZE_MACETA, NOZE_KATANA:
			{
				iDamage = iDamage * 2.5
				if (iDamage > 100.0)
					iDamage = 100.0
					
				SetHamParamFloat(4, iDamage)
				pev(victim, pev_origin, fTemp)
				CakanecKrve(fTemp, 1)
				return HAM_OVERRIDE
			}
			case NOZE_MOTOROVKA:
			{
				pev(victim, pev_origin, fTemp)
				CakanecKrve(fTemp, 3)
				SetHamParamFloat(4, 9999.0)
				return HAM_OVERRIDE
			}
		}
	}
	
	return HAM_IGNORED
}

public MaximalniRychlost(id)
{
	if(g_MojeInfo[id][Hrac_Rychlost] == 0.0 || !get_bit(g_ZivyHrac, id))
		return HAM_IGNORED
	
	set_pev(id, pev_maxspeed, g_MojeInfo[id][Hrac_Rychlost])
	return HAM_IGNORED
}

public AttackHrace_Pre(victim, attacker, Float:damage, Float:direction[3], tracehandle, damagebits)
{	
	if(!zivyHrac(victim) || !zivyHrac(attacker))
		return HAM_IGNORED
		
	switch(g_ServerInfo[Special_Typ])
	{
		case SPECIAL_FREEDAY, SPECIAL_HONENA:
		{
			return HAM_SUPERCEDE
		}
		case SPECIAL_ZADNY:
		{
			if (g_MojeInfo[attacker][Hrac_Team] == g_MojeInfo[victim][Hrac_Team] && (g_MojeInfo[victim][Hrac_Team] == CS_TEAM_CT || !g_ServerInfo[Box]))
				return HAM_SUPERCEDE
			
			if(g_ServerInfo[Duel_Bezi])
			{
				if((g_ServerInfo[Duel_idT] == attacker && g_ServerInfo[Duel_idCT] == victim) || (g_ServerInfo[Duel_idCT] == attacker && g_ServerInfo[Duel_idT] == victim))
				{
					if(g_ServerInfo[Duel_Mod] == 1 && get_tr2(tracehandle, TR_iHitgroup) != HIT_HEAD)
						return HAM_SUPERCEDE
						
					return HAM_IGNORED
					
				}
				
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_PRESTRELKA, SPECIAL_ARENA:
		{
			if (g_MojeInfo[attacker][Hrac_Team] == g_MojeInfo[victim][Hrac_Team] || get_user_weapon(attacker) == CSW_KNIFE)
				return HAM_SUPERCEDE
		}
		case SPECIAL_ONLYHS:
		{
			if (g_MojeInfo[attacker][Hrac_Team] == g_MojeInfo[victim][Hrac_Team] || get_user_weapon(attacker) == CSW_KNIFE || get_tr2(tracehandle, TR_iHitgroup) != HIT_HEAD)
				return HAM_SUPERCEDE
		}
		case SPECIAL_SCHOVKA:
		{
			if(!g_ServerInfo[Special_Spusten])
			{
				return HAM_SUPERCEDE
			} else
			{
				if (g_MojeInfo[attacker][Hrac_Team] == CS_TEAM_T || g_MojeInfo[attacker][Hrac_Team] == g_MojeInfo[victim][Hrac_Team])
					return HAM_SUPERCEDE
			}
		}
	}
	
	return HAM_IGNORED
}

public AttackHrace_Post(victim, attacker, Float:damage, Float:direction[3], tracehandle, damagebits)
{	
	if(!zivyHrac(victim) || !zivyHrac(attacker) || g_ServerInfo[Special_Typ] != SPECIAL_ZADNY)
		return HAM_IGNORED
	
	if (g_MojeInfo[attacker][Hrac_Team] != g_MojeInfo[victim][Hrac_Team] && !g_MojeInfo[attacker][PrimarniUtokKnife] && get_user_weapon(attacker) == CSW_KNIFE && get_tr2(tracehandle, TR_iHitgroup) == HIT_HEAD)
	{
		switch (g_MojeInfo[attacker][MujNuz])
		{
			case NOZE_OBUSEK, NOZE_PACIDLO:
			{
				static Float: fTemp[3]
				fTemp[0] = random_float(25.0 , 50.0);
				fTemp[1] = (random_num(0, 1) == 0) ? random_float(25.0 , 50.0) : random_float(-25.0 , -50.0)
				fTemp[2] = random_float(25.0 , 50.0);
				set_pev(victim, pev_punchangle, fTemp)
				
				message_begin(MSG_ONE , g_MsgScreenShake , {0,0,0} ,victim);
				write_short(1<<14);
				write_short(1<<14);
				write_short(1<<14);
				message_end();
				
				ScreenFade(victim, {0, 0, 0}, 100)
			}
		}
	}
	
	return HAM_IGNORED
}

public ZabitiHrace_Pre(victim, attacker, shouldgib)
{
	if(!hracPripojen(victim))
		return HAM_IGNORED
	
	static bool: worldKill
	worldKill = !playerId(attacker)
	
	if(task_exists(victim + TASK_CHCANI))
	{
		remove_task(victim + TASK_CHCANI)
		emit_sound(victim, CHAN_VOICE, "mjail/pissing.wav", 0.0, ATTN_NORM, 0, PITCH_NORM) 
	}
	
	clear_bit(g_ZivyHrac, victim)
	clear_bit(g_Trail, victim)
	
	UkoncitMenu(victim)
	AktualizovatPromenne(false)
	
	if (g_MojeInfo[victim][Padak_Ent])
	{
		if (pev_valid(g_MojeInfo[victim][Padak_Ent]))
			remove_entity(g_MojeInfo[victim][Padak_Ent])
			
		g_MojeInfo[victim][Padak_Ent] = 0
	}
	
	if(get_bit(g_RedBull, victim))
	{
		remove_task(victim + TASK_REDBULL)
		clear_bit(g_RedBull, victim)
	}
	
	static bool: headShot, bool: sebevrazda
	headShot = (get_pdata_int(victim, 75) == HIT_HEAD)
	sebevrazda = victim == attacker ? true : false
	
	switch(g_ServerInfo[Special_Typ])
	{
		case SPECIAL_ZADNY:
		{
			static bool: simonKill, bool: duelKill
			simonKill = false
			duelKill = g_ServerInfo[Duel_Bezi] && (g_ServerInfo[Duel_idT] == victim || g_ServerInfo[Duel_idCT] == victim)

			clear_bit(g_VolnyDen, victim)
			clear_bit(g_Mikrofon, victim)
			
			if(g_VolnyDen && HracuBezVD() == 1)
				UkoncitVolneDny()
			
			if (g_MojeInfo[victim][VezenTym] != VEZEN_TEAM_ZADNY)
				g_MojeInfo[victim][VezenTym] = _:VEZEN_TEAM_ZADNY
			
			if(g_ServerInfo[Simon_Id] == victim)
			{
				simonKill = true
				g_ServerInfo[Simon_Id] = 0
				JailDHud(0, 1, BarvyRGB[BARVA_CERVENA], 0, 2.5, 0.0, 0.2, 0.2, ">> Simon byl zabit! <<^nVsechny jeho prikazy se rusi!")
				UkoncitSimonFunkce()
				
				if(g_ZivychHracu[CS_TEAM_CT] > 1)
				{
					set_task(5.0, "VybratNahodnehoSimona", TASK_NOVYSIMON)
				} else if (g_ZivychHracu[CS_TEAM_CT] == 1)
				{
					VybratNahodnehoSimona()
				}
			}
			
			if (!worldKill && !sebevrazda)
			{
				if (g_MojeInfo[victim][Hrac_Team] == CS_TEAM_T)
				{
					g_MojeInfo[victim][Stats_Smrti]++
					if (get_bit(g_HledanyVezen, victim))
						PridatBody(attacker, headShot ? 8 : 4)
				}
				
				if(g_MojeInfo[attacker][Hrac_Team] == CS_TEAM_T)
				{
					if(g_MojeInfo[attacker][Hrac_Team] == g_MojeInfo[victim][Hrac_Team])
					{
						g_MojeInfo[victim][Stats_Smrti]++	
						set_pev(attacker, pev_frags, get_user_frags(attacker) + 2)
						g_MojeInfo[attacker][Stats_Kill_Box]++
						PridatBody(attacker, headShot ? 4 : 2)
					} else
					{
						static odmena
						odmena = headShot ? 8 : 4
						if (duelKill)
						{
							odmena += 4
						}
						
						if (simonKill)
						{
							g_MojeInfo[attacker][Stats_Kill_Simon]++
							odmena += 4
						} else
						{
							g_MojeInfo[attacker][Stats_Kill_Bachar]++
						}
						
						DropnoutEntitu(victim, g_MojeInfo[victim][MujNuz], false)
						PridatBody(attacker, odmena)
					}
				}
			} else
			{
				if (g_MojeInfo[victim][Hrac_Team] == CS_TEAM_T)
				{
					g_MojeInfo[victim][Stats_Smrti]++
				}
			}
			
			if (get_bit(g_HledanyVezen, victim))
			{
				clear_bit(g_HledanyVezen, victim)
				if (!g_HledanyVezen)
					g_ServerInfo[VzpouraVyvolana] = false
			}
			
			if(get_bit(g_VolnyDen, victim))
			{
				clear_bit(g_VolnyDen, victim)
				if(g_VolnyDen) 
					VolneDnyHudInfo()
			}
			
			if(duelKill)
			{
				remove_task(TASK_DUEL)
				g_ServerInfo[Duel_Bezi] = false
				if (g_ServerInfo[Duel_idCT] == victim)
				{
					if (!worldKill)
					{
						g_MojeInfo[attacker][Stats_VyhranychDuelu]++
					}
					
					if(g_ZivychHracu[CS_TEAM_CT])
					{
						fm_set_user_rendering(g_ServerInfo[Duel_idT])
						PosledniPrani_HlavniMenu(g_ServerInfo[Duel_idT])
						
						if (g_ServerInfo[Duel_Typ] == DUEL_BOX)
						{
							g_MojeInfo[g_ServerInfo[Duel_idT]][MujNuz] = _:g_MojeInfo[g_ServerInfo[Duel_idT]][MujNuz_Inventar]
							g_MojeInfo[g_ServerInfo[Duel_idT]][MujNuz_Inventar] = _:NOZE_RUCE
						}
						
						OdebratZbrane(g_ServerInfo[Duel_idT], 0)
					}
				}
				
				g_ServerInfo[Duel_idT] = 0
				g_ServerInfo[Duel_idCT] = 0
			} 
			
			SmazatPlayerFunkce(victim)
		}
		case SPECIAL_FREEDAY:
		{
			if (g_ZivychHracu[g_MojeInfo[victim][Hrac_Team]])
			{
				new data[1]
				data[0] = victim
				set_task(3.0, "RespawnoutHraceTask", victim + TASK_RESPAWN, data, 1)
			}
		}
		case SPECIAL_SCHOVKA:
		{
			if (g_MojeInfo[victim][Hrac_Team] == CS_TEAM_T)
			{
				static odmena
				odmena = (180 - g_ServerInfo[Odpocet_Cas]) / 10
				PridatBody(victim, odmena)
				PridatBody(attacker, headShot ? 4 : 2)
			}
		}
		case SPECIAL_PRESTRELKA, SPECIAL_ONLYHS, SPECIAL_ARENA, SPECIAL_ONESHOT:
		{
			OdebratZbrane(victim, 0)
			if (!sebevrazda && !worldKill)
			{
				PridatBody(attacker, headShot ? 4 : 2)
			}
		}
	}
	
	return HAM_IGNORED
}

public ZmacknutiButtonu(ent, id, idactivator, use_type, Float:value)
{
	if (!zivyHrac(id) || !pev_valid(ent))
		return HAM_IGNORED
		
	static szClass[33]
	pev(ent, pev_classname, szClass, 32)
	if (equal(szClass, "DropnutyKnife"))
	{
		if (g_MojeInfo[id][MujNuz] == Noze:NOZE_RUCE)
		{
			if (g_MojeInfo[id][UkrytyNuzStav])
			{
				client_print(id, print_center, "Nemuzes mit vice nozu! Musis dropnout aktualni!")
			} else
			{
				PrehoditKnife(id, ent)
			}
		} else	
		{
			DropnoutEntitu(id, g_MojeInfo[id][MujNuz], true)
			PrehoditKnife(id, ent)
		}
		
		return HAM_SUPERCEDE
	} else if (equal(szClass, EntityName[ENTITA_MOLOTOV_DROP]))
	{
		set_bit(g_Molotov, id)
		if (!user_has_weapon(id, CSW_FLASHBANG))
			fm_give_item(id, "weapon_flashbang")
		
		SmazatEntitu(ent)
		return HAM_SUPERCEDE
	} else if (g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT && jeSimonButton(ent) && g_ServerInfo[Simon_Id] != id)
	{
		client_print(id, print_center, "Nemuzes ovladat tlacitka pro simona!")
		return HAM_SUPERCEDE
	} else if (jeBlacklistButton(ent))
	{
		client_print(id, print_center, "Tenhle button neni povolen!")
		return HAM_SUPERCEDE
	}
	
	return HAM_IGNORED
}

public BloknoutHeal_Ham(entita, id, idactivator, use_type, Float:value)
{
	switch (g_ServerInfo[Special_Typ])
	{
		case SPECIAL_FREEDAY: 
		{
			return HAM_IGNORED
		}
		case SPECIAL_ZADNY:
		{
			if (zivyHrac(id) && (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id))
			{
				return HAM_SUPERCEDE
			} 
			
			return HAM_IGNORED
		}
	}
		
	return HAM_SUPERCEDE
}

public BloknoutHeal_Engine(ent, id)
{
	switch (g_ServerInfo[Special_Typ])
	{
		case SPECIAL_FREEDAY:
		{
			return PLUGIN_CONTINUE
		}
		case SPECIAL_ZADNY:
		{
			if (g_ServerInfo[Duel_idT] == id  || g_ServerInfo[Duel_idCT] == id)
				return PLUGIN_HANDLED
				
			return PLUGIN_CONTINUE
		}
	}

	return PLUGIN_HANDLED
}

public BloknoutZbrane(id, iWeapon) 
{
	if(!hracPripojen(id) || !pev_valid(iWeapon))
		return HAM_IGNORED
		
	new zbranCSW = fm_cs_get_weapon_id(iWeapon)
	if(!zbranCSW) 
		return HAM_IGNORED
	
	switch(g_ServerInfo[Special_Typ])
	{
		case SPECIAL_ZADNY:
		{
			if(g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT && zbranCSW == CSW_HEGRENADE)
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
			if (g_ServerInfo[Duel_Bezi] && (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id))
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_FREEDAY:
		{
			if (zbranCSW != CSW_KNIFE || user_has_weapon(id, CSW_KNIFE))
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_PRESTRELKA, SPECIAL_ONLYHS:
		{
			if (JeGranat(zbranCSW))
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_SCHOVKA:
		{
			if (JeGranat(zbranCSW) || g_MojeInfo[id][Hrac_Team] == CS_TEAM_T)
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_ONESHOT:
		{
			if (g_ServerInfo[Special_Spusten])
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			} 
			else if (zbranCSW != CSW_DEAGLE) 
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_HONENA:
		{
			if (!get_bit(g_VodniPistol, id) || zbranCSW != CSW_MP5NAVY)
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
		case SPECIAL_ARENA:
		{
			if(!(zbranCSW == CSW_KNIFE || zbranCSW == ZbraneCSW[g_ServerInfo[ArenaGun]]))
			{
				SetHamReturnInteger(0)
				return HAM_SUPERCEDE
			}
		}
	}

	return HAM_IGNORED
}

public SebraniZbrane(ent, id) 
{
	switch(g_ServerInfo[Special_Typ])
	{
		case SPECIAL_ZADNY:
		{
			if(g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT)
			{
				static model[33]
				pev(ent, pev_model, model, 32)
				if(model[8] == 'h' && model[9] == 'e')
					return PLUGIN_HANDLED
			}
			
			if (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)
			{
				return PLUGIN_HANDLED
			}	
		}
		case SPECIAL_SCHOVKA:
		{
			if(g_MojeInfo[id][Hrac_Team] == CS_TEAM_T)
				return PLUGIN_HANDLED
		}
		case SPECIAL_FREEDAY, SPECIAL_ONESHOT, SPECIAL_HONENA, SPECIAL_ARENA:
		{
			return PLUGIN_HANDLED
		}
	}
	
	return PLUGIN_CONTINUE
}

public NoveKolo()
{
	// TODO Obnovit spusteni volneho dne
	/*
	static volnyDen
	volnyDen = (random_num(0, 80) == 50)
	if(volnyDen || g_ServerInfo[Odehranych_Kol] == 1) 
	{
		VolnyDenAll()
	} 
	*/
	
	g_ZmenaTeamu = 0
	if (g_ServerInfo[Dealer_Ent])
	{
		if (!random_num(0, 3))
			PremistitDealera()
	} else if (g_ServerInfo[Special_Typ] != SPECIAL_FREEDAY)
	{
		#if defined DEBUG
		if (g_aDealerData && ArraySize(g_aDealerData) > 0)
			SpawnoutDealeraNahodny()
		#else
		SpawnoutDealeraNahodny()
		#endif
	}
	
	SmazatVsechnyEntity()
	set_task(3.0, "KontrolaPoslednihoVezne", 0)
	g_ServerInfo[Odehranych_Kol]++
	g_ServerInfo[Zacatek_Kola] = _:halflife_time()
}

public KonecKola()
{
	switch(g_ServerInfo[Special_Typ])
	{
		case SPECIAL_ZADNY:
		{
			g_VolnyDen = 0
			g_HledanyVezen = 0
			
			g_ServerInfo[Simon_Id] = 0
			g_ServerInfo[Duel_Bezi] = false
			g_ServerInfo[Duel_Mod] = 0
			g_ServerInfo[Duel_idT] = 0
			g_ServerInfo[Duel_idCT] = 0
			g_ServerInfo[Duel_NoZoom] = false
			g_ServerInfo[Posledni_Vezen] = 0
			g_ServerInfo[VzpouraVyvolana] = false
			
			UkoncitSimonFunkce()
			if(task_exists(TASK_NOVYSIMON))
				remove_task(TASK_NOVYSIMON)
				
			if (task_exists(TASK_DUEL))
				remove_task(TASK_DUEL)
		}
		case SPECIAL_FREEDAY:
		{
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (task_exists(i + TASK_RESPAWN))
					remove_task(i + TASK_RESPAWN)
			}
		}
		case SPECIAL_ONESHOT:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{				
				if(!get_bit(g_HracPripojen, i))
					continue
					
				if(task_exists(TASK_COLT + i))
					remove_task(TASK_COLT + 1)
			}
			
			unregister_forward(FM_UpdateClientData, g_fwUpdateClient, 1)
		}
		case SPECIAL_HONENA:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_HracPripojen, i))
					continue
					
				if(task_exists(TASK_HONENAODPOCET + i))
					remove_task(TASK_HONENAODPOCET + i)
					
				SetFreezeEnt(i, false)
			}

			g_VodniPistol = 0
			g_Zmrazen = 0
			
			remove_task(TASK_HONENABOX)
			
			ObnovitMapEntity("func_door")
			ObnovitMapEntity("func_door_rotating")
			
			if (pev_valid(g_ServerInfo[HonickaThinkEntita]))
			{
				engfunc(EngFunc_RemoveEntity, g_ServerInfo[HonickaThinkEntita])
				g_ServerInfo[HonickaThinkEntita] = 0
			}
			
			unregister_forward(FM_PlayerPreThink, g_fwPlayerThink)
			unregister_forward(FM_UpdateClientData, g_fwUpdateClient, 1)
		}
		case SPECIAL_SCHOVKA:
		{
			unregister_forward(FM_PlayerPostThink, g_fwPlayerThink_Post)
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_HracPripojen, i))
					fm_set_user_rendering(i, kRenderFxNone, 0, 0, 0, kRenderTransAlpha, 255)
				
				if (g_MojeInfo[i][Schovka_Entity] > 0)
				{
					engfunc(EngFunc_RemoveEntity, g_MojeInfo[i][Schovka_Entity])
					g_MojeInfo[i][Schovka_Entity] = 0
				}
			}
		}
	}
	
	RespawnoutBalon()
	client_cmd(0, "mp3 stop")
	g_ServerInfo[Special_Typ] = _:SPECIAL_ZADNY
	g_ServerInfo[Odpocet_Cas] = 0
	if(task_exists(TASK_ODPOCET))
		remove_task(TASK_ODPOCET)
}

public NormalniChat(id)
{
	static zprava[192]
	read_args (zprava, 191)
	remove_quotes (zprava)
	
	if(!zprava[0] || zprava[0] == '/') 
		return PLUGIN_CONTINUE

	static name[32], barva[2][10]
	get_user_name (id, name, 31)
	get_user_team (id, barva[0], 9)
	
	switch(g_MojeInfo[id][Ucet_Flags])
	{
		case HRAC_NORMAL: format(zprava, 191, "!ctt%s!cy: %s%s", name, ChatZpravaBarva[_:g_MojeInfo[id][Ucet_Flags]], zprava)
		default: format(zprava, 191, "!cy*!cg[!ctt%s!cg]!cy* !ctt%s!cy: %s%s", ChatTagPrefix[_:g_MojeInfo[id][Ucet_Flags]], name, ChatZpravaBarva[_:g_MojeInfo[id][Ucet_Flags]], zprava)
	}
	replace_all(zprava, 190, "!cg", "^4") 
	replace_all(zprava, 190, "!cy", "^1") 
	replace_all(zprava, 190, "!ctt", "^3")
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_HracPripojen, i))
			continue

		if((get_bit(g_ZivyHrac, id) && get_bit(g_ZivyHrac, i)) || (!get_bit(g_ZivyHrac, id) && !get_bit(g_ZivyHrac, i)))
		{
			get_user_team(i, barva[1], 9)
			message_begin(MSG_ONE, g_MsgTeamInfo, _, i)	
			write_byte(i)
			write_string(barva[0])
			message_end()	
				
			message_begin (MSG_ONE, g_MsgSayText, {0, 0, 0}, i)
			write_byte (i)					
			write_string (zprava)					
			message_end ()	
				
			message_begin(MSG_ONE, g_MsgTeamInfo, _, i)	
			write_byte(i)				
			write_string(barva[1])				
			message_end()
		}
	}
	
	return PLUGIN_CONTINUE
}

public TeamChat(id)
{
	static zprava[192]
	read_args (zprava, 191)
	remove_quotes (zprava)
	
	if(!zprava[0] || zprava[0] == '/') 
		return PLUGIN_CONTINUE

	static name[32], barva[2][10]
	get_user_name (id, name, 31)
	get_user_team (id, barva[0], 9)
	
	switch(g_MojeInfo[id][Ucet_Flags])
	{
		case HRAC_NORMAL: format(zprava, 191, "!cy(!ctt%s!cy) !ctt%s!cy: %s%s", ChatTeamNazev(id), name, ChatZpravaBarva[_:g_MojeInfo[id][Ucet_Flags]], zprava)
		default: format(zprava, 191, "!cy*!cg[!ctt%s!cg]!cy* (!ctt%s!cy) !ctt%s!cy: %s%s", ChatTagPrefix[_:g_MojeInfo[id][Ucet_Flags]], ChatTeamNazev(id), name, ChatZpravaBarva[_:g_MojeInfo[id][Ucet_Flags]], zprava)
	}
	
	replace_all(zprava, 190, "!cg", "^4") 
	replace_all(zprava, 190, "!cy", "^1") 
	replace_all(zprava, 190, "!ctt", "^3")
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_HracPripojen, i) || g_MojeInfo[id][Hrac_Team] != g_MojeInfo[i][Hrac_Team])
			continue

		if((get_bit(g_ZivyHrac, id) && get_bit(g_ZivyHrac, i)) || (!get_bit(g_ZivyHrac, id) && !get_bit(g_ZivyHrac, i)))
		{
			get_user_team(i, barva[1], 9)
			message_begin(MSG_ONE, g_MsgTeamInfo, _, i)	
			write_byte(i)
			write_string(barva[0])
			message_end()	
				
			message_begin (MSG_ONE, g_MsgSayText, {0, 0, 0}, i)
			write_byte (i)					
			write_string (zprava)					
			message_end ()	
				
			message_begin(MSG_ONE, g_MsgTeamInfo, _, i)	
			write_byte(i)				
			write_string(barva[1])				
			message_end()
		}
	}
	
	return PLUGIN_CONTINUE
}

public HudStatusThink(iEnt)
{
	if (g_ServerInfo[Special_Typ] == SPECIAL_ZADNY)
	{
		if(g_HledanyVezen)
		{
			static szName[33], szHud[812], znaku
			format(szHud, 811, "Hledani vezni:")
			znaku = strlen(szHud)
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_HledanyVezen, i))
					continue
						
				get_user_name(i, szName, 32)
				znaku += copy(szHud[znaku], 811 - znaku, "^n^t")
				znaku += copy(szHud[znaku], 811 - znaku, szName)
			}
					
			JailHud(0, 3, {176, 23, 31}, 0, 3.2, 0.0, 0.0, 0.0, szHud)
		}
	}
	
	set_pev(iEnt, pev_nextthink, get_gametime() + 3.0)
}

public HonenaPlayerTicker_Handler(iEnt)
{
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_ZivyHrac, i))
			continue
			
		if (g_MojeInfo[i][Honena_VodniGun_Effekt] != 0) 
		{
			client_print(0, print_chat, "id: %d - Effekt: %d", i, g_MojeInfo[i][Honena_VodniGun_Effekt])
			if (g_MojeInfo[i][Honena_VodniGun_Effekt] < 0)
			{
				g_MojeInfo[i][Honena_VodniGun_Effekt]++
				if (g_MojeInfo[i][Honena_VodniGun_Effekt] == 0)
				{
					NastavitRychlost(i, HONENA_RYCHLOST)
					fm_set_user_rendering(i)
				} else
				{
					static effectAbs
					effectAbs = abs(g_MojeInfo[i][Honena_VodniGun_Effekt])
					NastavitRychlost(i, HONENA_RYCHLOST - float(effectAbs * 20))
					PlayerGlowAlpha(i, {42, 170, 255}, effectAbs * 3)
				}
			} else
			{
				g_MojeInfo[i][Honena_VodniGun_Effekt]--
				if (g_MojeInfo[i][Honena_VodniGun_Effekt] == 0)	
				{
					NastavitRychlost(i, HONENA_RYCHLOST)
					fm_set_user_rendering(i)
				} else
				{
					NastavitRychlost(i, HONENA_RYCHLOST + float(g_MojeInfo[i][Honena_VodniGun_Effekt] * 8))
					PlayerGlowAlpha(i, {255, 42, 212}, g_MojeInfo[i][Honena_VodniGun_Effekt] * 3)
				}
			}
		} 
			
		if (g_MojeInfo[i][Honena_Chycen])
		{
			g_MojeInfo[i][Honena_Odpocet]--
			if (g_MojeInfo[i][Honena_Odpocet] % 5 == 0)
			{
				static Float: flOrigin[3]
				pev(i, pev_origin, flOrigin)
				flOrigin[2] -= 20
				BlastEfekt(flOrigin, BARVA_CERVENA)
			}
				
			if (!g_MojeInfo[i][Honena_Odpocet])
			{
				ZabitBezPripsani(i)
				g_ServerInfo[HonickaChycenych]--
			} 
		} 
	}
	
	set_pev(iEnt, pev_nextthink, get_gametime() + 1.0)
}

public SetinovaEntitaThink(iEnt)
{
	static Float: flCas; 
	flCas = get_gametime()	
	if(g_Trail)
	{
		for(new i = 1; i <= MAX_HRACU; i++)
		{
			if(!get_bit(g_Trail, i) || !get_bit(g_ZivyHrac, i))
				continue
				
			static iOrigin[3]
			get_user_origin(i, iOrigin)
			if(iOrigin[0] != g_MojeInfo[i][Trail_Origin][0] || iOrigin[1] != g_MojeInfo[i][Trail_Origin][1] || iOrigin[2] != g_MojeInfo[i][Trail_Origin][2])
			{
				if(g_MojeInfo[i][Trail_Ticku] > 60)
				{
					ZrusitTrailEffekt(i)
					TrailEffekt(i)
				}
				
				g_MojeInfo[i][Trail_Origin] = iOrigin
				g_MojeInfo[i][Trail_Ticku] = 0
			}
			else
			{
				g_MojeInfo[i][Trail_Ticku]++
			}
		}
	}
	
	if(g_ServerInfo[Hry_Stopky_Bezi])
	{
		JailHud(0, 7, {0,255, 0}, 0, 0.127, 0.0, 0.0, 0.0, "[-- %.1f --]", (flCas - g_ServerInfo[Hry_Stopky_Start]))
	}
	
	set_pev(iEnt, pev_nextthink, flCas + 0.1)
}
/*******************************************
				< Bachar >
********************************************/
public cmdGunMenu(id)
{
	if ((g_ServerInfo[Special_Typ] == SPECIAL_PRESTRELKA || g_ServerInfo[Special_Typ] == SPECIAL_ONLYHS) && !g_MojeInfo[id][GunMenu_Off])
	{
		GunMenu(id)
		return
	}
	
	if(!g_MojeInfo[id][GunMenu_Off])
		return
		
	g_MojeInfo[id][GunMenu_Off] = false
	ChatColor(id, "Vase gun menu bylo !gobnoveno!y!")
	if(!g_MojeInfo[id][GunMenu_Used]) 
		GunMenu(id)
}

public GunMenu(id)
{
	static buffer[5], predchozi
	predchozi = g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && g_MojeInfo[id][GunMenu_LastP]
	new menu = menu_create("\rPrimarni Zbran", "GunMenuHandler")

	if(predchozi)
	{
		menu_additem(menu, "\yPredchozi", "1")
		menu_additem(menu, "\yPredchozi \w+ \rNezobrazovat", "2")
		menu_addblank(menu, 0)
	}
	
	for(new i = 0; i < sizeof(CTGunMenu); i++)
	{
		num_to_str(i + 3, buffer, 4)
		menu_additem(menu, ZbraneNazvy[CTGunMenu[i]], buffer)
	}
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "0")
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public GunMenuHandler(id, menu, item)
{
	new data[6], szName[64], access, callback;
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);
	
	static volba
	volba = str_to_num(data)
	if(!volba)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	switch(volba)
	{
		case 1: PridelitPosledniZbran(id)
		case 2:
		{
			g_MojeInfo[id][GunMenu_Off] = true
			PridelitPosledniZbran(id)
			ChatColor(id, "Gun menu obnovite po napsani !g/gun!y!")
		}
		default:
		{
			static zbran; zbran = CTGunMenu[volba - 3]
			StripWeapons(id, Primary)
			fm_give_item(id, ZbraneGun[zbran])
			fm_cs_set_user_bpammo(id, ZbraneCSW[zbran], ZbraneAmmo[zbran])
			
			SekudnarniGunMenu(id)
			if (g_ServerInfo[Special_Typ] == SPECIAL_ZADNY)
			{
				g_MojeInfo[id][GunMenu_Used] = true
				g_MojeInfo[id][GunMenu_LastP] = zbran
			}
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public SekudnarniGunMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	menu = menu_create("\rSekundarni Zbran", "SekundarniGunMenuHandler")
	for(new i = 9; i < 13; i++)
		menu_additem(menu, ZbraneNazvy[i], "")
		
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public SekundarniGunMenuHandler(id, menu, item)
{
	if(item == MENU_EXIT)
		return PLUGIN_HANDLED
	
	StripWeapons(id, Secondary)
	g_MojeInfo[id][GunMenu_LastS] = item + 9
	fm_give_item(id, ZbraneGun[g_MojeInfo[id][GunMenu_LastS]])
	fm_cs_set_user_bpammo(id, ZbraneCSW[g_MojeInfo[id][GunMenu_LastS]], ZbraneAmmo[g_MojeInfo[id][GunMenu_LastS]])
	
	return PLUGIN_HANDLED
}
	
public PridelitPosledniZbran(id)
{
	g_MojeInfo[id][GunMenu_Used] = true
	fm_give_item(id, ZbraneGun[g_MojeInfo[id][GunMenu_LastP]])
	fm_cs_set_user_bpammo(id, ZbraneCSW[g_MojeInfo[id][GunMenu_LastP]], ZbraneAmmo[g_MojeInfo[id][GunMenu_LastP]])
	
	if(g_MojeInfo[id][GunMenu_LastS])
	{
		fm_give_item(id, ZbraneGun[g_MojeInfo[id][GunMenu_LastS]])
		fm_cs_set_user_bpammo(id, ZbraneCSW[g_MojeInfo[id][GunMenu_LastS]], ZbraneAmmo[g_MojeInfo[id][GunMenu_LastS]])
	} else
	{
		fm_give_item(id, ZbraneGun[7])
		fm_cs_set_user_bpammo(id, ZbraneCSW[7], ZbraneAmmo[7])
	}
}

public cmdSimon(id)
{
	if(!zivyHrac(id) || g_MojeInfo[id][Hrac_Team] != CS_TEAM_CT || g_ServerInfo[Simon_Id])
		return
	
	if (task_exists(TASK_NOVYSIMON))
		remove_task(TASK_NOVYSIMON)
	
	g_ServerInfo[Simon_Id] = id
	static szName[33]
	get_user_name(id, szName, 32)
	
	JailDHud(0, 1, BarvyRGB[BARVA_MODRA_SVETLA], 0, 3.0, 0.0, 0.5, 0.5, "%s je Simon!", szName)
	set_task(0.5, "VedouciVeznice", id)
}

public UkoncitSimonFunkce()
{
	if (g_ServerInfo[Box])
		VypnoutBox()
	
	if (g_ServerInfo[Hry_Stopky_Bezi])
		VypnoutStopky(true)
	
	if (g_ServerInfo[Hry_Spray_Metr])
		g_ServerInfo[Hry_Spray_Metr] = false
	
	if (g_ServerInfo[Hry_SimonKroky])
		g_ServerInfo[Hry_SimonKroky] = false
	
	if (PocetTeamVeznu())
		ZrusitRozdeleni(0)
	
	if (g_Mikrofon)
	{
		for (new i = 1; i <= MAX_HRACU; i++)
			if (get_bit(g_Mikrofon, i))
				JailDHud(i, 0, {255, 255, 42}, 0, 0.5, 0.0, 0.2, 0.2, ".:[Mikrofon: vypnut]:.")
				
				
		g_Mikrofon = 0
	}
}
/*******************************************
				< HlavniMenu >
********************************************/
public HlavniMenuVGUI(msgid, dest, id)
{
	static msgarg1
	msgarg1 = get_msg_arg_int(1)
	if(msgarg1 == 2)
	{
		HlavniMenu_Rozcestnik(id)
		return PLUGIN_HANDLED
	}

	return PLUGIN_CONTINUE
}

public HlavniMenuClassic(msgid, dest, id)
{
	static szTemp[33]
	get_msg_arg_string(4, szTemp, sizeof(szTemp) - 1)
	// (#Team_Select || #Team_Select_Spect) || (#IG_Team_Select)
	if((szTemp[1] == 'T' && szTemp[6] == 'S') || (szTemp[1] == 'I' && szTemp[4] == 'T'))
	{
		HlavniMenu_Rozcestnik(id)
		return PLUGIN_HANDLED
	}
	
	return PLUGIN_CONTINUE
}

public HlavniMenu_Rozcestnik(id)
{
	switch(g_MojeInfo[id][Hrac_Team])
	{
		case CS_TEAM_SPECTATOR, CS_TEAM_UNASSIGNED: HlavniMenu_Spectator(id)
		case CS_TEAM_CT: HlavniMenu_Bachari(id)
		case CS_TEAM_T: HlavniMenu_Vezen(id)
	}
}

public HlavniMenu_Spectator(id)
{
	static szTemp[112]
	format(szTemp, 111, "\rFunJail \y[\d%s\y]^n\y>> \d%s^n^n\r- \rVyber Team:", VERSION, WEB_ADRESA)
	new menu = menu_create(szTemp, "HlavniMenu_SpectatorHandler")
	
	format(szTemp, 111, "\yVezni \d[\r%d\d]", g_OnlineHracu[CS_TEAM_T])
	menu_additem(menu, szTemp, "")
	
	format(szTemp, 111, "%sBachar \d[\r%d\d]", (jeAdmin(id) || (PovolenoPripojeniCT() && !get_bit(g_CTBan, id))) ? "\y" : "\d", g_OnlineHracu[CS_TEAM_CT])
	menu_additem(menu, szTemp, "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "Pravidla", "")
	menu_additem(menu, "VIP Info", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public HlavniMenu_SpectatorHandler(id, menu, item)
{
	menu_destroy(menu)
	switch(item)
	{
		case 0:
		{
			if (!get_bit(g_ZmenaTeamu, id))
			{
				PripojitDoTeamu(id, CS_TEAM_T)
			} else
			{
				client_print(id, print_center, "Team muzes zmenit pouze jednou za kolo!")
				HlavniMenu_Spectator(id)
			}
		}
		case 1:
		{
			if (get_bit(g_CTBan, id))
			{
				client_print(id, print_center, "Mas aktivni zakaz pripojeni k bacharum!")
				CTBanner_MujBanInfo(id)
			} else if (get_bit(g_ZmenaTeamu, id))
			{
				client_print(id, print_center, "Team muzes zmenit pouze jednou za kolo!")
				HlavniMenu_Spectator(id)
			} else if (jeAdmin(id) || PovolenoPripojeniCT())
			{
				PripojitDoTeamu(id, CS_TEAM_CT)
			} else
			{
				client_print(id, print_center, "Bacharu uz je plny pocet!")
			}
		}
		case 2:
		{
			show_motd(id, MOTD_PRAVIDLA_URL, "Pravidla")
		}
	}
}

public HlavniMenu_Bachari(id)
{
	static buffer[112]
	format(buffer, 111, "\rFunJail \y[\r%s\y]", VERSION)
	new menu = menu_create(buffer, "HlavniMenu_BachariHandler")
	
	if (g_ServerInfo[Simon_Id] == id)
	{
		menu_additem(menu, "Simon", "2")
	} else
	{
		menu_additem(menu, (g_ServerInfo[Simon_Id] || g_ServerInfo[Special_Typ] != SPECIAL_ZADNY || !zivyHrac(id)) ? "\dVzit Simona!" : "\rVzit Simona!", "1")
	}
	
	menu_additem(menu, (zivyHrac(id) && g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && !(g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)) ? "Obchod" : "\dObchod", "3")
	format(buffer, 111, "%sZbrane", (zivyHrac(id) && !g_MojeInfo[id][GunMenu_Used]) ? "" : "\d")
	menu_additem(menu, buffer, "4")
	menu_additem(menu, "Pravidla", "5")
	menu_additem(menu, "\rVIP", "6")
	
	menu_addblank(menu, 0)
	menu_additem(menu, (jeAdmin(id) || !get_bit(g_ZmenaTeamu, id)) ? "\yVezni" : "\dVezni", "8")
	menu_additem(menu, "\yDivaci", "9")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public HlavniMenu_BachariHandler(id, menu, item)
{
	if( item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	new data[6], szName[64], access, callback, tlacitko
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);
	tlacitko = str_to_num(data)
	
	switch(tlacitko)
	{
		case 1:
		{
			if(g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && zivyHrac(id) && !g_ServerInfo[Simon_Id])
			{
				cmdSimon(id)
			} else 
			{
				HlavniMenu_Bachari(id)
			}
		}
		case 2:
		{
			if(g_ServerInfo[Simon_Id] == id)
			{
				VedouciVeznice(id)
			}
		}
		case 3:
		{	
			if (!zivyHrac(id))
			{
				client_print(id, print_center, "Musis byt zivy!")
				HlavniMenu_Bachari(id)
			} else if (g_ServerInfo[Special_Typ] != SPECIAL_ZADNY)
			{
				client_print(id, print_center, "Obchod je uzavren pri specialu!")
				HlavniMenu_Bachari(id)
			} else if (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)
			{
				client_print(id, print_center, "Nemuzes nakupovat pri duelu!")
				HlavniMenu_Bachari(id)
			} else
			{
				BacharskyShop(id)
			}
		}
		case 4: 
		{
			if (!zivyHrac(id))
			{
				client_print(id, print_center, "Nejsi zivy!")
				HlavniMenu_Bachari(id)
			} else if (g_MojeInfo[id][GunMenu_Used])
			{
				client_print(id, print_center, "Menu muzes pouzit pouze jednou!")
				HlavniMenu_Bachari(id)
			} else
			{
				GunMenu(id)
			}
		}
		case 5: 
		{
			show_motd(id, MOTD_PRAVIDLA_URL, "Pravidla")
		}
		case 6:
		{
			cmdVIPMenu(id)
		}
		case 8:
		{
			if (jeAdmin(id))
			{
				ZmenitTeam(id, CS_TEAM_T)
			} else if (get_bit(g_ZmenaTeamu, id))
			{
				client_print(id, print_center, "Team muzes zmenit pouze jednou za kolo!")
				HlavniMenu_Bachari(id)
			} else
			{
				PripojitDoTeamu(id, CS_TEAM_T)
			}
		}
		case 9:
		{
			PripojitDoTeamu(id, CS_TEAM_SPECTATOR)
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public HlavniMenu_Vezen(id)
{
	static szTemp[112]
	format(szTemp, 111, "\rFunJail \y[\r%s\y]", VERSION)
	new menu = menu_create(szTemp, "HlavniMenu_VezenHandler")
	
	if (zivyHrac(id) && jeMod(SPECIAL_SCHOVKA))
		menu_additem(menu, "Schovka", "1")
	
	menu_additem(menu, (zivyHrac(id) && g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && !(g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)) ? "Obchod" : "\dObchod", "2")
	menu_additem(menu, "Pravidla", "3")
	menu_additem(menu, "\rVIP", "4")
	
	menu_addblank(menu, 0)
	menu_additem(menu, (PovolenoPripojeniCT() && !get_bit(g_CTBan, id) && (jeAdmin(id) || !get_bit(g_ZmenaTeamu, id))) ? "\yBachar" : "\dBachar", "5")
	menu_additem(menu, "\yDivak", "6")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public HlavniMenu_VezenHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], access, callback, tlacitko
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);
	tlacitko = str_to_num(data)
	
	switch(tlacitko)
	{
		case 1:
		{
			SchovkaMainMenu(id)
		}
		case 2:
		{
			if (!get_bit(g_ZivyHrac, id))
			{
				client_print(id, print_center, "Musis byt nazivu!")
				HlavniMenu_Vezen(id)
			} else if (g_ServerInfo[Special_Typ] != SPECIAL_ZADNY)
			{
				client_print(id, print_center, "Obchod je aktualne uzavren!")
				HlavniMenu_Vezen(id)
			} else if (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)
			{
				client_print(id, print_center, "Nemuzes nakupovat pri duelu!")
				HlavniMenu_Vezen(id)
			} else
			{
				VezenskyShop(id)
			}
		}
		case 3: 
		{
			show_motd(id, MOTD_PRAVIDLA_URL, "Pravidla")
		}
		case 4:
		{
			cmdVIPMenu(id)	
		}
		case 5:
		{
			if (jeAdmin(id))
			{
				ZmenitTeam(id, CS_TEAM_CT)
			} else if (get_bit(g_CTBan, id))
			{
				client_print(id, print_center, "Mas aktivni zakaz pripojeni k bacharum!")
				CTBanner_MujBanInfo(id)
			} else if (get_bit(g_ZmenaTeamu, id))
			{
				client_print(id, print_center, "Team muzes zmenit pouze jednou za kolo!")
				HlavniMenu_Vezen(id)
			} else if (PovolenoPripojeniCT())
			{
				PripojitDoTeamu(id, CS_TEAM_CT)
			} else
			{
				client_print(id, print_center, "Bacharu je jiz plny pocet!")
				HlavniMenu_Vezen(id)
			}
		}
		case 6: 
		{
			PripojitDoTeamu(id, CS_TEAM_SPECTATOR)
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}
/*******************************************
			< VedouciVeznice >
********************************************/
public VedouciVeznice(id)
{
	if(!jeAdmin(id) && g_ServerInfo[Simon_Id] != id)
	{
		return
	}
	
	new menu = menu_create("\rVedouci Veznice", "VedouciVezniceHandler")
	menu_additem(menu, "Otevrit cely", "")
	menu_additem(menu, g_ServerInfo[Box] ? "\rVypnout box" : "Zapnout box", "")
	menu_additem(menu, JeMoznePridelitVD() ? "Volny Den" : "\dVolny Den", "")
	menu_additem(menu, (g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && SpecialPovolen() && g_ServerInfo[Zacatek_Kola] + SPECIAL_START_LIMIT > halflife_time()) ? "Specialni Den" : "\dSpecialni Den", "")
	menu_additem(menu, "Mikrofony", "")
	menu_additem(menu, "Doplnit HP", "")
	menu_additem(menu, "Hry", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public VedouciVezniceHandler(id, menu, item)
{
	menu_destroy(menu)
	switch(item)
	{
		case 0: 
		{
			OtevritCely()
			VedouciVeznice(id)
		}
		case 1: 
		{
			if(g_ServerInfo[Box])
			{
				VypnoutBox()
			} else
			{
				ZapnoutBox()
			}
			VedouciVeznice(id)
		}
		case 2:
		{
			if(JeMoznePridelitVD()) 
			{
				VolnyDenHracMenu(id)
			} else
			{
				client_print(id, print_center, "Neni mozne pridelit volny den! Musi byt aspon 3 hraci bez VD!")
				VedouciVeznice(id)
			}
		}
		case 3:
		{
			if(g_ServerInfo[Special_Typ] == SPECIAL_ZADNY)
			{
				#if defined DEBUG
				SpecialniDnyMenu(id)
				#else
				if (g_ServerInfo[Zacatek_Kola] + SPECIAL_START_LIMIT < halflife_time())
				{
					client_print(id, print_center, "Specialni den lze spustit pouze do 30 sekund od zacatku kola!")
					VedouciVeznice(id)
				} else if(!SpecialPovolen())
				{
					client_print(id, print_center, "Specialni den lze spustit az %s minut!", FormatovatSekundy(LimitVyprsi()))
					VedouciVeznice(id)
				} else
				{
					SpecialniDnyMenu(id)
				}
				#endif
			}
		}
		case 4: 
		{
			MikrofonMenu(id)
		}
		case 5:
		{
			VedouciVeznice(id)
			client_print(id, print_center, "Vezni maji nyni plny pocet HP!")
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (!zivyHrac(i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T)
					continue
				
				set_pev(i, pev_health, MAX_HEALTH_T)
			}
		}
		case 6: 
		{
			HryMenu(id)
		}
	}
}

public VolnyDenHracMenu(id)
{
	static szNum[3], szName[33]
	new menu = menu_create("\yVolny den", "VolnyDenHracMenu_Handler")
	
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (get_bit(g_VolnyDen, i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T)
			continue
			
		num_to_str(i, szNum, 2)
		get_user_name(i, szName, 32)
		menu_additem(menu, szName, szNum)
	}
	
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public VolnyDenHracMenu_Handler(id, menu, item)
{
	if( item == MENU_EXIT )
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new data[6], szName[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName,charsmax(szName), callback);
	
	new hracId = str_to_num(data)
	if (!zivyHrac(hracId))
	{
		VolnyDenHracMenu(id)
		client_print(id, print_center, "Tento hrac jiz neni nazivu!")
	} else
	{
		PridelitVolnyDen(hracId)
		get_user_name(hracId, szName, 32)
		client_print(id, print_center, "%s ma nyni volny den!", szName)
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public OdpocetMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	new szBuffer[33]
	menu = menu_create("\rOdpocet", "OdpocetMenuHandler")
	for(new i = 0; i < sizeof(CasyOdpoctu); i++)
	{
		format(szBuffer, 32, "\y%d \dSekund", CasyOdpoctu[i])
		menu_additem(menu, szBuffer, "")
	}
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public OdpocetMenuHandler(id, menu, item)
{
	if( item == MENU_EXIT)
	{
		VedouciVeznice(id)
		return PLUGIN_HANDLED
	}
	
	g_ServerInfo[Odpocet_Cas] = CasyOdpoctu[item]
	set_task(1.0, "Odpocet", TASK_ODPOCET, _, _, "b")
	Odpocet()
	
	menu_display(id, menu, 0)
	return PLUGIN_HANDLED
}

public Odpocet()
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		if(g_ServerInfo[Odpocet_Cas] < 11)
			emit_sound(0, CHAN_VOICE, OdpocetZvuky[g_ServerInfo[Odpocet_Cas] - 1], 1.0, ATTN_NORM, 0, PITCH_NORM)
		
		JailHud(0, 6, {0, 255, 0}, 0, 1.2, 0.0, 0.0, 0.0, "[-- %d --]", g_ServerInfo[Odpocet_Cas])
		g_ServerInfo[Odpocet_Cas]--
	} else
	{
		JailHud(0, 6, {0, 255, 0}, 0, 1.2, 0.0, 0.0, 0.0, "[-- Go Go Go --]", g_ServerInfo[Odpocet_Cas])
		remove_task(TASK_ODPOCET)
	}
}

public ZapnoutBox()
{
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T)
			continue
			
		set_pev(i, pev_health, 100.0)
	}
	
	JailDHud(0, 3, {0, 255, 0}, 0, 1.0, 0.0, 0.2, 0.2, "! Box je zapnut !")
	client_cmd(0, "spk %s", BoxZvuky[2])
	g_ServerInfo[Box] = 1
}

public VypnoutBox()
{
	JailDHud(0, 3, {250, 0, 0}, 0, 1.0, 0.0, 0.2, 0.2, "! Box byl vypnut !")
	g_ServerInfo[Box] = 0
}

public MikrofonMenu(id)
{
	static szMenuItem[66]
	format(szMenuItem, 65, "\rMikrofon menu^n^n\y>> \wGlobalni: \d%s^n\y>> \wPovolenych: \d%d", (g_ServerInfo[Hry_Globalni_Mic] ? "On" : "Off"), PocetMikrofonu())
	new menu = menu_create(szMenuItem, "MikrofonMenu_Handler")
	
	format(szMenuItem, 65, "%sPovolit vsem", g_ServerInfo[Hry_Globalni_Mic] ? "\d" : "")
	menu_additem(menu, szMenuItem, "")
	
	format(szMenuItem, 65, "%sZakazat vsem", (g_ServerInfo[Hry_Globalni_Mic] || g_Mikrofon) ? "" : "\d")
	menu_additem(menu, szMenuItem, "")
	
	menu_addblank(menu, 0)
	format(szMenuItem, 65, "\yVybrat vezne")
	menu_additem(menu, szMenuItem, "")
	
	menu_addblank(menu, 0)
	menu_addblank(menu, 0)
	
	format(szMenuItem, 65, "\rZavrit")
	menu_additem(menu, szMenuItem, "")
	
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public MikrofonMenu_Handler(id, menu, item)
{
	menu_destroy(menu)
	switch (item)
	{
		case 0:
		{
			if (g_ServerInfo[Hry_Globalni_Mic])
			{
				client_print(id, print_center, "Mikrofony pro vsechny vezne jsou jiz povoleny!")
				MikrofonMenu(id)
			} else
			{
				g_ServerInfo[Hry_Globalni_Mic] = true
				client_print(id, print_center, "Mikrofony zapnuty!")
				MikrofonMenu(id)
			}
		}
		case 1:
		{
			if (!g_ServerInfo[Hry_Globalni_Mic] && !g_Mikrofon)
			{
				client_print(id, print_center, "Zadne mikrofony nejsou aktivni!")
				MikrofonMenu(id)
			} else
			{
				g_Mikrofon = 0
				g_ServerInfo[Hry_Globalni_Mic] = false
				client_print(id, print_center, "Mikrofony vypnuty!")
				MikrofonMenu(id)
			}
		}
		case 2:
		{
			if (!PocetVeznuBezAdminu())
			{
				client_print(id, print_center, "Neni komu povolit mikrofon!")
				MikrofonMenu(id)
			} else
			{
				MikrofonHraciMenu(id)
			}
		}
		case 3:
		{
			VedouciVeznice(id)
		}
	}
}

public MikrofonHraciMenu(id)
{
	static szName[33], szItem[35], szNum[3]
	new menu = menu_create("\rVyber hrace", "MikrofonHraciMenu_Handler")
	
	new playerArray[2][MAX_HRACU], indexA, indexB
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_ZivyHrac, i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T || jeAdmin(i))
			continue
		
		if (get_bit(g_Mikrofon, i))
		{
			playerArray[0][indexA++] = i
		} else
		{
			playerArray[1][indexB++] = i
		}
	}
	
	for (new i = 0; i < indexA; i++)
	{
		num_to_str(playerArray[0][i], szNum, 2)
		get_user_name(playerArray[0][i], szName, 32)
		format(szItem, 34, "\y%s", szName)
		menu_additem(menu, szItem, szNum)
	}
	
	for (new i = 0; i < indexB; i++)
	{
		num_to_str(playerArray[1][i], szNum, 2)
		get_user_name(playerArray[1][i], szName, 32)
		menu_additem(menu, szName, szNum)
	}
	
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public MikrofonHraciMenu_Handler(id, menu, item)
{
	if( item == MENU_EXIT )
	{
		MikrofonMenu(id)
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}

	new data[6], szName[64], access, callback
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);
	
	new hracId = str_to_num(data)
	if (!zivyHrac(hracId))
	{
		MikrofonHraciMenu(id)
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	if (get_bit(g_Mikrofon, hracId))
	{
		clear_bit(g_Mikrofon, hracId)
	} else
	{
		set_bit(g_Mikrofon, hracId)
	}

	get_user_name(hracId, szName, 32)	
	client_print(id, print_center, "Mikrofon hrace %s byl %s!", szName, get_bit(g_Mikrofon, hracId) ? "zapnut" : "vypnut")
	JailDHud(hracId, 0, {255, 255, 42}, 0, 0.5, 0.0, 0.2, 0.2, ".:[Mikrofon: %s]:.", get_bit(g_Mikrofon, hracId) ? "zapnut" : "vypnut")
	MikrofonHraciMenu(id)
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public PridelitVolnyDen(id)
{
	static szName[33]
	get_user_name(id, szName, 32)
	set_bit(g_VolnyDen, id)
	PlayerGlow(id, BarvyRGB[BARVA_ZLUTA])
	VolneDnyHudInfo()
}

public SpecialniDnyMenu(id)
{
	new menu = menu_create("\rSpecialni Dny", "SpecialniDnyMenuHandler")
	menu_additem(menu, "Volny Den", "")
	menu_additem(menu, g_ServerInfo[Special_Limit][SPECIAL_PRESTRELKA] ? "\dPrestrelka" : "Prestrelka", "")
	menu_additem(menu, g_ServerInfo[Special_Limit][SPECIAL_ONLYHS] ? "\dOnly HS" : "Only HS", "")
	menu_additem(menu, g_ServerInfo[Special_Limit][SPECIAL_SCHOVKA] ? "\dSchovka" : "Schovka", "")
	menu_additem(menu, g_ServerInfo[Special_Limit][SPECIAL_ONESHOT] ? "\dOne Shot": "One Shot", "")
	menu_additem(menu, g_ServerInfo[Special_Limit][SPECIAL_ARENA] ? "\dWeapon Arena" : "Weapon Arena", "")
	menu_additem(menu, g_ServerInfo[Special_Limit][SPECIAL_HONENA] ? "\dHonena" : "Honena", "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public SpecialniDnyMenuHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	static szName[33]
	get_user_name(id, szName, 32)
	switch(item)
	{
		case 0: 
		{
			VolnyDenAll()
		}
		case 1:
		{
			if(!g_ServerInfo[Special_Limit][SPECIAL_PRESTRELKA])
			{
				JailDHud(0, 3, BarvyRGB[BARVA_ZELENA], 1, 3.0, 2.0, 0.5, 0.5, "%s spustil Prestrelku", szName)
				g_ServerInfo[Special_Typ] = _:SPECIAL_PRESTRELKA
				SpecialniDny(0)
			} else
			{
				client_print(id, print_center, "Prestrelka se uz hrala!")
				SpecialniDnyMenu(id)
			}
		}
		case 2:
		{
			if(!g_ServerInfo[Special_Limit][SPECIAL_ONLYHS])
			{
				JailDHud(0, 3, BarvyRGB[BARVA_ZELENA], 1, 3.0, 2.0, 0.5, 0.5, "%s spustil Only HS", szName)
				g_ServerInfo[Special_Typ] = _:SPECIAL_ONLYHS
				SpecialniDny(0)
			} else
			{
				client_print(id, print_center, "!Only HS prestrelka se uz hrala!")
				SpecialniDnyMenu(id)
			}
		}
		case 3:
		{
			if(!g_ServerInfo[Special_Limit][SPECIAL_SCHOVKA])
			{
				JailDHud(0, 3, BarvyRGB[BARVA_ZELENA], 1, 3.0, 2.0, 0.5, 0.5, "%s spustil Schovku!", szName)
				g_ServerInfo[Special_Typ] = _:SPECIAL_SCHOVKA
				SpecialniDny(0)
			} else
			{
				client_print(id, print_center, "Schovka se uz hrala!")
				SpecialniDnyMenu(id)
			}
		}
		case 4:
		{
			if(!g_ServerInfo[Special_Limit][SPECIAL_ONESHOT])
			{
				JailDHud(0, 3, BarvyRGB[BARVA_ZELENA], 1, 3.0, 2.0, 0.5, 0.5, "%s spustil OneShot!", szName)
				g_ServerInfo[Special_Typ] = _:SPECIAL_ONESHOT
				SpecialniDny(0)
			} else
			{
				client_print(id, print_center, "One Shot se uz hral!")
				SpecialniDnyMenu(id)
			}
		}
		case 5:
		{
			if(g_ServerInfo[Special_Limit][SPECIAL_ARENA])
			{
				client_print(id, print_center, "Weapon Arena se jiz hrala!")
				SpecialniDnyMenu(id)
			} else
			{
				WeaponArenaMenu(id)
			}
		}	
		case 6:
		{
			if(!g_ServerInfo[Special_Limit][SPECIAL_HONENA])
			{
				JailDHud(0, 3, BarvyRGB[BARVA_ZELENA], 1, 3.0, 2.0, 0.5, 0.5, "%s spustil Honenou!", szName)
				g_ServerInfo[Special_Typ] = _:SPECIAL_HONENA
				SpecialniDny(0)
			} else
			{
				client_print(id, print_center, "Honena se uz hrala!")
				SpecialniDnyMenu(id)
			}
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public SpecialniDny(spustilId)
{
	g_ServerInfo[Posledni_Vezen] = 0
	g_ServerInfo[Odpocet_Cas] = 0
	if(task_exists(TASK_ODPOCET))
		remove_task(TASK_ODPOCET)
	
	switch(g_ServerInfo[Special_Typ])
	{
		case SPECIAL_PRESTRELKA:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_ZivyHrac, i))
					continue
				
				GunMenu(i)
				UchovatKnife(i)
				OdebratZbrane(i, 0)
				set_pev(i, pev_health, g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT ? VypocitatPrestrelkaHP() : 100.0)
			}
			
			g_ServerInfo[Special_Limit][SPECIAL_PRESTRELKA] = true
			g_ServerInfo[Odpocet_Cas] = 180
			set_task(1.0, "OdpocetSpecialu", TASK_ODPOCET, _, _, "b")
		}
		case SPECIAL_ONLYHS:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_ZivyHrac, i))
					continue

				GunMenu(i)	
				UchovatKnife(i)
				OdebratZbrane(i, 0)
				set_pev(i, pev_health, g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT ? VypocitatPrestrelkaHP() : 100.0)
			}
			
			g_ServerInfo[Special_Limit][SPECIAL_ONLYHS] = true
			g_ServerInfo[Odpocet_Cas] = 180
			set_task(1.0, "OdpocetSpecialu", TASK_ODPOCET, _, _, "b")
		}
		case SPECIAL_SCHOVKA:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_ZivyHrac, i))
					continue
				
				UchovatKnife(i)
				OdebratZbrane(i, 0)
				UkoncitMenu(i)
				set_pev(i, pev_health, 100.0)
				
				switch(g_MojeInfo[i][Hrac_Team])
				{
					case CS_TEAM_T:
					{
						if (g_MojeInfo[i][VIP_Hat_Id] != -1)
							SmazatHat(i)
						
						g_MojeInfo[i][Schovka_Model] = -1
						SchovkaMainMenu(i)
						NastavitRychlost(i, 400.0)
					}
					case CS_TEAM_CT:
					{
						SetFreezeEnt(i, true)
						SchovkaFadeIn(i)
					}
				}
			}
			
			g_ServerInfo[Odpocet_Cas] = 20
			g_fwPlayerThink_Post = register_forward(FM_PlayerPostThink, "Schovka_Post_Think")
			set_task(1.0, "SchovkaCasNaUkryti", TASK_ODPOCET, _, _, "b")
			g_ServerInfo[Special_Limit][SPECIAL_SCHOVKA] = true
			g_ServerInfo[Special_Spusten] = false
		}
		case SPECIAL_ONESHOT:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_ZivyHrac, i))
					continue
				
				UchovatKnife(i)
				OdebratZbrane(i, 1)
				UkoncitMenu(i)
				set_pev(i, pev_health, 100.0)
				
				new deagle = fm_give_item(i, "weapon_deagle")
				if (pev_valid(deagle))
					fm_cs_set_weapon_ammo(deagle, 1)
					
				fm_cs_set_user_bpammo(i, CSW_DEAGLE, 1)
			}
			
			g_fwUpdateClient = register_forward(FM_UpdateClientData, "fw_UpdateClientData_Post", 1)
			g_ServerInfo[Special_Spusten] = true
			g_ServerInfo[Special_Limit][SPECIAL_ONESHOT] = true
			g_ServerInfo[Odpocet_Cas] = 180
			set_task(1.0, "OdpocetSpecialu", TASK_ODPOCET, _, _, "b")
		}
		case SPECIAL_ARENA:
		{	
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_ZivyHrac, i))
					continue

				UchovatKnife(i)
				OdebratZbrane(i, 0)
				UkoncitMenu(i)
				fm_give_item(i, ZbraneGun[g_ServerInfo[ArenaGun]])
				fm_cs_set_user_bpammo(i, ZbraneCSW[g_ServerInfo[ArenaGun]], ZbraneAmmo[g_ServerInfo[ArenaGun]])
				set_pev(i, pev_health, g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT ? VypocitatPrestrelkaHP() : 100.0)
			}
			if(spustilId) 
			{
				static iName[33]
				get_user_name(spustilId, iName, 32)
				JailDHud(0, 3, BarvyRGB[BARVA_ZELENA], 1, 3.0, 2.0, 0.5, 0.5, "%s spustil %s arenu!", iName, ZbraneNazvy[g_ServerInfo[ArenaGun]])
			}
			
			g_ServerInfo[Special_Limit][SPECIAL_ARENA] = true
			g_ServerInfo[Odpocet_Cas] = 180
			set_task(1.0, "OdpocetSpecialu", TASK_ODPOCET, _, _, "b")
		}
		case SPECIAL_HONENA:
		{
			for(new i = 1; i <= MAX_HRACU; i++)
			{
				if(!get_bit(g_ZivyHrac, i))
					continue
					
				UchovatKnife(i)
				UkoncitMenu(i)
				fm_strip_user_weapons(i)
				set_pev(i, pev_health, 100.0)
				
				if(g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT)
				{
					SetFreezeEnt(i, true) 
					g_MojeInfo[i][Special_Killu] = 0
				} else
				{
					g_MojeInfo[i][Honena_Vezen_Zivotu] = 3
				}
				
				g_MojeInfo[i][Honena_Odpocet] = 0
				g_MojeInfo[i][Honena_ItemBox_Odmena] = ODMENA_ZADNA
				g_MojeInfo[i][Honena_VodniGun_Effekt] = 0
			}
			
			SmazatMapEntity("func_door")
			SmazatMapEntity("func_door_rotating")
			
			g_fwPlayerThink = register_forward(FM_PlayerPreThink, "Honena_PlayerPreThink")
			g_fwUpdateClient = register_forward(FM_UpdateClientData, "fw_UpdateClientData_Post", 1)
			
			g_ServerInfo[HonickaThinkEntita] = fm_create_entity("info_target")
			set_pev(g_ServerInfo[HonickaThinkEntita], pev_classname, "HonenaPlayerTicker")
			set_pev(g_ServerInfo[HonickaThinkEntita], pev_nextthink, get_gametime() + 1.0)
			register_think("HonenaPlayerTicker", "HonenaPlayerTicker_Handler")
			
			#if defined DEBUG
			g_ServerInfo[Odpocet_Cas] = 2
			#else
			g_ServerInfo[Odpocet_Cas] = 20
			#endif
			
			set_task(1.0, "HonenaOdpocetStart", TASK_ODPOCET, _, _, "b")
			
			g_ServerInfo[Special_Limit][SPECIAL_HONENA] = true
			g_ServerInfo[Special_Spusten] = false
		}
	}
	
	SmazatBalon()
	DespawnoutDealera()
	ZrusitSimona()
	
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_VolnyDen, i))
			continue
				
		clear_bit(g_VolnyDen, i)
		fm_set_user_rendering(i)
		set_bit(g_AutoVolnyDen, i)
	}
	
	g_ServerInfo[PosledniSpecial] = _:get_gametime()
	set_task(1.0, "SmazatVsechnyEntity")
	OtevritCely()
}

public ZrusitSimona()
{
	if (!g_ServerInfo[Simon_Id])
		return
		
	static simonId
	simonId = g_ServerInfo[Simon_Id]
	g_ServerInfo[Simon_Id] = 0
	AktualizovatModel(simonId)
	UkoncitSimonFunkce()
}

public HonenaOdpocetStart(TaskID)
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		if (g_ServerInfo[Odpocet_Cas] <= 10)
			OdpocetZvuk(0, g_ServerInfo[Odpocet_Cas])
		
		JailHud(0, 6, BarvyRGB[BarvaOdpoctu(g_ServerInfo[Odpocet_Cas], 20, 10, 5)],0, 1.2, 0.0, 0.1, 0.1, "[-- Honena --]^n[-- Do zacatku zbyva %d sekund --]", g_ServerInfo[Odpocet_Cas])
		g_ServerInfo[Odpocet_Cas]--
		return
	}

	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i))
			continue
		
		if (g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT)
			SetFreezeEnt(i, false)
		
		set_pev(i, pev_gravity, HONENA_GRAVITACE)
		NastavitRychlost(i, HONENA_RYCHLOST)
	}
		
	remove_task(TaskID)
	g_ServerInfo[Odpocet_Cas] = 180
	set_task(1.0, "HonenaOdpocetKonce", TASK_ODPOCET, _, _, "b")
	set_task(random_float(10.0, 20.0), "VytvoritHonickaBox", TASK_HONENABOX)	
	g_ServerInfo[Special_Spusten] = true
}

public HonenaOdpocetKonce(taskId)
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		JailHud(0, 8, BarvyRGB[BarvaOdpoctu(g_ServerInfo[Odpocet_Cas], 180, 60, 30)], 0, 1.0, 0.0, 0.1, 0.1, "[-- >> Honena << --]^n[-- %s | %d / %d --]", FormatovatSekundy(g_ServerInfo[Odpocet_Cas]), g_ServerInfo[HonickaChycenych], g_ZivychHracu[CS_TEAM_T])
		if (g_ServerInfo[Odpocet_Cas] <= 30)
			OdpocetZvuk(0, g_ServerInfo[Odpocet_Cas])
			
		g_ServerInfo[Odpocet_Cas]--
		return
	}
	
	JailDHud(0, 1, BarvyRGB[BARVA_CERVENA], 0, 2.0, 0.0, 0.5, 0.5, ">> Vezni jsou vitezem teto hry <<")
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i))
			continue
			
		switch(g_MojeInfo[i][Hrac_Team])
		{
			case CS_TEAM_T: PridatBody(i, g_MojeInfo[i][Honena_Vezen_Zivotu] ? (10 + (g_MojeInfo[i][Honena_Vezen_Zivotu] * 5)) : 10)
			case CS_TEAM_CT: user_silentkill(i)
		}
	}
	
	remove_task(taskId)
}

public ZmrazitHrace(id)
{
	NastavitRychlost(id, 1.0)
	set_pev(id, pev_velocity, Float: {0.0, 0.0, 0.0})
	set_task(0.5, "ZmrazitHraceFunc", id + TASK_ICECUBE)
}

public ZmrazitHraceFunc(taskid)
{	
	static id, Float: flOrigin[3]
	id = taskid - TASK_ICECUBE

	set_bit(g_Zmrazen, id)
	fm_set_user_rendering(id)
	
	pev(id, pev_v_angle, g_MojeInfo[id][Honena_AimLock_Angles])
	set_pev(id, pev_fixangle, 1)
	
	pev(id, pev_origin, flOrigin)
	flOrigin[2] -= pev(id, pev_flags) & FL_DUCKING ? 15.0 : 35.0
	
	static ent
	ent	= create_entity("info_target")
	set_pev(ent, pev_classname, EntityName[Entity:ENTITA_ICECUBE])
	set_pev(ent, pev_movetype, MOVETYPE_FLY)
	set_pev(ent, pev_solid, SOLID_BBOX)
	set_pev(ent, pev_iuser1, id)
	SetSubModel(ent, ENTITA_ICECUBE)
	engfunc(EngFunc_SetSize, ent, Float:{-15.0, -15.0, -8.0 }, Float:{ 15.0, 15.0, 80.0 })
	engfunc(EngFunc_SetOrigin, ent, flOrigin)
	set_pev(ent, pev_nextthink, get_gametime() + 5.0)
	PridatEntituDoArray(ent, ENTITA_ICECUBE)

	if (task_exists(id + TASK_HONENAODPOCET))
		remove_task(id + TASK_HONENAODPOCET)
	
	if (get_bit(g_VodniPistol, id))	
	{
		fm_strip_user_weapons(id)
		clear_bit(g_VodniPistol, id)
	}
	
	emit_sound(ent, CHAN_BODY, RuzneZvuky[8], 1.0, ATTN_NORM, 0, PITCH_NORM)
	message_begin(MSG_ONE, g_MsgScreenFade, _, id)
	write_short(0) 										// duration
	write_short(0) 										// hold time
	write_short(0x0004) 								// fade type
	write_byte(0) 										// red
	write_byte(50) 										// green
	write_byte(200) 									// blue
	write_byte(150) 									// alpha
	message_end()
	
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, flOrigin, 0)
	write_byte(TE_BEAMCYLINDER) 						// TE id
	write_coord_fl(flOrigin[0]) 						// x
	write_coord_fl(flOrigin[1]) 						// y
	write_coord_fl(flOrigin[2]) 						// z
	write_coord_fl(flOrigin[0]) 						// x axis
	write_coord_fl(flOrigin[1]) 						// y axis
	write_coord_fl(flOrigin[2] + 470.0) 				// z axis
	write_short(g_sprIceCubeExploze) 					// sprite
	write_byte(0) 										// startframe
	write_byte(0) 										// framerate
	write_byte(4) 										// life
	write_byte(60) 										// width
	write_byte(0) 										// noise
	write_byte(0) 										// red
	write_byte(191) 									// green
	write_byte(255) 									// blue
	write_byte(200) 									// brightness
	write_byte(0) 										// speed
	message_end()
	
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, flOrigin, 0)
	write_byte(TE_BEAMCYLINDER) 						// TE id
	write_coord_fl(flOrigin[0]) 						// x
	write_coord_fl(flOrigin[1]) 						// y
	write_coord_fl(flOrigin[2]) 						// z
	write_coord_fl(flOrigin[0])							// x axis
	write_coord_fl(flOrigin[1]) 						// y axis
	write_coord_fl(flOrigin[2] + 555.0) 				// z axis
	write_short(g_sprIceCubeExploze) 					// sprite
	write_byte(0) 										// startframe
	write_byte(0) 										// framerate
	write_byte(4) 										// life
	write_byte(60) 										// width
	write_byte(0) 										// noise
	write_byte(0) 										// red
	write_byte(191) 									// green
	write_byte(255) 									// blue
	write_byte(200) 									// brightness
	write_byte(0) 										// speed
	message_end()
	
	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, flOrigin, 0)
	write_byte(TE_DLIGHT) 								// TE id
	write_coord_fl(flOrigin[0]) 						// x
	write_coord_fl(flOrigin[1]) 						// y
	write_coord_fl(flOrigin[2]) 						// z
	write_byte(50) 										// radio
	write_byte(0) 										// red
	write_byte(191) 									// green
	write_byte(255) 									// blue
	write_byte(30) 										// vida en 0.1, 30 = 3 segundos
	write_byte(30) 										// velocidad de decaimiento
	message_end()
	
	engfunc(EngFunc_MessageBegin, MSG_BROADCAST,SVC_TEMPENTITY, flOrigin, 0)
	write_byte(TE_EXPLOSION)
	write_coord_fl(flOrigin[0])							// x axis
	write_coord_fl(flOrigin[1]) 						// y axis
	write_coord_fl(flOrigin[2] + 10.0) 					// z axis
	write_short(g_sprIceCubeExploze)					// sprite index
	write_byte(17)										// scale in 0.1s
	write_byte(15)										// framerate
	write_byte(TE_EXPLFLAG_NOSOUND)						// flags
	message_end();
	
	engfunc(EngFunc_MessageBegin, MSG_BROADCAST,SVC_TEMPENTITY, flOrigin, 0)
	write_byte(TE_SPRITETRAIL) 							// TE ID
	write_coord_fl(flOrigin[0]) 						// x axis
	write_coord_fl(flOrigin[1]) 						// y axis
	write_coord_fl(flOrigin[2] + 40.0) 					// z axis
	write_coord_fl(flOrigin[0]) 						// x axis
	write_coord_fl(flOrigin[1]) 						// y axis
	write_coord_fl(flOrigin[2]) 						// z axis
	write_short(g_sprIceCubeFrostgib) 					// Sprite Index
	write_byte(30) 										// Count
	write_byte(10) 										// Life
	write_byte(4) 										// Scale
	write_byte(50) 										// Velocity Along Vector
	write_byte(10) 										// Rendomness of Velocity
	message_end();
}

public HonenaTouchFunkce(victim, attacker)
{
	if (g_ServerInfo[Special_Typ] != SPECIAL_HONENA || !g_ServerInfo[Special_Spusten] || !zivyHrac(victim) || !zivyHrac(attacker) || g_MojeInfo[victim][Hrac_Team] != CS_TEAM_T)
		return PLUGIN_CONTINUE
	
	switch(g_MojeInfo[attacker][Hrac_Team])
	{
		case CS_TEAM_T:
		{
			if(g_MojeInfo[victim][Honena_Chycen])
			{		
				g_MojeInfo[victim][Honena_Chycen] = false
				set_pev(victim, pev_gravity, HONENA_GRAVITACE)
				NastavitRychlost(victim, HONENA_RYCHLOST)
				fm_set_user_rendering(victim)
				g_ServerInfo[HonickaChycenych]--
			}
		}
		case CS_TEAM_CT:
		{
			if(!g_MojeInfo[victim][Honena_Chycen])
			{
				if (task_exists(victim + TASK_HONENAODPOCET))
					remove_task(victim + TASK_HONENAODPOCET)
				
				if (get_bit(g_VodniPistol, victim))
				{
					fm_strip_user_weapons(victim)
					clear_bit(g_VodniPistol, victim)
				}
				
				g_MojeInfo[victim][Honena_Vezen_Zivotu]--
				if (g_MojeInfo[victim][Honena_Vezen_Zivotu] <= 0)
				{
					ZabitBezPripsani(victim)
					g_ServerInfo[HonickaChycenych]--
				} else
				{
					g_MojeInfo[victim][Honena_Odpocet] = 30
					g_MojeInfo[victim][Honena_Chycen] = true
					PlayerGlow(victim, {255, 0, 10})
					NastavitRychlost(victim, 1.0)
					set_pev(victim, pev_gravity, 5.0)
					set_pev(victim, pev_velocity, Float:{0.0, 0.0, 0.0});	
					g_ServerInfo[HonickaChycenych]++
					KontrolaUkonceniHonicky()
				}
			}
		}
	}
	
	return PLUGIN_CONTINUE
}

public Honena_PlayerPreThink( id )
{
	if (get_bit(g_Zmrazen, id))
	{
		set_pev(id , pev_v_angle , g_MojeInfo[id][Honena_AimLock_Angles])
		set_pev(id , pev_fixangle , 1)
	}
	
	if (g_MojeInfo[id][Honena_ItemBox_Odmena] == ODMENA_NEVIDITELNOST)
	{
		set_pev(id, pev_flTimeStepSound, 999)
	}
	
	return FMRES_IGNORED
}

public HonenaPridelitOdmenu(id, ent)
{
	static Float: flOrigin[3]
	pev(ent, pev_origin, flOrigin)
	SmazatEntitu(ent)
	
	new odmena = random_num(1, 4)
	switch(odmena)
	{
		case 1:	NastavitRychlost(id, HONENA_RYCHLOST + 200.0)
		case 2: fm_set_user_rendering(id, kRenderFxNone, 0, 0, 0, kRenderTransAlpha, 80)
		case 3: PridelitVodniGun(id)
		case 4: ZmrazitHrace(id)
	}
	
	if (odmena != 4)
	{
		emit_sound(0, CHAN_AUTO, RuzneZvuky[7], 1.0, ATTN_NORM, 0, PITCH_NORM)
	
		engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, flOrigin, 0)
		write_byte(TE_SPRITE)								// message
		write_coord_fl(flOrigin[0])    						// position
		write_coord_fl(flOrigin[1])							// position
		write_coord_fl(flOrigin[2])							// position
		write_short(g_sprItemBox) 							// sprite index
		write_byte(16)			  							// scale
		write_byte(150)           							// brightness
		message_end() 
	}
	
	if (odmena == 1 || odmena == 2)
	{
		g_MojeInfo[id][Honena_Odpocet] = 10
		g_MojeInfo[id][Honena_ItemBox_Odmena] = HonenaOdmena:odmena	
		HonenaOdmenaOdpocet(id + TASK_HONENAODPOCET)
		set_task(1.0, "HonenaOdmenaOdpocet", id + TASK_HONENAODPOCET, _, _, "b")
	}
}

public PridelitVodniGun(id)
{
	set_bit(g_VodniPistol, id)
	new vodniGun = fm_give_item(id,"weapon_mp5navy")
	if( vodniGun > 0 )
	{
		fm_cs_set_weapon_ammo(vodniGun, 100)
		fm_cs_set_user_bpammo (id, CSW_MP5NAVY, 0)	
		set_pdata_float(id, OFFSET_NEXTATTACK, 1.0, EXTRAOFFSET)
	}
}

public HonenaOdmenaOdpocet(TaskID)
{
	static id; id = TaskID - TASK_HONENAODPOCET
	if(g_MojeInfo[id][Honena_Odpocet] > 0)
	{
		JailHud(id, 6, {0, 255, 0}, 0, 1.2, 0.0, 0.0,0.0, "[-- %s --]^nSchopnost vyprsi za %d sekund!", HonenaBoxyOdmena[g_MojeInfo[id][Honena_ItemBox_Odmena]], g_MojeInfo[id][Honena_Odpocet])
		g_MojeInfo[id][Honena_Odpocet]--
		return
	}

	switch(g_MojeInfo[id][Honena_ItemBox_Odmena])
	{
		case ODMENA_GRAVITACE, ODMENA_PAST: 
		{
			NastavitRychlost(id, HONENA_RYCHLOST)
			set_pev(id, pev_gravity, HONENA_GRAVITACE)
		}
		case ODMENA_NEVIDITELNOST:
		{
			fm_set_user_rendering(id)
		}
	}
	
	remove_task(TaskID)
	g_MojeInfo[id][Honena_ItemBox_Odmena] = _:ODMENA_ZADNA
}

public KontrolaUkonceniHonicky()
{
	if(g_ServerInfo[HonickaChycenych] == g_ZivychHracu[CS_TEAM_T])
	{
		JailDHud(0, 1, BarvyRGB[BARVA_MODRA], 0, 2.0, 0.0, 0.5, 0.5, ">> Bachari jsou vitezem teto hry <<")
		for(new i = 1; i <= MAX_HRACU; i++)
		{
			if(!get_bit(g_ZivyHrac, i))
				continue
				
			switch(g_MojeInfo[i][Hrac_Team])
			{
				case CS_TEAM_CT: PridatBody(i, g_MojeInfo[i][Special_Killu] ? (10 + (g_MojeInfo[i][Special_Killu] * 5)) : 10)
				case CS_TEAM_T: user_silentkill(i)
			}
		}
	}
}

public bool: HonenaVolnySpawn(spawnIndex)
{
	static ent; ent = -1
	while ((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", EntityName[ENTITA_ITEM_BOX])))
	{
		if (!pev_valid(ent))
			continue
			
		if (pev(ent, pev_iuser1) == spawnIndex)
			return false
	}
	
	return true
}
	
public SchovkaCasNaUkryti(taskId)
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		if(g_ServerInfo[Odpocet_Cas] <= 20)
			OdpocetZvuk(0, g_ServerInfo[Odpocet_Cas])
		
		JailHud(0, 6, BarvyRGB[BarvaOdpoctu(g_ServerInfo[Odpocet_Cas], 40, 20, 5)], 0, 1.0, 0.0, 0.1, 0.1, "[-- Schovka --]^n[-- Vezni maji %d sekund na ukryti! --]", g_ServerInfo[Odpocet_Cas])
		g_ServerInfo[Odpocet_Cas]--
		return
	}
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i))
			continue
				
		if (g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT)
		{
			GunMenu(i)
			SetFreezeEnt(i, false)
			SchovkaFadeOut(i)
		} else
		{
			NastavitRychlost(i, 0.0)
		}
	}
	
	remove_task(TASK_ODPOCET)
	g_ServerInfo[Special_Spusten] = true
	g_ServerInfo[Odpocet_Cas] = 180
	set_task(1.0, "SchovkaLimit", TASK_ODPOCET, _, _, "b")
}

public SchovkaLimit(TaskID)
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		if (g_ServerInfo[Odpocet_Cas] <= 20)
			OdpocetZvuk(0, g_ServerInfo[Odpocet_Cas])
			
		JailHud(0, 8, BarvyRGB[BarvaOdpoctu(g_ServerInfo[Odpocet_Cas], 180, 60, 30)], 0, 1.0, 0.0, 0.1, 0.1, "[-- >> Schovka << --]^n[-- %s --]", FormatovatSekundy(g_ServerInfo[Odpocet_Cas]))
		g_ServerInfo[Odpocet_Cas]--
		return
	}
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(get_bit(g_ZivyHrac, i) && g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT)
			user_silentkill(i)
	}
		
	JailDHud(0, 0, BarvyRGB[BARVA_CERVENA], 0, 2.0, 0.0, 0.5, 0.5, ">> Vyprsel casovy limit! Vezni se stali vitezi!<<")
	remove_task(TASK_ODPOCET)
}

public SchovkaMainMenu(id)
{
	new szBuffer[112]
	format(szBuffer, 111, "\rSchovka^n\y>> Model: [\d%s\y]^n\y>> \rOtaceni pomoci E!", g_MojeInfo[id][Schovka_Model] == -1 ? "Zadny" : Schovka_Model_Nazey[g_MojeInfo[id][Schovka_Model]])
	new menu = menu_create(szBuffer, "SchovkaMainMenu_Handler")
	
	menu_additem(menu, "Vybrat model", "")
	
	format(szBuffer, 111, "^n\dPohled^n\y-----------------------")
	menu_addtext(menu, szBuffer, -1)
	
	for (new i = 0; i < 4; i++)
	{
		format(szBuffer, 111, "%s%s", g_MojeInfo[id][Schovka_Kamera] == i ? "\r" : "", ViewMod_Nazev[i])
		menu_additem(menu, szBuffer, "")
	}
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public SchovkaMainMenu_Handler(id, menu, item)
{
	menu_destroy(menu)
	if (item != MENU_EXIT)
	{
		switch (item)
		{
			case 0: SchovkaModelMenu(id)
			case 1, 2, 3, 4:
			{
				ZmenitPohled(id, item - 1)
				SchovkaMainMenu(id)
			}
		}
	}
}

public ZmenitPohled(id, camera)
{
	if (g_MojeInfo[id][Schovka_Kamera] != camera)
	{
		g_MojeInfo[id][Schovka_Kamera] = camera
		set_view(id, camera)
	}
}

public SchovkaModelMenu(id)
{
	new menu = menu_create("\rVyber model", "SchovkaModelMenu_Handler")
	menu_additem(menu, "\rZadny", "")
	for (new i = MULTIMODEL_SCHOVKA_START; i <= MULTIMODEL_SCHOVKA_KONEC; i++)
		menu_additem(menu, Schovka_Model_Nazey[i - MULTIMODEL_SCHOVKA_START], "")
	
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpatky")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public SchovkaModelMenu_Handler(id, menu, item)
{
	menu_destroy(menu)
	if (item != MENU_EXIT)
	{
		if (item == 0)
		{
			g_MojeInfo[id][Schovka_Model] = -1
			if (get_bit(g_Schovan, id))
				OdmaskovatHrace(id)
		} else
		{
			NasaditSchovkaModel(id, item - 1)
		}
		
		SchovkaMainMenu(id)
	}
}

public NasaditSchovkaModel(id, model)
{
	g_MojeInfo[id][Schovka_Model] = model
	
	new Float: flOrigin[3]
	pev(id, pev_origin, flOrigin)
	flOrigin[2] -= (pev(id, pev_flags) & FL_DUCKING) ? 18.0 : 36.0
	
	if (g_MojeInfo[id][Schovka_Entity] != 0)
	{
		g_MojeInfo[id][Schovka_Angle] = _:0.0
		set_pev(g_MojeInfo[id][Schovka_Entity], pev_body, MULTIMODEL_SCHOVKA_START + model)
		set_pev(g_MojeInfo[id][Schovka_Entity], pev_origin, flOrigin)
		return
	}
	
	new ent = create_entity("info_target")
	set_pev(ent, pev_classname, "SchovkaPrevlekEnt")
	engfunc(EngFunc_SetModel, ent, ModelyEntit[1])
	set_pev(ent, pev_body, MULTIMODEL_SCHOVKA_START + model)
	set_pev(ent, pev_origin, flOrigin)
	set_pev(ent, pev_movetype, MOVETYPE_FLY)
	set_pev(ent, pev_solid, SOLID_NOT)

	if (g_MojeInfo[id][Schovka_Angle] != 0.0)
	{
		new Float: flAngles[3]
		flAngles[1] = g_MojeInfo[id][Schovka_Angle]
		set_pev(ent, pev_angles, flAngles)
	}

	g_MojeInfo[id][Schovka_Entity] = ent
	
	fm_set_user_rendering(id, kRenderFxNone, 0, 0, 0, kRenderTransAlpha, 0)
	set_bit(g_Schovan, id)
}

public OdmaskovatHrace(id)
{
	if (g_MojeInfo[id][Schovka_Model] != -1)
	{
		new Float: flAngles[3]
		pev(g_MojeInfo[id][Schovka_Entity], pev_angles, flAngles)
		g_MojeInfo[id][Schovka_Angle] = _:flAngles[1]
	}
	
	engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][Schovka_Entity])
	fm_set_user_rendering(id, kRenderFxNone, 0, 0, 0, kRenderTransAlpha, 255)
	clear_bit(g_Schovan, id)
	g_MojeInfo[id][Schovka_Entity] = 0
}

public Schovka_Post_Think(id)
{
	if(g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT || g_MojeInfo[id][Schovka_Model] == -1) 
		return FMRES_IGNORED
	
	static Float: flVec[3]
	pev(id, pev_velocity, flVec)
	if (get_bit(g_Schovan, id))
	{
		if (vector_length(flVec) > 5.0 || !(pev(id, pev_flags) & FL_ONGROUND))
		{
			OdmaskovatHrace(id)
		} else if (pev(id, pev_button) & IN_USE)
		{
			static Float: flPlrAngles[3], Float: flEntAngles[3]
			pev(id, pev_angles, flPlrAngles)
			pev(g_MojeInfo[id][Schovka_Entity], pev_angles, flEntAngles)
			flEntAngles[1] = flPlrAngles[1]
			g_MojeInfo[id][Schovka_Angle] = _:flPlrAngles[1]
			set_pev(g_MojeInfo[id][Schovka_Entity], pev_angles, flEntAngles)
		}
	} else
	{
		if (vector_length(flVec) < 5.0)
		{
			NasaditSchovkaModel(id, g_MojeInfo[id][Schovka_Model])
		}
	}
	
	return FMRES_IGNORED
}

public WeaponArenaMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	menu = menu_create("\rArena", "WeaponArenaMenuHandler")
	for(new i = 0; i < sizeof(ArenaGuns); i++)
		menu_additem(menu, ZbraneNazvy[ArenaGuns[i]], "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public WeaponArenaMenuHandler(id, menu, item)
{
	if (item < sizeof(ArenaGuns))
	{
		g_ServerInfo[ArenaGun] = ArenaGuns[item]
		g_ServerInfo[Special_Typ] = _:SPECIAL_ARENA
		SpecialniDny(id)
	}
	
	return PLUGIN_HANDLED
}

public HryMenu(id)
{
	new menu = menu_create("\rHerni Menu", "HryMenuHandler")
	
	menu_additem(menu, g_ServerInfo[Hry_Stopky_Bezi] ? "\rStopky" : "\wStopky", "")
	menu_additem(menu, "Team", "")
	menu_additem(menu, "Odpocet", "")
	menu_additem(menu, g_ServerInfo[Hry_Spray_Metr] ? "\rSpray Metr" : "\wSpray Metr", "")
	menu_additem(menu, g_ServerInfo[Hry_SimonKroky] ? "\rStopy" : "\wStopy", "")
	menu_additem(menu, "Balon", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public HryMenuHandler(id, menu, item)
{
	menu_destroy(menu)
	switch(item)
	{
		case 0:
		{
			if(g_ServerInfo[Hry_Stopky_Bezi]) VypnoutStopky(false); else SpustitStopky()
			HryMenu(id)
		}
		case 1: RozdeleniVeznuMenu(id)
		case 2: OdpocetMenu(id)
		case 3: 
		{
			g_ServerInfo[Hry_Spray_Metr] = g_ServerInfo[Hry_Spray_Metr] ? false : true
			client_print(id, print_center, "Mereni vysky spraye %s", g_ServerInfo[Hry_Spray_Metr] ? "zapnut" : "vypnut")
			HryMenu(id)
		}
		case 4:
		{
			g_ServerInfo[Hry_SimonKroky] = g_ServerInfo[Hry_SimonKroky] ? false : true
			client_print(id, print_center, "Stopy jsou %s!", g_ServerInfo[Hry_SimonKroky] ? "zapnuty" : "vypnuty")
			HryMenu(id)	
		}
		case 5:
		{
			RespawnoutBalon()
			client_print(id, print_center, "Balon byl respawnut!")
		}			
	}
}

public PocetTeamVeznu()
{
	new pocet
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if (g_MojeInfo[i][Hrac_Team] == CS_TEAM_T && g_MojeInfo[i][VezenTym] != VEZEN_TEAM_ZADNY)
			pocet++
	}
	
	return pocet
}

public RozdeleniVeznuMenu(id)
{
	static buffer[128]
	format(buffer, 127, "\rRozdeleni veznu^n^n\y>> \wZeleny\d: %d^n\y>> \wRuzovy\d: %d", PocetTeamHracu(VEZEN_TEAM_ZELENY), PocetTeamHracu(VEZEN_TEAM_RUZOVY))
	new menu = menu_create(buffer, "RozdeleniVeznuMenu_Handler")
	
	format(buffer, 127, "%sNahodne", (g_ZivychHracu[CS_TEAM_T] == 0 || PocetTeamVeznu()) ? "\d" : "\w")
	menu_additem(menu, buffer, "")
	
	format(buffer, 127, "%sManualni", PocetVeznuBezVD() < 2 ? "\d" : "\w")
	menu_additem(menu, buffer, "")
	
	format(buffer, 127, "%sZrusit", PocetTeamVeznu() ? "\w" : "\d")
	menu_additem(menu, buffer, "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\yZavrit", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER)
	menu_display(id, menu, 0)
}

public RozdeleniVeznuMenu_Handler(id, menu, item)
{
	menu_destroy(menu)
	switch (item)
	{
		case 0:
		{
			static pocetVeznuBezVD
			pocetVeznuBezVD = PocetVeznuBezVD();
			if (PocetTeamVeznu())
			{
				client_print(id, print_center, "Vezni jsou jiz rozdeleni!")
				RozdeleniVeznuMenu(id)
			} else if (pocetVeznuBezVD == 0 || pocetVeznuBezVD % 2 != 0)
			{
				client_print(id, print_center, "Veznu musi byt sudy pocet!")
				RozdeleniVeznuMenu(id)
			} else 
			{
				RozdelitVezne(id)
				RozdeleniVeznuMenu(id)
			}
		}
		case 1:
		{
			if (PocetVeznuBezVD() >= 2)
			{
				TeamHraci_Menu(id)
			} else
			{
				client_print(id, print_center, "Neni dostatecny pocet veznu!")
				RozdeleniVeznuMenu(id)
			}
		}
		case 2:
		{
			if (PocetTeamVeznu())
			{
				ZrusitRozdeleni(id)
			} else 
			{
				client_print(id, print_center, "Vezni nejsou rozdeleni!")
			}
			
			RozdeleniVeznuMenu(id)
		}
		case 3:
		{
			HryMenu(id)
		}
	}
}

public TeamHraciItemBarva(id, buffer[], delka)
{
	static name[33]
	get_user_name(id, name, 32)
	switch (g_MojeInfo[id][VezenTym])
	{
		case VEZEN_TEAM_ZADNY: format(buffer, delka, "\w%s", name)
		case VEZEN_TEAM_ZELENY: format(buffer, delka, "\y%s", name)
		case VEZEN_TEAM_RUZOVY: format(buffer, delka, "\r%s", name)
	}
}

public TeamHraci_Menu(id)
{
	static szStr[3], szBuffer[33] 
	new menu = menu_create("Vyber hrace", "TeamHraci_Menu_Handler")
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T || get_bit(g_VolnyDen, i))
			continue
			
		num_to_str(i, szStr, 2)
		TeamHraciItemBarva(i, szBuffer, 32)
		menu_additem(menu, szBuffer, szStr)
	}
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public TeamHraci_Menu_Handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu);
		RozdeleniVeznuMenu(id)
		return PLUGIN_HANDLED;
	}

	new data[6], szName[64], access, callback;
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);

	new teamHrac = str_to_num(data)
	if (zivyHrac(teamHrac))
	{
		HracTeam_Menu(id, teamHrac)
	} else
	{
		client_print(id, print_center, "Tento hrac jiz neni nazivu!")
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public HracTeam_Menu(id, teamHrac)
{
	static buffer[128], name[33]
	get_user_name(teamHrac, name, 32)
	format(buffer, 127, "\rPridelit Team^n^n\y>> Hrac: \d%s^n\y>> Team: \d%s", name, NazvyTeamu[g_MojeInfo[teamHrac][VezenTym]])
	new menu = menu_create(buffer, "HracTeam_Menu_Handler")

	menu_additem(menu, g_MojeInfo[teamHrac][VezenTym] == VEZEN_TEAM_ZELENY ? "\dZeleny" : "Zeleny", "")
	menu_additem(menu, g_MojeInfo[teamHrac][VezenTym] == VEZEN_TEAM_RUZOVY ? "\dRuzovy" : "Ruzovy", "")
	menu_additem(menu, g_MojeInfo[teamHrac][VezenTym] == VEZEN_TEAM_ZADNY ? "\dZrusit" : "Zrusit", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
	
	g_ServerInfo[Rozdeleni_Hrac_Id] = teamHrac
}

public HracTeam_Menu_Handler(id, menu, item)
{
	menu_destroy(menu)
	if (!zivyHrac(g_ServerInfo[Rozdeleni_Hrac_Id]))
	{
		g_ServerInfo[Rozdeleni_Hrac_Id] = 0
		client_print(id, print_center, "Tento hrac jiz neni nazivu!")
		TeamHraci_Menu(id)
		return
	}
	
	if (item == 2)
	{
		if (g_MojeInfo[g_ServerInfo[Rozdeleni_Hrac_Id]][VezenTym] != VEZEN_TEAM_ZADNY)
		{
			fm_set_user_rendering(g_ServerInfo[Rozdeleni_Hrac_Id])
			g_MojeInfo[g_ServerInfo[Rozdeleni_Hrac_Id]][VezenTym] = _:VEZEN_TEAM_ZADNY
			g_ServerInfo[Rozdeleni_Hrac_Id] = 0
			TeamHraci_Menu(id)
		} else
		{
			HracTeam_Menu(id, g_ServerInfo[Rozdeleni_Hrac_Id])
		}
		return
	}
	
	item++
	new VezenskyTeam: team = VezenskyTeam:item
	if (g_MojeInfo[g_ServerInfo[Rozdeleni_Hrac_Id]][VezenTym] == team)
		return
	
	new Barvy:barva = item == 1 ? BARVA_ZELENA : BARVA_RUZOVA
	PlayerGlow(g_ServerInfo[Rozdeleni_Hrac_Id], BarvyRGB[barva])
	ScreenFade(g_ServerInfo[Rozdeleni_Hrac_Id], BarvyRGB[barva], 100)
	g_MojeInfo[g_ServerInfo[Rozdeleni_Hrac_Id]][VezenTym] = _:team
	
	new szName[33]
	get_user_name(g_ServerInfo[Rozdeleni_Hrac_Id], szName, 32)
	client_print(id, print_center, "%s pridelen do %s teamu!", szName, NazvyTeamu[VezenskyTeam:item])
	TeamHraci_Menu(id)
	
	g_ServerInfo[Rozdeleni_Hrac_Id] = 0
}

public PocetVeznuBezVD()
{
	if (!g_VolnyDen)
	{
		return g_ZivychHracu[CS_TEAM_T]
	}
	
	new pocet = 0
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_VolnyDen, i))
			continue
			
		pocet++
	}
	
	return pocet
}

public RozdelitVezne(id)
{	
	static iTeamA, iTeamB, polovinaVeznu, priradit, Barvy: barva
	iTeamA = 0, iTeamB = 0, polovinaVeznu = (PocetVeznuBezVD() / 2)
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!zivyHrac(i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T || get_bit(g_VolnyDen, i))
			continue
			
		if(polovinaVeznu == iTeamA || polovinaVeznu == iTeamB)
		{
			if(polovinaVeznu == iTeamA)
				priradit = 1
			
			if(polovinaVeznu == iTeamB)
				priradit = 0
		}
		else
		{
			priradit = random_num(0, 1)
		}
		
		barva = priradit ? BARVA_ZELENA : BARVA_RUZOVA
		PlayerGlow(i, BarvyRGB[barva])
		ScreenFade(i, BarvyRGB[barva], 100)
		g_MojeInfo[i][VezenTym] = _:(priradit ? VEZEN_TEAM_ZELENY : VEZEN_TEAM_RUZOVY)
		priradit ? iTeamB++ : iTeamA++
	}
	
	client_print(id, print_center, "Vezni rozdeleni do dvou tymu!")
}

public ZrusitRozdeleni(id)
{
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(g_MojeInfo[i][VezenTym] == VEZEN_TEAM_ZADNY)
			continue
	
		fm_set_user_rendering(i)
		g_MojeInfo[i][VezenTym] = _:VEZEN_TEAM_ZADNY
	}
	
	if (id > 0)
		client_print(id, print_center, "Rozdeleni veznu bylo zruseno!")
}

public PocetTeamHracu(VezenskyTeam: team)
{
	new pocet
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (!zivyHrac(i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T || g_MojeInfo[i][VezenTym] != team)
			continue
		
		pocet++;
	}
	
	return pocet;
}

public SpustitStopky()
{
	g_ServerInfo[Hry_Stopky_Start] = _:get_gametime()
	g_ServerInfo[Hry_Stopky_Bezi] = true
}

public VypnoutStopky(bool: potichu)
{
	g_ServerInfo[Hry_Stopky_Bezi] = false
	if (!potichu)
		JailHud(0, 7, {125, 0, 255}, 0, 3.0, 0.0, 1.0, 1.0, "[-- %.1f --]", (get_gametime() - g_ServerInfo[Hry_Stopky_Start]))
}

public SprayMetr()
{
	if(!g_ServerInfo[Hry_Spray_Metr])
		return PLUGIN_CONTINUE
	
	static id
	id = read_data(2)
	if(g_MojeInfo[id][Hrac_Team] != CS_TEAM_T)
		return PLUGIN_CONTINUE
	
	static iOrigin[3], Float:vecDirection[3], Float:vecStop[3], Float:vecStart[3], szName[33], Float:vecOrigin[3]
	iOrigin[0] = read_data(3);
	iOrigin[1] = read_data(4);
	iOrigin[2] = read_data(5);
	IVecFVec(iOrigin, vecOrigin);
    
	velocity_by_aim(id, 5, vecDirection);
	xs_vec_add( vecOrigin, vecDirection, vecStop);
	xs_vec_mul_scalar(vecDirection, -1.0, vecDirection);
	xs_vec_add(vecOrigin, vecDirection, vecStart);
	
	engfunc(EngFunc_TraceLine, vecStart, vecStop, IGNORE_MONSTERS, -1, 0);
	get_tr2(0, TR_vecPlaneNormal, vecDirection);
	
	vecDirection[2] = 0.0;
	xs_vec_normalize(vecDirection, vecDirection);
	xs_vec_mul_scalar(vecDirection, 5.0, vecDirection);
	xs_vec_add(vecOrigin, vecDirection, vecStart);
	vecStop[2] -= 9999.0;
	
	engfunc(EngFunc_TraceLine, vecStart, vecStop, IGNORE_MONSTERS, -1, 0);
	get_tr2(0, TR_vecEndPos, vecStop);
    
	get_user_name(id, szName, 32);
	ChatColor(0, "SprayMetr! Hrac: !team%s !y- Vyska: !g%3.2f", szName, (vecStart[2] - vecStop[2]))

	return PLUGIN_CONTINUE
}

public OtevritCely()
{
	ExecuteHamB(Ham_Use, g_ServerInfo[Cely_Entita], 0, 0, 1, 1.0)
	entity_set_float(g_ServerInfo[Cely_Entita], EV_FL_frame, 0.0)
}

/*******************************************
			< PosledniPrani >
********************************************/
public cmdPosledniPrani(id)
{
	if (g_ServerInfo[Posledni_Vezen] == id)
	{
		PosledniPrani_HlavniMenu(id)
	} else
	{
		client_print(id, print_center, "Nejsi posledni vezen!")
	}
}

public PosledniPrani_HlavniMenu(id)
{
	new menu = menu_create("\rPosledni Prani", "PosledniPrani_HlavniMenuHandler")
	
	menu_additem(menu, "Volny Den", "")
	menu_additem(menu, "Box", "")
	menu_additem(menu, "Deagle", "")
	menu_additem(menu, "USP", "")
	menu_additem(menu, "Scout", "")
	menu_additem(menu, "Awp", "")
	menu_additem(menu, "Granaty", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\yAim Duely", "")
	
	static szBuffer[64]
	format(szBuffer, 63, "\yMod \d[\r%s\d]", NazvyDuelModu[g_ServerInfo[Duel_Mod]])
	menu_additem(menu, szBuffer, "")

	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")

	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public PosledniPrani_HlavniMenuHandler(id, menu, item)
{
	menu_destroy(menu);
	switch(item)
	{
		case 0:
		{
			ZabitBezPripsani(id)
			set_bit(g_AutoVolnyDen, id)
		}
		case 1, 2, 3, 4, 5, 6:	
		{
			g_ServerInfo[Duel_Zbran] = item - 1
			g_ServerInfo[Duel_Typ] = _:g_ServerInfo[Duel_Zbran]
			if(item == 4 || item == 5) 
			{
				PosledniPrani_NoZoom(id)
			} else
			{
				PosledniPrani_HraciMenu(id)
			}
		}
		case 7: PosledniPrani_AimMenu(id)
		case 8:
		{
			g_ServerInfo[Duel_Mod]++
			if(g_ServerInfo[Duel_Mod] == 2)
				g_ServerInfo[Duel_Mod] = 0
				
			PosledniPrani_HlavniMenu(id)
		}
	}
}

public PosledniPrani_AimMenu(id)
{
	new menu = menu_create("\rVyber zbran", "PosledniPrani_AimMenuHandler")
	for(new i = 0; i < sizeof(AimDuely); i++)
		menu_additem(menu, ZbraneNazvy[AimDuely[i]], "")
	
	menu_addblank(menu, 0)
	static szBuffer[65]
	format(szBuffer, 64, "\yMod \d[\r%s\d]", NazvyDuelModu[g_ServerInfo[Duel_Mod]])
	menu_additem(menu, szBuffer, "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public PosledniPrani_AimMenuHandler(id, menu, item)
{
	menu_destroy(menu)
	if (item < sizeof(AimDuely))
	{
		g_ServerInfo[Duel_Typ] = _:DUEL_AIM
		g_ServerInfo[Duel_Zbran] = item
		PosledniPrani_HraciMenu(id)
	} 
	else if (item == 7)
	{
		g_ServerInfo[Duel_Mod]++
		if(g_ServerInfo[Duel_Mod] == 3)
			g_ServerInfo[Duel_Mod] = 0
		
		PosledniPrani_AimMenu(id)
	} 
	else
	{
		PosledniPrani_HlavniMenu(id)
	}
}

public PosledniPrani_HraciMenu(id)
{
	static i, szStr[3], szName[33]
	new menu = menu_create("\rVyber protivnika!", "PosledniPrani_HraciMenuHandler")
	
	for(i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_CT)
			continue
			
		num_to_str(i, szStr, 2)
		get_user_name(i, szName, 32)
		menu_additem(menu, szName, szStr)
	}
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public PosledniPrani_HraciMenuHandler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	new data[6], szName[64], access, callback
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName, charsmax(szName), callback);
	
	new duelHrac = str_to_num(data)
	if(!get_bit(g_ZivyHrac, duelHrac))
	{
		client_print(id, print_center, "Tento hrac jiz neni zivy!")
	} else
	{
		VytvoritDuel(id, duelHrac)
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public PosledniPrani_NoZoom(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	menu = menu_create("\rSniper Duel", "PosledniPrani_NoZoomHandler")
	menu_additem(menu, "Zoom", "")
	menu_additem(menu, "No Zoom", "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public PosledniPrani_NoZoomHandler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		PosledniPrani_HlavniMenu(id)
		return PLUGIN_HANDLED
	}

	g_ServerInfo[Duel_NoZoom] = item == 1 ? true : false
	PosledniPrani_HraciMenu(id)

	return PLUGIN_HANDLED
}

public VytvoritDuel(id, id2)
{
	static Float:flHealth
	flHealth = g_ServerInfo[Duel_Mod] == 2 ? MAX_HEALTH_T : 100.0
	
	g_ServerInfo[Duel_idT] = id
	PlayerGlowAlpha(id, BarvyRGB[BARVA_CERVENA], 5)
	set_pev(id, pev_health, flHealth)
	
	g_ServerInfo[Duel_idCT] = id2
	PlayerGlowAlpha(id2, BarvyRGB[BARVA_MODRA], 5)
	set_pev(id2, pev_health, flHealth)
	
	task_r(5.0, "DuelEfekt", TASK_DUEL)
	emit_sound(0, CHAN_AUTO, RuzneZvuky[9], 0.0, ATTN_NORM, SND_STOP, PITCH_NORM)
	emit_sound(0, CHAN_AUTO, RuzneZvuky[9], 1.0, ATTN_NORM, 0, PITCH_NORM)
	if(g_ServerInfo[Duel_Typ] != DUEL_AIM)
	{
		switch(g_ServerInfo[Duel_Zbran])
		{
			case 0: 
			{
				g_MojeInfo[id][MujNuz_Inventar] = _:g_MojeInfo[id][MujNuz]
				g_MojeInfo[id][MujNuz] = _:NOZE_RUKAVICE		
				OdebratZbrane(id, 0)
				
				g_MojeInfo[id2][MujNuz_Inventar] = _:g_MojeInfo[id2][MujNuz]
				g_MojeInfo[id2][MujNuz] = _:NOZE_RUKAVICE
				OdebratZbrane(id2, 0)
			}
			case 5:
			{
				OdebratZbrane(id, 1)
				fm_give_item(id, "weapon_hegrenade")
				fm_cs_set_user_bpammo(id, CSW_HEGRENADE, 99)
				
				OdebratZbrane(id2, 1)
				fm_give_item(id2, "weapon_hegrenade")
				fm_cs_set_user_bpammo(id2, CSW_HEGRENADE, 99)
			}
			case 1, 2, 3, 4:
			{
				static zbran
				OdebratZbrane(id, 1)
				zbran = fm_give_item(id, ZbraneGun[DuelZbrane[g_ServerInfo[Duel_Zbran] - 1]])
				if(pev_valid(zbran))
				{
					fm_cs_set_weapon_ammo(zbran, 1)
				}
				fm_cs_set_user_bpammo(id, ZbraneCSW[DuelZbrane[g_ServerInfo[Duel_Zbran] - 1]], 1)
				
				OdebratZbrane(id2, 1)
				zbran = fm_give_item(id2, ZbraneGun[DuelZbrane[g_ServerInfo[Duel_Zbran] - 1]])
				if(pev_valid(zbran))
				{
					fm_cs_set_weapon_ammo(zbran, 1)
				}
				fm_cs_set_user_bpammo(id2, ZbraneCSW[DuelZbrane[g_ServerInfo[Duel_Zbran] - 1]], 1)
			}
		}
	}
	else
	{
		OdebratZbrane(id, 0)
		fm_give_item(id, ZbraneGun[AimDuely[g_ServerInfo[Duel_Zbran]]])
		fm_cs_set_user_bpammo(id, ZbraneCSW[AimDuely[g_ServerInfo[Duel_Zbran]]], ZbraneAmmo[AimDuely[g_ServerInfo[Duel_Zbran]]])
		
		OdebratZbrane(id2, 0)
		fm_give_item(id2, ZbraneGun[AimDuely[g_ServerInfo[Duel_Zbran]]])
		fm_cs_set_user_bpammo(id2, ZbraneCSW[AimDuely[g_ServerInfo[Duel_Zbran]]], ZbraneAmmo[AimDuely[g_ServerInfo[Duel_Zbran]]])
	}
	
	g_ServerInfo[Duel_Bezi] = true
}

public DuelEfekt()
{
	RingEffect(g_ServerInfo[Duel_idT], BarvyRGB[BARVA_CERVENA])
	RingEffect(g_ServerInfo[Duel_idCT], BarvyRGB[BARVA_MODRA])
}
/*******************************************
			   < Zaklad >
********************************************/
#if !defined DEBUG
public AutomatickyRestart()
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		OdpocetZvuk(0, g_ServerInfo[Odpocet_Cas])
		JailHud(0, 6, {0, 255, 0}, 0, 1.0, 0.0, 0.1, 0.1, ">> Automaticky restart za %d sekund <<", g_ServerInfo[Odpocet_Cas][ODPOCET_RESTART])
		g_ServerInfo[Odpocet_Cas]--
	} else
	{
		remove_task(TASK_ODPOCET)
		server_cmd("sv_restartround 1")
	}
}
#endif

public VolnyDenAll()
{
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_ZivyHrac, i))
			continue
			
		UkoncitMenu(i)
		UchovatKnife(i)
		OdebratZbrane(i, 0)
	}
	
	g_ServerInfo[Special_Typ] = _:SPECIAL_FREEDAY
	g_ServerInfo[Odpocet_Cas] = 180
	set_task(1.0, "OdpocetSpecialu", TASK_ODPOCET, _, _, "b")

	ZrusitSimona()
	DespawnoutDealera()
	client_cmd(0, "spk %s", RuzneZvuky[5])
	OtevritCely()
}

public UkoncitVolneDny()
{
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(get_bit(g_VolnyDen, i))
			fm_set_user_rendering(i)
	}
	
	g_VolnyDen = 0
	JailDHud(0, 1, BarvyRGB[BARVA_ZLUTA], 0, 2.0, 0.0, 0.5, 0.5, "Volne dny jsou ukonceny")
	client_cmd(0, "spk %s", RuzneZvuky[6])
}

public OdpocetSpecialu()
{
	if(g_ServerInfo[Odpocet_Cas] > 0)
	{
		if (g_ServerInfo[Odpocet_Cas] <= 30)
			OdpocetZvuk(0, g_ServerInfo[Odpocet_Cas])
			
		JailHud(0, 8, BarvyRGB[BarvaOdpoctu(g_ServerInfo[Odpocet_Cas], 180, 60, 30)], 0, 1.1, 0.0, 0.0, 0.0, "[-- >> %s << --]^n[-- %s --]", NazvySpecialu[g_ServerInfo[Special_Typ]], FormatovatSekundy(g_ServerInfo[Odpocet_Cas]))
		g_ServerInfo[Odpocet_Cas]--
		return
	}
	
	remove_task(TASK_ODPOCET)
	switch (g_ServerInfo[Special_Typ])
	{
		case SPECIAL_FREEDAY:
		{
			JailDHud(0, 1, BarvyRGB[BARVA_ZLUTA], 0, 2.0, 0.0, 0.5, 0.5, "Volne dny jsou ukonceny")
			client_cmd(0, "spk %s", RuzneZvuky[6])
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_ZivyHrac, i) && g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT)
					ZabitBezPripsani(i)
			}
		}
		case SPECIAL_ONESHOT, SPECIAL_ONLYHS, SPECIAL_PRESTRELKA, SPECIAL_ARENA:
		{
			JailDHud(0, 1, BarvyRGB[BARVA_CERVENA], 0, 2.0, 0.0, 0.5, 0.5, ">> Vyprsel casovy limit! Bachari jsou vitezem <<")
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_ZivyHrac, i))
				{
					if (g_MojeInfo[i][Hrac_Team] == CS_TEAM_T)
					{
						user_silentkill(i)
					} else
					{
						PridatBody(i, 10)
					}
				}
			}
		}
	}
}

public VolneDnyHudInfo()
{
	if(!g_VolnyDen)
		return
	
	static Hud[512], znaku, name[33]
	formatex(Hud, 511, "Volne dny:")
	znaku = strlen(Hud)
	
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_VolnyDen, i))
			continue
			
		get_user_name(i, name, 32)
		znaku += copy(Hud[znaku], 511 - znaku, "^n^t")
		znaku += copy(Hud[znaku], 511 - znaku, name)
	}
	
	JailHud(0, 4, {255, 140, 5}, 0, 3.0, 0.0, 0.8, 0.8, Hud) 
}

public KontrolaPoslednihoVezne()
{
	AktualizovatPromenne(false)
}
	
public AktualizovatPromenne(bool: Spawn)
{
	g_ZivychHracu[CS_TEAM_CT] = 0
	g_OnlineHracu[CS_TEAM_CT] = 0
	g_ZivychHracu[CS_TEAM_T] = 0
	g_OnlineHracu[CS_TEAM_T] = 0
	g_ZivychHracu[CS_TEAM_SPECTATOR] = 0
	g_OnlineHracu[CS_TEAM_SPECTATOR] = 0
	g_ZivychHracu[CS_TEAM_UNASSIGNED] = 0
	g_OnlineHracu[CS_TEAM_UNASSIGNED] = 0
	
	static posledniVezen
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_HracPripojen, i))
		{
			g_MojeInfo[i][Hrac_Team] = _:CS_TEAM_UNASSIGNED
			continue
		}
		
		g_OnlineHracu[g_MojeInfo[i][Hrac_Team]]++
		if(!get_bit(g_ZivyHrac, i))
			continue
			
		g_ZivychHracu[g_MojeInfo[i][Hrac_Team]]++
		if(g_MojeInfo[i][Hrac_Team] == CS_TEAM_T)
			posledniVezen = i
	}
	
	if(!Spawn && g_ServerInfo[Special_Typ] == SPECIAL_ZADNY && g_ZivychHracu[CS_TEAM_T] == 1 && g_ZivychHracu[CS_TEAM_CT] && g_ServerInfo[Posledni_Vezen] != posledniVezen)
	{
		static szName[33]
		get_user_name(posledniVezen, szName, 32)
		JailDHud(0, 2, {255, 150, 0}, 0, 1.5, 0.0, 0.5, 0.5, "%s je posledni vezen!", szName)
		g_ServerInfo[Posledni_Vezen] = posledniVezen
		PosledniPrani_HlavniMenu(posledniVezen)
			
		g_HledanyVezen = 0
		g_ServerInfo[VzpouraVyvolana] = false
	}
}
/*******************************************
			     < Data >
********************************************/
public NacistChatZpravy()
{
	new slozka[150], soubor[150]
	get_localinfo("amxx_basedir", slozka, 149)
	format(soubor, 149, "%s/configs/jail/%s.mlabs", slozka, FILE_STORAGE_ANNOUNCER)
	
	if(!file_exists(soubor))
	{
		write_file(soubor, "// Konfiguracni soubor pro definovani chat oznameni", 0)
		server_print("[Jail] Neexistuje config pro ukladani zprav do chatu! Vytvatim..")
		return
	}
	
	g_aChatAuto = ArrayCreate(65)
	new buffer[65], delka, zprav, radek
	
	while(read_file(soubor, radek++, buffer, sizeof buffer - 1, delka))
	{
		if(!buffer[0] || buffer[0] == '/' || buffer[0] == '*')
			continue
		
		ArrayPushString(g_aChatAuto, buffer)
		zprav++
	}
	
	server_print("[Jail] Nacteno celkem %d zprav do chat announceru!", zprav)
}

public UlozitSpawnyHonena(id)
{
	new soubor[130], mapa[35]
	get_localinfo("amxx_basedir", soubor, 129)
	get_mapname(mapa,  34)
		
	add(soubor, 129, "/configs/jail/honena/")
	if(!dir_exists(soubor))
		mkdir(soubor)
		
	add(soubor, 129, mapa); 
	add(soubor, 129, ".mlabs")
	if(file_exists(soubor))
		delete_file(soubor)
			
	new Float: flOrigin[3], buffer[64], spawnu = 0, ent = -1
	while((ent = engfunc(EngFunc_FindEntityByString, ent, "classname", EntityName[Entity:ENTITA_ITEM_BOX])))
	{
		pev(ent, pev_origin, flOrigin)
		format(buffer, 65, "%f %f %f", flOrigin[0], flOrigin[1], flOrigin[2])
		write_file(soubor, buffer, -1)
		spawnu++
	}
		
	ChatColor(id, "Bylo ulozene celkem !g%d !yhonena spawnu!", spawnu)
	HonenaAdminMenu(id)
}

public VytvoritHonickaBox()
{
	new celkemSpawnu, spawnIndex, Float:flOrigin[3]
	celkemSpawnu = ArraySize(g_aHonenaSpawn)
	spawnIndex = random_num(0, celkemSpawnu - 1)
	
	while (!HonenaVolnySpawn(spawnIndex))
	{
		spawnIndex = random_num(0, celkemSpawnu - 1)
	}
	
	ArrayGetArray(g_aHonenaSpawn, spawnIndex, flOrigin)
	VytvoritItemBoxEntitu(spawnIndex, flOrigin)
	
	if (g_ServerInfo[Odpocet_Cas] > 20)
	{
		set_task(random_float(10.0, 20.0), "VytvoritHonickaBox", TASK_HONENABOX)
	}
}
	
public NacistMapData()
{
	new mapa[33], buffer[256]
	get_mapname(mapa, 32)
	if (fvault_get_data(FILE_STORAGE_MAPDATA, mapa, buffer, 255))
	{
		new szParseData[5][128]
		ExplodeString(szParseData, 5, buffer, 127, '|')
		// Cely
		if (strlen(szParseData[0]) > 0 && !equal(szParseData[0], "N"))
		{
			#if defined DEBUG
			copy(g_EditorData[Editor_Ent_Cely], 32, szParseData[0])
			g_ServerInfo[Cely_Entita] = GetEntityIdByTarget("func_button", szParseData[0])
			if (g_ServerInfo[Cely_Entita] == -1)
			{
				server_print("[Debug] Ulozene id pro otevreni cel je invalidni!")
			} else
			{
				server_print("[Debug] Nalezena entita pro otevreni cel! Target: %s | Ent: %d", szParseData[0], g_ServerInfo[Cely_Entita])
			}
			#else
			g_ServerInfo[Cely_Entita] = GetEntityIdByTarget("func_button", szParseData[0])
			if (g_ServerInfo[Cely_Entita] == -1)
				PrepnoutNaZakladniMapu("Ulozene id pro entitu na otevreni celly se nepodarilo najit!")
			#endif
		} else
		{
			#if !defined DEBUG
			PrepnoutNaZakladniMapu("Neni definovan button pro otevreni cel!")
			#else
			server_print("Neni definovan button pro otevreni cel!")
			#endif
		}
			
		// Stary balon
		if (strlen(szParseData[1]) > 0 && !equal(szParseData[1], "N"))
		{
			#if defined DEBUG
			copy(g_EditorData[Editor_Ent_Balon], 32, szParseData[1])
			server_print("[Debug] Nactena entita stareho balonu! Id: %s", g_EditorData[Editor_Ent_Balon])
			new entityId = GetEntityIdByModel("func_pushable", g_EditorData[Editor_Ent_Balon])
			if (pev_valid(entityId))
			{
				engfunc(EngFunc_RemoveEntity, entityId)
				server_print("[Debug] Entita stareho balanu nalezena! Mazu...")
			}
			#else
			new ent = GetEntityIdByModel("func_pushable", szParseData[1])
			if (pev_valid(ent))
				engfunc(EngFunc_RemoveEntity, ent)
			#endif
		}
		
		// Novy balon
		if (strlen(szParseData[2]) > 0 && !equal(szParseData[2], "N"))
		{
			new szOrigin[3][16]
			parse(szParseData[2], szOrigin[0], 15, szOrigin[1], 15, szOrigin[2], 15)
			g_ServerInfo[Balon_SpawnOrigin][0] = _:str_to_float(szOrigin[0])
			g_ServerInfo[Balon_SpawnOrigin][1] = _:str_to_float(szOrigin[1])
			g_ServerInfo[Balon_SpawnOrigin][2] = _:str_to_float(szOrigin[2])
			VytvoritBalon(EnumOrigin(g_ServerInfo[Balon_SpawnOrigin]))
			#if defined DEBUG
			server_print("[Debug] Nacteni balonu! {%f, %f, %f}", g_ServerInfo[Balon_SpawnOrigin][0], g_ServerInfo[Balon_SpawnOrigin][1], g_ServerInfo[Balon_SpawnOrigin][2])
			#endif
		}
		
		// Simon tlacitka
		if (strlen(szParseData[3]) > 0 && !equal(szParseData[3], "N"))
		{
			#if defined DEBUG
			new szTarget[33], entId, index
			g_EditorData[Editor_Button_Simon] = ArrayCreate(33)
			while (argbreak(szParseData[3], szTarget, 15, szParseData[3], 64) > 0)
			{
				ArrayPushString(g_EditorData[Editor_Button_Simon], szTarget)
				entId = GetEntityIdByTarget("func_button", szTarget)
				if (entId != -1)
				{
					g_ServerInfo[Map_Button_Simon][index++] = entId
				} else
				{
					server_print("[Debug] #%s je invalidni! Simon list", szTarget)
				}
			}
				
			server_print("[Debug] Nacteno %d simon buttonu!", index)
			#else
			new szTarget[33], entId, index
			while (argbreak(szParseData[3], szTarget, 15, szParseData[3], 64) > 0)
			{
				entId = GetEntityIdByTarget("func_button", szTarget)
				if (entId != -1)
				{
					g_ServerInfo[Map_Button_Simon][index++] = entId
				}
			}
			#endif
		}
		
		// Tlacitka blacklist
		if (strlen(szParseData[4]) > 0 && !equal(szParseData[4], "N"))
		{
			#if defined DEBUG
			new szTarget[33], entId, index
			g_EditorData[Editor_Button_Blacklist] = ArrayCreate(33)
			while (argbreak(szParseData[4], szTarget, 15, szParseData[4], 64) > 0)
			{
				ArrayPushString(g_EditorData[Editor_Button_Blacklist], szTarget)
				entId = GetEntityIdByTarget("func_button", szTarget)
				if (entId != -1)
				{
					g_ServerInfo[Map_Button_Blacklist][index++] = entId
				} else
				{
					server_print("[Debug] #%s je invalidni! Blacklist button", szTarget)
				}
			}
				
			server_print("[Debug] Nacteno %d buttonu do blacklistu!", index)
			#else
			new szTarget[33], entId, index
			while (argbreak(szParseData[3], szTarget, 15, szParseData[3], 64) > 0)
			{
				entId = GetEntityIdByTarget("func_button", szTarget)
				if (entId != -1)
				{
					g_ServerInfo[Map_Button_Blacklist][index++] = entId
				}
			}
			#endif
		}
	} else
	{
		#if !defined DEBUG
		PrepnoutNaZakladniMapu("Nejsou definovany zadne data pro tuhle mapu!")
		#endif
	}
	
	new baseDir[150], soubor[150], parseBuffer[4][33], delka, radek
	get_localinfo("amxx_basedir", baseDir, 149)
	
	NacistZony(baseDir, mapa)
	
	// Dealer
	new dealerData[DealerData]
	format(soubor, 149, "%s/configs/jail/dealer/%s.mlabs", baseDir, mapa)
	if(file_exists(soubor))
	{
		g_aDealerData = ArrayCreate(DealerData)
		while(read_file(soubor, radek++, buffer, sizeof buffer - 1, delka))
		{
			if(!buffer[0] || buffer[0] == '/')
				continue
			
			parse(buffer, parseBuffer[0], 9, parseBuffer[1], 9, parseBuffer[2], 9, parseBuffer[3], 9)
			dealerData[flDealer_Origin][0] = _:str_to_float(parseBuffer[0])
			dealerData[flDealer_Origin][1] = _:str_to_float(parseBuffer[1])
			dealerData[flDealer_Origin][2] = _:str_to_float(parseBuffer[2])
			
			dealerData[flDealer_Angle] = _:str_to_float(parseBuffer[3])
			ArrayPushArray(g_aDealerData, dealerData)
		}
		
		SpawnoutDealeraNahodny()
		#if defined DEBUG
		server_print("[Debug] Nacteno %d delaer spawnu!", ArraySize(g_aDealerData))
		#endif
	}
	#if !defined DEBUG
	else
	{
		set_fail_state("Nejsou definovany spawny pro dealera!")
	}
	#endif
		
	// Honena
	format(soubor, 149, "%s/configs/jail/honena/%s.mlabs", baseDir, mapa)
	if(file_exists(soubor))
	{
		radek = 0, delka = 0;
		g_aHonenaSpawn = ArrayCreate(3)
		
		new Float:flOrigin[3]
		while(read_file(soubor, radek++, buffer, sizeof buffer - 1, delka))
		{
			if(!buffer[0] || buffer[0] == '/')
				continue
			
			parse(buffer, parseBuffer[0], 32, parseBuffer[1], 32, parseBuffer[2], 32)
			flOrigin[0] = str_to_float(parseBuffer[0])
			flOrigin[1] = str_to_float(parseBuffer[1])
			flOrigin[2] = str_to_float(parseBuffer[2])
			ArrayPushArray(g_aHonenaSpawn, flOrigin)
		}

		#if defined DEBUG
		server_print("[Debug] Celkem nacteno %d itemu boxu pro honenou!", ArraySize(g_aHonenaSpawn))
		#endif		
	}
	#if !defined DEBUG
	else
	{
		set_fail_state("Nejsou definovany zadne spawny pro item boxy!")
	}
	#endif

	return PLUGIN_HANDLED
}
/*******************************************
			     < Hrac >
********************************************/
public OdhoditKnife(id)
{
	if(!zivyHrac(id) || g_MojeInfo[id][Hrac_Team] != CS_TEAM_T)
		return
	
	if (g_ServerInfo[Special_Typ] != SPECIAL_ZADNY)
	{
		client_print(id, print_center, "Nemuzes hazet noze pri specialu!")
		return
	}
	
	if (g_ServerInfo[Duel_idT] == id)
	{
		client_print(id, print_center, "Nemuzes hazet noze pri duelu!")
		return
	}
	
	if(!g_MojeInfo[id][OdhazovacichNozu])
	{
		client_print(id, print_center, "Nemas Noze!")
		return
	}
	
	static Float: flCas
	flCas = get_gametime()
	if(g_MojeInfo[id][PosledniOdhozeni] + 0.5 > flCas)
		return
	
	static ent
	ent = create_entity("info_target")
	if (!ent) 
		return
	
	static Float: flOrigin[3], Float: flAngle[3]
	pev(id, pev_origin, flOrigin)
	pev(id, pev_v_angle, flAngle)
	flAngle[0] -= 90
		
	set_pev(ent, pev_classname, EntityName[ENTITA_ODHOZENY_NUZ])
	engfunc(EngFunc_SetModel, ent, ModelyEntit[1])
	set_pev(ent, pev_body, _:ENTITA_ODHOZENY_NUZ + 1)
	set_pev(ent, pev_sequence, MULTIMODEL_KNIFE_ROTATE)
	set_pev(ent, pev_frame, 0.0)
	set_pev(ent, pev_framerate, 1.5)
	
	static const Float:minBox[3] = {-1.0, -7.0, -1.0}
	static const Float:maxBox[3] = {1.0, 7.0, 1.0}
	engfunc(EngFunc_SetSize, ent, minBox, maxBox)

	set_pev(ent, pev_origin, flOrigin)
	set_pev(ent, pev_angles, flAngle)
	set_pev(ent, pev_effects, 2)
	set_pev(ent, pev_solid, SOLID_TRIGGER)
	set_pev(ent, pev_movetype, MOVETYPE_TOSS)
	set_pev(ent, pev_owner, id)
	
	static Float: flVelocity[3]
	VelocityByAim(id, random_num(1000, 1300), flVelocity)
	set_pev(ent, pev_velocity, flVelocity)	
	set_pev(ent, pev_nextthink, flCas + random_float(60.0, 120.0))
	
	g_MojeInfo[id][PosledniOdhozeni] = _:flCas
	g_MojeInfo[id][OdhazovacichNozu]--
	PridatEntituDoArray(ent, ENTITA_ODHOZENY_NUZ)
	
	static Barvy: barva; 
	barva = g_MojeInfo[id][OdhazovacichNozu] ? ((g_MojeInfo[id][OdhazovacichNozu] > 3) ? BARVA_ZELENA : BARVA_ZLUTA) : BARVA_CERVENA
	JailHud(id, 9, BarvyRGB[barva], 0, 0.4, 0.0, 0.1, 0.1, "---^n<-- [%d / %d] -->", g_MojeInfo[id][OdhazovacichNozu], MAX_ODHAZOVACICH_NOZU)
}

public ZmenitTeam(id, CsTeams: team)
{
	if (get_bit(g_ZivyHrac, id))
		ZabitBezPripsani(id)
	
	fm_cs_set_user_team(id, team)
	g_MojeInfo[id][Hrac_Team] = _:team
}

public PripojitDoTeamu(id, CsTeams:team)
{
	if (g_MojeInfo[id][Hrac_Team] == team)
		return PLUGIN_HANDLED

	if (zivyHrac(id))
		user_silentkill(id)
		
	static restore, vgui, msgblock
	restore = get_pdata_int(id, OFFSET_VGUI)
	vgui = restore & (1<<0)
	if(vgui)
		set_pdata_int(id, OFFSET_VGUI, restore & ~(1<<0))

	switch(team)
	{
		case CS_TEAM_SPECTATOR:
		{
			msgblock = get_msg_block(g_MsgShowMenu)
			set_msg_block(g_MsgShowMenu, BLOCK_ONCE)
			if(!(g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT || g_MojeInfo[id][Hrac_Team] == CS_TEAM_T))
			{
				dllfunc(DLLFunc_ClientPutInServer, id)
			} else
			{
				if (g_MojeInfo[id][Ucet_ID] > 0)
				{
					AktualizovatUcetCas(id)
					g_MojeInfo[id][Ucet_Cas] = _:0.0
				}
			}
			
			set_msg_block(g_MsgShowMenu, msgblock)
			set_pdata_int(id, OFFSET_HUDInitialized, 1)
			engclient_cmd(id, "jointeam", "6")
			
		}
		case CS_TEAM_T, CS_TEAM_CT:
		{
			msgblock = get_msg_block(g_MsgShowMenu)
			set_msg_block(g_MsgShowMenu, BLOCK_ONCE)
			engclient_cmd(id, "jointeam", (team == CS_TEAM_CT) ? "2" : "1")
			engclient_cmd(id, "joinclass", "1")
			set_msg_block(g_MsgShowMenu, msgblock)
			
			fm_cs_set_user_deaths(id, 0)
			set_pev(id, pev_frags, 0)
			
			if (g_ServerInfo[Special_Typ] == SPECIAL_FREEDAY)
			{
				static data[1]
				data[0] = id
				set_task(3.0, "RespawnoutHraceTask", id + TASK_RESPAWN, data, 1)
			}	
		}
	}
	
	if(vgui)
		set_pdata_int(id, OFFSET_VGUI, restore)
	
	g_MojeInfo[id][Hrac_Team] = _:fm_cs_get_user_team(id)
	set_bit(g_ZmenaTeamu, id)
	
	return PLUGIN_HANDLED
}

public AktualizovatModel(id)
{
	if (g_MojeInfo[id][Simon_Id] == id)
	{
		copy(g_MojeInfo[id][Model_Name], 45, CTSkiny[3])
	} else
	{
		switch (g_MojeInfo[id][Model_Typ])
		{
			case MODEL_CUSTOM:
			{
				if(TrieKeyExists(g_tSpecialniSkiny, g_MojeInfo[id][AuthID]))
					TrieGetString(g_tSpecialniSkiny, g_MojeInfo[id][AuthID], g_MojeInfo[id][Model_Name], 45)
			}
			case MODEL_NORMAL:
			{
				if (g_MojeInfo[id][Hrac_Team] == CS_TEAM_T)
				{
					static nahodnySkin;
					copy(g_MojeInfo[id][Model_Name], 45, TSkiny[0])
					nahodnySkin = random_num(0, sizeof(Vezen_Submodels) - 1)
					set_pev(id, pev_skin, Vezen_Submodels[nahodnySkin])
				} else
				{
					copy(g_MojeInfo[id][Model_Name], 45, CTSkiny[0])
				}
			}
			case MODEL_VIP_FEMALE, MODEL_VIP_MALE:
			{
				copy(g_MojeInfo[id][Model_Name], 45, g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT ? (CTSkiny[_:g_MojeInfo[id][Model_Typ]]) : (TSkiny[_:g_MojeInfo[id][Model_Typ]]))
			}
		}
	}
	
	static aktualniModel[46]
	fm_cs_get_user_model(id, aktualniModel, 45)
	if(!equal(aktualniModel, g_MojeInfo[id][Model_Name]))
		fm_cs_set_user_model(id)
	
	return PLUGIN_CONTINUE
}

/*******************************************
			   < Entity >
********************************************/
public EntitaSize_Min_Minus(ent, osa, units) 
{
	new Float:flPos[3], Float:mins[3], Float:maxs[3]
	pev(ent, pev_origin, flPos)
	pev(ent, pev_mins, mins)
	pev(ent, pev_maxs, maxs)

	mins[osa] -= float(units) / 2.0
	maxs[osa] += float(units) / 2.0
	flPos[osa] -= float(units) / 2.0
	
	set_pev(ent, pev_origin, flPos)
	engfunc(EngFunc_SetSize, ent, mins, maxs)
}

public EntitaSize_Min_Plus(ent, osa, units) 
{
	new Float:pos[3], Float:mins[3], Float:maxs[3]
	pev(ent, pev_origin, pos)
	pev(ent, pev_mins, mins)
	pev(ent, pev_maxs, maxs)

	if ((floatabs(mins[osa]) + maxs[osa]) < units + 1)
		return

	mins[osa] += float(units) / 2.0
	maxs[osa] -= float(units) / 2.0
	pos[osa] += float(units) / 2.0
	
	set_pev(ent, pev_origin, pos)
	engfunc(EngFunc_SetSize, ent, mins, maxs)
}

public EntitaSize_Max_Minus(ent, osa, units) 
{
	new Float:pos[3], Float:mins[3], Float:maxs[3]
	pev(ent, pev_origin, pos)
	pev(ent, pev_mins, mins)
	pev(ent, pev_maxs, maxs)

	if ((floatabs(mins[osa]) + maxs[osa]) < units + 1) 
		return

	mins[osa] += float(units) / 2.0
	maxs[osa] -= float(units) / 2.0
	pos[osa] -= float(units) / 2.0
	
	set_pev(ent, pev_origin, pos)
	engfunc(EngFunc_SetSize, ent, mins, maxs)
}

public EntitaSize_Max_Plus(ent, osa, units) 
{
	new Float:pos[3], Float:mins[3], Float:maxs[3]
	pev(ent, pev_origin, pos)
	pev(ent, pev_mins, mins)
	pev(ent, pev_maxs, maxs)

	mins[osa] -= float(units) / 2.0
	maxs[osa] += float(units) / 2.0
	pos[osa] += float(units) / 2.0
	
	set_pev(ent, pev_origin, pos)
	engfunc(EngFunc_SetSize, ent, mins, maxs)
}
#if defined DEBUG
public cmdEntitaTest(id)
{
	static iOrigin[3], Float: fOrigin[3]
	get_user_origin(id, iOrigin, 3)
	IVecFVec(iOrigin, fOrigin)
	VytvoritEntitu(fOrigin, ENTITA_ITEM_BOX)
}

new g_Debug_Entity_Editor
new g_Debug_Entity_Id
new g_Debug_Entity_Osa
new g_Debug_Entity_Unit

new const ZonaOsy[3][] = {"X", "Y", "Z"}

public cmdEntitySize(id)
{
	new body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Invalidni entita!")
	} else
	{
		g_Debug_Entity_Editor = id
		g_Debug_Entity_Id = ent
		g_Debug_Entity_Unit = 1
		EntitySizeEditor(id)
		set_task(0.2, "ZobrazitEntitySize", TASK_DRAW_SIZE, _, _, "b")
	}
}

public EntitySizeEditor(id)
{
	new szTemp[126]
	new menu = menu_create("\rEntity size", "EntitySizeEditor_Handler")
		
	format(szTemp, 125, "Osa: \d[\r%s\d]", ZonaOsy[g_Debug_Entity_Osa])
	menu_additem(menu, szTemp, "")
	
	format(szTemp, 125, "Units: \d%d", g_Debug_Entity_Unit)
	menu_additem(menu, szTemp, "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\y<<<<<", "")
	menu_additem(menu, "\y>>>>>", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\r<<<<<", "")
	menu_additem(menu, "\r>>>>>", "")
	
	menu_addblank(menu, 0)
	menu_addtext(menu, "-----------------", 0)
	menu_additem(menu, "Print", "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public EntitySizeEditor_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		if (task_exists(TASK_DRAW_SIZE))
			remove_task(TASK_DRAW_SIZE)
			
		g_Debug_Entity_Id = 0
		g_Debug_Entity_Osa = 0
		g_Debug_Entity_Unit = 0
		return
	}
	
	menu_destroy(menu)
	switch (item)
	{
		case 0: g_Debug_Entity_Osa = (g_Debug_Entity_Osa < 2) ? g_Debug_Entity_Osa + 1 : 0
		case 1: g_Debug_Entity_Unit = (g_Debug_Entity_Unit < 10) ? g_Debug_Entity_Unit++ : 1
		case 2: EntitaSize_Max_Minus(g_Debug_Entity_Id, g_Debug_Entity_Osa, g_Debug_Entity_Unit)
		case 3: EntitaSize_Max_Plus(g_Debug_Entity_Id, g_Debug_Entity_Osa, g_Debug_Entity_Unit)
		case 4: EntitaSize_Min_Minus(g_Debug_Entity_Id, g_Debug_Entity_Osa, g_Debug_Entity_Unit)
		case 5: EntitaSize_Min_Plus(g_Debug_Entity_Id, g_Debug_Entity_Osa, g_Debug_Entity_Unit)
		case 6:
		{
			new szNazev[33], Float:flMins[3], Float:flMaxs[3]
			pev(g_Debug_Entity_Id, pev_classname, szNazev, 32)
			pev(g_Debug_Entity_Id, pev_mins, flMins)
			pev(g_Debug_Entity_Id, pev_maxs, flMaxs)
			
			server_print("!!!!!!!!!!!!!!!!!!!!!!!!")
			server_print("Entity Size:")
			server_print("Class: %s", szNazev)
			server_print("MinS: {%f, %f, %f}", flMins[0], flMins[1], flMins[2]) 
			server_print("MaxS: {%f, %f, %f}", flMaxs[0], flMaxs[1], flMaxs[2])
			server_print("!!!!!!!!!!!!!!!!!!!!!!!!")
		}
	}
	
	EntitySizeEditor(id)
}

public ZobrazitEntitySize()
{
	if (!pev_valid(g_Debug_Entity_Id))
	{
		if (task_exists(TASK_DRAW_SIZE))
			remove_task(TASK_DRAW_SIZE)
			
		g_Debug_Entity_Id = 0
		g_Debug_Entity_Osa = 0
		g_Debug_Entity_Unit = 0
		return
	}
	
	new Float:flOrigin[3], Float:mins[3], Float:maxs[3]
	pev(g_Debug_Entity_Id, pev_origin, flOrigin)
	pev(g_Debug_Entity_Id, pev_mins, mins)
	pev(g_Debug_Entity_Id, pev_maxs, maxs)
	
	mins[0] += flOrigin[0]
	mins[1] += flOrigin[1]
	mins[2] += flOrigin[2]
	
	maxs[0] += flOrigin[0]
	maxs[1] += flOrigin[1]
	maxs[2] += flOrigin[2]
	
	DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], maxs[2], mins[0], maxs[1], maxs[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], maxs[2], maxs[0], mins[1], maxs[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], maxs[2], maxs[0], maxs[1], mins[2], {170, 42, 255})
	
	DrawLine(g_Debug_Entity_Editor, mins[0], mins[1], mins[2], maxs[0], mins[1], mins[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, mins[0], mins[1], mins[2], mins[0], maxs[1], mins[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, mins[0], mins[1], mins[2], mins[0], mins[1], maxs[2], {170, 42, 255})
	
	DrawLine(g_Debug_Entity_Editor, mins[0], maxs[1], maxs[2], mins[0], maxs[1], mins[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, mins[0], maxs[1], mins[2], maxs[0], maxs[1], mins[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], mins[2], maxs[0], mins[1], mins[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, maxs[0], mins[1], mins[2], maxs[0], mins[1], maxs[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, maxs[0], mins[1], maxs[2], mins[0], mins[1], maxs[2], {170, 42, 255})
	DrawLine(g_Debug_Entity_Editor, mins[0], mins[1], maxs[2], mins[0], maxs[1], maxs[2], {170, 42, 255})
	
	switch (g_Debug_Entity_Osa)
	{
		case 0:
		{
			DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], maxs[2], maxs[0], mins[1], mins[2], {0, 255, 0})
			DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], mins[2], maxs[0], mins[1], maxs[2], {0, 255, 0})
			
			DrawLine(g_Debug_Entity_Editor, mins[0], maxs[1], maxs[2], mins[0], mins[1], mins[2], {255, 0, 0})
			DrawLine(g_Debug_Entity_Editor, mins[0], maxs[1], mins[2], mins[0], mins[1], maxs[2], {255, 0, 0})
		}
		case 1:
		{
			DrawLine(g_Debug_Entity_Editor, mins[0], mins[1], mins[2], maxs[0], mins[1], maxs[2], {255, 0, 0})
			DrawLine(g_Debug_Entity_Editor, maxs[0], mins[1], mins[2], mins[0], mins[1], maxs[2], {255, 0, 0})
			
			DrawLine(g_Debug_Entity_Editor, mins[0], maxs[1], mins[2], maxs[0], maxs[1], maxs[2], {0, 255, 0})
			DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], mins[2], mins[0], maxs[1], maxs[2], {0, 255, 0})
		}
		case 2:
		{
			DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], maxs[2], mins[0], mins[1], maxs[2], {0, 255, 0})
			DrawLine(g_Debug_Entity_Editor, maxs[0], mins[1], maxs[2], mins[0], maxs[1], maxs[2], {0, 255, 0})
			
			DrawLine(g_Debug_Entity_Editor, maxs[0], maxs[1], mins[2], mins[0], mins[1], mins[2], {255, 0, 0})
			DrawLine(g_Debug_Entity_Editor, maxs[0], mins[1], mins[2], mins[0], maxs[1], mins[2], {255, 0, 0})
		}
	}
}
#endif

public DropnoutBodyEntitu(id, hodnota)
{
	static ent, Float: flOrigin[3], Float: flVelocity[3]
	ent = create_entity("info_target")
	set_pev(ent, pev_classname, EntityName[ENTITA_BODY])	
	set_pev(ent, pev_iuser1, hodnota)

	SetSubModel(ent, ENTITA_BODY)
	set_pev(ent, pev_solid, SOLID_TRIGGER)
	set_pev(ent, pev_movetype, MOVETYPE_TOSS)
	
	VelocityByAim(id, 30, flVelocity)
	pev(id, pev_origin, flOrigin)	
	flOrigin[0] += flVelocity[0]
	flOrigin[1] += flVelocity[1]
	set_pev(ent, pev_origin, flOrigin)
	
	VelocityByAim(id, random_num(180, 230), flVelocity)
	set_pev(ent, pev_velocity, flVelocity)
	
	static const Float:fMaxs[3] = { 2.0, 2.0, 4.0 }
	static const Float:fMins[3] = { -2.0, -2.0, -4.0 }
	engfunc(EngFunc_SetSize, ent, fMins, fMaxs)
	
	set_pev(ent, pev_velocity, flVelocity)
	set_pev(ent, pev_nextthink, get_gametime() + 5.0)
	
	PridatEntituDoArray(ent, ENTITA_BODY)
}

public DropnoutMolotov(id)
{
	ham_strip_weapon(id, "weapon_flashbang")
	clear_bit(g_Molotov, id)
	
	static ent, Float: flOrigin[3], Float: flVelocity[3]
	ent = create_entity("func_button")
	set_pev(ent, pev_classname, EntityName[ENTITA_MOLOTOV_DROP])	
	
	SetSubModel(ent, ENTITA_MOLOTOV)
	set_pev(ent, pev_solid, SOLID_TRIGGER)
	set_pev(ent, pev_movetype, MOVETYPE_TOSS)
	
	VelocityByAim(id, 30, flVelocity)
	pev(id, pev_origin, flOrigin)	
	flOrigin[0] += flVelocity[0]
	flOrigin[1] += flVelocity[1]
	set_pev(ent, pev_origin, flOrigin)
	
	VelocityByAim(id, random_num(180, 230), flVelocity)
	set_pev(ent, pev_velocity, flVelocity)
	
	static const Float:fMaxs[3] = { 2.0, 2.0, 4.0 }
	static const Float:fMins[3] = { -2.0, -2.0, -4.0 }
	engfunc(EngFunc_SetSize, ent, fMins, fMaxs)
	set_pev(ent, pev_velocity, flVelocity)
	
	PridatEntituDoArray(ent, ENTITA_MOLOTOV_DROP)
}

public DropnoutEntitu(id, Noze: nuz, bool: velocity)
{
	static ent, Float: flOrigin[3]
	ent = create_entity("func_button")
	set_pev(ent, pev_classname, "DropnutyKnife")	
	set_pev(ent, pev_iuser1, nuz)

	if (velocity)
	{
		static Float: flVelocity[3], Float: flEntAngles[3], Float: flPlayerAngles[3]
		VelocityByAim(id, 50, flVelocity)
		pev(id, pev_origin, flOrigin)	
		flOrigin[0] += flVelocity[0]
		flOrigin[1] += flVelocity[1]
		set_pev(ent, pev_origin, flOrigin)
		
		VelocityByAim(id, 200, flVelocity)
		set_pev(ent, pev_velocity, flVelocity)
		
		pev(id, pev_angles, flPlayerAngles)
		flEntAngles[0] = 0.0 
		flEntAngles[1] = flPlayerAngles[1]
		flEntAngles[2] = 0.0
		set_pev(ent, pev_angles, flEntAngles)
	} else 
	{
		pev(id, pev_origin, flOrigin)	
		set_pev(ent, pev_origin, flOrigin)
	}
	
	static Entity: typEntity
	typEntity = KnifeToEnt(nuz)
	if (typEntity == ENTITA_MOTOROVKA)
	{
		engfunc(EngFunc_SetModel, ent, ModelyEntit[0])
	} else
	{
		SetSubModel(ent, typEntity)
	}
	
	set_pev(ent, pev_solid, SOLID_TRIGGER)
	set_pev(ent, pev_movetype, MOVETYPE_TOSS)
	
	PridatEntituDoArray(ent, typEntity)
}

public VytvoritEntitu(Float: flOrigin[3], Entity: entityTyp)
{
	new ent = create_entity("info_target")
	set_pev(ent, pev_classname, EntityName[entityTyp])
	engfunc(EngFunc_SetOrigin, ent, flOrigin)
	SetSubModel(ent, entityTyp)
	
	set_pev(ent, pev_solid, SOLID_BBOX)
	set_pev(ent, pev_movetype, MOVETYPE_NONE)
	
	static const Float:fMaxs[3] = { 2.0, 2.0, 4.0 }
	static const Float:fMins[3] = { -2.0, -2.0, -4.0 }
	
	engfunc(EngFunc_SetSize, ent, fMins, fMaxs)
	PridatEntituDoArray(ent, entityTyp)
}

public VytvoritItemBoxEntitu(spawnIndex, Float: flOrigin[3])
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_DLIGHT) 
	write_coord_fl(flOrigin[0]) // PosX
	write_coord_fl(flOrigin[1]) // PosY
	write_coord_fl(flOrigin[2]) // PosZ
	write_byte(30) 				// Radius
	write_byte(115) 			// Red
	write_byte(216)				// Green
	write_byte(250) 			// Blue
	write_byte(10)				// Life 
	write_byte(45)				// Decay
	message_end()
	
	new ent = create_entity("info_target")
	set_pev(ent, pev_classname, EntityName[Entity:ENTITA_ITEM_BOX])
	engfunc(EngFunc_SetOrigin, ent, flOrigin)
	SetModelAndPlayAnim(ent, ENTITA_ITEM_BOX)
	
	set_pev(ent, pev_solid, SOLID_TRIGGER)
	set_pev(ent, pev_movetype, MOVETYPE_FLY)
	set_pev(ent, pev_nextthink, get_gametime() + 20.0)
	
	static const Float:fMaxs[3] = { 2.0, 2.0, 4.0 }
	static const Float:fMins[3] = { -2.0, -2.0, -4.0 }
	
	engfunc(EngFunc_SetSize, ent, fMins, fMaxs)
	set_pev(ent, pev_iuser1, spawnIndex)
	
	PridatEntituDoArray(ent, Entity:ENTITA_ITEM_BOX)
}

public fw_EntityThink(iEnt)
{
	if(!pev_valid(iEnt))
		return FMRES_IGNORED
		
	static nazevEnt[33]
	entity_get_string(iEnt, EV_SZ_classname, nazevEnt, 32)
	
	if (equal(nazevEnt, EntityName[Entity:ENTITA_ICECUBE]))
	{
		static Float: flOrigin[3], owner
		pev(iEnt, pev_origin, flOrigin)
		
		engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, flOrigin, 0)
		write_byte(TE_BREAKMODEL) 							// TE id
		write_coord_fl(flOrigin[0]) 						// x
		write_coord_fl(flOrigin[1]) 						// y
		write_coord_fl(flOrigin[2] + 24) 					// z
		write_coord(16) 									// size x
		write_coord(16) 									// size y
		write_coord(16) 									// size z
		write_coord(random_num(-50, 50)) 					// velocity x
		write_coord(random_num(-50, 50)) 					// velocity y
		write_coord(25) 									// velocity z
		write_byte(10) 										// random velocity
		write_short(g_sprGlass) 							// model
		write_byte(10) 										// count
		write_byte(25) 										// life
		write_byte(0x01) 									// flags
		message_end()
	
		owner = pev(iEnt, pev_iuser1)
		if (zivyHrac(owner))
		{
			clear_bit(g_Zmrazen, owner)
			NastavitRychlost(owner, HONENA_RYCHLOST)
			message_begin(MSG_ONE, g_MsgScreenFade, _, owner)
			write_short((1<<12)) 							// duration
			write_short(0) 									// hold time
			write_short(0x0000) 							// fade type
			write_byte(0) 									// red
			write_byte(50) 									// green
			write_byte(200) 								// blue
			write_byte(100) 								// alpha
			message_end()
		}
		
		SmazatEntitu(iEnt)
		return FMRES_IGNORED
	}
	
	if(equal(nazevEnt, EntityName[Entity:ENTITA_ITEM_BOX])
	|| equal(nazevEnt, EntityName[Entity:ENTITA_ODHOZENY_NUZ])
	|| equal(nazevEnt, EntityName[Entity:ENTITA_ITEM_BOX]))
	{
		SmazatEntitu(iEnt)
		return FMRES_IGNORED
	}
	
	if (equal(nazevEnt, EntityName[ENTITA_MOLOTOV]))
	{
		static ticku
		ticku = pev(iEnt, pev_iuser3)
		if (ticku == 1)
		{
			SmazatEntitu(iEnt)
			return FMRES_IGNORED
		}
		
		static majitel, CsTeams: team, Float: flOrigin[3], i, rand, obet;
		pev(iEnt, pev_origin, flOrigin)
		majitel = pev(iEnt, pev_iuser1) 
		team = CsTeams:pev(iEnt, pev_iuser2)
		obet = -1
		
		for (i = 1; i <= 5; i++) {
			rand = random_num(5, 15);
			message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
			write_byte(TE_SPRITE);
			write_coord_fl(flOrigin[0] + random_float(-100.0, 100.0));		// Position
			write_coord_fl(flOrigin[1] + random_float(-100.0, 100.0));		// Position
			write_coord_fl(flOrigin[2] + float(rand) * 5.0);							// Position
			write_short(g_sprFire);											// Sprite index
			write_byte(rand);												// Scale
			write_byte(100);												// Brightness
			message_end();
		}
		
		if (ticku % 5 == 0)
		{
			emit_sound(iEnt, CHAN_AUTO, RuzneZvuky[2], 0.5, ATTN_NORM, 0, PITCH_NORM);
		}
		
		if (ticku % 2 == 0)
		{
			message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
			write_byte(TE_SMOKE);
			write_coord_fl(flOrigin[0] + random_float(-100.0, 100.0))		// Position
			write_coord_fl(flOrigin[1] + random_float(-100.0, 100.0))		// Position
			write_coord_fl(flOrigin[2] - 10.0)								// Position
			write_short(g_sprSmoke)											// Sprite index
			write_byte(30)													// Scale
			write_byte(random_num(10, 20))									// Framerate
			message_end()
			
			while ((obet = engfunc(EngFunc_FindEntityInSphere, obet, flOrigin, 222.0)) != 0)
			{	
				if(!zivyHrac(obet))
					continue
				
				if (team == g_MojeInfo[obet][Hrac_Team] && obet != majitel)
					continue
				
				ExecuteHamB(Ham_TakeDamage, obet, 0, majitel, 10.0,  DMG_BURN)
			}
		}
		
		set_pev(iEnt, pev_iuser3, ticku - 1)
		set_pev(iEnt, pev_nextthink, get_gametime() + 0.2)
		
		return FMRES_IGNORED
	}
	
	if(equal(nazevEnt, EntityName[Entity:ENTITA_MINA]))
	{
		static Float:fCurrTime
		fCurrTime = get_gametime()
		switch(pev(iEnt, pev_iuser3))
		{
			case POWERUP_THINK :
			{
				static Float:fPowerupTime;
				pev(iEnt, pev_fuser2, fPowerupTime)
				if(fCurrTime > fPowerupTime)
				{
					set_pev(iEnt, pev_solid, SOLID_BBOX)
					set_pev(iEnt, pev_iuser3, BEAMBREAK_THINK)
					emit_sound(iEnt, CHAN_VOICE, MinySound[2], 0.5, ATTN_NORM, 1, 75)
				}
				set_pev(iEnt, pev_nextthink, fCurrTime + 0.1);
			}
			case BEAMBREAK_THINK :
			{
				static Float:vEnd[3], Float:vOrigin[3]
				pev(iEnt, pev_origin, vOrigin)
				pev(iEnt, pev_vuser1, vEnd)
	
				static iHit, Float:fFraction
				engfunc(EngFunc_TraceLine, vOrigin, vEnd, DONT_IGNORE_MONSTERS, iEnt, 0)
				get_tr2(0, TR_flFraction, fFraction)
				iHit = get_tr2(0, TR_pHit)
	
				if(fFraction < 1.0 && zivyHrac(iHit) && !get_bit(g_AntiLaser, iHit))
				{
					static majitel
					majitel = pev(iEnt, pev_iuser2)
					if (g_MojeInfo[iHit][Hrac_Team] != g_MojeInfo[majitel][Hrac_Team] || iHit == majitel)
					{
						ExecuteHamB(Ham_TakeDamage, iHit, 0, majitel, 90.0, DMG_BULLET)
						emit_sound(iHit, CHAN_WEAPON, MinySound[3], 1.0, ATTN_NORM, 0, PITCH_NORM)
						set_pev(iEnt, pev_nextthink, fCurrTime + random_float( 0.1, 0.3 ));
					}
				}
				
				static Float:fHealth;
				pev(iEnt, pev_health, fHealth);
				if(fHealth <= 0.0 || (pev(iEnt, pev_flags) & FL_KILLME))
				{
					set_pev(iEnt, pev_iuser3, EXPLOSE_THINK );
					set_pev(iEnt, pev_nextthink, fCurrTime + random_float( 0.1, 0.3 ) );
				}
			    
				static Float:fBeamthink
				pev(iEnt, pev_fuser3, fBeamthink)
				if(fBeamthink < fCurrTime)
				{
					static Barvy:barva
					barva = Barvy:pev(iEnt, pev_iuser1)
					
					message_begin(MSG_BROADCAST,SVC_TEMPENTITY);
					write_byte(TE_BEAMPOINTS);
					engfunc(EngFunc_WriteCoord,vOrigin[0]);	
					engfunc(EngFunc_WriteCoord,vOrigin[1]);
					engfunc(EngFunc_WriteCoord,vOrigin[2]);
					engfunc(EngFunc_WriteCoord,vEnd[0]);
					engfunc(EngFunc_WriteCoord,vEnd[1]);
					engfunc(EngFunc_WriteCoord,vEnd[2]); 
					write_short(g_sprRing);
					write_byte(0);
					write_byte(0);
					write_byte(1);	
					write_byte(5);	
					write_byte(0);	
					write_byte(BarvyRGB[barva][0]); 
					write_byte(BarvyRGB[barva][1]);
					write_byte(BarvyRGB[barva][2]);
					write_byte(255);
					write_byte(255);
					message_end();
						
					set_pev(iEnt, pev_fuser3, fCurrTime + 0.1)
				}
					
				set_pev(iEnt, pev_nextthink, fCurrTime + 0.01)
			}
			case EXPLOSE_THINK :
			{
				set_pev(iEnt, pev_nextthink, 0.0)
				OdpalitMinu(iEnt)
				engfunc(EngFunc_RemoveEntity, iEnt)
			}
		}
	}
	
	if (equal(nazevEnt, EntityName[Entity:ENTITA_BODY]))
	{
		client_print(0, print_chat, "Smazani bodove entity! entId: %d", iEnt)
		SmazatEntitu(iEnt)
	}
	
	return FMRES_IGNORED
}

public PrehoditKnife(id, ent)
{
	g_MojeInfo[id][MujNuz] = _:pev(ent, pev_iuser1)
	AktualizovatKnifeModel(id)
	emit_sound(id, CHAN_BODY, "items/gunpickup2.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
	SmazatEntitu(ent)
	return PLUGIN_CONTINUE
}

public Entity_Touch_Body(ent, id)
{
	if (get_bit(g_ZivyHrac, id))
	{
		new bodu = pev(ent, pev_iuser1)
		SmazatEntitu(ent)
		emit_sound(id, CHAN_WEAPON, RuzneZvuky[11], VOL_NORM, ATTN_NORM, 0, PITCH_NORM)
		if (bodu)
		{
			PridatBody(id, bodu)
		}
	}
}

public Entity_Touch_ItemBox(ent, id)
{
	#if defined DEBUG
	if (g_ServerInfo[Special_Typ] != SPECIAL_HONENA)
		return
	#endif
	if (get_bit(g_ZivyHrac, id))
	{
		HonenaPridelitOdmenu(id, ent)
	}
}

public EntityTouch(ent, id)
{
	if(!pev_valid(ent))
		return HAM_IGNORED
	
	static entName[33]
	pev(ent, pev_classname, entName, 32)
	if(equal(entName, EntityName[ENTITA_ODHOZENY_NUZ]))
	{
		if(!playerId(id))
		{
			static movetype
			movetype = pev(ent, pev_movetype)
			if(movetype != MOVETYPE_NONE)
			{
				set_pev(ent, pev_sequence, MULTIMODEL_KNIFE_IDLE)
				set_pev(ent, pev_frame, 0.0)
				set_pev(ent, pev_framerate, 0.0)
				set_pev(ent, pev_movetype, MOVETYPE_NONE)
				set_pev(ent, pev_nextthink, get_gametime() + 10.0)
				emit_sound(ent, CHAN_ITEM, "weapons/knife_hitwall1.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
			}
			
			return HAM_IGNORED
		}
		
		if (g_MojeInfo[id][Hrac_Team] == CS_TEAM_T)
		{
			static movetype
			movetype = pev(ent, pev_movetype)
			if(movetype == MOVETYPE_NONE && g_MojeInfo[id][OdhazovacichNozu] < MAX_ODHAZOVACICH_NOZU)
			{
				static Barvy: barva; 
				barva = g_MojeInfo[id][OdhazovacichNozu] ? ((g_MojeInfo[id][OdhazovacichNozu] > 3) ? BARVA_ZELENA : BARVA_ZLUTA) : BARVA_CERVENA
				JailHud(id, 9, BarvyRGB[barva], 0, 0.4, 0.0, 0.1, 0.1, "---^n<-- [%d / %d] -->", g_MojeInfo[id][OdhazovacichNozu], MAX_ODHAZOVACICH_NOZU)
				emit_sound(ent, CHAN_ITEM, "weapons/knife_deploy1.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
				g_MojeInfo[id][OdhazovacichNozu]++
				SmazatEntitu(ent)
			}
		} else 
		{
			static utocnik
			utocnik = pev(ent, pev_owner)
			if (utocnik == id)
				return HAM_IGNORED
			
			if (g_MojeInfo[id][Hrac_Team] == CS_TEAM_T && !g_ServerInfo[Box])
			{
				SmazatEntitu(ent)
				return HAM_IGNORED
			}
			
			static movetype
			movetype = pev(ent, pev_movetype)
			if (movetype)
			{
				static Float: flOrigin[3]
				pev(id, pev_origin, flOrigin)
				CakanecKrve(flOrigin, 1)
			
				ExecuteHam(Ham_TakeDamage, id, "weapon_knife", utocnik, random_float(10.0, 14.0), DMG_GENERIC);
			}
			
			SmazatEntitu(ent)
		}
	}
	
	return HAM_IGNORED
}

/*******************************************
				 < VIP >
********************************************/	
public cmdVIPMenu(id)
{
	if (!jeVIP(id))
	{
		VIPMenu(id)
	} else
	{
		VIPVyhodyMenu(id)
	}
}

public VIPMenu(id)
{
	new menu = menu_create("\rVIP Menu", "VIPMenu_Handler")
	menu_additem(menu, "\dZakoupit", "")
	menu_additem(menu, "Aktivovat", "")
	menu_additem(menu, "Vyhody", "")

	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public VIPMenu_Handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	switch (item)
	{
		case 0:
		{
			client_print(id, print_center, "Pracujeme na tom :)")
			VIPMenu(id)
		}
		case 1: 
		{
			if (!BlokovaneVIPKodPokusy(id))
			{
				client_cmd(id, "messagemode VIPKod")
				client_print(id, print_center, "Zadej platny aktivacni kod!")
			} else
			{
				client_print(id, print_center, "Dalsi pokus o aktivaci bude povolen do 5 minut!")
				VIPMenu(id)
			}
		}
		case 2:
		{
			show_motd(id, "vip.txt", "VIP")
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public VIPSystem_KodHandler(id)	
{
	static zprava[300]
	read_args(zprava, charsmax(zprava))
	remove_quotes(zprava)

	if (!zprava[0])
	{
		client_print(id, print_center, "Nezadal jsi zadne znaky!")
		VIPMenu(id)
		return PLUGIN_HANDLED
	}

	if (strlen(zprava) != 5)
	{
		client_print(id, print_center, "Tento kod je neplatny!")
		VIPMenu(id)
		return PLUGIN_HANDLED
	}
	
	KontrolaVIPKodu(id, zprava)
	return PLUGIN_HANDLED
}

public KontrolaVIPKodu(id, vipkod[])
{
	static dotaz[256], data[1]
	data[0] = id
	copy(g_MojeInfo[id][Aktivace_Kod], 5, vipkod)
	
	format(dotaz, 255, "SELECT delka, aktivovan FROM %s WHERE klic = '%s' LIMIT 1", TABULKA_VIP_KODY, vipkod)
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "KontrolaVIPKodu_Handler", dotaz, data, 1)
}

public KontrolaVIPKodu_Handler(FailState, Handle:Query, Error[], Errcode, Data[], DataSize)
{
	static id; id = Data[0]
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_VIP, Error)
			arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
			client_print(id, print_center, "Nastala chyba pri overeni kodu! Zkuste to pozdeji...")
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_VIP, "Nastala chyba pri overeni aktivacniho kodu!", Error)
			arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
			client_print(id, print_center, "Nastala chyba pri overeni kodu! Zkuste to pozdeji...")
		}
		case TQUERY_SUCCESS:
		{
			if (SQL_NumResults(Query))
			{
				static aktivovan
				aktivovan = SQL_ReadResult(Query, 1)
				server_print("aktivovan: %d", aktivovan)
				if (!aktivovan)
				{
					g_MojeInfo[id][Aktivace_Delka] = SQL_ReadResult(Query, 0)
					server_print("Aktivace_Delka: %d", g_MojeInfo[id][Aktivace_Delka])
					DeaktivovatVIPKod(id)
				} else
				{
					arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
					client_print(id, print_center, "Tento aktivacni kod jiz byl pouzit!")
					VIPMenu(id)
				}
			} else
			{
				g_MojeInfo[id][Aktivace_Pokusu]++
				client_print(id, print_center, "Neplatny kod! Pokusu %d / 3", g_MojeInfo[id][Aktivace_Pokusu])
				arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
				
				if (g_MojeInfo[id][Aktivace_Pokusu] == 3)
				{
					if (!g_tVIPKodBlockList)
						g_tVIPKodBlockList = TrieCreate()
		
					TrieSetCell(g_tVIPKodBlockList, g_MojeInfo[id][AuthID], get_gametime() + 300.0)
					g_MojeInfo[id][Aktivace_Pokusu] = 0
				} else
				{
					VIPMenu(id)
				}
			}
		}
	}
	 
	SQL_FreeHandle(Query)
}

public bool: BlokovaneVIPKodPokusy(id)
{
	if (!g_tVIPKodBlockList || !TrieKeyExists(g_tVIPKodBlockList, g_MojeInfo[id][AuthID]))
		return false

	static Float: flExpire
	TrieGetCell(g_tVIPKodBlockList, g_MojeInfo[id][AuthID], flExpire)
	if (flExpire < get_gametime())
	{
		TrieDeleteKey(g_tVIPKodBlockList, g_MojeInfo[id][AuthID])
		return false
	}
	
	return true
}

public DeaktivovatVIPKod(id)
{
	static dotaz[256], data[1]
	data[0] = id

	format(dotaz, 255, "UPDATE %s SET aktivovan = CURRENT_TIME WHERE klic = '%s'", TABULKA_VIP_KODY, g_MojeInfo[id][Aktivace_Kod])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "DeaktivovatVIPKod_Handler", dotaz, data, 1)
}

public DeaktivovatVIPKod_Handler(FailState, Handle:Query, Error[], Errcode, Data[], DataSize)
{
	static id; id = Data[0]
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_VIP, Error)
			client_print(id, print_center, "Nastala chyba pri aktivaci tohoto VIP kodu! Zkuste to pozdeji...")
			arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
			g_MojeInfo[id][Aktivace_Delka] = -1
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_VIP, "Nastala chyba pri deaktivaci kodu!", Error)
			client_print(id, print_center, "Nastala chyba pri aktivaci tohoto VIP kodu! Zkuste to pozdeji...")
			arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
			g_MojeInfo[id][Aktivace_Delka] = -1
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Deaktivace VIP kodu dokoncena! Id: %d | Kod: %s", id, g_MojeInfo[id][Aktivace_Kod])
			#endif
			
			if (SQL_AffectedRows(Query))
			{
				VytvoritVIP(id, g_MojeInfo[id][Aktivace_Delka])
			} else
			{
				g_MojeInfo[id][Aktivace_Delka] = -1
				arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
				client_print(id, print_center, "Nastala chyba pri aktivaci VIP kodu!")
			}
		}
	}

	SQL_FreeHandle(Query)
}

public VytvoritVIP(id, delka)
{
	new dotaz[256], data[1]
	data[0] = id
	g_MojeInfo[id][Aktivace_Vyprsi] = get_systime() + (delka * 86400)

	format(dotaz, 255, "INSERT INTO %s (id, typ, vyprsi) VALUES (%d, 0, %d)", g_MojeInfo[id][Ucet_ID], g_MojeInfo[id][Aktivace_Vyprsi])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "VytvoritVIP_Handler", dotaz, data, 1)
}

public VytvoritVIP_Handler(FailState, Handle:Query, Error[], Errcode, Data[], DataSize)
{
	static id; id = Data[0]
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_VIP, Error)
			client_print(id, print_center, "Nastala chyba pri aktivaci VIP kodu!")
			g_MojeInfo[id][Aktivace_Delka] = -1
			arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
			g_MojeInfo[id][Aktivace_Vyprsi] = 0
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_VIP, "Nastala chyba pri aktivaci kodu! (Steam)", Error)
			client_print(id, print_center, "Nastala chyba pri aktivaci VIP kodu!")
			g_MojeInfo[id][Aktivace_Delka] = -1
			arrayset(g_MojeInfo[id][Aktivace_Kod], 5, 0)
			g_MojeInfo[id][Aktivace_Vyprsi] = 0
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Funkce: VytvoritSteamVIP_Handler | Error: TQUERY_SUCCESS | id: %d", id)
			#endif
		}
	}

	SQL_FreeHandle(Query)
}


public FormatovatDelkuVIP(output[], len, delkaVip)
{
	if (delkaVip)
	{
		format(output, len, "Permanetni")
	} else
	{
		format(output, len, "%d Dnu", delkaVip)
	}
}

public VIPInfoMenu(id)
{
	static buffer[256], data[64], bool: steamHrac
	steamHrac = JeSteamHrac(id)

	new menu = menu_create("\rAktivace VIP dokoncena!", "MenuDestroy_Handler")

	format(buffer, 255, "Typ: \r%s", steamHrac ? "SteamID" : "Nick + Heslo")
	menu_additem(menu, buffer, "")
	menu_addtext(menu, "\y>> \dTyp aktivace (Steam/NS)", -1)
	menu_addblank(menu, 0)
	
	if (!steamHrac)
	{
		format(buffer, 255, "Heslo: \r%s", g_MojeInfo[id][Aktivace_Heslo])
		menu_additem(menu, buffer, "")
		menu_addtext(menu, "\y>> \dHeslo pro aktivaci^n\r!! Heslo dobre uschovejte !!", -1)
		menu_addblank(menu, 0)
	}
	
	FormatovatDelkuVIP(data, 65, g_MojeInfo[id][Aktivace_Delka])
	format(buffer, 255, "Delka: \r%s", data)
	menu_additem(menu, buffer, "")
	menu_addtext(menu, "\y>> \dDelka platnosti VIP", -1)
	menu_addblank(menu, 0)

	if (g_MojeInfo[id][Aktivace_Vyprsi])
	{
		Db_FormatovatCas(g_MojeInfo[id][Aktivace_Vyprsi], data, 65)
		format(buffer, 255, "Vyprsi: \r%s", data)
		menu_additem(menu, buffer, "")
		menu_addtext(menu, "\y>> \dDatum expirace", -1)
		menu_addblank(menu, 0)
	}
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "Zavrit")
	menu_display(id, menu, 0)
}

public VIPVyhodyMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	menu = menu_create("\rVIP vyhody", "VIPVyhodyMenu_Handler")
	menu_additem(menu, MaPovoleneHats(id) ? "Hats" : "\dHats", "")
	menu_additem(menu, "Skiny", "")

	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public VIPVyhodyMenu_Handler(id, menu, item)
{
	switch (item)
	{
		case 0:
		{
			if (MaPovoleneHats(id))
			{
				HatMenu(id)
			} else
			{
				client_print(id, print_center, "Nemuzes mit hatku pri schovce!")
			}
		}
		case 1: PlayerSkinMenu(id)
	}

	return PLUGIN_HANDLED
}

public bool: MaPovoleneHats(id)
{
	if (g_ServerInfo[Special_Typ] != SPECIAL_SCHOVKA)
		return true
		
	return g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT
}

public HatMenu(id)
{
	static i, szbuffer[45]
	new menu = menu_create("\rZvolte hat", "HatMenu_Handler")
	
	for (i = 0; i < sizeof Klobouk_Nazvy; i++)
	{
		format(szbuffer, 44, "%s%s", (g_MojeInfo[id][VIP_Hat_Id] == (i + 1)) ? "\d" : "\w", Klobouk_Nazvy[i])
		menu_additem(menu, szbuffer, "")
	}
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpatky")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public HatMenu_Handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	if (g_MojeInfo[id][VIP_Hat_Id] != item)
	{
		if (g_MojeInfo[id][VIP_Hat_Ent] == 0)
		{
			VytvoritHat(id, item)
		} else
		{
			ZmenitHat(id, item)
		}
	}  else
	{
		HatMenu(id)
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public VytvoritHat(id, hat)
{
	g_MojeInfo[id][VIP_Hat_Ent] = fm_create_entity("info_target")
	g_MojeInfo[id][VIP_Hat_Id] = hat
	
	set_pev(g_MojeInfo[id][VIP_Hat_Ent], pev_classname, "Hat")
	set_pev(g_MojeInfo[id][VIP_Hat_Ent], pev_movetype, MOVETYPE_FOLLOW)
	set_pev(g_MojeInfo[id][VIP_Hat_Ent], pev_aiment, id)
	set_pev(g_MojeInfo[id][VIP_Hat_Ent], pev_rendermode, kRenderNormal)
	engfunc(EngFunc_SetModel, g_MojeInfo[id][VIP_Hat_Ent], ModelyEntit[3])
	set_pev(g_MojeInfo[id][VIP_Hat_Ent], pev_body, Klobouk_Submodel[hat])
}

public SmazatHat(id)
{
	if (pev_valid(g_MojeInfo[id][VIP_Hat_Ent]))
		engfunc(EngFunc_RemoveEntity, g_MojeInfo[id][VIP_Hat_Ent])
		
	g_MojeInfo[id][VIP_Hat_Ent] = 0
	g_MojeInfo[id][VIP_Hat_Id] = -1
}

public ZmenitHat(id, hat)
{
	if (pev_valid(g_MojeInfo[id][VIP_Hat_Ent]))
		set_pev(g_MojeInfo[id][VIP_Hat_Ent], pev_body, Klobouk_Submodel[hat])
}

public cmdChcat(id)
{
	if(g_MojeInfo[id][VIP_Chcal])
	{
		client_print(id, print_center, "Muzes se vychcat pouze jednou za den!")
		return PLUGIN_HANDLED
	}
	
	if (!jeVIP(id))
	{
		client_print(id, print_center, "Nejsi VIP!")
		return PLUGIN_HANDLED
	}
	
	g_MojeInfo[id][VIP_Chcal] = true
	g_MojeInfo[id][VIP_ChcaniOdpocet] = 48
	
	set_task(0.2, "ChcaniTask", TASK_CHCANI + id, _, _, "b")
	emit_sound(id, CHAN_VOICE, RuzneZvuky[0], 1.0, ATTN_NORM, 0, PITCH_NORM) 
	
	return PLUGIN_HANDLED
}

public ChcaniTask(TaskID)
{
	static id
	id = TaskID - TASK_CHCANI
	
	if (g_MojeInfo[id][VIP_ChcaniOdpocet] == 0)
	{
		remove_task(TaskID)
		return
	}
	
	static vec[3], aimvec[3], velocityvec[3], length, distance, speed
	get_user_origin(id,vec) 
	get_user_origin(id,aimvec,3) 
	distance = get_distance(vec,aimvec) 
	speed = floatround(distance*1.9)
	
	velocityvec[0]=aimvec[0]-vec[0] 
	velocityvec[1]=aimvec[1]-vec[1] 
	velocityvec[2]=aimvec[2]-vec[2] 
	
	length=sqrt(velocityvec[0]*velocityvec[0]+velocityvec[1]*velocityvec[1]+velocityvec[2]*velocityvec[2]) 
	
	velocityvec[0]=velocityvec[0]*speed/length 
	velocityvec[1]=velocityvec[1]*speed/length 
	velocityvec[2]=velocityvec[2]*speed/length 
	
	message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
	write_byte(TE_BLOODSTREAM)
	write_coord(vec[0])
	write_coord(vec[1])
	write_coord(vec[2])
	write_coord(velocityvec[0]) 
	write_coord(velocityvec[1]) 
	write_coord(velocityvec[2]) 
	write_byte(102)
	write_byte(160)
	message_end()	
	
	g_MojeInfo[id][VIP_ChcaniOdpocet]--
}

public PlayerSkinMenu(id)
{
	new menu = menu_create("\rVyber skin", "PlayerSkinMenu_Handler")
	
	menu_additem(menu, g_MojeInfo[id][Model_Typ] == MODEL_NORMAL ? "\rZakladni" : "\wZakladni", "")
	menu_additem(menu, g_MojeInfo[id][Model_Typ] == MODEL_VIP_MALE ? "\rMuzsky" : "\wMuzsky", "")
	menu_additem(menu, g_MojeInfo[id][Model_Typ] == MODEL_VIP_FEMALE ? "\rZensky" : "\wZensky", "")
	
	if (TrieKeyExists(g_tSpecialniSkiny, g_MojeInfo[id][AuthID]))
		menu_additem(menu, g_MojeInfo[id][Model_Typ] == MODEL_CUSTOM ? "\rVlastni" : "\wVlastni", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public PlayerSkinMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	if (g_MojeInfo[id][Model_Typ] == PlayerModelTyp:item) 
	{
		menu_display(id, menu, 0)
	} else 
	{
		menu_destroy(menu)
		g_MojeInfo[id][Model_Typ] = _:item;
		AktualizovatModel(id)
	}
	
	return PLUGIN_HANDLED
}

public OdhoditBody(id)
{
	static Float: flTime, szArgs[10]
	flTime = get_gametime()
	
	if (g_MojeInfo[id][Odhozeni_Xp_Last] + 0.5 > flTime)
		return PLUGIN_HANDLED
	
	if (!read_argv(1, szArgs, 9))
	{
		client_print(id, print_center, "Neni definovany pocet XP k odhozeni!")
		return PLUGIN_HANDLED
	}
	
	static bodu;
	bodu = str_to_num(szArgs)
	if (!bodu)
	{
		client_print(id, print_center, "Musis doplnit i hodnotu! Prikad: fa_body 5")
		return PLUGIN_HANDLED
	}
	
	if (bodu > 100)
	{
		client_print(id, print_center, "Maximalni hodnota k odhozeni je 100!")
		return PLUGIN_HANDLED
	}
	
	if (!maXP(id, bodu))
	{
		client_print(id, print_center, "Nemas tolik XP!")
		return PLUGIN_HANDLED
	}
	
	OdebratBody(id, bodu)
	DropnoutBodyEntitu(id, bodu)
	g_MojeInfo[id][Odhozeni_Xp_Last] = _:flTime
	
	return PLUGIN_HANDLED
}

/*******************************************
				< Obchod >
********************************************/
public cmdShop(id)
{
	if(!get_bit(g_ZivyHrac, id))
	{
		return
	}
	
	if(!(g_ServerInfo[Special_Typ] == SPECIAL_ZADNY || g_ServerInfo[Special_Typ] == SPECIAL_FREEDAY))
	{
		client_print(id, print_center, "Nemuzes nakupovat pri specialnim dnu!")
		return
	}
	
	if (g_ServerInfo[Duel_idT] == id || g_ServerInfo[Duel_idCT] == id)
	{
		client_print(id, print_center, "Nemuzes nakupovat pri duelu!")
		return
	}
	
	if(g_MojeInfo[id][Hrac_Team] == CS_TEAM_T)
	{
		VezenskyShop(id)
	} else
	{	
		BacharskyShop(id)
	}
}

public VezenskyShop(id)
{
	static szTemp[120]
	FormatovatCastku(g_MojeInfo[id][Ucet_Konto], szTemp, 119)
	format(szTemp, 119, "\rVezensky Obchod \y[\r%s\y]", szTemp)
	new menu = menu_create(szTemp, "VezenskyShopHandler")
	
	format(szTemp, 119, "%sSroubovak \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_SROUBOVAK || !MuzeSebratKnife(id) ? "\d" : "\w", OBCHOD_VEZEN_SROUBOVAK)
	menu_additem(menu, szTemp, "1")
	
	format(szTemp, 119, "%sNozik \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_ZABIKUCH || !MuzeSebratKnife(id) ? "\d" : "\w", OBCHOD_VEZEN_ZABIKUCH)
	menu_additem(menu, szTemp, "2")
	
	format(szTemp, 119, "%sPacidlo \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_PACIDLO || !MuzeSebratKnife(id) ? "\d" : "\w", OBCHOD_VEZEN_PACIDLO)	
	menu_additem(menu, szTemp, "3")
	
	format(szTemp, 119, "%sMaceta \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_MACETA || !MuzeSebratKnife(id) ? "\d" : "\w", OBCHOD_VEZEN_MACETA)	
	menu_additem(menu, szTemp, "4")
	
	format(szTemp, 119, "%sMotorovka \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_MOTOROVKA || !MuzeSebratKnife(id) ? "\d" : "\w", OBCHOD_VEZEN_MOTOROVKA)
	menu_additem(menu, szTemp, "5") 
	
	format(szTemp, 119, "%sVolny Den \y[\r%d\y]", (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_VOLNYDEN || get_bit(g_VolnyDen, id)) ? "\d" : "\w", OBCHOD_VEZEN_VOLNYDEN)
	menu_additem(menu, szTemp, "6")
	
	if(!jeVIP(id))
	{
		format(szTemp, 119, "%sPadak \y[\r%d\y]", (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_PADAK || get_bit(g_Padak, id)) ? "\d" : "\w", OBCHOD_VEZEN_PADAK)
		menu_additem(menu, szTemp, "7")
	}
	
	format(szTemp, 119, "%sGranaty \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_GRANATY ? "\d" : "\w", OBCHOD_VEZEN_GRANATY)
	menu_additem(menu, szTemp, "8")
	
	format(szTemp, 119, "%sDeagle \y[\r%d\y]", g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_DEAGLE ? "\d" : "\w", OBCHOD_VEZEN_DEAGLE)
	menu_additem(menu, szTemp, "9")
	
	format(szTemp, 119, "%sRedBull \y[\r%d\y]", (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_REDBULL || get_bit(g_RedBull, id)) ? "\d" : "\w", OBCHOD_VEZEN_REDBULL)
	menu_additem(menu, szTemp, "10")
	
	format(szTemp, 119, "%sAnti-Laser \y[\r%d\y]", (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_ANTILASER || get_bit(g_AntiLaser, id)) ? "\d" : "\w", OBCHOD_VEZEN_ANTILASER)
	menu_additem(menu, szTemp, "11")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public VezenskyShopHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName,charsmax(szName), callback);
	
	new menuItem = str_to_num(data)
	switch(menuItem)
	{
		case 1: ZakoupitNuz(id, Noze:NOZE_SROUBOVAK, OBCHOD_VEZEN_SROUBOVAK)
		case 2: ZakoupitNuz(id, Noze:NOZE_ZABIKUCH, OBCHOD_VEZEN_ZABIKUCH)
		case 3: ZakoupitNuz(id, Noze:NOZE_PACIDLO, OBCHOD_VEZEN_PACIDLO)
		case 4: ZakoupitNuz(id, Noze:NOZE_MACETA, OBCHOD_VEZEN_MACETA)
		case 5: ZakoupitNuz(id, Noze:NOZE_MOTOROVKA, OBCHOD_VEZEN_MOTOROVKA)
		case 6:
		{
			if (get_bit(g_VolnyDen, id))
			{
				VezenskyShop(id)
			} else if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_VOLNYDEN)
			{
				NemasBody(id, OBCHOD_VEZEN_VOLNYDEN)
				VezenskyShop(id)
			} else 
			{
				PridelitVolnyDen(id)
				OdebratBody(id, OBCHOD_VEZEN_VOLNYDEN)
			}
		}
		case 7:
		{
			if (get_bit(g_Padak, id))
			{
				client_print(id, print_center, "Padak jiz mas zakoupen!")
				VezenskyShop(id)
			} else if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_PADAK)
			{
				NemasBody(id, OBCHOD_VEZEN_PADAK)
				VezenskyShop(id)
			} else
			{
				set_bit(g_Padak, id)
				ZakoupitItem(id, OBCHOD_VEZEN_PADAK)
			}
		}
		case 8:
		{
			if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_GRANATY)
			{
				NemasBody(id, OBCHOD_VEZEN_GRANATY)
			} else 
			{
				StripWeapons(id, Grenades)
				fm_give_item(id, "weapon_hegrenade")
				fm_give_item(id, "weapon_smokegrenade")
				fm_give_item(id, "weapon_flashbang")
				fm_cs_set_user_bpammo(id, CSW_FLASHBANG, 2)
				OdebratBody(id, OBCHOD_VEZEN_GRANATY)
			}
		}
		case 9:
		{
			if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_DEAGLE)
			{
				NemasBody(id, OBCHOD_VEZEN_DEAGLE)
				VezenskyShop(id)
			} else
			{
				StripWeapons(id, Secondary)
				new dgl = fm_give_item(id, "weapon_deagle")
				if(pev_valid(dgl)) 
					fm_cs_set_weapon_ammo(dgl, 1)
				
				fm_cs_set_user_bpammo(id, CSW_DEAGLE, 0)
				OdebratBody(id, OBCHOD_VEZEN_DEAGLE)
			}
		}
		case 10:
		{
			if (get_bit(g_RedBull, id))
			{
				VezenskyShop(id)
			} else if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_REDBULL)
			{
				NemasBody(id, OBCHOD_VEZEN_REDBULL)
				VezenskyShop(id)
			} else
			{
				VytvoritRedBull(id)
				ZakoupitItem(id, OBCHOD_VEZEN_REDBULL)
			}
		}
		case 11:
		{
			if (get_bit(g_AntiLaser, id))
			{
				VezenskyShop(id)
			} else if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_VEZEN_ANTILASER)
			{
				NemasBody(id, OBCHOD_VEZEN_ANTILASER)
				VezenskyShop(id)
			} else
			{
				set_bit(g_AntiLaser, id)
				ZobrazitIconu(id, "suit_full", 1)
				ZakoupitItem(id, OBCHOD_VEZEN_ANTILASER)
			}
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public BacharskyShop(id)
{
	static szTemp[127]
	FormatovatCastku(g_MojeInfo[id][Ucet_Konto], szTemp, 127)
	format(szTemp, 127, "\wObchod bacharu \y[\r%s\y]", szTemp)
	new menu = menu_create(szTemp, "BacharskyShopHandler")
	
	format(szTemp, 126, "%sKatana \y[\r%d\y]", g_MojeInfo[id][MujNuz] != Noze:NOZE_KATANA && maXP(id, OBCHOD_BACHAR_KATANA) ? "\w" : "\d", OBCHOD_BACHAR_KATANA)
	menu_additem(menu, szTemp, "1")
	
	if (!jeVIP(id))
	{
		format(szTemp, 126, "%sPadak \y[\r%d\y]", (g_MojeInfo[id][Ucet_Konto] < OBCHOD_BACHAR_PADAK || get_bit(g_Padak, id)) ? "\d" : "\w", OBCHOD_BACHAR_PADAK)
		menu_additem(menu, szTemp, "2")
	}

	format(szTemp, 126, "%sMina \y[\r%d\y]", (g_MojeInfo[id][Ucet_Konto] < OBCHOD_BACHAR_MINA || g_MojeInfo[id][PocetMin]) ? "\d" : "\w", OBCHOD_BACHAR_MINA)
	menu_additem(menu, szTemp, "3")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_NEXTNAME, "\yVice..")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	
	menu_display(id, menu, 0)
}

public BacharskyShopHandler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], access, callback
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName,charsmax(szName), callback)
	
	new menuItem = str_to_num(data)
	switch(menuItem)
	{
		case 1:
		{
			if (g_MojeInfo[id][MujNuz] == Noze:NOZE_KATANA)
			{
				BacharskyShop(id)
			} else if (!maXP(id, OBCHOD_BACHAR_KATANA))
			{
				NemasBody(id, OBCHOD_BACHAR_KATANA)
				BacharskyShop(id)
			} else
			{
				g_MojeInfo[id][MujNuz] = _:NOZE_KATANA
				AktualizovatKnifeModel(id)
				OdebratBody(id, OBCHOD_BACHAR_KATANA)
			}
		}
		case 2:
		{
			if (get_bit(g_Padak, id))
			{
				BacharskyShop(id)	
			} else if (!maXP(id, OBCHOD_BACHAR_PADAK))
			{
				NemasBody(id, OBCHOD_BACHAR_PADAK)
				BacharskyShop(id)
			} else
			{
				set_bit(g_Padak, id)
				ZakoupitItem(id, OBCHOD_BACHAR_PADAK)
			}
		}
		case 3:
		{
			if (g_MojeInfo[id][PocetMin])
			{
				client_print(id, print_center, "Nemuzes mit u sebe vice nez jednu minu!")
				BacharskyShop(id)
			} else if (g_MojeInfo[id][Ucet_Konto] < OBCHOD_BACHAR_MINA)
			{
				NemasBody(id, OBCHOD_BACHAR_MINA)
				BacharskyShop(id)
			} else
			{
				g_MojeInfo[id][PocetMin] += 1
				ZakoupitItem(id, OBCHOD_BACHAR_MINA)
			}
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public ZakoupitItem(id, cena)
{
	OdebratBody(id, cena)
	emit_sound(id, CHAN_BODY, "items/gunpickup2.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
}

public ZakoupitNuz(id, Noze: nuz, cena)
{
	if (!MuzeSebratKnife(id))
	{
		client_print(id, print_center, "Nemuzes vlastnit vice nozu!")
		return
	}
	
	if (g_MojeInfo[id][Ucet_Konto] < cena)
	{
		NemasBody(id, cena)
		return
	}
	
	g_MojeInfo[id][MujNuz] = _:nuz
	AktualizovatKnifeModel(id)
	ZakoupitItem(id, cena)
}

public VytvoritRedBull(id)
{
	set_bit(g_RedBull, id)
	set_pev(id, pev_gravity, 0.6)
	set_pev(id, pev_health, pev(id, pev_health) + 2.0)
	NastavitRychlost(id, 400.0)
	g_MojeInfo[id][RedBullOdpocet] = 8
	VytvoritTrail(id, BARVA_ZELENA)
	set_task(2.0, "RedBullVyhody", id + TASK_REDBULL, _, _, "b")
}

public RedBullVyhody(TaskID)
{
	static id
	id = TaskID - TASK_REDBULL
	if(g_MojeInfo[id][RedBullOdpocet] > 0)
	{
		set_pev(id, pev_health, pev(id, pev_health) + 2.0)
		g_MojeInfo[id][RedBullOdpocet]--
	} else
	{
		ZrusitTrail(id)
		remove_task(TaskID)
		clear_bit(g_RedBull, id)
		set_pev(id, pev_gravity, 1.0)
		NastavitRychlost(id, 0.0)
	}
}
/*******************************************
				< LaserMiny >
********************************************/
public ZacitPlantovatMinu(id)
{
	if(!get_bit(g_ZivyHrac, id) || g_MojeInfo[id][Hrac_Team] != CS_TEAM_CT || g_ServerInfo[Special_Typ] != SPECIAL_ZADNY)
		return PLUGIN_HANDLED
	
	if(!g_MojeInfo[id][PocetMin])
	{
		client_print(id, print_center, "Nemas zadne miny!")
		return PLUGIN_HANDLED
	}
	
	static Float:vTraceDirection[3], Float:vTraceEnd[3],Float:vOrigin[3], Float:fFraction,Float:vTraceNormal[3];
	pev(id, pev_origin, vOrigin);
	velocity_by_aim(id, 128, vTraceDirection);
	xs_vec_add(vTraceDirection, vOrigin, vTraceEnd);
	engfunc(EngFunc_TraceLine, vOrigin, vTraceEnd, DONT_IGNORE_MONSTERS, id, 0);
	get_tr2( 0, TR_flFraction, fFraction );
	
	if(fFraction < 1.0 )
	{
		NastavitRychlost(id, 1.0)
		get_tr2(0, TR_vecEndPos, vTraceEnd);
		get_tr2(0, TR_vecPlaneNormal, vTraceNormal);
				
		message_begin( MSG_ONE, 108, {0,0,0}, id );
		write_byte(1);
		write_byte(0);
		message_end();
	
		set_task(1.2, "VytvoritMinu", (TASK_PLANT + id));
	} else
	{
		client_print(id, print_center, "Miny se pokladaji pouze na zdi!")
	}
	
	return PLUGIN_HANDLED
}

public ZrusitPlantovaniMiny(id)
{
	if (!get_bit(g_ZivyHrac, id))
		return 
		
	NastavitRychlost(id, 0.0)
	remove_task(TASK_PLANT + id)
	
	message_begin(MSG_ONE, 108, {0,0,0}, id)
	write_byte(0)
	write_byte(0)
	message_end()
}

public VytvoritMinu(id)
{
	id -= TASK_PLANT
	NastavitRychlost(id, 0.0)
	new iEnt = engfunc(EngFunc_CreateNamedEntity, g_ServerInfo[Mina_EntTyp]);
	if(!iEnt) 
		return PLUGIN_HANDLED_MAIN;
	
	static Float:vOrigin[3], Float:vNewOrigin[3], Float:vNormal[3], Float:vTraceDirection[3], Float:vTraceEnd[3], Float:vEntAngles[3], Float:fFraction; 
	set_pev(iEnt, pev_classname, EntityName[ENTITA_MINA])
	SetSubModel(iEnt, ENTITA_MINA)
	
	set_pev(iEnt, pev_solid, SOLID_NOT);
	set_pev(iEnt, pev_movetype, MOVETYPE_FLY);
	set_pev(iEnt, pev_takedamage, DAMAGE_YES);
	set_pev(iEnt, pev_dmg, 100.0);
	set_pev(iEnt, pev_health, 250.0)
	
	pev(id, pev_origin, vOrigin);
	velocity_by_aim(id, 128, vTraceDirection);
	xs_vec_add(vTraceDirection, vOrigin, vTraceEnd);
	engfunc(EngFunc_TraceLine, vOrigin, vTraceEnd, DONT_IGNORE_MONSTERS, id, 0);
	get_tr2(0, TR_flFraction, fFraction);
	
	if ( fFraction < 1.0 )
	{
		get_tr2(0, TR_vecEndPos, vTraceEnd);
		get_tr2(0, TR_vecPlaneNormal, vNormal);
	}

	xs_vec_mul_scalar(vNormal, 8.0, vNormal);
	xs_vec_add(vTraceEnd, vNormal, vNewOrigin)

	engfunc(EngFunc_SetSize, iEnt, Float:{ -4.0, -4.0, -4.0 }, Float:{ 4.0, 4.0, 4.0 } );
	engfunc(EngFunc_SetOrigin, iEnt, vNewOrigin );

	vector_to_angle(vNormal, vEntAngles );
	set_pev(iEnt, pev_angles, vEntAngles );

	static Float:vBeamEnd[3], Float:vTracedBeamEnd[3], Float: fCurrTime
	xs_vec_mul_scalar(vNormal, 8192.0, vNormal);
	xs_vec_add(vNewOrigin, vNormal, vBeamEnd);
	engfunc(EngFunc_TraceLine, vNewOrigin, vBeamEnd, IGNORE_MONSTERS, -1, 0 );
	get_tr2(0, TR_vecPlaneNormal, vNormal);
	get_tr2(0, TR_vecEndPos, vTracedBeamEnd);

	fCurrTime = get_gametime();
	set_pev(iEnt, pev_iuser1, random_num(0, sizeof(BarvyRGB) - 1))
	set_pev(iEnt, pev_iuser2, id );
	set_pev(iEnt, pev_iuser3, POWERUP_THINK);
	set_pev(iEnt, pev_vuser1, vTracedBeamEnd);
	set_pev(iEnt, pev_fuser2, fCurrTime + 2.5 );
	set_pev(iEnt, pev_nextthink, fCurrTime + 0.2 );
	
	emit_sound(iEnt, CHAN_VOICE, MinySound[0], VOL_NORM, ATTN_NORM, 0, PITCH_NORM);
	emit_sound(iEnt, CHAN_BODY, MinySound[1], 0.2, ATTN_NORM, 0, PITCH_NORM);
	
	PridatEntituDoArray(iEnt, ENTITA_MINA)
	g_MojeInfo[id][PocetMin]--
	return PLUGIN_HANDLED
}

public OdpalitMinu(iEnt)
{
	static Float:vOrigin[3];
	pev(iEnt, pev_origin, vOrigin);
	
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY);
	write_byte(TE_KILLBEAM);
	write_short(iEnt);
	message_end();

	engfunc(EngFunc_MessageBegin, MSG_PVS, SVC_TEMPENTITY, vOrigin, 0);
	write_byte(TE_EXPLOSION);
	engfunc(EngFunc_WriteCoord, vOrigin[0]);
	engfunc(EngFunc_WriteCoord, vOrigin[1]);
	engfunc(EngFunc_WriteCoord, vOrigin[2]);
	write_short(g_sprExploze);
	write_byte(30);
	write_byte(15);
	write_byte(0);
	message_end();
	
	static ent, Majitel
	Majitel = pev(iEnt, pev_iuser2)
	ent = -1
	
	while((ent = engfunc(EngFunc_FindEntityInSphere, ent, vOrigin, 200.0)) != 0)
	{
		if(!zivyHrac(ent)) 
			continue
			
		ExecuteHamB(Ham_TakeDamage, ent, 0, Majitel, 20.0, DMG_BLAST)
	}
}
/*******************************************
			   < Dealer >
********************************************/
#if defined DEBUG
public DealerAdminMenu(id)
{
	static buffer[100]
	format(buffer, 99, "\rDealer Admin^n\y>> \dSpawnu: \r%d \y| \r%d", g_aDealerData ? ArraySize(g_aDealerData) : 0, PocetDealerSpawnu())
	new menu = menu_create(buffer, "DealerAdminMenu_Handler")
	
	menu_additem(menu, "Vytvorit spawn", "1")
	menu_additem(menu, "Smazat spawn", "2")
	menu_additem(menu, "Smazat spawny", "3")
	menu_additem(menu, "Ulozit", "4")

	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public DealerAdminMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		MartasAdministrace(id)
		menu_destroy(menu)
		return
	}
	
	menu_destroy(menu)
	switch(item)
	{
		case 0: VytvoritDealerSpawn(id)
		case 1: SmazatDealerSpawn(id)
		case 2: SmazatDealerSpawny(id)
		case 3: UlozitDealerSpawny(id)
	}
	
	DealerAdminMenu(id)
}

public PocetDealerSpawnu()
{
	new dealer = -1, pocet
	while((dealer = engfunc(EngFunc_FindEntityByString, dealer, "classname", "Dealer")))
	{
		pocet++
	}
	
	return pocet
}
#endif

public VytvoritDealerSpawn(id)
{
	static Float: flOrigin[3], Float: flAngles[3]
	pev(id, pev_origin, flOrigin)
	flOrigin[2] += 80.0; 
	pev(id, pev_angles, flAngles)
	SpawnoutDealera(flOrigin, flAngles, true)
}

public SmazatDealerSpawn(id)
{
	static body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
		return
	
	static className[33]
	pev(ent, pev_classname, className, 32)
	if (!equal(className, "Dealer"))
		return
	
	engfunc(EngFunc_RemoveEntity, ent)
}

public SmazatDealerSpawny(id)
{
	new iBox = -1
	while((iBox = engfunc(EngFunc_FindEntityByString, iBox, "classname", "Dealer")))
		engfunc(EngFunc_RemoveEntity, iBox)
		
	if (g_aDealerData)
		ArrayClear(g_aDealerData)
}

public UlozitDealerSpawny(id)
{
	static buffer[66], soubor[66]
	get_localinfo("amxx_basedir", buffer, 65)
	format(soubor, 149, "%s/configs/jail/dealer/", buffer)
	if (!dir_exists(soubor))
		mkdir(soubor)
	
	get_mapname(buffer, 65)
	add(soubor, 149, buffer)
	add(soubor, 149, ".mlabs")
	if (file_exists(soubor))
		delete_file(soubor)

	static Float: flOrigin[3], Float: flAngles[3], pocet, dealer
	pocet = 0
	dealer = -1
	
	while((dealer = engfunc(EngFunc_FindEntityByString, dealer, "classname", "Dealer")))
	{
		pev(dealer, pev_origin, flOrigin)
		pev(dealer, pev_angles, flAngles)
		
		format(buffer, 65, "%f %f %f %f", flOrigin[0], flOrigin[1], flOrigin[2], flAngles[1])
		write_file(soubor, buffer, pocet++)
	}
	
	ChatColor(id, "Ulozeno celkem !g%d !yspawnu!", pocet)
	return PLUGIN_HANDLED
}

public DealerThink(ent) 
{
	if (!pev_valid(ent))
		return PLUGIN_HANDLED
	
	if (!g_ServerInfo[Dealer_Animation_Idle])
	{
		Util_PlayAnimation(ent, random_num(1, 8))
		set_pev(ent, pev_nextthink, get_gametime() + 3.0)
	} else
	{
		Util_PlayAnimation(ent, 0)
		set_pev(ent, pev_nextthink, get_gametime() + random_float(10.0, 20.0))
	}
	
	g_ServerInfo[Dealer_Animation_Idle] = g_ServerInfo[Dealer_Animation_Idle] ? false : true
	return PLUGIN_HANDLED
}
	
public SpawnoutDealeraNahodny()
{
	static spawnId, Float: flAngles[3], dealerData[DealerData] 
	spawnId = random_num(0, ArraySize(g_aDealerData) - 1)
	ArrayGetArray(g_aDealerData, spawnId, dealerData)
	flAngles[0] = 0.0
	flAngles[1] = dealerData[flDealer_Angle]
	flAngles[2] = 0.0
	
	SpawnoutDealera(EnumOrigin(dealerData[flDealer_Origin]), flAngles, false)
}

public SpawnoutDealera(Float: flOrigin[3], Float: flAngles[3], bool: editor)
{
	new dealerEnt = create_entity("info_target")
	entity_set_string(dealerEnt, EV_SZ_classname, "Dealer")
	entity_set_origin(dealerEnt, flOrigin)
	entity_set_vector(dealerEnt, EV_VEC_angles, flAngles);

	entity_set_model(dealerEnt, ModelyEntit[2]);
	entity_set_int(dealerEnt, EV_INT_movetype, MOVETYPE_TOSS );
	entity_set_int(dealerEnt, EV_INT_solid, SOLID_BBOX)
	
	new const Float: mins[3] = {-16.0, -16.0, 0.0 }
	new const Float: maxs[3] = { 16.0, 16.0, 72.0 }

	entity_set_size(dealerEnt, mins, maxs);
	entity_set_byte(dealerEnt, EV_BYTE_controller1, 125);
	entity_set_byte(dealerEnt, EV_BYTE_controller2, 125);
	entity_set_byte(dealerEnt, EV_BYTE_controller3, 125);
	entity_set_byte(dealerEnt, EV_BYTE_controller4, 125);
    
	drop_to_floor(dealerEnt);
	
	if (!editor)
	{
		entity_set_float(dealerEnt, EV_FL_nextthink, get_gametime() + 5.0)
		Util_PlayAnimation(dealerEnt, 1)
		
		g_ServerInfo[Dealer_Ent] = dealerEnt
		if (!g_tDealerSklad)
			NaplnitDealerSklad()
	}
}  

public DespawnoutDealera()
{
	if (!g_ServerInfo[Dealer_Ent])
		return
	
	if (pev_valid(g_ServerInfo[Dealer_Ent]))
		engfunc(EngFunc_RemoveEntity, g_ServerInfo[Dealer_Ent])
		
	g_ServerInfo[Dealer_Ent] = 0
	return
}

public PremistitDealera()
{
	if (!pev_valid(g_ServerInfo[Dealer_Ent]))
		return
		
	static spawnid, Float: flAngles[3], dealerData[DealerData] 
	spawnid = random_num(0, ArraySize(g_aDealerData) - 1)
	ArrayGetArray(g_aDealerData, spawnid, dealerData)
	set_pev(g_ServerInfo[Dealer_Ent], pev_origin, dealerData[flDealer_Origin])
	flAngles[1] = dealerData[flDealer_Angle]
	set_pev(g_ServerInfo[Dealer_Ent], pev_angles, flAngles)
}

public NaplnitDealerSklad()
{
	if (!g_tDealerSklad)
	{
		g_tDealerSklad = TrieCreate()
		for (new i; i < sizeof(Dealer_Itemy_Nazev); i++)
			TrieSetCell(g_tDealerSklad, Dealer_Itemy_Nazev[i], random_num(1, Dealer_Itemy_Max[i]))
	} else 
	{
		static itemTypu, buffer
		itemTypu = random_num(2, sizeof(Dealer_Itemy_Nazev))
		for (new i; i < sizeof(Dealer_Itemy_Nazev); i++)
		{
			TrieGetCell(g_tDealerSklad, Dealer_Itemy_Nazev[i], buffer)
			if (!buffer)
			{
				TrieSetCell(g_tDealerSklad, Dealer_Itemy_Nazev[i], random_num(1, Dealer_Itemy_Max[i]))
				
				itemTypu--
				if (itemTypu == 0)
					break;
			}
		}
		
		if (itemTypu)
		{
			for (new i; i < itemTypu; i++)
				TrieSetCell(g_tDealerSklad, Dealer_Itemy_Nazev[random_num(0, sizeof(Dealer_Itemy_Nazev) - 1)], random_num(1, Dealer_Itemy_Max[i]))
		}
	}
}

public bool: JeNaSklade(item[])
{
	static pocetItemu
	TrieGetCell(g_tDealerSklad, item, pocetItemu)
	return pocetItemu > 0
}

public ZakoupitDealerItem(id, item[], cena)
{
	OdebratBody(id, cena)
	emit_sound(id, CHAN_BODY, "items/gunpickup2.wav", 1.0, ATTN_NORM, 0, PITCH_NORM)
	
	static pocetItemu
	TrieGetCell(g_tDealerSklad, item, pocetItemu)
	pocetItemu--
	TrieSetCell(g_tDealerSklad, item, pocetItemu)
}

public DealerObchod(id)
{
	static buffer[120], szTemp[33]
	FormatovatCastku(g_MojeInfo[id][Ucet_Konto], szTemp, 32)
	format(buffer, 129, "\rNabidka dealera^n\y>> \dKonto: \w%s", szTemp)
	new menu = menu_create(buffer, "DealerObchod_Handler")
	
	format(buffer, 119, "%sMolotov \y[\r%d\y]", JeNaSklade("Molotov") ? "\w" : "\d", DEALER_MOLOTOV)
	menu_additem(menu, buffer, "")	
	
	format(buffer, 119, "%sPrevlek \y[\r%d\y]", JeNaSklade("Prevlek") ? "\w" : "\d", DEALER_PREVLEK)
	menu_additem(menu, buffer, "")
	
	format(buffer, 119, "%sNozik \y[\r%d\y]", JeNaSklade("Nozik") ? "\w" : "\d", DEALER_ODHAZOVACI_NOZIK)
	menu_additem(menu, buffer, "")
	
	format(buffer, 119, "%sPacidlo \y[\r%d\y]", JeNaSklade("Pacidlo") ? "\w" : "\d", DEALER_PACIDLO)
	menu_additem(menu, buffer, "")
	
	format(buffer, 119, "%sMotorovka \y[\r%d\y]", JeNaSklade("Motorovka") ? "\w" : "\d", DEALER_MOTOROVKA)
	menu_additem(menu, buffer, "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public DealerObchod_Handler(id, menu, item)
{
	if(item == MENU_EXIT || !jeBlizko(id, g_ServerInfo[Dealer_Ent], 15.0))
	{
		menu_destroy(menu);
		return PLUGIN_HANDLED;
	}
	
	switch(item)
	{
		case 0:
		{
			if (!JeNaSklade("Molotov"))
			{
				client_print(id, print_center, "Molotov neni na sklade!")
				DealerPrehratAnimaci(9)
			} else if (g_MojeInfo[id][Ucet_Konto] < DEALER_MOLOTOV)
			{
				client_print(id, print_center, "Vrat se az budes mit prachy!")
				DealerPrehratAnimaci(9)
			} else if (get_bit(g_Molotov, id))
			{
				client_print(id, print_center, "Nemuzes vlastnit vice molotovu!")
				DealerPrehratAnimaci(9)
			} else 
			{
				set_bit(g_Molotov, id)
				fm_give_item(id, "weapon_flashbang")
				ZakoupitDealerItem(id, "Molotov", DEALER_MOLOTOV)
			}
		}
		case 1:
		{
			if (!JeNaSklade("Prevlek"))
			{
				client_print(id, print_center, "Prevlek neni na sklade!")
				DealerPrehratAnimaci(9)
			} else if (g_MojeInfo[id][Ucet_Konto] < DEALER_PREVLEK)
			{
				client_print(id, print_center, "Vrat se az budes mit prachy!")
				DealerPrehratAnimaci(9)
			} else
			{
				ZakoupitDealerItem(id, "Prevlek", DEALER_PREVLEK)
				copy(g_MojeInfo[id][Model_Name], 45, CTSkiny[0])
				set_task(0.2, "fm_cs_set_user_model", id)
			}
		}
		case 2:
		{
			if (!JeNaSklade("Nozik"))
			{
				client_print(id, print_center, "Odhazovaci noze nejsou na sklade!")
				DealerPrehratAnimaci(9)
			} else if (g_MojeInfo[id][Ucet_Konto] < DEALER_ODHAZOVACI_NOZIK)
			{
				client_print(id, print_center, "Vrat se az budes mit prachy!")
				DealerPrehratAnimaci(9)
			} else if (g_MojeInfo[id][OdhazovacichNozu] == MAX_ODHAZOVACICH_NOZU)
			{
				client_print(id, print_center, "Nemuzes vlastnit vice nozu na odhozeni!")
				DealerPrehratAnimaci(9)
			} else
			{
				g_MojeInfo[id][OdhazovacichNozu] += 1
				ZakoupitDealerItem(id, "Nozik", DEALER_ODHAZOVACI_NOZIK)
			}
		}
		case 3:
		{
			if (!JeNaSklade("Pacidlo"))
			{
				client_print(id, print_center, "Momentalne zadne pacidlo nemam. Zkus to pozdeji..")
				DealerPrehratAnimaci(9)
			} else if (!MuzeSebratKnife(id))
			{
				client_print(id, print_center, "Nemas volne misto!")
				DealerPrehratAnimaci(9)
			} else if (g_MojeInfo[id][Ucet_Konto] < DEALER_PACIDLO)
			{
				client_print(id, print_center, "Vrat se az budes mit prachy!")
				DealerPrehratAnimaci(9)
			} else
			{
				g_MojeInfo[id][MujNuz] = _:NOZE_PACIDLO
				AktualizovatKnifeModel(id)
				ZakoupitDealerItem(id, "Pacidlo", DEALER_PACIDLO)
			}
		}
		case 4:
		{
			if (!JeNaSklade("Motorovka"))
			{
				client_print(id, print_center, "Momentalne zadnou motorovku nemam. Zkus to pozdeji..")
				DealerPrehratAnimaci(9)
			} else if (!MuzeSebratKnife(id))
			{
				client_print(id, print_center, "Nemas volne misto!")
				DealerPrehratAnimaci(9)
			} else if (g_MojeInfo[id][Ucet_Konto] < DEALER_MOTOROVKA)
			{
				client_print(id, print_center, "Vrat se az budes mit prachy!")
				DealerPrehratAnimaci(9)
			} else
			{
				g_MojeInfo[id][MujNuz] = _:NOZE_MOTOROVKA
				AktualizovatKnifeModel(id)
				ZakoupitDealerItem(id, "Motorovka", DEALER_MOTOROVKA)
			}
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public DealerPrehratAnimaci(seq)
{
	static Float: cas
	cas = get_gametime()
	if (cas > g_ServerInfo[Dealer_LastAnimation] + 1.0)
	{
		Util_PlayAnimation(g_ServerInfo[Dealer_Ent], seq)
		g_ServerInfo[Dealer_Animation_Idle] = true
		set_pev(g_ServerInfo[Dealer_Ent], pev_nextthink, cas + 1.0)
		g_ServerInfo[Dealer_LastAnimation] = _:cas
	}
}
/*******************************************
				< Balon >
********************************************/
#if defined DEBUG
public AdministraceBalonu(id)
{
	new menu = menu_create("\rStary^n\d-----------------", "AdministraceBalonuHandler")
	menu_additem(menu, g_EditorData[Editor_Ent_Balon] == 0 ? "\yOznacit" : "\ySmazat", "")
	
	menu_addtext(menu, "^n\rNovy^n\d-----------------", -1)
	menu_additem(menu, "Vytvorit", "")
	menu_additem(menu, g_EditorData[Editor_Ent_Balon] != 0 ? "Respawn" : "\dRespawn", "")
	menu_additem(menu, "Ulozit", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public AdministraceBalonuHandler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	switch(item)
	{
		case 0:
		{
			SmazatStaryBalon(id)
		}
		case 1:
		{
			if(g_ServerInfo[Balon_Ent] != 0)
			{
				if(pev_valid(g_ServerInfo[Balon_Ent]))
					engfunc(EngFunc_RemoveEntity, g_ServerInfo[Balon_Ent])
					
				g_ServerInfo[Balon_Ent] = 0
			}
			
			new Float: flOrigin[3], iOrigin[3]
			get_user_origin(id, iOrigin, 3)
			IVecFVec(iOrigin, flOrigin)
			VytvoritBalon(flOrigin)
			g_ServerInfo[Balon_SpawnOrigin] = _:flOrigin
		}
		case 2:
		{
			if(g_ServerInfo[Balon_Ent] != 0)
			{
				RespawnoutBalon(); 
			} else
			{
				client_print(id, print_center, "Balon neni vytvoren! Neni mozne ho respawnut!")
			}
		}
		case 3:
		{
			if(g_ServerInfo[Balon_SpawnOrigin][0])
			{
				AktualizovatMapData(id)
			} else
			{
				client_print(id, print_center, "Neni urceny spawn pro balon!")
			}
		}
	}
	
	AdministraceBalonu(id)
	menu_destroy(menu)
	return PLUGIN_HANDLED
}
#endif

public VytvoritBalon(Float: flOrigin[3])
{
	g_ServerInfo[Balon_Ent] = create_entity("info_target")
	set_pev(g_ServerInfo[Balon_Ent], pev_classname, EntityName[ENTITA_BALON])
	set_pev(g_ServerInfo[Balon_Ent], pev_solid, SOLID_BBOX)
	set_pev(g_ServerInfo[Balon_Ent], pev_movetype, MOVETYPE_FLY)
	SetSubModel(g_ServerInfo[Balon_Ent], ENTITA_BALON)
	engfunc(EngFunc_SetSize, g_ServerInfo[Balon_Ent], Float:{ -15.0, -15.0, 0.0 }, Float:{ 15.0, 15.0, 12.0 })
	set_pev(g_ServerInfo[Balon_Ent], pev_origin, flOrigin)
}

public OdkopnoutBalon(id) 
{
	static Float:flOrigin[3]
	pev(g_ServerInfo[Balon_Ent], pev_origin, flOrigin)
	flOrigin[2] += 35
    
	if(PointContents(flOrigin) != CONTENTS_EMPTY)
		return

	set_pev(g_ServerInfo[Balon_Ent], pev_solid, SOLID_BBOX)
	set_pev(g_ServerInfo[Balon_Ent], pev_movetype, MOVETYPE_BOUNCE)
	set_pev(g_ServerInfo[Balon_Ent], pev_iuser1, 0)
	set_pev(g_ServerInfo[Balon_Ent], pev_origin, flOrigin)
		
	static Float:flVelocity[3], Float:flAngles[3]
	velocity_by_aim(id, 650, flVelocity)
	set_pev(g_ServerInfo[Balon_Ent], pev_velocity, flVelocity)

	vector_to_angle(flVelocity, flAngles)
	set_pev(g_ServerInfo[Balon_Ent], pev_angles, flAngles)
	
	set_pev(g_ServerInfo[Balon_Ent], pev_frame, 0.0)
	set_pev(g_ServerInfo[Balon_Ent], pev_framerate, 1.0)
	set_pev(g_ServerInfo[Balon_Ent], pev_animtime, get_gametime())
		
	g_ServerInfo[Balon_Drzi] = 0
}

public RespawnoutBalon()
{
	if (!g_ServerInfo[Balon_Ent] || !pev_valid(g_ServerInfo[Balon_Ent]))
	{
		VytvoritBalon(EnumOrigin(g_ServerInfo[Balon_SpawnOrigin]))
		return
	}
	
	set_pev(g_ServerInfo[Balon_Ent], pev_velocity, Float:{ 0.0, 0.0, 0.0 } )
	set_pev(g_ServerInfo[Balon_Ent], pev_solid, SOLID_BBOX)
	set_pev(g_ServerInfo[Balon_Ent], pev_movetype, MOVETYPE_FLY)
	set_pev(g_ServerInfo[Balon_Ent], pev_iuser1, 0)
	set_pev(g_ServerInfo[Balon_Ent], pev_frame, 0.0)
	set_pev(g_ServerInfo[Balon_Ent], pev_framerate, 0.0)
	set_pev(g_ServerInfo[Balon_Ent], pev_origin, EnumOrigin(g_ServerInfo[Balon_SpawnOrigin]))
	g_ServerInfo[Balon_Drzi] = 0
}

public SmazatBalon()
{
	if (g_ServerInfo[Balon_Ent] == 0)
		return
	
	if (pev_valid(g_ServerInfo[Balon_Ent]))
		engfunc(EngFunc_RemoveEntity, g_ServerInfo[Balon_Ent])
		
	g_ServerInfo[Balon_Ent] = 0
}
/*******************************************
				< Admin >
********************************************/
#if defined DEBUG
public MartasAdministrace(id)
{
	static buffer[130]
	format(buffer, 129, "\rAdministrace^n\y>> \dStary balon: \d[%s\d]^n\y>> \dNovy Balon: [%s\d]", g_EditorData[Editor_Ent_Balon] ? "\ySmazany" : "\rNeexistuje", ExistujeBalonSpawn() ? "\yVytvoren" : "\rNeexistuje")
	new menu = menu_create(buffer, "MartasAdministraceHandler")
	menu_additem(menu, "Balon", "")
	menu_addblank(menu, 0)
		
	format(buffer, 129, "\y>> \dSpawnu: \d[\r%d\d]", g_aHonenaSpawn ? ArraySize(g_aHonenaSpawn) : 0)
	menu_addtext(menu, buffer, -1)
	menu_additem(menu, "\yHonena", "")
	menu_addblank(menu, 0)
	
	format(buffer, 129, "\y>> \dSpawnu: \d[\r%d\d]", g_aDealerData ? ArraySize(g_aDealerData) : 0)
	menu_addtext(menu, buffer, -1)
	menu_additem(menu, "\yDealer", "")
	
	menu_addblank(menu, 0)
	format(buffer, 129, "\y>> \dCT zon: \d[\r%d\d]", PocetZon())
	menu_addtext(menu, buffer, -1)
	menu_additem(menu, "\yEditor", "")
	
	menu_addblank(menu, 0)
	format(buffer, 129, "\y>> \dTlacitek: \d[\r%s\d|\r%d\d|\r%d\d]", g_EditorData[Editor_Ent_Cely] ? "E" : "N", PocetSimonButtonu(), PocetBlacklistButtonu())
	menu_addtext(menu, buffer, -1)
	menu_additem(menu, "\yEditor", "")

	menu_addblank(menu, 0)
	format(buffer, 129, "Camera: \d[\r%s\d]", ViewMod_Nazev[g_EditorData[Camera_Mod]])
	menu_additem(menu, buffer, "")
	
	format(buffer, 129, "NoClip: \d[\r%s\d]", fm_get_user_noclip(id) ? "On" : "Off")
	menu_additem(menu, buffer, "")
	
	menu_additem(menu, "Bot Menu", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER)
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public MartasAdministraceHandler(id, menu, item)
{
	if( item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	switch(item)
	{
		case 0: AdministraceBalonu(id)
		case 1: HonenaAdminMenu(id)
		case 2: DealerAdminMenu(id)	
		case 3: AdministraceZon(id)
		case 4: CTButtonAdminMenu(id)
		case 5:
		{
			g_EditorData[Camera_Mod]++
			if (g_EditorData[Camera_Mod] == 4)
				g_EditorData[Camera_Mod] = 0
			
			MartasAdministrace(id)			
			set_view(id, g_EditorData[Camera_Mod])
		}
		case 6:
		{
			fm_set_user_noclip(id, !fm_get_user_noclip(id))
			MartasAdministrace(id)
		}
		case 7:
		{
			BotMenu(id)
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public BotMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	menu = menu_create("\rBOT Admin", "BotMenuHanler")
	menu_additem(menu, "Pridat T", "")
	menu_additem(menu, "Pridat CT", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "Odebrat T", "")
	menu_additem(menu, "Odebrat CT", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "Ukoncit Kolo", "")
	menu_additem(menu, "Smazat All", "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public BotMenuHanler(id, menu, item)
{
	if (item == MENU_EXIT)
		return
	
	BotMenu(id)
	switch (item)
	{
		case 0: SpawnoutBota(id, CS_TEAM_T)
		case 1: SpawnoutBota(id, CS_TEAM_CT)
		case 2: OdebratBota(id, CS_TEAM_T)
		case 3: OdebratBota(id, CS_TEAM_CT)
		case 4:
		{
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (is_user_bot(i))
					user_kill(i, 1)
			}
		}
		case 5:
		{
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (is_user_bot(i))
					server_cmd("pb remove #%d", get_user_userid(i))
			}
		}
	}
}

public SpawnoutBota(id, CsTeams: team)
{
	server_cmd("pb add %d %d %d %d", random_num(0, 20), 5, team, random_num(1, 4))
}

public OdebratBota(id, CsTeams: team)
{
	new blist[32], bnum
	get_players(blist, bnum, "de", team == CS_TEAM_CT ? "CT" : "TERRORIST")
	for (new i = 0; i < bnum; i++) {
		server_cmd("pb remove #%d", get_user_userid(blist[i]))
		break
	}
}

public CTButtonAdminMenu(id)
{
	new szBuffer[64]
	format(szBuffer, 63, "\rCely^n\y-------------------^n\y>> \dDvere: \r%s", g_ServerInfo[Cely_Entita] ? "Existuje" : "Neexistuje")
	new menu = menu_create(szBuffer, "CTButtonAdminMenu_Handler")

	menu_additem(menu, "Oznacit", "")
	menu_addblank(menu, 0)
	
	format(szBuffer, 63, "\rSimon \w[\d%d\w]^n\y-------------------", PocetSimonButtonu())
	menu_addtext(menu, szBuffer, -1)
	
	menu_additem(menu, "Oznacit", "")
	menu_additem(menu, "Smazat", "")
	menu_additem(menu, "Smazat All", "")
	menu_addblank(menu, 0)
	
	format(szBuffer, 63, "\rBlacklist \w[\d%d\w]^n\y-------------------", PocetBlacklistButtonu())
	menu_addtext(menu, szBuffer, -1)
	
	menu_additem(menu, "Oznacit", "")
	menu_additem(menu, "Smazat", "")
	menu_additem(menu, "Smazat All", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\yUlozit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public CTButtonAdminMenu_Handler(id, menu, item)
{
	menu_destroy(menu)
	switch (item)
	{
		case 0: OznacitDvere(id)
		case 1: OznacitSimonButton(id)
		case 2: SmazatSimonButton(id, false)
		case 3: SmazatSimonButton(id, true)
		case 4: OznacitButtonBlacklist(id)
		case 5: SmazatButtonBlacklist(id, false)
		case 6: SmazatButtonBlacklist(id, true)
		case 7: AktualizovatMapData(id)
	}
	
	if (item != MENU_EXIT)
	{
		CTButtonAdminMenu(id)
	}
}


public PocetSimonButtonu()
{
	return g_EditorData[Editor_Button_Simon] ? ArraySize(g_EditorData[Editor_Button_Simon]) : 0
}

public SmazatSimonButton(id, bool:vsechny)
{
	if (vsechny)
	{
		arrayset(g_ServerInfo[Map_Button_Simon], 0, 10)
		ArrayClear(g_EditorData[Editor_Button_Simon])
		client_print(id, print_center, "Vsechny simon buttony smazany!")
		return
	}
	
	new body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Invalidni entita!")
		return
	}
	
	new szClass[13]
	pev(ent, pev_classname, szClass, 12)
	if (!equal(szClass, "func_button"))
	{
		client_print(id, print_center, "Entita musi byt func_button!")
		return
	}
	
	new szTarget[33], index
	entity_get_string(ent, EV_SZ_target, szTarget, charsmax(szTarget))
	index = ArrayFindString(g_EditorData[Editor_Button_Simon], szTarget)
	if (index != -1)
	{
		client_print(id, print_center, "Button #%s smazan!", szTarget)
		ArrayDeleteItem(g_EditorData[Editor_Button_Simon], index)
		for (new i = 0; i < 10; i++)
		{
			if (g_ServerInfo[Map_Button_Simon][i] == ent)
			{
				g_ServerInfo[Map_Button_Simon][i] = 0
				break
			}
		}
	} else
	{
		client_print(id, print_center, "Tento button neni ulozen!")
	}
}

public OznacitSimonButton(id)
{
	new body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Invalidni entita!")
		return
	}
	
	new szClass[13]
	pev(ent, pev_classname, szClass, 12)
	if (!equal(szClass, "func_button"))
	{
		client_print(id, print_center, "Entita musi byt func_button!")
		return
	}
	
	new szTarget[32]
	entity_get_string(ent, EV_SZ_target, szTarget, charsmax(szTarget))
	if (!g_EditorData[Editor_Button_Simon])
		g_EditorData[Editor_Button_Simon] =  ArrayCreate(33)
	if (ArrayFindString(g_EditorData[Editor_Button_Simon], szTarget) != -1)
	{
		client_print(id, print_center, "Button #%s jiz je prirazen!")
	} else
	{
		ArrayPushString(g_EditorData[Editor_Button_Simon], szTarget)
		client_print(id, print_center, "Button #%s pridan mezi simon buttony!", szTarget)
		for (new i = 0; i < 10; i++)
		{
			if (g_ServerInfo[Map_Button_Simon][i] <= 0)
			{
				g_ServerInfo[Map_Button_Simon][i] = ent
				break
			}
		}
	}
}

public bool: jeSimonButton(entId)
{
	for (new i = 0; i < 10; i++)
	{
		if (g_ServerInfo[Map_Button_Simon][i] == entId)
			return true
	}
	
	return false
}

public PocetBlacklistButtonu()
{
	return g_EditorData[Editor_Button_Blacklist] ? ArraySize(g_EditorData[Editor_Button_Blacklist]) : 0
}

public OznacitButtonBlacklist(id)
{
	new body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Invalidni entita!")
		return
	}
	
	new szClass[32]
	pev(ent, pev_classname, szClass, 31)
	if (!equal(szClass, "func_button"))
	{
		client_print(id, print_center, "Entita musi byt func_button!")
		return
	}
	
	new szTarget[32]
	entity_get_string(ent, EV_SZ_target, szTarget, charsmax(szTarget))
	if (!g_EditorData[Editor_Button_Blacklist])
		g_EditorData[Editor_Button_Blacklist] = ArrayCreate(33)
	if (ArrayFindString(g_EditorData[Editor_Button_Blacklist], szTarget) != -1)
	{
		client_print(id, print_center, "Button #%s jiz je prirazen!")
	} else
	{
		ArrayPushString(g_EditorData[Editor_Button_Blacklist], szTarget)
		client_print(id, print_center, "Button #%s pridan do blacklistu!", szTarget)
		for (new i = 0; i < 10; i++)
		{
			if (g_ServerInfo[Map_Button_Blacklist][i] == 0)
			{
				g_ServerInfo[Map_Button_Blacklist][i] = ent
				break
			}
		}
	}
}

public SmazatButtonBlacklist(id, bool: vsechny)
{
	if (vsechny)
	{
		arrayset(g_ServerInfo[Map_Button_Blacklist], 0, 10)
		ArrayClear(g_EditorData[Editor_Button_Blacklist])
		client_print(id, print_center, "Button blacklist vycisten!")
		return
	}
	
	new body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Invalidni entita!")
		return
	}
	
	new szClass[32]
	pev(ent, pev_classname, szClass, 31)
	if (!equal(szClass, "func_button"))
	{
		client_print(id, print_center, "Entita musi byt func_button!")
		return
	}
	
	new szTarget[33], index
	entity_get_string(ent, EV_SZ_target, szTarget, charsmax(szTarget))
	index = ArrayFindString(g_EditorData[Editor_Button_Blacklist], szTarget)
	if (index != -1)
	{
		client_print(id, print_center, "Button #%s smazan!", szTarget)
		ArrayDeleteItem(g_EditorData[Editor_Button_Blacklist], index)
		for (new i = 0; i < 10; i++)
		{
			if (g_ServerInfo[Map_Button_Blacklist][i] == ent)
			{
				g_ServerInfo[Map_Button_Blacklist][i] = 0
				break
			}
		}
	} else
	{
		client_print(id, print_center, "Tento button neni ulozen!")
	}
}

public bool: jeBlacklistButton(entId)
{
	for (new i = 0; i < 10; i++)
	{
		if (g_ServerInfo[Map_Button_Blacklist][i] == entId)
			return true
	}
	
	return false
}

public OznacitDvere(id)
{
	new body, ent
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Tohle neni validni entita!")
		return
	}
	
	new szClass[13]
	pev(ent, pev_classname, szClass, 12)
	if (!equal(szClass, "func_button"))
	{
		client_print(id, print_center, "Tohle neni func_button!")
		return
	}
	
	new szTarget[33]
	entity_get_string(ent, EV_SZ_target, szTarget, charsmax(szTarget))
	if (equal(szTarget, g_EditorData[Editor_Ent_Cely]))
	{
		client_print(id, print_center, "Cely jsou jiz nastaveny na tohle tlacitko!")
		return
	}
	
	copy(g_EditorData[Editor_Ent_Cely], 32, szTarget)
	g_ServerInfo[Cely_Entita] = ent
	AktualizovatMapData(id)
}

public SmazatStaryBalon(id)
{
	new ent, body
	get_user_aiming(id, ent, body)
	if (!pev_valid(ent))
	{
		client_print(id, print_center, "Tohle neni validni entita!")
		return
	}

	new class[15]
	pev(ent, pev_classname, class, 14)
	if (!equal(class, "func_pushable"))
	{
		client_print(id, print_center, "Tohle neni func_pushable!")
		return
	}
	
	pev(ent, pev_model, g_EditorData[Editor_Ent_Balon], 32)
	engfunc(EngFunc_RemoveEntity, ent)
	AktualizovatMapData(id)
}

public AktualizovatMapData(id)
{
	new szMapa[33], szBalon[66], szCely[33], szOldBalon[33], szListSimon[101], szListBlack[101], szMainBuffer[256]
	get_mapname(szMapa, 32)

	if (ExistujeBalonSpawn())
	{
		format(szBalon, 65, "%.1f %.1f %.1f", g_ServerInfo[Balon_SpawnOrigin][0], g_ServerInfo[Balon_SpawnOrigin][1], g_ServerInfo[Balon_SpawnOrigin][2])
	} else
	{
		format(szBalon, 65, "N")
	}
	
	if (PocetSimonButtonu())
	{
		new szBuffer[33], szData[33]
		for (new i = 0; i < ArraySize(g_EditorData[Editor_Button_Simon]); i++)
		{
			ArrayGetString(g_EditorData[Editor_Button_Simon], i, szData, 32)
			format(szBuffer, 32, "%s%s", i == 0 ? "" : " ", szData)
			add(szListSimon, 100, szBuffer)
		}
	} else
	{
		format(szListSimon, 100, "N")
	}
	
	if (PocetBlacklistButtonu())
	{
		new szBuffer[33], szData[33]
		for (new i = 0; i < ArraySize(g_EditorData[Editor_Button_Blacklist]); i++)
		{
			ArrayGetString(g_EditorData[Editor_Button_Blacklist], i, szData, 32)
			format(szBuffer, 32, "%s%s", i == 0 ? "" : " ", szData)
			add(szListBlack, 100, szBuffer)
		}
	} else
	{
		format(szListBlack, 100, "N")
	}
	
	if (strlen(g_EditorData[Editor_Ent_Cely]))
	{
		copy(szCely, 32, g_EditorData[Editor_Ent_Cely])
	} else
	{
		format(szCely, 32, "N")
	}
	
	if (strlen(g_EditorData[Editor_Ent_Balon]))
	{
		copy(szOldBalon, 32, g_EditorData[Editor_Ent_Balon])
	} else
	{
		format(szOldBalon, 32, "N")
	}
	
	format(szMainBuffer, 255, "%s|%s|%s|%s|%s", szCely, szOldBalon, szBalon, szListSimon, szListBlack)
	fvault_set_data(FILE_STORAGE_MAPDATA, szMapa, szMainBuffer)
	client_print(id, print_center, "Data pro tuhle mapu aktualizovany!")
}

public HonenaAdminMenu(id)
{
	static szTemp[65]
	format(szTemp, 64, "\rHonena Admin^n\y- \dSpawnu: %s%d \y| \r%d", g_aHonenaSpawn ? "\y" : "\r", g_aHonenaSpawn ? ArraySize(g_aHonenaSpawn) : 0, PocetEntit(ENTITA_ITEM_BOX))
	new menu = menu_create(szTemp, "HonenaAdminMenuHandler")
	
	menu_additem(menu, "Vytvorit Spawn", "")
	menu_additem(menu, "Smazat Spawn", "")
	menu_additem(menu, "Smazat Spawny", "")
	menu_additem(menu, "Ulozit", "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
	
	if (g_aHonenaSpawn && ArraySize(g_aHonenaSpawn) > 0)
	{
		new Float:flOrigin[3]
		for (new i = 0; i < ArraySize(g_aHonenaSpawn); i++)
		{
			ArrayGetArray(g_aHonenaSpawn, i, flOrigin)
			VytvoritEntitu(flOrigin, ENTITA_ITEM_BOX)
		}
	}
}

public HonenaAdminMenuHandler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		SmazatEntityTyp(ENTITA_ITEM_BOX)
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	switch(item)
	{
		case 0: 
		{
			static iOrigin[3], Float: flOrigin[3]
			get_user_origin(id, iOrigin, 3)
			IVecFVec(iOrigin, flOrigin)
			VytvoritEntitu(flOrigin, ENTITA_ITEM_BOX)
		}
		case 1: 
		{
			new body, ent
			get_user_aiming(id, ent, body)
			if(pev_valid(ent))
			{
				static className[33]
				pev(ent, pev_classname, className, 32)
				if(equal(className, EntityName[Entity:ENTITA_ITEM_BOX]))
				{
					SmazatEntitu(ent)
				} else
				{
					client_print(id, print_center, "Tohle neni item box entita!")
				}
			} else
			{
				client_print(id, print_center, "Tohle neni validni entita!")
			}
		}
		case 2:
		{
			if(PocetEntit(ENTITA_ITEM_BOX))
			{
				SmazatEntityTyp(ENTITA_ITEM_BOX)
				client_print(id, print_center, "Vsechny honena spawny byly smazany!")
			} else
			{
				client_print(id, print_center, "Na tahle mape nejsou aktualne zadne spawny!")
			}
		}
		case 3:
		{
			if(PocetEntit(ENTITA_ITEM_BOX) < 5)
			{
				client_print(id, print_center, "Musi byt aspon 5 spawnu!")
			} else
			{
				UlozitSpawnyHonena(id)
			}
		}
	}
	
	HonenaAdminMenu(id)
	menu_destroy(menu)
	return PLUGIN_HANDLED
}
#endif
/**********************************************
***************** SQL Sekce *****************
**********************************************/
public Db_KontrolaPripojeni()
{
	g_ServerInfo[SQL_Tuple] = _:SQL_MakeDbTuple(SQL_HOST, SQL_USER, SQL_PASS, SQL_DB)
	new ErrorCode, SQLError[128], Handle:SqlConnection = SQL_Connect(g_ServerInfo[SQL_Tuple], ErrorCode, SQLError, charsmax(SQLError))
	if(SqlConnection == Empty_Handle)
	{
		server_print("[SQL] Nastala chyba pri napojeni na databazi!")
		server_print("[SQL] Error: %s", SQLError)
		set_fail_state("Nastala chyba pri napojeni na databazi!")
	}
	
	SQL_FreeHandle(SqlConnection)
	return PLUGIN_HANDLED
}

public Db_ChybaPripojeni(LogTypy: logTyp, const error[])
{
	g_ServerInfo[SQL_Error] = true
	
	server_print("[Db] -----------------------------------------")
	server_print("[Db] Doslo k chybe pri napojeni na databazi!")
	server_print("[Db] Error: %s", error)
	server_print("[Db] -----------------------------------------")
	
	new logFile[64]
	Db_FormatLogFile(logTyp, logFile, 63)
	
	log_to_file(logFile, "Nepodarilo se napojit na databazi!")
	log_to_file(logFile, "Error: %s", error)
}

public Db_QueryError(LogTypy: logTyp, const info[], const error[])
{
	server_print("[Db] -----------------------------------------")
	server_print("[Db] Doslo k chyba pri provedeni SQL dotazu!")
	server_print("[Db] Data:")
	server_print("[Db] >> %s", info)
	server_print("[Db] >> %s", error)
	server_print("[Db] -----------------------------------------")
	
	static logFile[64]
	Db_FormatLogFile(logTyp, logFile, 63)
	
	log_to_file(logFile, info) 
	log_to_file(logFile, "Error:")
	log_to_file(logFile, error)
}

public Db_FormatLogFile(LogTypy: logTyp, file[], len)
{
	static cas[20]
	get_localinfo("amxx_basedir", file, len)
	
	add(file, len, "/logs/sql")
	if(!dir_exists(file))
		mkdir(file)
		
	add(file, len, LogSlozky[logTyp])
	if(!dir_exists(file))
		mkdir(file)
		
	get_time("%d.%m.%Y.log", cas, 19)
	add(file, len, cas)
}

public Db_FormatovatCas(uTime, output[], delka)
{
	static iRok, iMesic, iDen, iHodina, iMinuta, iSekunda
	UnixToTime(uTime, iRok, iMesic, iDen, iHodina, iMinuta, iSekunda, UT_TIMEZONE_SERVER)
	format(output, delka, "%d.%d - %s%d:%s%d", iDen, iMesic, iHodina > 9 ? "" : "0", iHodina, iMinuta > 9 ? "" : "0", iMinuta)
}
/*******************************************
			      < Ucty >
********************************************/
public NacistUcet(id, bool: vytvoreni)
{
	static dotaz[256], data[2]
	data[0] = id
	data[1] = vytvoreni
	
	format(dotaz, 255, "SELECT id, xp, nick FROM %s WHERE game_id = '%s' LIMIT 1", TABULKA_UCTY, g_MojeInfo[id][AuthID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "NacistUcet_Handler", dotaz, data, 2)
}

public NacistUcet_Handler(FailState, Handle:Query, Error[], Errcode, Data[], DataSize)
{
	static id, vytvoreni
	id = Data[0]
	vytvoreni = Data[1]
	
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_UCTY, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_UCTY, "Nastala chyba pri nacteni uctu!", Error)
			#if defined DEBUG
			server_print("[Debug] Funkce: NacistUcet | Error: TQUERY_QUERY_FAILED | id: %d", id)
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Nacteni uctu probehlo uspesne")
			#endif
			if (SQL_NumResults(Query))
			{
				g_MojeInfo[id][Ucet_ID] = SQL_ReadResult(Query, 0)
				g_MojeInfo[id][Ucet_Konto] = SQL_ReadResult(Query, 1)
				
				#if defined DEBUG
				server_print("[Debug] UcetData | dbId: %d - XP: %d", g_MojeInfo[id][Ucet_ID], g_MojeInfo[id][Ucet_Konto])
				#endif
				
				NacistPrava(id)
				CTBanner_KontrolaHrace(id)
				
				if (vytvoreni)
				{
					VytvoritHracStats(id)
				} else
				{
					NacistHracStats(id)
				}
				
				static dbNick[33], name[33]
				SQL_ReadResult(Query, 2, dbNick, 32)
				get_user_name(id, name, 32)
				
				if (!equal(dbNick, name))
				{
					AktualizovatUcetNick(id, name)
				}
			} else
			{
				VytvoritUcet(id)
			}
		}
	}
	
	SQL_FreeHandle(Query)
}

public VytvoritUcet(id)
{
	static dotaz[256], name[33], data[1]
	data[0] = id
	
	get_user_name(id, name, 32)
	format(dotaz, 255, "INSERT INTO %s (game_id, nick) VALUES ('%s', '%s')", TABULKA_UCTY, g_MojeInfo[id][AuthID], name)
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "VytvoritUcet_Handler", dotaz, data, 1)
}

public VytvoritUcet_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	static id; id = Data[0]
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_UCTY, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_UCTY, "Nastala chyba pri vytvareni uctu!", Error)
			#if defined DEBUG
			server_print("[Debug] Funkce: VytvoritUcet | Error: TQUERY_QUERY_FAILED | id = %d", id)
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		case TQUERY_SUCCESS:
		{
			NacistUcet(id, true)
			#if defined DEBUG
			server_print("[Debug] Vytvoreni uctu uspesne dokonceno!")
			#endif
		}
	}
	
	SQL_FreeHandle(Query)
}

public AktualizovatUcetNick(id, name[])
{
	static dotaz[256], data[1]
	data[0] = id
	
	format(dotaz, 255, "UPDATE %s SET nick = '%s' WHERE id = '%d'", TABULKA_UCTY, name, g_MojeInfo[id][Ucet_ID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "AktualizovatUcetNick_Handler", dotaz, data, 1)
}

public AktualizovatUcetNick_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	#if defined DEBUG
	static id
	id = Data[0]
	#endif
	
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_UCTY, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_UCTY, "Nastala chyba pri aktualizaci nicku!", Error)
			#if defined DEBUG
			server_print("[Debug] Funkce: AktualizovatUcetNick | Error: TQUERY_QUERY_FAILED | id = %d", id)
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		#if defined DEBUG
		case TQUERY_SUCCESS:
		{
			server_print("[Debug] Aktualizace nicku uctu dokoncena")
		}
		#endif
	}
	
	SQL_FreeHandle(Query)
}

public AktualizovatKonto(id)
{
	static dotaz[256], data[1]
	data[0] = id
	
	format(dotaz, 255, "UPDATE %s SET xp = '%d' WHERE id = '%d'", TABULKA_UCTY, g_MojeInfo[id][Ucet_Konto], g_MojeInfo[id][Ucet_ID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "AktualizovatKonto_Handler", dotaz, data, 1)
}

public AktualizovatKonto_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_UCTY, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_UCTY, "Nastala chyba pri aktualizaci konta!", Error)
			
			#if defined DEBUG
			static id
			id = Data[0]
			server_print("[Debug] Funkce: AktualizovatKonto | Error: TQUERY_QUERY_FAILED | id = %d", id)
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		#if defined DEBUG
		case TQUERY_SUCCESS:
		{
			server_print("[Debug] Aktualizace konta uspesne dokoncena")
		}
		#endif
	}
	
	SQL_FreeHandle(Query)
}

public AktualizovatUcetCas(id)
{
	if (g_MojeInfo[id][Ucet_Cas] == 0.0)
		return
		
	static szDotaz[101], Data[1], iCas
	Data[0] = id
	
	iCas = floatround(get_gametime() - g_MojeInfo[id][Ucet_Cas]) / 60
	if (iCas < 1)
		return 
		
	format(szDotaz, 100, "UPDATE %s SET cas = cas + %d WHERE id = %d", TABULKA_UCTY, iCas, g_MojeInfo[id][Ucet_ID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "AktualizovatUcetCas_Handler", szDotaz, Data, 1)
}

public AktualizovatUcetCas_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	SQL_FreeHandle(Query)
	switch (FailState)
	{
		case TQUERY_CONNECT_FAILED: Db_ChybaPripojeni(LOG_UCTY, Error)
		case TQUERY_QUERY_FAILED: Db_QueryError(LOG_UCTY, "Nastala chyba pri aktualizaci odehranych minut!", Error)
		#if defined DEBUG
		case TQUERY_SUCCESS: server_print("[Debug] Aktualizovan pocet odehranych minut")
		#endif
	}
}
/*******************************************
			  < Admin System >
********************************************/
public NacistPrava(id)
{
	static dotaz[256], data[1]
	data[0] = id
	
	format(dotaz, 255, "SELECT admin FROM %s WHERE id = '%d' AND (admin = 1 OR vyprsi > CURRENT_TIMESTAMP()) LIMIT 1;", TABULKA_PRAVA, g_MojeInfo[id][Ucet_ID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "NacistPrava_Handler", dotaz, data, 1)
}
	
public NacistPrava_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED:
		{
			Db_ChybaPripojeni(LOG_UCTY, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_UCTY, "Nastala chyba pri nacteni prav!", Error)
			g_MojeInfo[Data[0]][Ucet_Flags] = _:HRAC_NORMAL
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Dotaz na nacteni adminu uspesny!")
			#endif
			if (!SQL_NumResults(Query))
			{
				g_MojeInfo[Data[0]][Ucet_Flags] = _:HRAC_NORMAL
				return
			}
			
			new typ = SQL_ReadResult(Query, 0)
			#if defined DEBUG
			server_print("[Debug] Nacteny prava! Id: %d | Typ: %s", g_MojeInfo[Data[0]][Ucet_ID], typ == 1 ? "Admin" : "VIP")
			#endif
			
			if (typ == 1)
			{
				g_MojeInfo[Data[0]][Ucet_Flags] = _:HRAC_ADMIN
			} else
			{
				g_MojeInfo[Data[0]][Ucet_Flags] = _:HRAC_VIP
			}
		}
	}
	
	SQL_FreeHandle(Query)
}

public AmxmodxMenu(id)
{
	if (!jeAdmin(id))
		return
		
	static menu
	if (menu)
	{
		 menu_display(id, menu, 0)
		 return
	}
	
	menu = menu_create("\rAdmin menu", "AmxmodxMenu_Handler")
	menu_additem(menu, "Simon", "")
	menu_additem(menu, "Ban CT", "")
	menu_additem(menu, "Kick", "")
	menu_additem(menu, "Banovani", "")
	menu_additem(menu, "Mapa", "")
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public AmxmodxMenu_Handler(id, menu, item)
{
	switch (item)
	{
		case 0: VedouciVeznice(id)
		case 1: CTBanner_MenuHraci(id)
		case 2: KickHraciMenu(id)
		case 3: BanMenuHraci(id)
		case 4: AdminMapMenu(id)
	}
}

public KickHraciMenu(id)
{
	new szBuffer[33], szNum[3]
	new menu = menu_create("\rVyber hrace", "KickHraciMenu_Handler")
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (!get_bit(g_HracPripojen, i) || g_MojeInfo[i][Ucet_Flags] == HRAC_ADMIN)
			continue
			
		num_to_str(i, szNum, 2)
		get_user_name(i, szBuffer, 32)
		menu_additem(menu, szBuffer, szNum)
	}
	
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public KickHraciMenu_Handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	new data[6], szName[64], access, callback
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);
	
	new hracId = str_to_num(data)
	if (!hracPripojen(hracId))
	{
		client_print(id, print_center, "Tento hrac se jiz odpojil!")
		return PLUGIN_HANDLED
	}
	
	g_AdminInfo[id][Target_ID] = hracId
	client_cmd(id, "messagemode DuvodKicku")
	client_print(id, print_center, "Zadej duvod k vyhozeni hrace!")
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public KickDuvod_Handler(id)
{
	new szText[300], delka
	read_args(szText, charsmax(szText))
	remove_quotes(szText)
	delka = strlen(szText)
	
	if (!delka)
	{
		client_print(id, print_center, "Nezadal jsi zadny duvod!")
		KickHraciMenu(id)
		return PLUGIN_HANDLED
	}
	
	new szName[33], szAdmin[33], szInfo[201]
	get_user_name(id, szAdmin, 32)
	get_user_name(g_AdminInfo[id][Target_ID], szName, 32)
	
	format(szInfo, 200, "Byl jsi vyhozen!^nAdmin: %s | Duvod: %s", szAdmin, szText)
	KicknoutHrace(g_AdminInfo[id][Target_ID], szText)
	
	ChatColor(0, "!g%s !ykicknul !team%s!y! [Duvod: !g%s!y]", szAdmin, szName, szText)
	client_print(id, print_center, "%s byl vyhozen!", szName)
	
	new szFile[64], szMapa[33], szCas[20]
	get_mapname(szMapa, 32)
	get_time("%d.%m.%Y", szCas, 19)
	FormatAdminLogFile(LOG_ADMIN_KICK, szFile, 63)
	
	if (file_exists(szFile))
		log_to_file(szFile, "******************************************")
	
	log_to_file(szFile, "Datum: %s - Mapa: %s", szCas, szMapa)
	log_to_file(szFile, "Hrac: %s (Id: %d)(AuthID: %s)", szName, g_MojeInfo[g_AdminInfo[id][Target_ID]][Ucet_ID], g_MojeInfo[g_AdminInfo[id][Target_ID]][AuthID])
	log_to_file(szFile, "Admin: %s (Id: %d)(AuthID: %s)", szAdmin, g_MojeInfo[id][Ucet_ID], g_MojeInfo[id][AuthID])
	log_to_file(szFile, "Duvod: %s", szText)
	
	g_AdminInfo[id][Target_ID] = 0
	return PLUGIN_HANDLED
}

public FormatAdminLogFile(AdminLogTyp: logTyp, file[], len)
{
	new szCas[20]
	get_localinfo("amxx_basedir", file, len)
	
	add(file, len, "/logs/admin")
	if(!dir_exists(file))
		mkdir(file)
	
	add(file, len, "/")
	add(file, len, AdminLogSlozky[logTyp])
	if(!dir_exists(file))
		mkdir(file)
		
	get_time("/%d.%m.%Y.log", szCas, 19)
	add(file, len, szCas)
}

public BanMenuHraci(id)
{
	new szBuffer[33], szNum[3]
	new menu = menu_create("\rVyber hrace", "BanMenuHraci_Handler")
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		#if defined DEBUG
		if (!get_bit(g_HracPripojen, i))
			continue
		#else
		if (!get_bit(g_HracPripojen, i) || g_MojeInfo[i][Ucet_Flags] == HRAC_ADMIN)
			continue
		#endif
		
		num_to_str(i, szNum, 2)
		get_user_name(i, szBuffer, 32)
		menu_additem(menu, szBuffer, szNum)
	}
	
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public BanMenuHraci_Handler(id, menu, item)
{
	if(item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], access, callback
	menu_item_getinfo(menu, item, access, data,charsmax(data), szName,charsmax(szName), callback);
	
	new hracId = str_to_num(data)
	if (!hracPripojen(hracId))
	{
		client_print(id, print_center, "Tento hrac se jiz odpojil!")
	} else
	{
		g_AdminInfo[id][Target_ID] = hracId
		BanDuvodMenu(id)
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public BanDuvodMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	menu = menu_create("\rDuvod banu?", "BanDuvodMenu_Handler")
	for (new i = 0; i < sizeof(Bany_Duvody); i++)
		menu_additem(menu, Bany_Duvody[i], "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public BanDuvodMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
		return
		
	if (item == 0)
	{
		client_cmd(id, "messagemode DuvodBanu")
		client_print(id, print_center, "Zadej duvod k udeleni banu!")
	} else
	{
		copy(g_AdminInfo[id][Target_Duvod], 65, Bany_Duvody[item])
		DelkaBanuMenu(id)
	}
}

public BanDuvod_Handler(id)
{
	new szText[300], delka
	read_args(szText, charsmax(szText))
	remove_quotes(szText)
	delka = strlen(szText)
	
	if (!delka)
	{
		client_print(id, print_center, "Nezadal jsi zadny duvod!")
		BanMenuHraci(id)
		return PLUGIN_HANDLED
	}
	
	if (delka > 65)
	{
		client_print(id, print_center, "Maximalni delka duvodu je 65 znaku!")
		client_cmd(id, "messagemode DuvodBanu")
	} else
	{
		copy(g_AdminInfo[id][Target_Duvod], 65, szText)
		DelkaBanuMenu(id)
	}
	
	return PLUGIN_HANDLED
}

public DelkaBanuMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	new szBuffer[33]
	menu = menu_create("\rDelka banu?", "DelkaBanuMenu_Handler")
	for(new i = 0; i < sizeof(Bany_DelkaNazev); i++)
	{
		format(szBuffer, 32, "%s%s", i ? "\w" : "\r", Bany_DelkaNazev[i])
		menu_additem(menu, szBuffer, "")
	}
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public DelkaBanuMenu_Handler(id, menu, item)
{
	if (item == 0)
	{
		client_cmd(id, "messagemode DelkaBanu")
		g_AdminInfo[id][Target_Delka] = -1
	} else if (item < sizeof(Bany_DelkaNazev)) // Musi to byt, kvuli buttonu  na zavreni menu
	{
		g_AdminInfo[id][Target_Delka] = Bany_Delka[item]
		KontrolaBanuAdmin_Online(id)
	}
	
	return PLUGIN_HANDLED
}

public BanDelka_Handler(id)
{
	new szText[300], delka
	read_args(szText, charsmax(szText))
	remove_quotes(szText)
	delka = strlen(szText)
	
	if (!delka)
	{
		client_print(id, print_center, "Nezadal jsi delku!")
		DelkaBanuMenu(id)
		return PLUGIN_HANDLED
	}
	
	new delkaBanu
	if (containi(szText, "m"))
	{
		replace_all(szText, 299, "m", "")
		delkaBanu = str_to_num(szText)
	} else if (containi(szText, "h"))
	{
		replace_all(szText, 299, "h", "")
		delkaBanu = str_to_num(szText) * 60
	} else if (containi(szText, "d"))
	{
		replace_all(szText, 299, "d", "")
		delkaBanu = (str_to_num(szText) * 60) * 24
	}
	
	if (delka < 1)
	{
		client_print(id, print_center, "Neplatna nebo nulova delka banu!")
		return PLUGIN_HANDLED
	}
	
	g_AdminInfo[id][Target_Delka] = delkaBanu
	KontrolaBanuAdmin_Online(id)
	return PLUGIN_HANDLED
}

public KontrolaBanu(id)
{
	new szDotaz[256], szIP[17], data[1]
	data[0] = id
	get_user_ip(id, szIP, 16)
	
	format(szDotaz, 255, "SELECT ban_id, duvod, delka, UNIX_TIMESTAMP(vyprsi) FROM %s WHERE (delka = 0 OR vyprsi > CURRENT_TIMESTAMP()) AND (ip = '%s' OR game_id = '%s');", TABULKA_BANY, szIP, g_MojeInfo[id][AuthID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "KontrolaBanu_Handler", szDotaz, data, 1)
}

public KontrolaBanu_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED:
		{
			Db_ChybaPripojeni(LOG_BANY, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_BANY, "Nastala chyba pri kontrole existence banu pri pripojeni!", Error)
			KicknoutHrace(Data[0], "Nastala chyba pri nacteni uctu!^nZkus to pozdeji nebo kontaktuj adminy...")
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Kontrola banu pri pripojeni uspesne dokoncena!")
			#endif
			if (SQL_NumResults(Query))
			{
				new szBanInfo[255], banId, szDuvod[60], delka, szDelka[33], szVyprsi[16]
				banId = SQL_ReadResult(Query, 0)
				SQL_ReadResult(Query, 1, szDuvod, 59)
				delka = SQL_ReadResult(Query, 2)
				if (delka > 0)
				{
					FormatBanTime(delka, szDelka, 32) 
					Db_FormatovatCas(SQL_ReadResult(Query, 3), szVyprsi, 15)
				} else
				{
					format(szDelka, 32, "Perm")
					format(szVyprsi, 15, "Nikdy")
				}
				
				format(szBanInfo, 254, "Mas aktivni ban! Id banu: #%d^nDelka: %s | Vyprsi: %s | Duvod: %s", banId, szDelka, szVyprsi, szDuvod)
				server_print(szBanInfo)
				KicknoutHrace(Data[0], szBanInfo)
				#if defined DEBUG
				server_print("[Debug] Nalezen ban! AuthID: %s | BanId: %s", g_MojeInfo[Data[0]][AuthID], banId)
				#endif
			} else
			{
				NacistUcet(Data[0], false)
			}
		}
	}
	
	SQL_FreeHandle(Query)
}

public KontrolaBanuAdmin_Online(id)
{
	new szDotaz[256], szIP[17], data[1]; data[0] = id
	get_user_ip(g_AdminInfo[id][Target_ID], szIP, 16)
	
	format(szDotaz, 255, "SELECT 1 FROM %s WHERE ip = '%s' OR game_id = '%s';", TABULKA_BANY, szIP, g_MojeInfo[g_AdminInfo[id][Target_ID]][AuthID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "KontrolaBanuAdmin_Online_H", szDotaz, data, 1)
}

public KontrolaBanuAdmin_Online_H(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED:
		{
			Db_ChybaPripojeni(LOG_BANY, Error)
			
			client_print(Data[0], print_center, "Nastala chyba pri zabanovani hrace!")
			ChatColor(Data[0], "Nastala chyba pri zabanovani hrace!")
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_BANY, "Nastala chyba pri kontrole existence banu!", Error)
			
			client_print(Data[0], print_center, "Nastala chyba pri zabanovani hrace!")
			ChatColor(Data[0], "Nastala chyba pri zabanovani hrace!")
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Kontrola existence banu probehla uspesna")
			#endif
			if (!SQL_NumResults(Query) || SQL_ReadResult(Query, 0) == 0)
			{
				ZabanovatHrace_Online(Data[0])
			} else
			{
				client_print(Data[0], print_center, "Tento hrac jiz ma aktivni ban!")
				g_AdminInfo[Data[0]][Target_ID] = 0
				g_AdminInfo[Data[0]][Target_Delka] = 0
				arrayset(g_AdminInfo[Data[0]][Target_Duvod], 0, 65)
			}
		}
	}
	
	SQL_FreeHandle(Query)
}

public ZabanovatHrace_Online(id)
{
	new szDotaz[256], szName[33], szIP[17], data[2]
	data[0] = id
	data[1] = get_systime() + (g_AdminInfo[id][Target_Delka] * 60)
	get_user_name(g_AdminInfo[id][Target_ID], szName, 32)
	get_user_ip(g_AdminInfo[id][Target_ID], szIP, 16)
	
	format(szDotaz, 255, "INSERT INTO %s (ip, game_id, nick, admin, duvod, delka, vyprsi) VALUES ('%s', '%s', '%s', '%d', '%s', '%d', FROM_UNIXTIME('%d'));", TABULKA_BANY, szIP, g_MojeInfo[g_AdminInfo[id][Target_ID]][AuthID], szName, g_MojeInfo[id][Ucet_ID], g_AdminInfo[id][Target_Duvod], g_AdminInfo[id][Target_Delka], data[1]) 
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "ZabanovatHrace_Handler", szDotaz, data, 2)
}

public ZabanovatHrace_Online_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED:
		{
			Db_ChybaPripojeni(LOG_BANY, Error)
			
			client_print(Data[0], print_center, "Nastala chyba pri zabanovani hrace!")
			ChatColor(Data[0], "Nastala chyba pri zabanovani hrace!")
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_BANY, "Nastala chyba pri zabanovani hrace!", Error)
			
			client_print(Data[0], print_center, "Nastala chyba pri zabanovani hrace!")
			ChatColor(Data[0], "Nastala chyba pri zabanovani hrace!")
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Ban uspesne pridan do db! (Online)")
			#endif
			
			new szAdmin[33], szVyprsi[50], szDelka[50]
			get_user_name(Data[0], szAdmin, 32)
			Db_FormatovatCas(Data[1], szVyprsi, 49)
			FormatBanTime(g_AdminInfo[Data[0]][Target_Delka], szDelka, 49)
				
			if (hracPripojen(g_AdminInfo[Data[0]][Target_ID]))
			{
				new szKickText[356]
				client_print(g_AdminInfo[Data[0]][Target_ID], print_console, "============================================")
				client_print(g_AdminInfo[Data[0]][Target_ID], print_console, "=       Tento ucet je nyni zabanovan       =")
				client_print(g_AdminInfo[Data[0]][Target_ID], print_console, "============================================")
				client_print(g_AdminInfo[Data[0]][Target_ID], print_console, "^tAdmin: %s^n^tDelka: %s^n^tVyprsi: %s^n^tDuvod: %s", szAdmin, szDelka, szVyprsi, g_AdminInfo[Data[0]][Target_Duvod])
				
				format(szKickText, 355, "Byl jsi zabanovan!^nAdmin: %s | Delka: %s | Vyprsi: %s^nDuvod: %s", szAdmin, szDelka, szVyprsi, g_AdminInfo[Data[0]][Target_Duvod])
				KicknoutHrace(g_AdminInfo[Data[0]][Target_ID], szKickText)
			}
			
			// TODO HUD
			new szFile[64], szMapa[33], szCas[20], szName[33]
			get_mapname(szMapa, 32)
			get_user_name(g_AdminInfo[Data[0]][Target_ID], szName, 32)
			get_time("%d.%m.%Y", szCas, 19)
			FormatAdminLogFile(LOG_ADMIN_BAN, szFile, 63)
			
			if (file_exists(szFile))
				log_to_file(szFile, "******************************************")
			
			log_to_file(szFile, "Cas: %s - Mapa: %s", szCas, szMapa)
			log_to_file(szFile, "Delka: %s - Vyprsi: %s", szDelka, szVyprsi)
			log_to_file(szFile, "Hrac: %s (%d)(%s)", szName, g_MojeInfo[g_AdminInfo[Data[0]][Target_ID]][Ucet_ID], g_MojeInfo[g_AdminInfo[Data[0]][Target_ID]][AuthID])
			log_to_file(szFile, "Admin: %s (%d)(%s)", szAdmin, g_MojeInfo[Data[0]][Ucet_ID], g_MojeInfo[Data[0]][AuthID])
			log_to_file(szFile, "Duvod: %s", g_AdminInfo[Data[0]][Target_Duvod])
			
			g_AdminInfo[Data[0]][Target_ID] = 0
			g_AdminInfo[Data[0]][Target_Delka] = 0
			arrayset(g_AdminInfo[Data[0]][Target_Duvod], 0, 65)
		}
	}
	
	SQL_FreeHandle(Query)
}
/*******************************************
			   < Statistiky >
********************************************/
public cmdStatsTest(id)
{
	client_print(id, print_chat, "%d %d %d %d %d %d", g_MojeInfo[id][Stats_Kill_Simon], g_MojeInfo[id][Stats_Kill_Bachar], g_MojeInfo[id][Stats_Kill_Box], g_MojeInfo[id][Stats_Smrti], g_MojeInfo[id][Stats_Vzpour], g_MojeInfo[id][Stats_VyhranychDuelu])
}

public NacistHracStats(id)
{	
	static dotaz[256], data[1]
	data[0] = id
	
	format(dotaz, 255, "SELECT kill_simon, kill_bachar, kill_box, smrti, vzpour, vyhranych_duelu FROM %s WHERE id = '%d'", TABULKA_VEZEN_STATS, g_MojeInfo[id][Ucet_ID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "NacistHracStats_Handler", dotaz, data, 1)
}

public NacistHracStats_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_STATS, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_STATS, "Nastala chyba pri nacteni statistik!", Error)
			#if defined DEBUG
			server_print("[Debug] Nastala chyba pri nacteni vezenskych statistik!")
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		case TQUERY_SUCCESS:
		{
			#if defined DEBUG
			server_print("[Debug] Nacteni vezenskych statistik uspesne!")
			#endif
			static id; id = Data[0]
			if (SQL_NumResults(Query))
			{
				g_MojeInfo[id][Stats_Existuji] = true
				g_MojeInfo[id][Stats_Kill_Simon] = SQL_ReadResult(Query, 0)
				g_MojeInfo[id][Stats_Kill_Bachar] = SQL_ReadResult(Query, 1)
				g_MojeInfo[id][Stats_Kill_Box] = SQL_ReadResult(Query, 2)
				g_MojeInfo[id][Stats_Smrti] = SQL_ReadResult(Query, 3)
				g_MojeInfo[id][Stats_Vzpour] = SQL_ReadResult(Query, 4)
				g_MojeInfo[id][Stats_VyhranychDuelu] = SQL_ReadResult(Query, 5)
				#if defined DEBUG
				server_print("[Debug] KillSimon: %d, KillBachar: %d, KillBox: %d, Smrti: %d, Vzpour: %d, Duelu: %d", g_MojeInfo[id][Stats_Kill_Simon], g_MojeInfo[id][Stats_Kill_Bachar], g_MojeInfo[id][Stats_Kill_Box], g_MojeInfo[id][Stats_Smrti], g_MojeInfo[id][Stats_Vzpour], g_MojeInfo[id][Stats_VyhranychDuelu])
				#endif
			} else
			{
				g_MojeInfo[id][Stats_Existuji] = false
			}
		}
	}
	
	SQL_FreeHandle(Query)
}

public VytvoritHracStats(id)
{
	static dotaz[256], data[1]
	data[0] = id
	
	format(dotaz, 255, "INSERT INTO %s (id, kill_simon, kill_bachar, kill_box, smrti, vzpour, vyhranych_duelu, respekt) VALUES ('%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d')", TABULKA_VEZEN_STATS, g_MojeInfo[id][Ucet_ID], g_MojeInfo[id][Stats_Kill_Simon], g_MojeInfo[id][Stats_Kill_Bachar], g_MojeInfo[id][Stats_Kill_Box], g_MojeInfo[id][Stats_Smrti], g_MojeInfo[id][Stats_Vzpour], g_MojeInfo[id][Stats_VyhranychDuelu], VypocitatRankPointy(id))
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "VytvoritHracStats_Handler", dotaz, data, 1)
}

public VytvoritHracStats_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_STATS, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_STATS, "Nastala chyba pri vytvoreni statistik pro hrace!", Error)
			
			#if defined DEBUG
			server_print("[Debug] Nastala chyba pri vytvoreni vezenskych statistik!")
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		case TQUERY_SUCCESS:
		{
			static id; id = Data[0]
			g_MojeInfo[id][Stats_Existuji] = true 
			#if defined DEBUG
			server_print("[Debug] Vytvoreni vezenskych statistik dokonceno!")
			#endif
		}
	}
	
	SQL_FreeHandle(Query)
}

public UpdateVezenskeStats(id)
{
	#if defined DEBUG
	if (is_user_bot(id))
		return
	#endif
	
	static dotaz[256]
	format(dotaz, 255, "UPDATE %s SET kill_simon = '%d', kill_bachar = '%d', kill_box = '%d', smrti = '%d', vzpour = '%d', vyhranych_duelu = '%d', respekt = '%d' WHERE id = '%d'", TABULKA_VEZEN_STATS, g_MojeInfo[id][Stats_Kill_Simon], g_MojeInfo[id][Stats_Kill_Bachar], g_MojeInfo[id][Stats_Kill_Box], g_MojeInfo[id][Stats_Smrti], g_MojeInfo[id][Stats_Vzpour], g_MojeInfo[id][Stats_VyhranychDuelu], VypocitatRankPointy(id), g_MojeInfo[id][Ucet_ID])
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "UpdateVezenskeStats_Handler", dotaz)
}

public UpdateVezenskeStats_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	SQL_FreeHandle(Query)
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_STATS, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_STATS, "Nastala chyba pri aktualizaci statistik!", Error)
			
			#if defined DEBUG
			server_print("[Debug] Nastala chyba pri aktualizaci vezenskych statistik!")
			server_print("[Debug] Error: %s", Error)
			#endif
		}
		#if defined DEBUG
		case TQUERY_SUCCESS:
		{
			server_print("[Debug] Aktualizace vezenskych statistik dokoncena!")
		}
		#endif
	}
}

/*
public NacistTOP10()
{
	static dotaz[256]
	format(dotaz, 255, "SELECT kill_simon, kill_bachar, kill_box, smrti, vzpour, vyhranych_duelu FROM %s ORDER BY respekt DESC LIMIT 10", TABULKA_VEZEN_STATS)
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "NacistTOP10_Handler", dotaz)
}

public NacistTOP10_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			MySQL_ChybaPripojeni()
			#if defined DEBUG
			server_print("[SQL] Funkce: NacistTOP10 , Error: TQUERY_CONNECT_FAILED")
			server_print("[SQL] %s", Error)
			#endif
		}
		case TQUERY_QUERY_FAILED:
		{
			#if defined DEBUG
			server_print("[SQL] Funkce: NacistTOP10 , Error: TQUERY_QUERY_FAILED")
			server_print("[SQL] %s", Error)
			#endif
		}
		case TQUERY_SUCCESS:
		{
			
		}
	}
	SQL_FreeHandle(Query)
}
*/
public VypocitatRankPointy(id)
{
	static rankPointy;
	rankPointy = 0;
	
	if (g_MojeInfo[id][Stats_Kill_Simon] > 0)
		rankPointy += g_MojeInfo[id][Stats_Kill_Simon] * 3
		
	if (g_MojeInfo[id][Stats_Kill_Bachar] > 0)
		rankPointy += g_MojeInfo[id][Stats_Kill_Bachar] * 2
	
	if (g_MojeInfo[id][Stats_Kill_Box] > 0)
		rankPointy += g_MojeInfo[id][Stats_Kill_Box]
		
	if (g_MojeInfo[id][Stats_Vzpour] > 0)
		rankPointy += g_MojeInfo[id][Stats_Vzpour]
		
	if (g_MojeInfo[id][Stats_VyhranychDuelu] > 0)
		rankPointy += g_MojeInfo[id][Stats_VyhranychDuelu]
		
	if (g_MojeInfo[id][Stats_Smrti] > 0)
	{
		rankPointy -= g_MojeInfo[id][Stats_Smrti]
		if (rankPointy < 0)
			rankPointy = 0
	}
	
	return rankPointy
}

/*******************************************
				< CT Banner >
********************************************/
public CTBanner_KontrolaHrace(id)
{
	static dotaz[356], data[1]
	data[0] = id
	
	format(dotaz, 355, "SELECT %s.nick, %s.duvod, %s.delka, UNIX_TIMESTAMP(%s.datum), UNIX_TIMESTAMP(%s.vyprsi) FROM %s JOIN %s ON %s.id = %s.admin WHERE %s.id = '%d' AND %s.vyprsi > CURRENT_TIMESTAMP()", TABULKA_UCTY, TABULKA_CTBANY, TABULKA_CTBANY, TABULKA_CTBANY, TABULKA_CTBANY, TABULKA_CTBANY, TABULKA_UCTY, TABULKA_UCTY, TABULKA_CTBANY, TABULKA_CTBANY, g_MojeInfo[id][Ucet_ID], TABULKA_CTBANY)
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "CTBanner_KontrolaHrace_Handler", dotaz, data, 1)
}

public CTBanner_KontrolaHrace_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED: 
		{
			Db_ChybaPripojeni(LOG_CTBANNER, Error)
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_CTBANNER, "Nastala chyba pri kontrole existence CT zakazu!", Error)
		}
		case TQUERY_SUCCESS:
		{
			if(SQL_NumResults(Query))
			{
				set_bit(g_CTBan, Data[0])
				SQL_ReadResult(Query, 0, g_MojeInfo[Data[0]][MCTBan_Admin], 32)
				SQL_ReadResult(Query, 1, g_MojeInfo[Data[0]][MCTBan_Duvod], 32)
				g_MojeInfo[Data[0]][MCTBan_Delka] = SQL_ReadResult(Query, 2)
				g_MojeInfo[Data[0]][MCTBan_Vytvoren] = SQL_ReadResult(Query, 3)
				g_MojeInfo[Data[0]][MCTBan_Vyprsi] = SQL_ReadResult(Query, 4)
				
				#if defined DEBUG
				server_print("[Debug] Nalezen CT ban!")
				server_print("[Debug] Admin: %s", g_MojeInfo[Data[0]][MCTBan_Admin])
				server_print("[Debug] Duvod: %s", g_MojeInfo[Data[0]][MCTBan_Duvod])
				server_print("[Debug] Delka: %d", g_MojeInfo[Data[0]][MCTBan_Delka])
				server_print("[Debug] Vytvoren: %d", g_MojeInfo[Data[0]][MCTBan_Vytvoren])
				server_print("[Debug] Vyprsi: %d", g_MojeInfo[Data[0]][MCTBan_Vyprsi])
				#endif
			}
		}
	}
	
	SQL_FreeHandle(Query)
}

public CTBanner_MenuHraci(id)
{
	static sStr[3], nazev[33]
	new menu = menu_create("\rKoho chces banovat?", "CTBanner_MenuHraci_Handler")
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		#if defined DEBUG
		if(!hracPripojen(i) || get_bit(g_CTBan, i))
			continue
		#else
		if(!hracPripojen(i) || get_bit(g_CTBan, i) || jeAdmin(i))
			continue
		#endif
		
		num_to_str(i, sStr, 2)
		get_user_name(i, nazev, 32)
		menu_additem(menu, nazev, sStr)
	}
	
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_NEXTNAME, "\yDalsi")
	menu_setprop(menu, MPROP_BACKNAME, "\yZpet")
	menu_display(id, menu, 0)
}

public CTBanner_MenuHraci_Handler(id, menu, item)
{
	if( item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}

	new data[6], szName[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName, charsmax(szName), callback);
	
	g_AdminInfo[id][Target_ID] = str_to_num(data)
	if(get_bit(g_HracPripojen, g_AdminInfo[id][Target_ID])) 
	{
		CTBanner_DelkaMenu(id)
	} else
	{
		g_AdminInfo[id][Target_ID] = 0
		client_print(id, print_center, "Tento hrac se odpojil!")
		CTBanner_MenuHraci(id)
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public CTBanner_DelkaMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	new szBuffer[33]
	menu = menu_create("\rDelka zakazu ?", "CTBanner_DelkaMenu_Handler")
	for(new i = 0; i < sizeof(CTBannerDelkaBanuNazev); i++)
	{
		format(szBuffer, 32, "%s%s", i ? "\w" : "\r", CTBannerDelkaBanuNazev[i])
		menu_additem(menu, szBuffer, "")
	}
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public CTBanner_DelkaMenu_Handler(id, menu, item)
{
	if (item >= sizeof(CTBannerDelkaBanuNazev))
		return PLUGIN_HANDLED
	
	g_AdminInfo[id][Target_Delka] = item
	CTBanner_DuvodMenu(id)
	return PLUGIN_HANDLED
}

public CTBanner_DuvodMenu(id)
{
	static menu
	if (menu)
	{
		menu_display(id, menu, 0)
		return
	}
	
	new szBuffer[64]
	menu = menu_create("\rDuvod zakazu?", "CTBanner_DuvodMenu_Handler")
	for(new i = 0; i < sizeof(CTBannerDuvody); i++)
	{
		format(szBuffer, 63, "%s %s", i ? "\w" : "\y", CTBannerDuvody[i])
		menu_additem(menu, szBuffer, "")
	}
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_display(id, menu, 0)
}

public CTBanner_DuvodMenu_Handler(id, menu, item)
{

	if(!item)
	{
		client_print(id, print_center, "Zadej duvod banu!")
		client_cmd(id, "messagemode Duvod")
	} else if (item < sizeof(CTBannerDuvody))
	{
		copy(g_AdminInfo[id][Target_Duvod], 32, CTBannerDuvody[item])
		CTBanner_ZabanovatHrace(id)
	}
	
	return PLUGIN_HANDLED
}

public CTBanner_ZabanovatHrace(id)
{
	new dotaz[512], ip[16], data[3], cas, vyprsi
	
	cas = get_systime()
	get_user_ip(g_AdminInfo[id][Target_ID], ip, 15, 1)
	vyprsi = g_AdminInfo[id][Target_Delka] ?  (cas + CTBannerDelkaBanu[g_AdminInfo[id][Target_Delka]]) : 0
	
	data[0] = id 
	data[1] = cas
	data[2] = vyprsi
	
	format(dotaz, 511, "INSERT INTO %s (id, admin, duvod, delka, vyprsi) VALUES ('%d', '%d', '%s', '%d', FROM_UNIXTIME('%d'))", TABULKA_CTBANY, g_MojeInfo[g_AdminInfo[id][Target_ID]][Ucet_ID], g_MojeInfo[id][Ucet_ID], g_AdminInfo[id][Target_Duvod], g_AdminInfo[id][Target_Delka], vyprsi)
	SQL_ThreadQuery(g_ServerInfo[SQL_Tuple], "CTBanner_ZabanovatHrace_Handler", dotaz, data, 3)
}

public CTBanner_ZabanovatHrace_Handler(FailState,Handle:Query,Error[],Errcode,Data[],DataSize)
{
	new id = Data[0]
	switch(FailState)
	{
		case TQUERY_CONNECT_FAILED:
		{
			Db_ChybaPripojeni(LOG_CTBANNER, Error)
			ChatColor(id, "Nastala chyba pri zapsani banu do databaze! Zkus to znova...")
			client_print(id, print_center, "Nastala chyba pri zapsani banu do databaze!")
		}
		case TQUERY_QUERY_FAILED:
		{
			Db_QueryError(LOG_CTBANNER, "Nastala chyba pri vytvoreni CT banu!", Error)
			ChatColor(id, "Nastala chyba pri zapsani banu do databaze! Zkus to znova...")
			client_print(id, print_center, "Nastala chyba pri zapsani banu do databaze!")
		}
		case TQUERY_SUCCESS:
		{
			if (get_bit(g_HracPripojen, g_AdminInfo[id][Target_ID]))
			{
				set_bit(g_CTBan, g_AdminInfo[id][Target_ID])
				if(g_MojeInfo[g_AdminInfo[id][Target_ID]][Hrac_Team] == CS_TEAM_CT)
					ZmenitTeam(g_AdminInfo[id][Target_ID], CS_TEAM_T)
			}
			
			static szName[33], szIP[18], szAdminName[33], szDatum[33], szVyprsi[33]
			Db_FormatovatCas(Data[1], szDatum, 32)
			Db_FormatovatCas(Data[2], szVyprsi, 32)
			
			get_user_name(id, szAdminName, 32)
			get_user_name(g_AdminInfo[id][Target_ID], szName, 32)
			get_user_ip(g_AdminInfo[id][Target_ID], szIP, 17, 0)
			
			// TODO Hud, print ?
			
			#if defined DEBUG
			server_print("[Debug] CTBan uspesne vytvoren!")
			server_print("[Debug] Nick: %s", szName)
			server_print("[Debug] Datum: %s", szDatum)
			server_print("[Debug] Vyprsi: %s", szVyprsi)
			server_print("[Debug] IP: %s", szIP)
			server_print("[Debug] Admin: %s", szAdminName)
			server_print("[Debug] Duvod: %s", g_AdminInfo[id][Target_Duvod])
			#endif
			
			g_AdminInfo[id][Target_ID] = 0
			g_AdminInfo[id][Target_Delka] = 0
			arrayset(g_AdminInfo[id][Target_Duvod], 0, 65)
		}
	}
	
	SQL_FreeHandle(Query)
}
	
public CTBanner_DuvodSayHandler(id)
{
	static iSay[300], delka
	read_args(iSay, charsmax(iSay))
	remove_quotes(iSay)
	delka = strlen(iSay)
	
	if(!iSay[0])
	{
		client_print(id, print_center, "Nezadal jsi zadne znaky!")
		CTBanner_DuvodMenu(id)
		return PLUGIN_HANDLED
	}
	
	if(delka < 3)
	{
		client_print(id, print_center, "Minimalni delka jsou 3 znaky!")
		CTBanner_DuvodMenu(id)
		return PLUGIN_HANDLED
	}
	
	if(delka > 59)
	{
		client_print(id, print_center, "Maximalni delka je 60 znaku!")
		CTBanner_DuvodMenu(id)
		return PLUGIN_HANDLED
	}
	
	copy(g_AdminInfo[id][Target_Duvod], 32, iSay)
	CTBanner_ZabanovatHrace(id)
	return PLUGIN_HANDLED
}

public CTBanner_MujBanInfo(id)
{
	static szTemp[128], szCas[64]
	format(szTemp, 127, "\rMuj Ban \y#\d%d", g_MojeInfo[id][Ucet_ID])
	new menu = menu_create(szTemp, "MenuDestroy_Handler")
	 
	format(szTemp, 127, "Delka: \r%s^n\y>> \dDelka zakazu k CT!", CTBannerDelkaBanuNazev[g_MojeInfo[id][MCTBan_Delka]])
	menu_additem(menu, szTemp, "1")
	menu_addblank(menu, 0)
	
	Db_FormatovatCas(g_MojeInfo[id][MCTBan_Vytvoren], szCas, 63)
	format(szTemp, 127, "Datum: \r%s^n\y>> \dDatum vytvoreni zakazu!", szCas)
	menu_additem(menu, szTemp, "1")
	menu_addblank(menu, 0)
	
	format(szTemp, 127, "Duvod: \r%s^n\y>> \dDuvod zakazu pristupu k CT!", g_MojeInfo[id][MCTBan_Duvod])
	menu_additem(menu, szTemp, "1")
	menu_addblank(menu, 0)
	
	format(szTemp, 127, "Admin: \r%s^n\y>> \dAdmin ktery udelil zakaz!", g_MojeInfo[id][MCTBan_Admin])
	menu_additem(menu, szTemp, "1")
	menu_addblank(menu, 0)
	
	Db_FormatovatCas(g_MojeInfo[id][MCTBan_Vyprsi], szCas, 63)
	format(szTemp, 127, "Vyprsi: \r%s^n\y>> \dDatum vyprseni zakazu!", szCas)
	menu_additem(menu, szTemp, "1")
	menu_addblank(menu, 3)
	
	menu_additem(menu, "Zavrit", "1")
	menu_setprop(menu, MPROP_NUMBER_COLOR, "\r")
	menu_setprop(menu, MPROP_EXIT, MEXIT_NEVER)
	menu_display(id, menu, 0)
}
/*******************************************
			   < Ostatni >
********************************************/
public SmazatPlayerFunkce(id)
{
	clear_bit(g_Molotov, id)
	clear_bit(g_Padak, id)
	
	if (get_bit(g_AntiLaser, id))
	{
		clear_bit(g_AntiLaser, id)
		ZobrazitIconu(id, "suit_full", 0)
	}
	
	g_MojeInfo[id][MujNuz] = _:NOZE_RUCE
	g_MojeInfo[id][UkrytyNuz] = _:NOZE_RUCE
	g_MojeInfo[id][UkrytyNuzStav] = false
}

public KontrolaPingu(param[])
{
	new id = param[0] 
	new p, l 
	
	get_user_ping( id , p , l ) 
	
	g_MojeInfo[id][Ping_Sum] += p
	++g_MojeInfo[id][Ping_Samples]
	
	if (g_MojeInfo[id][Ping_Samples] > 5 && (g_MojeInfo[id][Ping_Sum] / g_MojeInfo[id][Ping_Samples] > 100))    
	{
		static name[32]
		get_user_name(id, name, 31)
		ChatColor(0, "Hrac !g%s !ybyl vyhozen kvuli vysokemu pingu!", name)
		server_cmd("kick #%d ^"Mas vysoky ping! Zkus to pozdeji...^"", get_user_userid(id))
	}
}

/*******************************************
			  < CS Message >
********************************************/
public MessageReturnHandled(msgid, dest, id)
{
	return PLUGIN_HANDLED
}

public MessageStatusIcon(msgid, dest, id)
{
	static icon[5] 
	get_msg_arg_string(2, icon, charsmax(icon))
	// buyzone
	if((icon[0] == 'b' && icon[2] == 'y' && icon[3] == 'z'))
	{
		set_pdata_int(id, 235, get_pdata_int(id, 235) & ~(1<<0))
		return PLUGIN_HANDLED
	}

	return PLUGIN_CONTINUE
}

public MessageTextMsg(msgid, dest, id)
{
	static buffer[33];
	get_msg_arg_string(2, buffer, 32)
	
	// #Terrorists_Win
	if(buffer[1] == 'T' && buffer[6] == 'r' && buffer[12] == 'W')
	{
		if (g_ServerInfo[Special_Typ] == SPECIAL_SCHOVKA)
		{
			JailDHud(0, 1, BarvyRGB[BARVA_CERVENA], 0, 2.5, 0.0, 0.2, 0.2, ">> Vezni jsou vitezem schovky <<")
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_ZivyHrac, i) && g_MojeInfo[i][Hrac_Team] == CS_TEAM_T)
					PridatBody(i, 10)
			}
		} else if (g_ServerInfo[Special_Typ] == SPECIAL_HONENA)
		{
			JailDHud(0, 1, BarvyRGB[BARVA_CERVENA], 0, 2.5, 0.0, 0.2, 0.2, ">> Vezni jsou vitezem honene! <<")
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_ZivyHrac, i) && g_MojeInfo[i][Hrac_Team] == CS_TEAM_T)
					PridatBody(i, g_MojeInfo[i][Honena_Vezen_Zivotu] ? (10 + (g_MojeInfo[i][Honena_Vezen_Zivotu] * 5)) : 10)
			}
		} else if (g_ServerInfo[Special_Typ] != SPECIAL_FREEDAY)
		{
			JailDHud(0, 1, BarvyRGB[BARVA_CERVENA], 0, 2.5, 0.0, 0.2, 0.2, ">> Vezni obsadili veznici! <<")
		}
		
		return PLUGIN_HANDLED
	}
		
	// #CTs_Win
	if(buffer[1] == 'C' && buffer[2] == 'T' && buffer[5] == 'W')
	{
		if (g_ServerInfo[Special_Typ] == SPECIAL_SCHOVKA)
		{
			JailDHud(0, 1, BarvyRGB[BARVA_MODRA_SVETLA], 0, 2.5, 0.0, 0.2, 0.2, ">> Bachari jsou vitezem schovky! <<")
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_ZivyHrac, i) && g_MojeInfo[i][Hrac_Team] == CS_TEAM_CT)
					PridatBody(i, 10)
			}
		} else if (g_ServerInfo[Special_Typ] == SPECIAL_HONENA)
		{
			JailDHud(0, 1, BarvyRGB[BARVA_MODRA_SVETLA], 0, 2.0, 0.0, 0.5, 0.5, ">> Bachari jsou vitezem honene! <<")
			for (new i = 1; i <= MAX_HRACU; i++)
			{
				if (get_bit(g_ZivyHrac, i) && g_MojeInfo[i][Hrac_Team] == CS_TEAM_T)
					PridatBody(i, g_MojeInfo[i][Special_Killu] ? (10 + (g_MojeInfo[i][Special_Killu] * 5)) : 10)
			}
		} else if (g_ServerInfo[Special_Typ] != SPECIAL_FREEDAY)
		{
			JailDHud(0, 1, BarvyRGB[BARVA_MODRA_SVETLA], 0, 2.5, 0.0, 0.2, 0.2, ">> Bachari uhlidali veznici! <<")
		}
		
		return PLUGIN_HANDLED
	}
	
	// #Only_1_Team_Change
	if(buffer[1] == 'O' && buffer[6] == '1' && buffer[13] == 'C')
	{
		HlavniMenu_Rozcestnik(id)
		return PLUGIN_HANDLED
	}
	
	// #Killed_Teammate
	if(buffer[1] == 'K' && buffer[5] == 'e' && buffer[11] == 'm')
		return PLUGIN_HANDLED
			
	// #Game_teammate_kills
	if(buffer[1] == 'G' && buffer[5] == '_' && buffer[13] == 'e')
		return PLUGIN_HANDLED
	
	// #Weapon_Cannot_Be_Dropped
	if(buffer[1] == 'W' && buffer[8] == 'C' && buffer[15] == 'B')
		return PLUGIN_HANDLED
	
	// #Fire_in_the_hole
	if (buffer[0] == '8' && g_ServerInfo[Duel_Bezi] && g_ServerInfo[Duel_Typ] == DUEL_GRANATY)
		return PLUGIN_HANDLED
	
	return PLUGIN_CONTINUE
}

public MessageMoney(msgid, dest, id)
{
	set_pdata_int(id, OFFSET_MONEY, 0);
	set_msg_arg_int(1, ARG_LONG, 0);
}

public MessageHideWeapon(msgid, dest, id)
{
	set_msg_arg_int(1, ARG_BYTE, get_msg_arg_int(1) | (1<<5));
}
	
public Message_AmmoX(msgid, dest, id)
{
	if(!zivyHrac(id))
		return PLUGIN_CONTINUE
	
	if (g_ServerInfo[Special_Typ] == SPECIAL_ZADNY)
	{
		if (g_MojeInfo[id][Hrac_Team] != CS_TEAM_CT || g_ServerInfo[Duel_idCT] == id)
			return PLUGIN_CONTINUE
	} else
	{
		if (!NekonecnoZasobniku())
			return PLUGIN_CONTINUE
	}
	
	static iAmmoID, iMaxBpAmmo
	iAmmoID = get_msg_arg_int(1)
	if(iAmmoID > 0 && iAmmoID < 15)
	{
		iMaxBpAmmo = WeaponsMaxBpAmmo[iAmmoID]
		if( get_msg_arg_int(2) < iMaxBpAmmo && iAmmoID <= 10)
		{
			set_msg_arg_int(2, ARG_BYTE, iMaxBpAmmo)
			set_pdata_int(id, OFFSET_PLAYERITEM + iAmmoID, iMaxBpAmmo, EXTRAOFFSET)
		}
	}
	
	return PLUGIN_CONTINUE
}

public MessageDeathMsg(msgid, dest, id)
{	
	if(!g_ServerInfo[BlockDeathMsg])
		return PLUGIN_CONTINUE
	
	set_msg_block(g_MsgDeathMsg, BLOCK_ONCE)
	g_ServerInfo[BlockDeathMsg] = false
	return PLUGIN_HANDLED
}

public MessageScoreAttrib(msgid, dest, iReceiver) 
{
	new id = get_msg_arg_int(SCOREATTRIB_ARG_PLAYERID)
	if(jeVIP(id))
		set_msg_arg_int( SCOREATTRIB_ARG_FLAGS, ARG_BYTE, SCOREATTRIB_FLAG_VIP)
}
/*******************************************
			  < Zone Manager >
********************************************/
public ZonaTouch(ent, id)
{
	if (g_MojeInfo[id][Hrac_Team] == CS_TEAM_T)
	{
		g_MojeInfo[id][Dotknuti_Zony] = _:get_gametime()
		if (!task_exists(TASK_ZONA + id))
		{
			new data[1]; data[0] = id
			set_task(0.5, "KontrolaHraceZona", TASK_ZONA + id, data, 1, "b")
			ZobrazitIconu(id, "dmg_bio", 1) 
		}
	}
}

public KontrolaHraceZona(data[1])
{
	static Float: flCas
	flCas = get_gametime()
	if (g_MojeInfo[data[0]][Dotknuti_Zony] + 0.5 < flCas)
	{
		remove_task(TASK_ZONA + data[0])
		ZobrazitIconu(data[0], "dmg_bio", 0)
	}
}

public NacistZony(const config[150], const mapa[33])
{
	new file[200]
	format(file, 199, "%s/configs/jail/zony/%s.mlabs", config, mapa)
	if (!file_exists(file))
		return
	
	new input[1000], line = 0, len
	while( (line = read_file(file , line , input , 127 , len) ) != 0 ) 
	{
		if (!strlen(input)  || (input[0] == ';')) 
			continue;
		
		new data[33], Float:mins[3], Float:maxs[3], Float:pos[3]
		argbreak(input, data, 33, input, 999)
		#if defined DEBUG
			server_print("[Debug] Nacteni zony: %s", data)
			new szNazev[33]
			copy(szNazev, 32, data)
		#endif
		
		argbreak(input, data, 33, input, 999);  pos[0] = str_to_float(data);
		argbreak(input, data, 33, input, 999);	pos[1] = str_to_float(data);
		argbreak(input, data, 33, input, 999);	pos[2] = str_to_float(data);
		
		argbreak(input, data, 33, input, 999);	mins[0] = str_to_float(data);
		argbreak(input, data, 33, input, 999);	mins[1] = str_to_float(data);
		argbreak(input, data, 33, input, 999);	mins[2] = str_to_float(data);
		
		argbreak(input, data, 33, input, 999);	maxs[0] = str_to_float(data);
		argbreak(input, data, 33, input, 999);	maxs[1] = str_to_float(data);
		argbreak(input, data, 33, input, 999);	maxs[2] = str_to_float(data);
		
		#if defined DEBUG
			VytvoritZonuEdit(szNazev, pos, mins, maxs)
		#else
			VytvoritZonu(pos, mins, maxs)
		#endif
	}
}

public UlozitZony(id)
{
	new zonefile[200]
	new mapname[50]

	get_localinfo("amxx_basedir", zonefile, 199)
	format(zonefile, 199, "%s/configs/jail/zony", zonefile)
	if (!dir_exists(zonefile))
		mkdir(zonefile)
	
	get_mapname(mapname, 49)
	format(zonefile, 199, "%s/%s.mlabs", zonefile, mapname)
	delete_file(zonefile)	
	
	new entity = -1, pocetZon, zony[20] 
	while((entity = fm_find_ent_by_class(entity, "zona_ent")))
		zony[pocetZon++] = entity
	
	if (!pocetZon)
	{
		client_print(id, print_center, "Zadne zony nejsou vytvoreny!")
		return;
	}
		
	for(new i = 0; i < pocetZon; i++)
	{
		new szNazev[33], Float:pos[3], Float:mins[3], Float:maxs[3]
		pev(zony[i], pev_message, szNazev, 32)
		pev(zony[i], pev_origin, pos)
		pev(zony[i], pev_mins, mins)
		pev(zony[i], pev_maxs, maxs)
		
		new output[500]
		format(output, 499, szNazev)
		format(output, 499, "%s %.1f %.1f %.1f", output, pos[0], pos[1], pos[2])
		format(output, 499, "%s %.0f %.0f %.0f", output, mins[0], mins[1], mins[2])
		format(output, 499, "%s %.0f %.0f %.0f", output, maxs[0], maxs[1], maxs[2])

		write_file(zonefile, output)
	}
	
	client_print(id, print_center, "Celkem ulozeno %d zon!", pocetZon)
}

public VytvoritZonu(Float:flOrigin[3], Float:flmins[3], Float:flmaxs[3]) {
	new entity = fm_create_entity("info_target")
	set_pev(entity, pev_classname, "zona_ent")
	set_pev(entity, pev_origin, flOrigin)

	set_pev(entity, pev_movetype, MOVETYPE_NONE)
	set_pev(entity, pev_solid, SOLID_TRIGGER)
	
	engfunc(EngFunc_SetSize, entity, flmins, flmaxs)
	set_pev(entity, pev_effects, pev(entity, pev_effects) | EF_NODRAW)
	
	return entity
}

#if defined DEBUG
new g_ZonaEdit_EditorId
new g_ZonaEdit_EntId
new g_ZonaEdit_Osa
new g_ZonaEdit_Units

public VytvoritZonuEdit(const szNazev[33], Float:flOrigin[3], Float:flmins[3], Float:flmaxs[3]) {
	new entity = VytvoritZonu(flOrigin, flmins, flmaxs)
	set_pev(entity, pev_message, szNazev)
	
	return entity
}

public AdministraceZon(id)
{
	new szTemp[65]
	format(szTemp, 64, "\rEditor zon^n\y>> \dExistuje: %d zon", PocetZon())
	new menu = menu_create(szTemp, "AdministraceZon_Handler")
	
	menu_additem(menu, "Vytvorit", "")
	menu_additem(menu, "Upravit", "")
	menu_additem(menu, "Smazat All", "")
	menu_additem(menu, "Ulozit", "")
	
	menu_setprop(menu, MPROP_EXITNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public AdministraceZon_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	switch (item)
	{
		case 0:
		{
			new Float: flOrigin[3]
			new Float:flMins[3] = { -32.0, -32.0, -32.0 }
			new Float:flMaxs[3] = { 32.0, 32.0, 32.0 }
			
			g_ZonaEdit_EditorId = id
			pev(id, pev_origin, flOrigin)
			g_ZonaEdit_EntId = VytvoritZonu(flOrigin, flMins, flMaxs)
			g_ZonaEdit_Units = 1
			
			set_task(0.2, "ZobrazitZonu", TASK_DRAW_SIZE, _, _, "b")
			VytvoreniZonyMenu(id)
		}
		case 1: 
		{
			if (!PocetZon())
			{
				client_print(id, print_center, "Nejsou vytvorene zadne zony!")
			} else
			{
				EditorZonMenu(id)
			}
		}
		case 2:
		{
			SmazatZony(id)
			EditorZonMenu(id)
		}
		case 3: 
		{
			UlozitZony(id)
			EditorZonMenu(id)
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public VytvoreniZonyMenu(id)
{
	new szTemp[64], szNazev[33]
	new menu = menu_create("\rVytvoreni zony", "VytvoreniZonyMenu_Handler")
	
	pev(g_ZonaEdit_EntId, pev_message, szNazev, 32)
	format(szTemp, 63, "Nazev: \d[\r%s\d]", strlen(szNazev) ? szNazev : "Nedefinovan")
	menu_additem(menu, szTemp, "")
	
	menu_addblank(menu, 0)
	format(szTemp, 63, "Osa: \d[\r%s\d]", ZonaOsy[g_ZonaEdit_Osa])
	menu_additem(menu, szTemp, "")
	
	format(szTemp, 63, "Units: \d%d", g_ZonaEdit_Units)
	menu_additem(menu, szTemp, "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\y<<<<<", "")
	menu_additem(menu, "\y>>>>>", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\r<<<<<", "")
	menu_additem(menu, "\r>>>>>", "")
	
	menu_addblank(menu, 0)
	menu_addtext(menu, "-----------------", 0)
	menu_additem(menu, "Smazat", "")
	menu_additem(menu, "Ulozit", "")
	
	menu_addblank(menu, 0)
	menu_additem(menu, "\rZavrit", "")
	
	menu_setprop(menu, MPROP_PERPAGE, 0)
	menu_display(id, menu, 0)
	
}

public VytvoreniZonyMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{	
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	switch (item)
	{
		case 0: client_cmd(id, "messagemode NazevZony")
		case 1:
		{
			g_ZonaEdit_Osa = (g_ZonaEdit_Osa < 2) ? g_ZonaEdit_Osa + 1 : 0
			VytvoreniZonyMenu(id)
		}
		case 2:
		{
			g_ZonaEdit_Units = (g_ZonaEdit_Units < 100) ? g_ZonaEdit_Units * 10 : 1
			VytvoreniZonyMenu(id)
		}
		case 3:
		{
			EntitaSize_Max_Minus(g_ZonaEdit_EntId, g_ZonaEdit_Osa, g_ZonaEdit_Units)
			VytvoreniZonyMenu(id)
		}
		case 4: 
		{
			EntitaSize_Max_Plus(g_ZonaEdit_EntId, g_ZonaEdit_Osa, g_ZonaEdit_Units)
			VytvoreniZonyMenu(id)
		}
		case 5:
		{
			EntitaSize_Min_Minus(g_ZonaEdit_EntId, g_ZonaEdit_Osa, g_ZonaEdit_Units)
			VytvoreniZonyMenu(id)
		}
		case 6: 
		{
			EntitaSize_Min_Plus(g_ZonaEdit_EntId, g_ZonaEdit_Osa, g_ZonaEdit_Units)
			VytvoreniZonyMenu(id)
		}
		case 7:
		{
			remove_task(TASK_DRAW_SIZE)
			engfunc(EngFunc_RemoveEntity, g_ZonaEdit_EntId)
			g_ZonaEdit_EntId = 0
			g_ZonaEdit_EditorId = 0
			AdministraceZon(id)
		}
		case 8:
		{
			UlozitZony(id)
			VytvoreniZonyMenu(id)
		}
		case 9:
		{
			g_ZonaEdit_EditorId = 0
			g_ZonaEdit_EntId = 0
			
			if (task_exists(TASK_DRAW_SIZE))
				remove_task(TASK_DRAW_SIZE)
			
			AdministraceZon(id)
		}
	}
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public EditorZonMenu(id)
{
	new entity = -1, pocet, zony[20], szNazev[33], szNum[6]
	while( (entity = fm_find_ent_by_class(entity, "zona_ent")) )
		zony[pocet++] = entity
	
	new menu = menu_create("\rExistujici zony", "EditorZonMenu_Handler")
	for (new i; i < pocet; i++)
	{
		num_to_str(zony[i], szNum, 5)
		pev(zony[i], pev_message, szNazev, 32)
		menu_additem(menu, szNazev, szNum) 
	}
	
	menu_setprop(menu, MPROP_BACKNAME, "\rZavrit")
	menu_display(id, menu, 0)
}

public EditorZonMenu_Handler(id, menu, item)
{
	if (item == MENU_EXIT)
	{
		AdministraceZon(id)
		menu_destroy(menu)
		return PLUGIN_HANDLED
	}
	
	new data[6], szName[64], access, callback;
	menu_item_getinfo(menu, item, access, data, charsmax(data), szName,charsmax(szName), callback);
	
	g_ZonaEdit_EditorId = id
	g_ZonaEdit_EntId = str_to_num(data)
	g_ZonaEdit_Units = 1
	
	if (!task_exists(TASK_DRAW_SIZE))
		set_task(0.2, "ZobrazitZonu", TASK_DRAW_SIZE, _, _, "b")
		
	VytvoreniZonyMenu(id)
	
	menu_destroy(menu)
	return PLUGIN_HANDLED
}

public SmazatZony(id)
{
	new entity = -1, pocet
	while( (entity = fm_find_ent_by_class(entity, "zona_ent")) )
	{
		engfunc(EngFunc_RemoveEntity, entity)
		pocet++
	}
	
	client_print(id, print_center, "Smazano celkem %d zon!", pocet)
}

public NazevZony_SayHandler(id)
{
	if (!g_ZonaEdit_EntId)
		return PLUGIN_HANDLED
		
	new szSay[300], delka
	read_args(szSay, 299)
	remove_quotes(szSay)
	trim(szSay)
	delka = strlen(szSay)
	
	if (!delka || delka > 32)
	{
		client_print(id, print_center, "Nazev zony musi byt mezi 0 az 32 znaky!")
		return PLUGIN_HANDLED
	}
		
	set_pev(g_ZonaEdit_EntId, pev_message, szSay)
	VytvoreniZonyMenu(id)
	return PLUGIN_HANDLED
}

public PocetZon()
{
	new entity = -1, maxzones = 0
	while( (entity = fm_find_ent_by_class(entity, "zona_ent")) )
	{
		maxzones++
	}
	
	return maxzones
}

public ZobrazitZonu() 
{
	if (!g_ZonaEdit_EntId) 
	{
		remove_task(TASK_DRAW_SIZE)
		return
	}
	
	new Float:flOrigin[3], Float:mins[3], Float:maxs[3]
	pev(g_ZonaEdit_EntId, pev_origin, flOrigin)
	pev(g_ZonaEdit_EntId, pev_mins, mins)
	pev(g_ZonaEdit_EntId, pev_maxs, maxs)

	mins[0] += flOrigin[0]
	mins[1] += flOrigin[1]
	mins[2] += flOrigin[2]
	
	maxs[0] += flOrigin[0]
	maxs[1] += flOrigin[1]
	maxs[2] += flOrigin[2]
	
	DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], maxs[2], mins[0], maxs[1], maxs[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], maxs[2], maxs[0], mins[1], maxs[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], maxs[2], maxs[0], maxs[1], mins[2], {170, 42, 255})

	DrawLine(g_ZonaEdit_EditorId, mins[0], mins[1], mins[2], maxs[0], mins[1], mins[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, mins[0], mins[1], mins[2], mins[0], maxs[1], mins[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, mins[0], mins[1], mins[2], mins[0], mins[1], maxs[2], {170, 42, 255})

	DrawLine(g_ZonaEdit_EditorId, mins[0], maxs[1], maxs[2], mins[0], maxs[1], mins[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, mins[0], maxs[1], mins[2], maxs[0], maxs[1], mins[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], mins[2], maxs[0], mins[1], mins[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, maxs[0], mins[1], mins[2], maxs[0], mins[1], maxs[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, maxs[0], mins[1], maxs[2], mins[0], mins[1], maxs[2], {170, 42, 255})
	DrawLine(g_ZonaEdit_EditorId, mins[0], mins[1], maxs[2], mins[0], maxs[1], maxs[2], {170, 42, 255})

	switch (g_ZonaEdit_Osa)
	{
		case 0:
		{
			DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], maxs[2], maxs[0], mins[1], mins[2], {0, 255, 0})
			DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], mins[2], maxs[0], mins[1], maxs[2], {0, 255, 0})
			
			DrawLine(g_ZonaEdit_EditorId, mins[0], maxs[1], maxs[2], mins[0], mins[1], mins[2], {255, 0, 0})
			DrawLine(g_ZonaEdit_EditorId, mins[0], maxs[1], mins[2], mins[0], mins[1], maxs[2], {255, 0, 0})
		}
		case 1:
		{
			DrawLine(g_ZonaEdit_EditorId, mins[0], mins[1], mins[2], maxs[0], mins[1], maxs[2], {255, 0, 0})
			DrawLine(g_ZonaEdit_EditorId, maxs[0], mins[1], mins[2], mins[0], mins[1], maxs[2], {255, 0, 0})
			
			DrawLine(g_ZonaEdit_EditorId, mins[0], maxs[1], mins[2], maxs[0], maxs[1], maxs[2], {0, 255, 0})
			DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], mins[2], mins[0], maxs[1], maxs[2], {0, 255, 0})
		}
		case 2:
		{
			DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], maxs[2], mins[0], mins[1], maxs[2], {0, 255, 0})
			DrawLine(g_ZonaEdit_EditorId, maxs[0], mins[1], maxs[2], mins[0], maxs[1], maxs[2], {0, 255, 0})
			
			DrawLine(g_ZonaEdit_EditorId, maxs[0], maxs[1], mins[2], mins[0], mins[1], mins[2], {255, 0, 0})
			DrawLine(g_ZonaEdit_EditorId, maxs[0], mins[1], mins[2], mins[0], maxs[1], mins[2], {255, 0, 0})
		}
	}
}

public DrawLine(id, Float:x1, Float:y1, Float:z1, Float:x2, Float:y2, Float:z2, color[3]) 
{
	message_begin(MSG_ONE_UNRELIABLE, SVC_TEMPENTITY, _, id) 
	write_byte(TE_BEAMPOINTS) 
	write_coord_fl(x1) 
	write_coord_fl(y1)
	write_coord_fl(z1)
	write_coord_fl(x2)
	write_coord_fl(y2)
	write_coord_fl(z2)
	write_short(g_sprDot)		// sprite index
	write_byte(1)				// framestart 
	write_byte(1)				// framerate 
	write_byte(4)				// life in 0.1's 
	write_byte(5)				// width
	write_byte(0) 				// noise 
	write_byte(color[0])   		// r, g, b 
	write_byte(color[1])   		// r, g, b 
	write_byte(color[0])	  	// r, g, b 
	write_byte(200)  			// brightness 
	write_byte(0)   			// speed 
	message_end() 
}
#endif
/*******************************************
			  < Fakemeta_Util >
********************************************/
public fm_cs_set_user_bpammo(id, weapon, amount)
{
	static offset
	switch(weapon)
	{
		case CSW_AWP: offset = OFFSET_AWM_AMMO;
		case CSW_SCOUT,CSW_AK47,CSW_G3SG1: offset = OFFSET_SCOUT_AMMO;
		case CSW_M249: offset = OFFSET_PARA_AMMO;
		case CSW_M4A1,CSW_FAMAS,CSW_AUG,CSW_SG550,CSW_GALI,CSW_SG552: offset = OFFSET_FAMAS_AMMO;
		case CSW_M3,CSW_XM1014: offset = OFFSET_M3_AMMO;
		case CSW_USP,CSW_UMP45,CSW_MAC10: offset = OFFSET_USP_AMMO;
		case CSW_FIVESEVEN,CSW_P90: offset = OFFSET_FIVESEVEN_AMMO;
		case CSW_DEAGLE: offset = OFFSET_DEAGLE_AMMO;
		case CSW_P228: offset = OFFSET_P228_AMMO;
		case CSW_GLOCK18,CSW_MP5NAVY,CSW_TMP,CSW_ELITE: offset = OFFSET_GLOCK_AMMO;
		case CSW_FLASHBANG: offset = OFFSET_FLASH_AMMO;
		case CSW_HEGRENADE: offset = OFFSET_HE_AMMO;
		case CSW_SMOKEGRENADE: offset = OFFSET_SMOKE_AMMO;
		case CSW_C4: offset = OFFSET_C4_AMMO;
		default: return;
	}
    
	set_pdata_int(id, offset, amount, EXTRAOFFSET);
} 

public fm_cs_get_user_bpammo(client, weapon)
{
	static offset
	switch(weapon)
	{
		case CSW_AWP: offset = OFFSET_AWM_AMMO;
		case CSW_SCOUT,CSW_AK47,CSW_G3SG1: offset = OFFSET_SCOUT_AMMO;
		case CSW_M249: offset = OFFSET_PARA_AMMO;
		case CSW_M4A1,CSW_FAMAS,CSW_AUG,CSW_SG550,CSW_GALI,CSW_SG552: offset = OFFSET_FAMAS_AMMO;
		case CSW_M3,CSW_XM1014: offset = OFFSET_M3_AMMO;
		case CSW_USP,CSW_UMP45,CSW_MAC10: offset = OFFSET_USP_AMMO;
		case CSW_FIVESEVEN,CSW_P90: offset = OFFSET_FIVESEVEN_AMMO;
		case CSW_DEAGLE: offset = OFFSET_DEAGLE_AMMO;
		case CSW_P228: offset = OFFSET_P228_AMMO;
		case CSW_GLOCK18,CSW_MP5NAVY,CSW_TMP,CSW_ELITE: offset = OFFSET_GLOCK_AMMO;
		case CSW_FLASHBANG: offset = OFFSET_FLASH_AMMO;
		case CSW_HEGRENADE: offset = OFFSET_HE_AMMO;
		case CSW_SMOKEGRENADE: offset = OFFSET_SMOKE_AMMO;
		case CSW_C4: offset = OFFSET_C4_AMMO;
	}
	
	return get_pdata_int(client, offset, EXTRAOFFSET);
}

public fm_cs_get_weapon(id)
{
	return get_pdata_cbase(id, OFFSET_ACTIVEITEM, 5)
}

public CsTeams:fm_cs_get_user_team(client)
{
	return CsTeams:get_pdata_int(client, OFFSET_TEAM, EXTRAOFFSET);
}

public fm_cs_set_user_team(client, {CsTeams,_}:team)
{
	set_pdata_int(client, OFFSET_TEAM, _:team, EXTRAOFFSET);
	dllfunc(DLLFunc_ClientUserInfoChanged, client, engfunc(EngFunc_GetInfoKeyBuffer, client));
	static const team_names[CsTeams][] =
	{
		"UNASSIGNED",
		"TERRORIST",
		"CT",
		"SPECTATOR"
	};
	
	static teamInfo;
	if( teamInfo || (teamInfo = get_user_msgid("TeamInfo")) )
	{
		message_begin(MSG_BROADCAST, teamInfo);
		write_byte(client);
		write_string(team_names[team]);
		message_end();
	}
}

public fm_cs_set_user_deaths(id, smrti)
{
	set_pdata_int(id, OFFSET_CSDEATHS, smrti, EXTRAOFFSET)
	message_begin(MSG_ALL, g_MsgScoreInfo)
	write_byte(id)
	write_short(pev(id, pev_frags))
	write_short(smrti)
	write_short(0)
	write_short(get_user_team(id))
	message_end()
}

public fm_cs_get_weapon_id(entity)
{
	return get_pdata_int(entity, OFFSET_WEAPONID, EXTRAOFFSET_WEAPONS);
}

public fm_cs_get_weapon_ammo(entity)
{
	return get_pdata_int(entity, OFFSET_CLIPAMMO, EXTRAOFFSET_WEAPONS);
}

public fm_cs_set_weapon_ammo(entity, clip)
{
	set_pdata_int(entity, OFFSET_CLIPAMMO, clip, EXTRAOFFSET_WEAPONS);
}

public fm_cs_set_user_model(player)
{
	engfunc(EngFunc_SetClientKeyValue, player, engfunc(EngFunc_GetInfoKeyBuffer, player), "model", g_MojeInfo[player][Model_Name])
}

public fm_cs_get_user_model( player, model[], len)
{
	engfunc(EngFunc_InfoKeyValue, engfunc(EngFunc_GetInfoKeyBuffer, player), "model", model, len)
}

public MenuDestroy_Handler(id, menu, item)
{
	menu_destroy(menu);
}

public HandlerReturn()
{
	return PLUGIN_HANDLED	
}

public VytvoritTrail(id, Barvy: barva)
{
	set_bit(g_Trail, id)
	g_MojeInfo[id][Trail_Barva] = BarvyRGB[barva]
	TrailEffekt(id)
}

public ZrusitTrail(id)
{
	clear_bit(g_Trail, id)
	ZrusitTrailEffekt(id)
}

public JailHud(id, hudPos, RGB[3], Efekt, Float: Delka, Float: EfektDelka, Float: In, Float: Out, Zprava[], any:...)
{
	static vZprava[191]
	vformat(vZprava, 190, Zprava, 10)
	set_hudmessage(RGB[0], RGB[1], RGB[2], g_HudSync[hudPos][_x], g_HudSync[hudPos][_y], Efekt, EfektDelka, Delka, In, Out)
	ShowSyncHudMsg(id, g_HudSync[hudPos][_hudPos], vZprava)
}

public JailDHud(id, Hud, RGB[3], Efekt, Float: Delka, Float: EfektDelka, Float: In, Float: Out, Zprava[], any:...)
{
	static vZprava[191]
	vformat(vZprava, 190, Zprava, 10)
	set_dhudmessage(RGB[0], RGB[1], RGB[2], g_DHudPozice[Hud][x], g_DHudPozice[Hud][y], Efekt, EfektDelka, Delka, In, Out)
	show_dhudmessage(id, vZprava)
}

public RespawnoutHraceTask(data[1])
{
	static id; id = data[0]
	if (!zivyHrac(id) && hracPripojen(id))
		ExecuteHamB(Ham_CS_RoundRespawn, id) 
}

public SmazatMapEntity(const class[])
{
	new ent = -1, properties[32], Float:amt;
	while( (ent = engfunc(EngFunc_FindEntityByString, ent, "classname", class)) )
	{
		pev(ent, pev_renderamt, amt);
		formatex(properties, 31, "^"%i^" ^"%f^" ^"%i^"", pev(ent, pev_rendermode), amt, pev(ent, pev_solid));
		set_pev(ent, pev_message, properties);
		set_pev(ent, pev_rendermode, kRenderTransAlpha);
		set_pev(ent, pev_renderamt, 0.0);
		set_pev(ent, pev_solid, SOLID_NOT);
	}
}

public ObnovitMapEntity(const class[])
{
	new ent = -1, properties[32], rendermode[4], amt[16], solid[4];
	while( (ent = engfunc(EngFunc_FindEntityByString, ent, "classname", class)) )
	{
		pev(ent, pev_message, properties, 31);
		parse(properties, rendermode, 3, amt, 15, solid, 3);
		set_pev(ent, pev_rendermode, str_to_num(rendermode));
		set_pev(ent, pev_renderamt, str_to_float(amt));
		set_pev(ent, pev_solid, str_to_num(solid));
		set_pev(ent, pev_message, "");
	}
}

public PlayerGlow(id, rgb[3]) 
{
	fm_set_user_rendering(id, kRenderFxGlowShell, rgb[0], rgb[1], rgb[2], kRenderNormal, 40)
}

public PlayerGlowAlpha(id, rgb[3], alpha) 
{
	fm_set_user_rendering(id, kRenderFxGlowShell, rgb[0], rgb[1], rgb[2], kRenderNormal, alpha)
}

public MuzeSebratKnife(id)
{
	return g_MojeInfo[id][MujNuz] == Noze:NOZE_RUCE && g_MojeInfo[id][UkrytyNuz] == Noze:NOZE_RUCE
}

public OdebratZbrane(id, nuz)
{
	fm_strip_user_weapons(id)
	if(!nuz)
	{
		fm_give_item(id, "weapon_knife")
		set_pdata_int(id, OFFSET_PRIMWEAPON, 0)
	}
}

public ChatTeamNazev(id)
{
	static Team[20]
	switch(g_MojeInfo[id][Hrac_Team])
	{
		case CS_TEAM_T: format(Team, 19, "Vezni")
		case CS_TEAM_CT: format(Team, 19, "CT")
		case CS_TEAM_SPECTATOR: format(Team, 19, "Divaci")
	}
	
	return Team
}

public FormatovatSekundy(timer)
{
	static szTimer[20]
	format(szTimer, 19, "%d:%s%d", timer / 60, ((timer - ((timer / 60) * 60) < 10)) ? "0" : "", timer - ((timer / 60) * 60))
	return szTimer
}

public Barvy: BarvaOdpoctu(sekund, zelena, zluta, cervena)
{
	if(sekund <= zelena && sekund >= zluta)
		return BARVA_ZELENA
		
	if(sekund < zluta && sekund >= cervena)
		return BARVA_ZLUTA
		
	if(sekund < cervena)
		return BARVA_CERVENA
		
	return BARVA_ZELENA
}

public PocetVolnychDnu()
{
	if (!g_VolnyDen)
		return 0
		
	static i, hracu; hracu = 0
	for(i = 1; i <= MAX_HRACU; i++)
	{
		if(get_bit(g_VolnyDen, i))
			hracu++
	}
	
	return hracu
}

public HracuBezVD()
{
	return g_ZivychHracu[CS_TEAM_T] - PocetVolnychDnu()
}

public bool: JeMoznePridelitVD()
{
	if (g_ZivychHracu[CS_TEAM_T] < 3)
		return false
	
	return g_ZivychHracu[CS_TEAM_T] - PocetVolnychDnu() > 2
}

public bool: NekonecnoZasobniku()
{
	if(g_ServerInfo[Special_Typ] == SPECIAL_PRESTRELKA || g_ServerInfo[Special_Typ] == SPECIAL_ONLYHS || g_ServerInfo[Special_Typ] == SPECIAL_SCHOVKA || g_ServerInfo[Special_Typ] == SPECIAL_ARENA)
		return true;
		
	return false;
}

public PocetVeznuBezAdminu()
{
	new veznu = 0
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!get_bit(g_ZivyHrac, i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_T || jeAdmin(i))
			continue
		
		veznu++
	}
	
	return veznu
}

public PocetMikrofonu()
{
	new pocet
	for (new i = 1; i <= MAX_HRACU; i++)
	{
		if (get_bit(g_Mikrofon, i))
			pocet++
	}
	
	return pocet
}

public PridatBody(id, body)
{
	static Float: flCas
	flCas = get_gametime()
	if(g_MojeInfo[id][BodyStatusCas] + 1.0 > flCas)
	{
		g_MojeInfo[id][BodyStatusPozice]++
		if(g_MojeInfo[id][BodyStatusPozice] == 3)
			g_MojeInfo[id][BodyStatusPozice] = 0
	} else
	{
		g_MojeInfo[id][BodyStatusPozice] = 0
	}
	
	JailHud(id, g_MojeInfo[id][BodyStatusPozice], {0, 255, 0}, 0, 1.5, 0.0, 0.5, 0.5, "[+ %d]", body)
	g_MojeInfo[id][BodyStatusCas] = _:flCas
	
	g_MojeInfo[id][Ucet_Konto] += body
	if (g_MojeInfo[id][Ucet_ID] > 0)
		AktualizovatKonto(id)
}

public OdebratBody(id, body)
{
	JailDHud(id, 3, {255, 0, 0}, 0, 2.0, 0.0, 0.5, 0.5, "[%d$]", body)
	g_MojeInfo[id][Ucet_Konto] -= body
	
	if (g_MojeInfo[id][Ucet_ID] > 0)
		AktualizovatKonto(id)
}
	
public TrailEffekt(id)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_BEAMFOLLOW)
	write_short(id)									// entity
	write_short(g_sprTrail)							// sprite index
	write_byte(60 * 10)									
	write_byte(10)
	write_byte(g_MojeInfo[id][Trail_Barva][0])
	write_byte(g_MojeInfo[id][Trail_Barva][1])
	write_byte(g_MojeInfo[id][Trail_Barva][2])
	write_byte(100)
	message_end()
}

public ZrusitTrailEffekt(id)
{
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_KILLBEAM)
	write_short(id)
	message_end()
}

public SchovkaFadeIn(id)
{
	message_begin(MSG_ONE, g_MsgScreenFade, {0, 0, 0}, id);
	write_short(4096); 									    	// Duration
	write_short(floatround(4096.0 * 1.5, floatround_round));    // HoldTime
	write_short(0x0004);										// Flag
	write_byte(0);												// ColorR
	write_byte(0);												// ColorG
	write_byte(0);												// ColorB
	write_byte(255);											// Alpha
	message_end();
}

public SchovkaFadeOut(id)
{
	message_begin(MSG_ONE, g_MsgScreenFade, {0, 0, 0}, id);
	write_short(0)    											// Duration
	write_short(0)    											// HoldTime
	write_short(0x0002)											// Flag
	write_byte(0)												// ColorR
	write_byte(0)												// ColorG
	write_byte(0)												// ColorB
	write_byte(255)												// Alpha
	message_end()
}

public BlastEfekt(Float: flOrigin[3], Barvy: barva)
{
	new iOrigin[3]
	FVecIVec(flOrigin, iOrigin)
	message_begin(MSG_PVS, SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2])
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2] + 285)
	write_short(g_sprShockwave) 		// sprite
	write_byte(0)						// start frame
	write_byte(0)						// framerate
	write_byte(4)						// ife in 0.1's
	write_byte(60)						// line width in 0.1's
	write_byte(0)						// noise amplitude
	write_byte(BarvyRGB[barva][0])		// Red
	write_byte(BarvyRGB[barva][1])		// Green
	write_byte(BarvyRGB[barva][2])		// Blue
	write_byte(100)						// alpha
	write_byte(0)						// scroll speed
	message_end()
	
	message_begin(MSG_PVS, SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2])
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2] + 385)
	write_short(g_sprShockwave) 		// sprite
	write_byte(0)						// start frame
	write_byte(0)						// framerate
	write_byte(4)						// ife in 0.1's
	write_byte(60)						// line width in 0.1's
	write_byte(0)						// noise amplitude
	write_byte(BarvyRGB[barva][0])		// Red
	write_byte(BarvyRGB[barva][1])		// Green
	write_byte(BarvyRGB[barva][2])		// Blue
	write_byte(100)						// alpha
	write_byte(0)						// scroll speed
	message_end()
	
	message_begin(MSG_PVS, SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2])
	write_coord(iOrigin[0])
	write_coord(iOrigin[1])
	write_coord(iOrigin[2] + 485)
	write_short(g_sprShockwave) 		// sprite
	write_byte(0)						// start frame
	write_byte(0)						// framerate
	write_byte(4)						// ife in 0.1's
	write_byte(60)						// line width in 0.1's
	write_byte(0)						// noise amplitude
	write_byte(BarvyRGB[barva][0])		// Red
	write_byte(BarvyRGB[barva][1])		// Green
	write_byte(BarvyRGB[barva][2])		// Blue
	write_byte(100)						// alpha
	write_byte(0)						// scroll speed
	message_end()
}

public RingEffect(id, rgb[3])
{
	static Float: flOrigin[3]
	pev(id, pev_origin, flOrigin)
	
	message_begin(MSG_BROADCAST, SVC_TEMPENTITY)
	write_byte(TE_BEAMCYLINDER)
	write_coord_fl(flOrigin[0])						//position.x
	write_coord_fl(flOrigin[1])						//position.y
	write_coord_fl(flOrigin[2] - 20.0)				//position.z
	write_coord_fl(flOrigin[0])    					//axis.x
	write_coord_fl(flOrigin[1])    					//axis.y
	write_coord_fl(flOrigin[2] + 200.0)				//axis.z
	write_short(g_sprRing)							//sprite index
	write_byte(0)       							//starting frame
	write_byte(1)       							//frame rate in 0.1's
	write_byte(6)        							//life in 0.1's
	write_byte(2)        							//line width in 0.1's
	write_byte(1)       	 						//noise amplitude in 0.01's
	write_byte(rgb[0])								//color.R
	write_byte(rgb[1])								//color.G
	write_byte(rgb[2])								//color.B
	write_byte(255)									// brightness
	write_byte(0)									// scroll speed in 0.1's
	message_end()
}
	
public CakanecKrve(Float: flOrigin[3], pocet)
{
	static Float: effectData[3]
	for(new i = 0; i < pocet; i++) 
	{
		effectData[0] = random_float(-15.0,15.0)
		effectData[1] = random_float(-15.0,15.0)
		effectData[2] = random_float(-20.0,25.0)

		for(new j = 0; j < 2; j++) 
		{
			message_begin(MSG_BROADCAST,SVC_TEMPENTITY)
			write_byte(TE_BLOODSPRITE)
			write_coord_fl(flOrigin[0] + (effectData[0] * j))
			write_coord_fl(flOrigin[1] + (effectData[1] * j))			
			write_coord_fl(flOrigin[2] + (effectData[2] * j))
			write_short(g_sprBloodSpray)
			write_short(g_sprBloodDrop)
			write_byte(247) 
			write_byte(15)
			message_end()
		}
	}	
}

public Float: VypocitatPrestrelkaHP()
{
	if(g_ZivychHracu[CS_TEAM_CT] >= g_ZivychHracu[CS_TEAM_T])
		return 100.0
		
	static rozdil
	rozdil = g_ZivychHracu[CS_TEAM_T] - g_ZivychHracu[CS_TEAM_CT]
	return float(100 + rozdil * 5)
}

public UchovatKnife(id)
{
	if (g_MojeInfo[id][UkrytyNuzStav])
	{
		g_MojeInfo[id][MujNuz_Inventar] = _:g_MojeInfo[id][UkrytyNuz]
		g_MojeInfo[id][UkrytyNuz] = _:NOZE_RUCE
		g_MojeInfo[id][UkrytyNuzStav] = false
	} else
	{
		if (g_MojeInfo[id][MujNuz] != Noze:NOZE_RUCE)
		{
			g_MojeInfo[id][MujNuz_Inventar] = _:g_MojeInfo[id][MujNuz]
			g_MojeInfo[id][MujNuz] = _:NOZE_RUCE
		}
	}
}

public sqrt(num) 
{ 
	new div = num 
	new result = 1 
	while (div > result) 
	{ 
		div = (div + result) / 2 
		result = num / div 
	} 
	
	return div 
} 

public ZabitBezPripsani(id)
{
	user_silentkill(id)
	fm_cs_set_user_deaths(id, get_user_deaths(id) - 1)
}

public PridatEntituDoArray(ent, Entity:typ)
{
	new entityData[EntityData]
	entityData[Ent_Data_Id] = ent
	entityData[Ent_Data_Typ] = _:typ
	ArrayPushArray(g_aEntityArray, entityData)
}

public SmazatVsechnyEntity()
{
	new pocetEntit
	pocetEntit = ArraySize(g_aEntityArray)
	if(pocetEntit < 1) 
		return

	new entityData[EntityData]
	for(new i = 0; i < pocetEntit; i++)
	{
		ArrayGetArray(g_aEntityArray, i, entityData)
		if(pev_valid(entityData[Ent_Data_Id]))
			engfunc(EngFunc_RemoveEntity, entityData[Ent_Data_Id])
	}
	
	ArrayClear(g_aEntityArray)
}
	
public SmazatEntityTyp(Entity:entTyp)
{
	new pocetEntit = ArraySize(g_aEntityArray)
	if(pocetEntit < 1) 
		return
	
	new entityData[EntityData], smazatIdList[30], index
	for(new i = 0; i < pocetEntit; i++)
	{
		ArrayGetArray(g_aEntityArray, i, entityData)
		if(entityData[Ent_Data_Typ] == entTyp)
		{
			smazatIdList[index++] = i
			if (pev_valid(entityData[Ent_Data_Id]))
				engfunc(EngFunc_RemoveEntity, entityData[Ent_Data_Id])
		}
	}
	
	if (index > 0)
		for (new i = 0; i < index; i++)
			ArrayDeleteItem(g_aEntityArray, smazatIdList[i])
}

public SmazatEntitu(entId)
{
	static pocetEntit
	pocetEntit = ArraySize(g_aEntityArray)
	if (!pocetEntit)
		return
		
	static entityData[EntityData]
	for (new i = 0; i < pocetEntit; i++)
	{
		ArrayGetArray(g_aEntityArray, i, entityData)
		if (entityData[Ent_Data_Id] == entId)
		{
			ArrayDeleteItem(g_aEntityArray, i)
			if (pev_valid(entId))
				engfunc(EngFunc_RemoveEntity, entId)
		
			break
		}
	}
}

public PocetEntit(Entity:entTyp)
{
	new pocet = ArraySize(g_aEntityArray)
	if (pocet == 0)
		return 0
		
	new nalezenych, entityData[EntityData]
	for (new i = 0; i < pocet; i++)
	{
		ArrayGetArray(g_aEntityArray, i, entityData)
		if (entityData[Ent_Data_Typ] == entTyp)
			nalezenych++
	}
	
	return nalezenych
}

public LimitVyprsi()
{
	static Float:flCas
	flCas = get_gametime()
	return floatround((g_ServerInfo[PosledniSpecial] + DELAY_SPECIAL) - flCas)
}

public bool: SpecialPovolen()
{
	if(!g_ServerInfo[PosledniSpecial])
		return true
		
	static Float: flCas
	flCas = get_gametime()
	if((g_ServerInfo[PosledniSpecial] + DELAY_SPECIAL) > flCas)
		return false
	
	return true
}

public WeaponAnim(id, anim)
{
	entity_set_int(id, EV_INT_weaponanim, anim)
	message_begin(MSG_ONE, SVC_WEAPONANIM, {0, 0, 0}, id)
	write_byte(anim)
	write_byte(entity_get_int(id, EV_INT_body))
	message_end()
}

public ScreenFade(id, rgb[3], alpha)
{
	message_begin(MSG_ONE, g_MsgScreenFade, {0,0,0}, id)
	write_short(1<<10)   										// Duration
	write_short(1<<10)   										// Hold time
	write_short(0x0000)  										// Fade type
 	write_byte(rgb[0])   										// Red
	write_byte(rgb[1])   										// Green
	write_byte(rgb[2])   										// Blue
	write_byte(alpha)    										// Alpha
	message_end()
}

public VybratNahodnehoSimona()
{
	static hraci[MAX_HRACU], hracu, novySimon
	for(new i = 1; i <= MAX_HRACU; i++)
	{
		if(!zivyHrac(i) || g_MojeInfo[i][Hrac_Team] != CS_TEAM_CT)
			continue
		
		hraci[hracu] = i
		hracu++
	}
	
	novySimon = random_num(0, hracu - 1)
	cmdSimon(hraci[novySimon])
}

public NemasBody(id, castka)
{	
	static chybi
	chybi = castka - g_MojeInfo[id][Ucet_Konto]
	client_print(id, print_center, "Nemas dostatek bodu na zaplaceni! Chybi ti %d$ !!", chybi)
}

public SetFreezeEnt(ent, bool:freeze)
{
	new iFlags = pev(ent, pev_flags)
	if (freeze)
	{
		if( ~iFlags & FL_FROZEN ) 
        { 
            set_pev(ent, pev_flags, iFlags | FL_FROZEN) 
        } 
	} else
	{
		if( iFlags & FL_FROZEN ) 
        { 
            set_pev(ent, pev_flags, iFlags & ~FL_FROZEN) 
        }
	}
}

public NastavitRychlost(id, Float:flSpeed)
{
	if(flSpeed == 0.0)
	{
		g_MojeInfo[id][Hrac_Rychlost] = _:0.0
		ExecuteHamB(Ham_Item_PreFrame, id)
	} else
	{
		g_MojeInfo[id][Hrac_Rychlost] = _:flSpeed
		set_pev(id, pev_maxspeed, flSpeed)
	}
}

public AktualizovatKnifeModel(id)
{
	if (get_user_weapon(id) == CSW_KNIFE) 
	{
		set_pev(id, pev_viewmodel2, vModelyNozu[g_MojeInfo[id][MujNuz]])
		set_pev(id, pev_weaponmodel2, pModelyNozu[g_MojeInfo[id][MujNuz]])	
		WeaponAnim(id, 3)
	}
}

public bool: PovolenoPripojeniCT()
{
	if(!g_OnlineHracu[CS_TEAM_CT])	
		return true
	
	static tretina
	tretina = g_OnlineHracu[CS_TEAM_T] / 3
	if(tretina < g_OnlineHracu[CS_TEAM_CT])
		return true
		
	return false
}

public Util_PlayAnimation(index, sequence)
{
    set_pev(index, pev_animtime, get_gametime())
    set_pev(index, pev_framerate,  1.0)
    set_pev(index, pev_frame, 0.0)
    set_pev(index, pev_sequence, sequence)
}  

public SetSubModel(ent, Entity:entita)
{
	engfunc(EngFunc_SetModel, ent, ModelyEntit[1])
	set_pev(ent, pev_body, _:entita + 1)
	set_pev(ent, pev_sequence, entita)
	set_pev(ent, pev_frame, 0.0)
	set_pev(ent, pev_framerate, 0.0)
}

public SetModelAndPlayAnim(ent, Entity:entita)
{
	engfunc(EngFunc_SetModel, ent, ModelyEntit[1])
	set_pev(ent, pev_body, _:entita + 1)
	Util_PlayAnimation(ent, _:entita) 
}

public ZobrazitIconu(id, const icon[], typ)
{
	message_begin(MSG_ONE, g_MsgIconStatus, {0,0,0}, id);
	write_byte(typ); 			// status (0=hide, 1=show, 2=flash)
	write_string(icon);			// sprite name
	write_byte(0); 				// red
	write_byte(255); 			// green
	write_byte(0); 				// blue
	message_end();
}

public OdpocetZvuk(id, cas)
{
	if (cas == 0)
		return
		
	if (cas > 0 && cas < 11)
	{
		emit_sound(id, CHAN_VOICE, OdpocetZvuky[cas - 1], 1.0, ATTN_NORM, 0, PITCH_NORM)
		return
	}
	
	if (cas == 20)
	{
		emit_sound(id, CHAN_VOICE, OdpocetZvuky[10], 1.0, ATTN_NORM, 0, PITCH_NORM)
	} else if (cas == 30)
	{
		emit_sound(id, CHAN_VOICE, OdpocetZvuky[11], 1.0, ATTN_NORM, 0, PITCH_NORM)
	}
}

public UkoncitMenu(id)
{
	menu_cancel(id)
	show_menu(id, 0, "^n", 1)
}

public bool: JeSteamHrac(id)
{
	return equal(g_MojeInfo[id][AuthID], "STEAM_", 5) ? true : false
}

public bool: JeGranat(csw)
{
	return csw == CSW_SMOKEGRENADE || csw == CSW_FLASHBANG || csw == CSW_HEGRENADE
}

public bool: ExistujeBalonSpawn()
{
	return g_ServerInfo[Balon_SpawnOrigin][0] != 0.0 || g_ServerInfo[Balon_SpawnOrigin][1] != 0.0 || g_ServerInfo[Balon_SpawnOrigin][2] != 0.0
}

public bool: PadakPovolen(id)
{
	switch (g_ServerInfo[Special_Typ])
	{
		case SPECIAL_ZADNY:
		{
			return get_bit(g_Padak, id) || jeVIP(id)
		}
		case SPECIAL_FREEDAY:
		{
			return jeVIP(id)
		}
		case SPECIAL_SCHOVKA, SPECIAL_PRESTRELKA, SPECIAL_ONLYHS, SPECIAL_ARENA, SPECIAL_ONESHOT:
		{
			if (g_MojeInfo[id][Hrac_Team] == CS_TEAM_CT)
			{
				return true;
			}
		}
	}
	
	return false
}

public PrepnoutNaZakladniMapu(const error[], any:...)
{
	new szError[123]
	vformat(szError, 122, error, 2)
	server_print(szError)
	
	server_cmd("changelevel %s", ZAKLADNI_MAPA)
}

public ExplodeString(szOutput[][], iMax, szInput[], iSize, szDelimiter)
{
    new iIdx = 0, l = strlen(szInput)
    new iLen = (1 + copyc( szOutput[iIdx], iSize, szInput, szDelimiter ))
    while( (iLen < l) && (++iIdx < iMax) )
        iLen += (1 + copyc( szOutput[iIdx], iSize, szInput[iLen], szDelimiter ))
    return iIdx
} 

public KicknoutHrace(id, duvod[])
{
	message_begin(MSG_ONE, SVC_DISCONNECT, _, id)
	write_string(duvod)
	message_end()
}

public FormatBanTime(unitCnt, output[], outputLen)
{
    if (unitCnt > 0)
    {
        new weekCnt = 0, dayCnt = 0, hourCnt = 0, minuteCnt = 0, secondCnt = 0
        secondCnt = unitCnt * 60

        weekCnt = secondCnt / 604800
        secondCnt -= (weekCnt * 604800)

        dayCnt = secondCnt / 86400
        secondCnt -= (dayCnt * 86400)

        hourCnt = secondCnt / 3600
        secondCnt -= (hourCnt * 3600)

        minuteCnt = secondCnt / 60
        secondCnt -= (minuteCnt * 60)

        new maxElementIdx = -1
        new timeElement[5][33]

        if (weekCnt > 0)
            format(timeElement[++maxElementIdx], 32, "%it", weekCnt)
        if (dayCnt > 0)
            format(timeElement[++maxElementIdx], 32, "%id", dayCnt)
        if (hourCnt > 0)
            format(timeElement[++maxElementIdx], 32, "%ih", hourCnt)
        if (minuteCnt > 0)
            format(timeElement[++maxElementIdx], 32, "%im", minuteCnt)
        if (secondCnt > 0)
            format(timeElement[++maxElementIdx], 32, "%is", secondCnt)

        switch(maxElementIdx)
        {
            case 0: format(output, outputLen, "%s", timeElement[0])
            case 1: format(output, outputLen, "%s:%s", timeElement[0], timeElement[1])
            case 2: format(output, outputLen, "%s:%s:%s", timeElement[0], timeElement[1], timeElement[2])
            case 3: format(output, outputLen, "%s:%s:%s:%s", timeElement[0], timeElement[1], timeElement[2], timeElement[3])
            case 4: format(output, outputLen, "%s:%s:%s:%s:%s", timeElement[0], timeElement[1], timeElement[2], timeElement[3], timeElement[4])
        }
    }
}

public ChatColor(const id, const input[], any:...)
{
	new count = 1, players[32]
	static msg[191], msg2[191]
	vformat(msg, 190, input, 3)
	format(msg2, 190, "%s %s", CHATPREFIX, msg)
    
	replace_all(msg2, 190, "!g", "^4") 				// Green Color
	replace_all(msg2, 190, "!y", "^1") 				// Default Color
	replace_all(msg2, 190, "!team", "^3") 			// Team Color
	replace_all(msg2, 190, "!team2", "^0") 			// Team2 Color
   
	if (id) players[0] = id; else get_players(players, count, "ch")
	{
		for (new i = 0; i < count; i++)
		{
			if(get_bit(g_HracPripojen, players[i]))
			{
				message_begin(MSG_ONE_UNRELIABLE, g_MsgSayText, _, players[i])
				write_byte(players[i]);
				write_string(msg2);
				message_end();
			}
		}
	}
}	