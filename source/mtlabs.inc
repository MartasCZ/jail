#if defined _mtlabs_included
  #endinput
#endif
#define _mtlabs_included

#define get_bit(%1,%2) 		( %1 &   1 << ( %2 & 31 ) )
#define set_bit(%1,%2)	 	%1 |=  ( 1 << ( %2 & 31 ) )
#define clear_bit(%1,%2)	%1 &= ~( 1 << ( %2 & 31 ) )

#define CHATPREFIX			"!g[!yCrazyGaming!g]!y"
#define WEB_ADRESA			"CrazyGaming.eu"

#include <amxmodx>

stock GenerovatHash(output[], delka)
{
	static i
	for(i = 0; i < delka; i++) 
	{ 
		switch( i % 3 ) 
		{ 
			case 0: output[i] = random_num( '0', '9' ); 
			case 1: output[i] = random_num( 'a', 'z' ); 
			case 2: output[i] = random_num( 'A', 'Z' ); 
		} 
	} 
}

stock FormatovatCastku( iNum , szOutput[] , iLen )
{
    new szTmp[ 15 ] , iOutputPos , iNumPos , iNumLen;
    
    if ( iNum < 0 )
    {
        szOutput[ iOutputPos++ ] = '-';
        iNum = abs( iNum );
    }
    
    iNumLen = num_to_str( iNum , szTmp , charsmax( szTmp ) );

    if ( iNumLen <= 3 )
    {
        iOutputPos += copy( szOutput[ iOutputPos ] , iLen , szTmp );
    }
    else
    {
        while ( ( iNumPos < iNumLen ) && ( iOutputPos < iLen ) ) 
        {
            szOutput[ iOutputPos++ ] = szTmp[ iNumPos++ ];
        
            if( ( iNumLen - iNumPos ) && !( ( iNumLen - iNumPos ) % 3 ) ) 
                szOutput[ iOutputPos++ ] = ',';
        }
        
        szOutput[ iOutputPos ] = EOS;
    }
    
    return iOutputPos;
}  

stock Float: EnumOrigin(Float: inOrigin[])
{
	static Float: fTmpOrigin[3]
	fTmpOrigin[0] = inOrigin[0]
	fTmpOrigin[1] = inOrigin[1]
	fTmpOrigin[2] = inOrigin[2]
	return fTmpOrigin
}

stock bool: jeBlizko(idA, idB, Float: delka)
{
	static Float: flOriginA[3], Float: flOriginB[3]
	pev(idA, pev_origin, flOriginA)
	pev(idB, pev_origin, flOriginB)
	
	if (floatabs(flOriginA[0] - flOriginB[0]) <= delka)
		return true
		
	if (floatabs(flOriginA[1] - flOriginB[1]) <= delka)
		return true
	
	if (floatabs(flOriginA[2] - flOriginB[2]) <= delka)
		return true
	
	return false
}

stock Procent(pocet, max) 
{
	return (pocet * 100 / max)
}

stock ProcentniHodnota(procent, hodnota)
{
	return (procent / 100) * hodnota
}

stock ham_strip_weapon(id,weapon[])
{
	if(!equal(weapon,"weapon_",7)) 
		return 0;

	new wId = get_weaponid(weapon);
	if(!wId) 
		return 0;

	new wEnt;
	while((wEnt = engfunc(EngFunc_FindEntityByString,wEnt,"classname",weapon)) && pev(wEnt,pev_owner) != id) {}
	if(!wEnt)
		return 0;

	if(get_user_weapon(id) == wId) 
		ExecuteHamB(Ham_Weapon_RetireWeapon,wEnt);

	if(!ExecuteHamB(Ham_RemovePlayerItem,id,wEnt))
		return 0;
	
	ExecuteHamB(Ham_Item_Kill,wEnt);
	set_pev(id,pev_weapons,pev(id,pev_weapons) & ~(1<<wId))
	
	return 1;
}
