-- --------------------------------------------------------
-- Hostitel:                     127.0.0.1
-- Verze serveru:                5.5.39 - MySQL Community Server (GPL)
-- OS serveru:                   Win32
-- HeidiSQL Verze:               9.3.0.5119
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Exportování struktury pro tabulka jail.jail_bany
CREATE TABLE IF NOT EXISTS `jail_bany` (
  `ban_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Unikatni Id banu',
  `ip` char(16) NOT NULL COMMENT 'IP Banu',
  `game_id` char(30) NOT NULL COMMENT 'Herni id',
  `nick` char(33) NOT NULL COMMENT 'Nick hrace',
  `admin` mediumint(8) unsigned NOT NULL COMMENT 'Id admina',
  `duvod` varchar(60) NOT NULL COMMENT 'Duvod banu',
  `delka` smallint(5) unsigned NOT NULL COMMENT 'Delka banu v minutach',
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Datum vytvoreni banu',
  `vyprsi` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Datum vyprseni banu',
  PRIMARY KEY (`game_id`),
  KEY `ban_id` (`ban_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Tabulka pro ulozeni banu';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka jail.jail_ct_bany
CREATE TABLE IF NOT EXISTS `jail_ct_bany` (
  `id` mediumint(8) unsigned NOT NULL,
  `admin` mediumint(8) unsigned NOT NULL,
  `duvod` varchar(60) NOT NULL,
  `delka` tinyint(1) unsigned NOT NULL,
  `datum` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vyprsi` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabulka pro banovani pristupu k CT';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka jail.jail_prava
CREATE TABLE IF NOT EXISTS `jail_prava` (
  `id` mediumint(8) unsigned NOT NULL,
  `admin` tinyint(1) NOT NULL COMMENT '0: VIP | 1: Admin',
  `vytvoren` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `vyprsi` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_jail_admin_jail_ucty` FOREIGN KEY (`id`) REFERENCES `jail_ucty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci COMMENT='Tabulka pro ulouzeni hracu s pravy. (Admin / VIP)';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka jail.jail_stats_vezen
CREATE TABLE IF NOT EXISTS `jail_stats_vezen` (
  `id` mediumint(8) unsigned NOT NULL,
  `kill_simon` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `kill_bachar` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `kill_box` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `smrti` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `vzpour` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `vyhranych_duelu` smallint(5) unsigned NOT NULL DEFAULT '0',
  `respekt` mediumint(8) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  CONSTRAINT `FK_jail_stats_vezen_jail_ucty` FOREIGN KEY (`id`) REFERENCES `jail_ucty` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabulka pro ulozeni hernich statistik.';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka jail.jail_ucty
CREATE TABLE IF NOT EXISTS `jail_ucty` (
  `game_id` char(36) NOT NULL,
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `xp` int(10) unsigned NOT NULL DEFAULT '0',
  `nick` char(33) NOT NULL DEFAULT '0',
  `cas` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`game_id`),
  KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Tabulka pro ulozeni jail uctu';

-- Export dat nebyl vybrán.
-- Exportování struktury pro tabulka jail.jail_vip_kody
CREATE TABLE IF NOT EXISTS `jail_vip_kody` (
  `klic` char(5) NOT NULL,
  `delka` mediumint(8) unsigned NOT NULL,
  `vytvoren` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `aktivovan` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `amx_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`klic`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Tabulka pro ulozeni aktivacnich VIP kodu.';

-- Export dat nebyl vybrán.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
